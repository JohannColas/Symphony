#ifndef ICONS_H
#define ICONS_H

#include <QObject>
#include <QMap>
#include <QIcon>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Icons
		: public QObject
{
	Q_OBJECT
private:
	QString _iconPack = "Dark Theme";
	QMap<QString, QIcon> _icons;
	QIcon _symphony;
	QIcon _welcome;
	QIcon _projectsExplorer;
	QIcon _project;
	QIcon _folder;
	QIcon _rawData;
	QIcon _data;
	QIcon _graph;
	QIcon _picture;
	QIcon _settings;
	QIcon _add;
	QIcon _new;
	QIcon _change;
	QIcon _open;
	QIcon _save;
	QIcon _delete;
	// Text Icons
	QIcon _alignLeft;
	QIcon _alignCenter;
	QIcon _alignRight;
	QIcon _alignJustify;
	QIcon _textItalic;
	QIcon _textBold;
	QIcon _textUnder;
	QIcon _textSuper;
	QIcon _textSub;

public:
	QIcon getIcon( const QString& name )
	{
		return QIcon( "./icons/" + _iconPack + "/" + name +".svg" );
	}
	QIcon get( const QString& name )
	{
		return QIcon( "./icons/" + _iconPack + "/" + name + ".svg" );
	}
	void update() {
		_icons.insert( "Symphony",
					   QIcon( "./icons/" + _iconPack + "/Symphony.svg" ) );
		_symphony = QIcon( "./icons/" + _iconPack + "/Symphony.svg" );
		_welcome = QIcon( "./icons/" + _iconPack + "/welcome.svg" );
		_projectsExplorer = QIcon( "./icons/" + _iconPack + "/projectsExplorer.svg" );
		_project = QIcon( "./icons/" + _iconPack + "/project.svg" );
		_folder = QIcon( "./icons/" + _iconPack + "/folder.svg" );
		_rawData = QIcon( "./icons/" + _iconPack + "/rawData.svg" );
		_data = QIcon( "./icons/" + _iconPack + "/data.svg" );
		_graph = QIcon( "./icons/" + _iconPack + "/graph.svg" );
		_picture = QIcon( "./icons/" + _iconPack + "/picture.svg" );
		_settings = QIcon( "./icons/" + _iconPack + "/settings.svg" );
		_add = QIcon( "./icons/" + _iconPack + "/add.svg" );
		_new = QIcon( "./icons/" + _iconPack + "/new.svg" );
		_change = QIcon( "./icons/" + _iconPack + "/change.svg" );
		_open = QIcon( "./icons/" + _iconPack + "/open.svg" );
		_save = QIcon( "./icons/" + _iconPack + "/save.svg" );
		_delete = QIcon( "./icons/" + _iconPack + "/delete.svg" );
		// Text Icons
		_alignLeft = QIcon( "./icons/" + _iconPack + "/text_align_left.svg" );
		_alignCenter = QIcon( "./icons/" + _iconPack + "/text_align_center.svg" );
		_alignRight = QIcon( "./icons/" + _iconPack + "/text_align_right.svg" );
		_alignJustify = QIcon( "./icons/" + _iconPack + "/text_align_justify.svg" );
		_textItalic = QIcon( "./icons/" + _iconPack + "/text_italic.svg" );
		_textBold = QIcon( "./icons/" + _iconPack + "/text_bold.svg" );
		_textUnder = QIcon( "./icons/" + _iconPack + "/text_under.svg" );
		_textSuper = QIcon( "./icons/" + _iconPack + "/text_superscript.svg" );
		_textSub = QIcon( "./icons/" + _iconPack + "/text_subscript.svg" );
	}
	QString getCurrentIconPack() const {
		return _iconPack;
	}
	void setCurrentIconPack( const QString& value )	{
		_iconPack = value;
		update();
		emit changed();
	}
	QIcon Symphony() const {
		return _symphony;
	}
	QIcon welcome() const {
		return _welcome;
	}
	QIcon projectsExplorer() const {
		return _projectsExplorer;
	}
	QIcon project() const {
		return _project;
	}
	QIcon folder() const {
		return _folder;
	}
	QIcon rawData() const {
		return _rawData;
	}
	QIcon data() const {
		return _data;
	}
	QIcon graph() const {
		return _graph;
	}
	QIcon picture() const {
		return _picture;
	}
	QIcon settings() const {
		return _settings;
	}
	QIcon add() const {
		return _add;
	}
	QIcon newi() const {
		return _new;
	}
	QIcon change() const {
		return _change;
	}
	QIcon open() const {
		return _open;
	}
	QIcon save() const {
		return _save;
	}
	QIcon deletei() const {
		return _delete;
	}
	// Text Icons
	QIcon alignLeft() const {
		return _alignLeft;
	}
	QIcon alignCenter() const {
		return _alignCenter;
	}
	QIcon alignRight() const {
		return _alignRight;
	}
	QIcon alignJustify() const {
		return _alignJustify;
	}
	QIcon textItalic() const {
		return _textItalic;
	}
	QIcon textBold() const {
		return _textBold;
	}
	QIcon textUnder() const {
		return _textUnder;
	}
	QIcon textSuper() const {
		return _textSuper;
	}
	QIcon textSub() const {
		return _textSub;
	}

signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ICONS_H
