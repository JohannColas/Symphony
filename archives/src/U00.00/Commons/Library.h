#ifndef LIBRARY_H
#define LIBRARY_H

#include <QObject>
#include <QSettings>
#include "../Commons/TrackList.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Library
		: public TrackList
{
	Q_OBJECT
private:
	QString _name = "";
	QString _path = "";
	bool _hide = false;

public:
	Library( const QString& name )
		: _name( name ), TrackList( name )
	{
//		update();
	}
	bool hide() const
	{
		return _hide;
	}

public slots:
	void update() override
	{
		QElapsedTimer timer;
		timer.start();
		QSettings _setsFile( "./libraries/" + name() + ".slb", QSettings::IniFormat );
		setPath( _setsFile.value("path").toString() );
		setHide( _setsFile.value("hide").toInt() );

		QDirIterator it( path(),
		{"*.mp3", "*.ogg", ".wav", ".flac", ".m4a", ".wma"},
						 QDir::Files,
						 QDirIterator::Subdirectories );
		while ( it.hasNext() )
		{
			QString path = it.next();
			addTrack( path );
//			QFile f( it.next() );
//			qDebug() << path;//f.fileName();
		}
		qDebug() << "Time to get ALL Metadata :" << timer.elapsed() << "milliseconds";
		qDebug() << "---------------";
	}
	void setHide( bool hide )
	{
		_hide = hide;
	}

signals:

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARY_H
