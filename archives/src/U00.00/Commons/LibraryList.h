#ifndef LIBRARYLIST_H
#define LIBRARYLIST_H

#include <QObject>
#include <QSettings>
#include "../Commons/Library.h"
#include <QTextStream>
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LibraryList
		: public QObject
{
	Q_OBJECT
private:
	QList<Library*> _list;

public:
	LibraryList()
	{
		update();
	}
	QList<Library*> list()
	{
		return _list;
	}
	int nbTracks() const
	{
		return _list.count();
	}
	bool isIndexValid( int index ) const
	{
		return ( index > -1 && index < nbTracks() );
	}

public slots:
	void update()
	{
		QFile file( "./libraries/#libraries.slbs" );
		if ( file.open(QIODevice::ReadOnly) )
		{
			QTextStream in(&file);
			while ( !in.atEnd() )
			{
				QString name = in.readLine();
				addLibrary( name );
			}
		}
		else
			qDebug() << "error Reading file";
	}
	void addLibrary( const QString& name )
	{
		_list.append( new Library(name) );
	}
	void moveLibrary( int from, int to )
	{
		_list.move( from, to );
	}
	void removeLibrary( int index )
	{
		_list.removeAt( index );
	}
	void clear()
	{
		_list.clear();
		emit cleared();
	}
	void setCurrentIndex( int index )
	{
		if ( isIndexValid( index ) )
		{
//			_currentTracklistIndex = index;
			emit currentLibraryChanged( _list.at(index) );
		}
	}

signals:
	void cleared();
	void currentLibraryChanged( TrackList* library );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARYLIST_H
