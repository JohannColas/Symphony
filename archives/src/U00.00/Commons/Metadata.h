#ifndef METADATA_H
#define METADATA_H

#include <QObject>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QString>
#include <QImage>
#include <QTime>
#include <QEventLoop>
#include <QFile>
#include <QUrl>
#include "../Commons/Track.h"

// Taglib
#include <mpegfile.h>
#include <id3v2tag.h>
#include <commentsframe.h>
using namespace TagLib;
#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Metadata
		: public QObject
{
	Q_OBJECT
private:
	QString _path = "Unknown";
	QString _title = "Unknown";
	QString _artist = "Unknown";
	QString _album = "Unknown";
	QDate _date = QDate();
	QString _genre = "Unknown";
	QImage _cover;
	QTime _duration = QTime( 0, 0 );
	int _year = -1;
	int _number = -1;
	QString _albumArtists = "";

public:
	Metadata( Track* track = 0 )
	{
		if ( track )
		{
			_path = track->path();
			update();
		}
	}
	QString path() const
	{ return _path; }
	QString title() const
	{ return _title; }
	QString artist() const
	{ return _artist; }
	QString album() const
	{ return _album; }
	QDate date() const
	{ return _date; }
	QString genre() const
	{ return _genre; }
	QImage cover() const
	{ return _cover; }
	QTime duration() const
	{ return _duration; }
	int year() const
	{ return _year; }
	int number() const
	{ return _number; }
	QString albumArtists() const
	{ return _albumArtists; }

public slots:
	void setTitle( QString title )
	{ _title = title; }
	void setArtist( QString artist )
	{ _artist = artist; }
	void setAlbum( QString album )
	{ _album = album; }
	void setAlbumArtists( QString albumartists )
	{ _albumArtists = albumartists; }
	void setDate( QDate date )
	{ _date = date; }
	void setGenre( QString genre )
	{ _genre = genre; }
	void setCover( QImage cover )
	{ _cover = cover; }
	void setDuration( QTime durat )
	{ _duration = durat; }
	void setYear( int year )
	{ _year = year; }
	void setNumber( int number )
	{ _number = number; }
	void update()
	{
		if ( _path != "" )
		{

			MPEG::File f("/home/johanncolas/Musique/47Ter - Côte Ouest.mp3");

			ID3v2::Tag *id3v2tag = f.ID3v2Tag();

			if(id3v2tag) {

			  qDebug() << "ID3v2."
				   << id3v2tag->header()->majorVersion()
				   << "."
				   << id3v2tag->header()->revisionNumber()
				   << ", "
				   << id3v2tag->header()->tagSize()
				   << " bytes in tag";

			  ID3v2::FrameList::ConstIterator it = id3v2tag->frameList().begin();
			  for(; it != id3v2tag->frameList().end(); it++) {
				cout << (*it)->frameID();

				if(ID3v2::CommentsFrame *comment = dynamic_cast<ID3v2::CommentsFrame *>(*it))
				  if(!comment->description().isEmpty())
					qDebug() << " [" << comment->description().toCString(true) << "]" << endl;
//				string str = (*it)->toString().to8Bit();
//				string str2 = (*it)->toString().to8Bit(true);
				qDebug() << " - \""
						 << (*it)->toString().toCString()
						 << "\""
						 << " - \""
						 << (*it)->toString().toCString(true)
						 << "\"";
			  }
			}
			else
			  qDebug() << "file does not have a valid id3v2 tag" << endl;




//			QMediaPlayer *player = new QMediaPlayer();
//			player->setMedia( QUrl::fromLocalFile(_path) );

////			emit player->metaDataChanged();
//			QEventLoop loop;
//			connect(player, SIGNAL( metaDataChanged() ), &loop, SLOT( quit() ) );
//			loop.exec();

//			// Get Title
//			QString tmp = player->metaData("Title").toString();
//			if ( tmp != "" )
//				setTitle( tmp );
//			// Get Artist
//			tmp = player->metaData("ContributingArtist").toString();
//			if ( tmp != "" )
//				setArtist( tmp );
//			// Get Album
//			tmp = player->metaData("AlbumTitle").toString();
//			if ( tmp != "" )
//				setAlbum( tmp );
//			// Get Genre
//			tmp = player->metaData("Genre").toString();
//			if ( tmp != "" )
//				setGenre( tmp );
//			// Get Album
//			tmp = player->metaData("AlbumArtist").toString();
//			if ( tmp != "" )
//				setAlbumArtists( tmp );
//			// Get Number
//			setNumber( player->metaData("TrackNumber").toInt() );
//			// Get Year
//			setYear( player->metaData("Year").toInt() );
//			// Get Date
//			setDate( player->metaData("Date").value<QDate>() );
//			// Get Duration
//			setDuration( player->metaData("Duration").value<QTime>() );
//			// Get Cover
//			QImage cover = player->metaData("CoverArtImage").value<QImage>();
//			setCover( player->metaData("CoverArtImage").value<QImage>() );

//			delete player;
		}
	}
	void display()
	{
		qDebug() << "-----------------";
		qDebug() << "Path : " << path();
		qDebug() << "Title : " << title();
		qDebug() << "Artist : " << artist();
		qDebug() << "Album : " << album();
		qDebug() << "Genre : " << genre();
		qDebug() << "Album Artists : " << albumArtists();
		qDebug() << "Year : " << year();
		qDebug() << "Date : " << date();
		qDebug() << "Duration : " << duration();
		qDebug() << "Number : " << number();
		qDebug() << "-----------------";
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // METADATA_H
