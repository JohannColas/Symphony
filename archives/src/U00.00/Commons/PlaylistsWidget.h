#ifndef PLAYLISTSWIDGET_H
#define PLAYLISTSWIDGET_H

#include <QListWidget>
#include <QFile>
#include <QDirIterator>
#include "../Commons/Icons.h"
#include "../Commons/TrackListWidget.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Playlists
		: public QObject
{
	Q_OBJECT
private:
	QList<TrackList*> _tracklists;
	int _currentTracklistIndex = -1;

public:
	Playlists()
	{
		update();
	}
	int currentTracklistIndex() const
	{
		return _currentTracklistIndex;
	}
	TrackList* currentTracklist() const
	{
		if ( isIndexValid( _currentTracklistIndex ) )
			return _tracklists.at(_currentTracklistIndex);
		return new TrackList();
	}
	int nbTracks() const
	{
		return _tracklists.count();
	}
	bool isIndexValid( int index ) const
	{
		return (index > -1 && index < nbTracks());
	}

public slots:
	void addTracklist( const QString& path )
	{
		_tracklists.append( new TrackList(path) );
		emit tracklistAdded();
	}
	void add( TrackList* tracklist )
	{
		_tracklists.append( tracklist );
		emit tracklistAdded();
	}
	void move( int from, int to )
	{
		if ( isIndexValid( from ) &&
			 isIndexValid( to ) )
		{
			_tracklists.move( from, to );
			emit tracklistMoved();
		}
	}
	void remove( int index )
	{
		if ( isIndexValid( index ) )
		{
			_tracklists.removeAt( index );
			emit tracklistRemoved();
		}
	}
	void clear()
	{
		_tracklists.clear();
		emit cleared();
	}
	QList<TrackList*> list()
	{
		return _tracklists;
	}
	void update()
	{
		QFile file( "./playlists/#playlists.spls" );
		if ( file.open(QIODevice::ReadOnly) )
		{
			QTextStream in(&file);
			while ( !in.atEnd() )
			{
				QString line = in.readLine();
				addTracklist( line );
			}
		}
		else
			qDebug() << "error Reading file";

	}
	void setCurrentIndex( int index )
	{
		if ( isIndexValid( index ) )
		{
			_currentTracklistIndex = index;
			emit currentTracklistChanged( currentTracklist() );
		}
	}

signals:
	void tracklistAdded();
	void tracklistMoved();
	void tracklistRemoved();
	void cleared();
	void currentTracklistChanged( TrackList* tracklist );
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistWidget
		: public QListWidget
{
	Q_OBJECT
private:
	Icons* _icons;
	Playlists* _playlists = new Playlists;

public:
	PlaylistWidget( QWidget* parent = nullptr )
		: QListWidget( parent )
	{
//		setMaximumWidth( 400 );
		setPlaylists( _playlists );
		connect( this, &PlaylistWidget::currentRowChanged,
				 _playlists, &Playlists::setCurrentIndex );
		connect( _playlists, &Playlists::currentTracklistChanged,
				 this, &PlaylistWidget::currentTracklistChanged );
	}
	Playlists* list()
	{
		return _playlists;
	}

public slots:
	void addPlaylist( const QString& /*name*/ )
	{

	}
	void setPlaylists( Playlists* playlists )
	{
		_playlists = playlists;
		clear();
		for ( TrackList* tracklist : playlists->list() )
		{
			addTracklist( tracklist );
		}
		setCurrentRow( playlists->currentTracklistIndex() );
	}
	void addTracklist( TrackList* tracklist )
	{
//		TrackItem *trackItem = new TrackItem( track, _icons );
		QListWidgetItem *item = new QListWidgetItem( tracklist->name() );
		addItem( item );
//		setItemWidget( item, trackItem );
	}
	void setIcons( Icons* icons )
	{
		_icons = icons;
	}
	void updateList()
	{

	}

signals:
	void currentTracklistChanged( TrackList* tracklist );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTSWIDGET_H
