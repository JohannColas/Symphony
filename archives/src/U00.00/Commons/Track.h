#ifndef TRACK_H
#define TRACK_H

#include <QObject>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QString>
#include <QImage>
#include <QTime>
#include <QEventLoop>
#include <QFile>
#include <QUrl>
#include <QElapsedTimer>
#include <QProcess>
// Taglib
#include <mpegfile.h>
#include <attachedpictureframe.h>
#include <id3v2tag.h>
#include <commentsframe.h>
using namespace TagLib;
#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Track
		: public QObject
{
	Q_OBJECT
private:
	QString _path = "Unknown";
	QString _title = "Unknown";
	QString _artist = "Unknown";
	QString _album = "Unknown";
	QDate _date = QDate();
	QString _genre = "Unknown";
	QImage _cover;
	QTime _duration = QTime( 0, 0 );
	int _year = -1;
	int _number = -1;
	QString _albumArtists = "";

public:
	virtual ~Track()
	{

	}
	Track( const QString& path )
	{
		_path = path;
		updateMetadata();
	}
	QString path() const
	{ return _path; }
	QString title() const
	{ return _title; }
	QString artist() const
	{ return _artist; }
	QString album() const
	{ return _album; }
	QDate date() const
	{ return _date; }
	QString genre() const
	{ return _genre; }
	QImage cover() const
	{ return _cover; }
	QTime duration() const
	{ return _duration; }
	int year() const
	{ return _year; }
	int number() const
	{ return _number; }
	QString albumArtists() const
	{ return _albumArtists; }

public slots:
	void setTitle( QString title )
	{ _title = title; }
	void setArtist( QString artist )
	{ _artist = artist; }
	void setAlbum( QString album )
	{ _album = album; }
	void setAlbumArtists( QString albumartists )
	{ _albumArtists = albumartists; }
	void setDate( QDate date )
	{ _date = date; }
	void setGenre( QString genre )
	{ _genre = genre; }
	void setCover( QImage cover )
	{ _cover = cover; }
	void setDuration( QTime durat )
	{ _duration = durat; }
	void setYear( int year )
	{ _year = year; }
	void setNumber( int number )
	{ _number = number; }
	void updateMetadata()
	{
		QElapsedTimer timer;
		timer.start();

		if ( _path != "" )
		{

			MPEG::File file( _path.toUtf8() );
			if ( file.isValid() )
			{
				ID3v2::Tag *tag = file.ID3v2Tag( true );
				if ( tag )
				{
					setAlbum( tag->album().toCString(true) );
					setTitle( tag->title().toCString(true) );
					setArtist( tag->artist().toCString(true) );
					setGenre( tag->genre().toCString(true) );
					setYear( tag->year() );
					setNumber( tag->track());
					QImage image;
					ID3v2::AttachedPictureFrame *frame = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(tag->frameList("APIC").front());
					if ( frame )
					{
						image.loadFromData(
									(const uchar *)frame->picture().data(),
									frame->picture().size());
					}
					setCover( image );

				}
				else
				  qDebug() << "file does not have a valid id3v2 tag" << endl;
			}
			else
			  qDebug() << "file \"" + _path + "\" invalid" << endl;


//			MPEG::File f(_path.toUtf8());

//			ID3v2::Tag *id3v2tag = f.ID3v2Tag(true);

////			ID3v2::FrameList frames = id3v2tag->frameList("APIC");
////			ID3v2::AttachedPictureFrame *frame =
////				static_cast<ID3v2::AttachedPictureFrame *>(frames.front());
//			bool hasCover = false;

//			if(id3v2tag)
//			{
//				setAlbum( id3v2tag->album().toCString(true) );
//				setTitle( id3v2tag->title().toCString(true) );
//				setArtist( id3v2tag->artist().toCString(true) );
//				setGenre( id3v2tag->genre().toCString(true) );
//				setYear( id3v2tag->year() );
//				setNumber( id3v2tag->track());


//				QImage image;
//				ID3v2::AttachedPictureFrame *f =
//						static_cast<TagLib::ID3v2::AttachedPictureFrame *>(id3v2tag->frameList("APIC").front());
//				if ( f )
//				{
//					image.loadFromData((const uchar *) f->picture().data(), f->picture().size());
//					hasCover = true;
//				}
//				setCover( image );

//				ID3v2::FrameList Frame = id3v2tag->frameListMap()["APIC"] ;
//				   if (!Frame.isEmpty() )
//				   {
//					 for(TagLib::ID3v2::FrameList::ConstIterator it = Frame.begin(); it != Frame.end(); ++it)
//					 {
//					   ID3v2::AttachedPictureFrame *PicFrame = (TagLib::ID3v2::AttachedPictureFrame *)(*it) ;
//					   //  if ( PicFrame->type() ==
//					   //TagLib::ID3v2::AttachedPictureFrame::FrontCover)
//					   {
//						   QByteArray pic( PicFrame->picture().data() );
//						   QImage image( PicFrame->picture().data() );
////						 // extract image (in it’s compressed form)
////						  unsigned long Size = PicFrame->picture().size() ;
////						 void* SrcImage = malloc ( Size ) ;
////						 if ( SrcImage )
////						 {
////						   memcpy ( SrcImage, PicFrame->picture().data(), Size ) ;
////						   fwrite(SrcImage,Size,1, jpegFile);
////						   fclose(jpegFile);
////						   free( SrcImage ) ;
//						 }

//					   }
//					 }

//			  cout << "ID3v2."
//				   << id3v2tag->header()->majorVersion()
//				   << "."
//				   << id3v2tag->header()->revisionNumber()
//				   << ", "
//				   << id3v2tag->header()->tagSize()
//				   << " bytes in tag" << endl;

//			  ID3v2::FrameList map = id3v2tag->frameList();

////			  cout << 'TIT2'  << map.find( ByteVector("TIT2") );

//			  ID3v2::AttachedPictureFrame * pictureFrame;

//			  ID3v2::FrameList::ConstIterator it = id3v2tag->frameList().begin();
//			  for(; it != id3v2tag->frameList().end(); it++) {
////				  string tagFrame = (*it)->frameID();
//				cout << (*it)->frameID().data();

//				if(ID3v2::CommentsFrame *comment = dynamic_cast<ID3v2::CommentsFrame *>(*it))
//				  if(!comment->description().isEmpty())
//					cout << " [" << comment->description().toCString(true) << "]";
//				pictureFrame = static_cast<ID3v2::AttachedPictureFrame *> (*it);//cast Frame * to AttachedPictureFrame*

//								//Warning. format of picture assumed to be jpg. This may be false, for example it may be png.
////								FILE * fout;
////								fopen_s(&fout, "outputFile.jpg", "wb");
////								cout<<"processing the file "<< argv[1] <<endl<<endl;
////								fwrite(pictureFrame->picture().data(), pictureFrame->picture().size(), 1, fout);
////								fclose(fout);
////								cout<<" The picture has been written to \t outputFile.jpg  \nRemember that the file type .jpg is just assumed for simplicity"<<endl<<endl;


////				string str = (*it)->toString().to8Bit();
////				string str2 = (*it)->toString().to8Bit(true);
//				cout << " - \""
//						 << (*it)->toString().toCString()
//						 << "\""
//						 << " - \""
//						 << (*it)->toString().toCString(true)
//						 << "\"" << endl;
//			  }
//			}
//			else
//			  qDebug() << "file does not have a valid id3v2 tag" << endl;

//			display();
			qDebug() << "Time to get Metadata with TagLib :" << timer.elapsed() << "milliseconds";
//			qDebug() << "";
//			timer.restart();
//			QMediaPlayer *player = new QMediaPlayer();
//			player->setMedia( QUrl::fromLocalFile(_path) );

//			QEventLoop loop;
//			connect(player, SIGNAL( metaDataChanged() ), &loop, SLOT( quit() ) );
//			emit player->metaDataChanged();
//			loop.exec();

//			// Get Title
//			QString tmp = player->metaData("Title").toString();
//			if ( tmp != "" )
//				setTitle( tmp );
//			// Get Artist
//			tmp = player->metaData("ContributingArtist").toString();
//			if ( tmp != "" )
//				setArtist( tmp );
//			// Get Album
//			tmp = player->metaData("AlbumTitle").toString();
//			if ( tmp != "" )
//				setAlbum( tmp );
//			// Get Genre
//			tmp = player->metaData("Genre").toString();
//			if ( tmp != "" )
//				setGenre( tmp );
//			// Get Album
//			tmp = player->metaData("AlbumArtist").toString();
//			if ( tmp != "" )
//				setAlbumArtists( tmp );
//			// Get Number
//			setNumber( player->metaData("TrackNumber").toInt() );
//			// Get Year
//			setYear( player->metaData("Year").toInt() );
//			// Get Date
//			setDate( player->metaData("Date").value<QDate>() );
//			// Get Duration
//			setDuration( player->metaData("Duration").value<QTime>() );
//			// Get Cover
//			QImage cover = player->metaData("CoverArtImage").value<QImage>();
//			setCover( player->metaData("CoverArtImage").value<QImage>() );

//			delete player;
//			display();
//			qDebug() << "Time to get Metadata with QMediaPlayer :" << timer.elapsed() << "milliseconds";
		}
		qDebug() << "---------------------------";
		qDebug() << "";
	}
	void display( bool hasCover = false )
	{
		qDebug() << "-----------------";
//		qDebug() << "Path : " << path();
		qDebug() << "Title : " << title();
		qDebug() << "Artist : " << artist();
		qDebug() << "Album : " << album();
		qDebug() << "Genre : " << genre();
//		qDebug() << "Album Artists : " << albumArtists();
		qDebug() << "Year : " << year();
//		qDebug() << "Date : " << date();
//		qDebug() << "Duration : " << duration();
		qDebug() << "Number : " << number();
		if ( hasCover )
			qDebug() << "hasCover";
		qDebug() << "-----------------";
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACK_H
