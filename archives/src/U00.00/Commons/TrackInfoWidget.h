#ifndef TRACKINFOWIDGET_H
#define TRACKINFOWIDGET_H

#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QMediaPlayer>

#include "../Commons/Icons.h"
#include "../Commons/Lang.h"

#include <QDebug>

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackCover
		: public QLabel
{
	Q_OBJECT

public:
	TrackCover( QWidget* parent = nullptr )
		: QLabel( parent )
	{

	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackTitle
		: public QPushButton
{
	Q_OBJECT

public:
	TrackTitle( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setFlat( true );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackArtist
		: public QPushButton
{
	Q_OBJECT

public:
	TrackArtist( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setFlat( true );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackInfoWidget
		: public QFrame
{
	Q_OBJECT
protected:
	QMediaPlayer* _player = 0;
	QGridLayout* lay_main = new QGridLayout;
	TrackCover* lb_cover = new TrackCover;
	TrackTitle* lb_title = new TrackTitle;
	TrackArtist* lb_artist = new TrackArtist;
	Icons* _icons = 0;
	//	QLabel* lb_Title = new QLabel;
	//	QLabel* lb_Title = new QLabel;
	//	QLabel* lb_Title = new QLabel;
public:
	~TrackInfoWidget()
	{
		delete lay_main;
		delete lb_title;
		delete lb_artist;
	}
	TrackInfoWidget( QWidget* parent = nullptr )
		: QFrame( parent )
	{
		lay_main->addWidget( lb_cover, 0, 0 );
		lay_main->addWidget( lb_title, 1, 0 );
		lay_main->addWidget( lb_artist, 2, 0 );
		lay_main->addItem(
					new QSpacerItem( 0,
									 0,
									 QSizePolicy::Preferred,
									 QSizePolicy::Expanding),
					3, 0 );

		lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );

		setLayout( lay_main );
	}

public slots:
	void setPlayer( QMediaPlayer* player )
	{
		_player = player;
		connect( player, SIGNAL(metaDataChanged()),
				 this, SLOT(update()) );
	}
	void update()
	{
		if ( _player )
		{
			QImage cover = _player->metaData("CoverArtImage").value<QImage>();
			QPixmap pix = QPixmap();
			if ( pix.convertFromImage( cover ) )
				lb_cover->setPixmap(
							pix.scaled( lb_cover->width(),
										lb_cover->height(),
										Qt::KeepAspectRatio,
										Qt::SmoothTransformation) );
			else
			{
				if ( _icons )
				{
					QIcon icon = _icons->get("track");
					lb_cover->setPixmap(
								icon.pixmap(
									lb_cover->width(),
									lb_cover->height()) );
				}
			}

			lb_title->setText( _player->metaData("Title").toString() );
			lb_artist->setText( _player->metaData("ContributingArtist").toString() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKINFOWIDGET_H
