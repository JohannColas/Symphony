#ifndef TRACKLIST_H
#define TRACKLIST_H

#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QDirIterator>
#include <QSettings>
#include "../Commons/Track.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackList
		: public QObject
{
	Q_OBJECT
public:
	enum Type
	{
		TACKLIST,
		PLAYLIST,
		CURRENTPLAYLIST,
		LIBRARY
	};
private:
	QList<Track*> _list;
	QString _name = "";
	TrackList::Type _type = Type::TACKLIST;
	QString _path = "";
	bool _hide = false;
	int _currentTrack = 0;

public:
	TrackList()
	{

	}
	TrackList( const QString& name )
		: _name( name )
	{
		updateList();
	}
	Track* track( int index ) const
	{
		if ( isIndexValid( index ) )
			return _list.at( index );
		return new Track("");
	}
	QList<Track*> list() const
	{ return _list; }
	QString name() const
	{ return _name; }
	TrackList::Type type() const
	{ return _type; }
	QString path() const
	{ return _path; }
	bool hide() const
	{ return _hide;	}
	int currentTrackIndex() const
	{ return _currentTrack;	}
	Track* currentTrack() const
	{
		if ( isIndexValid( _currentTrack ) )
			return _list.at( _currentTrack );
		return new Track("");
	}
	int nbTracks() const
	{
		return _list.size();
	}
	bool isIndexValid( int index ) const
	{
		return (index > -1 && index < nbTracks());
	}
	QMediaPlaylist* mediaPlaylist()
	{
		QMediaPlaylist* playlist = new QMediaPlaylist;
		for ( Track* track : _list )
		{
			playlist->addMedia( QUrl::fromLocalFile( track->path() ) );
		}
		return playlist;
	}

public slots:
	void setName( const QString& name )
	{ _name = name; }
	void setType( const TrackList::Type& type )
	{ _type = type; }
	void setPath( const QString& path )
	{ _path = path; }
	void setHide( bool hide )
	{ _hide = hide; }
	void setCurrentTrack( int index )
	{
		if ( isIndexValid( index ) )
			_currentTrack = index;
	}
	void addTrack( const QString& path )
	{
		_list.append( new Track(path) );
		emit trackAdded();
	}
	void add( Track* track )
	{
		_list.append( track );
		emit trackAdded();
	}
	void move( int from, int to )
	{
		if ( isIndexValid( from ) &&
			 isIndexValid( to ) )
		{
			_list.move( from, to );
			emit trackMoved();
		}
	}
	void remove( int index )
	{
		if ( isIndexValid( index ) )
		{
			_list.removeAt( index );
			emit trackRemoved();
		}
	}
	void clear()
	{
		_list.clear();
		emit cleared();
	}
	void updateList()
	{
		QString path = "./playlists/" + name() + ".spl";
		QFile file( path );
		if ( file.open(QIODevice::ReadOnly) )
		{
			QTextStream in(&file);
			while ( !in.atEnd() )
			{
				QString line = in.readLine();
				addTrack( line );
			}
		}
		else
			qDebug() << "error Reading file :" << QFileInfo(file).absoluteFilePath();
	}
	virtual void update()
	{
		if ( type() == Type::CURRENTPLAYLIST ||
			 type() == Type::PLAYLIST )
		{
			QString path = "./playlists/" + name() + ".spl";
			QFile file( path );
			if ( file.open(QIODevice::ReadOnly) )
			{
				QTextStream in(&file);
				while ( !in.atEnd() )
				{
					QString line = in.readLine();
					addTrack( line );
				}
			}
			else
				qDebug() << "error Reading file :"
						 << QFileInfo(file).absoluteFilePath();
		}
		else if ( type() == Type::LIBRARY )
		{
			QSettings _setsFile( "./libraries/" + name() + ".slb", QSettings::IniFormat );
			setPath( _setsFile.value("path").toString() );
			setHide( _setsFile.value("hide").toInt() );

			QDirIterator it( path(),
			{"*.mp3", "*.ogg", ".wav", ".flac", ".m4a", ".wma"},
							 QDir::Files,
							 QDirIterator::Subdirectories );
			while ( it.hasNext() )
			{
				QFile f( it.next() );
				qDebug() << f.fileName();
			}
//			QString path = "./playlists/" + name() + ".spl";
//			QFile file( path );
//			if ( file.open(QIODevice::ReadOnly) )
//			{
//				QTextStream in(&file);
//				while ( !in.atEnd() )
//				{
//					QString line = in.readLine();
//					addTrack( line );
//				}
//			}
//			else
//				qDebug() << "error Reading file :"
//						 << QFileInfo(file).absoluteFilePath();
		}
		QString path = "./playlists/" + name() + ".spl";
		QFile file( path );
		if ( file.open(QIODevice::ReadOnly) )
		{
			QTextStream in(&file);
			while ( !in.atEnd() )
			{
				QString line = in.readLine();
				addTrack( line );
			}
		}
		else
			qDebug() << "error Reading file :" << path;
	}

signals:
	void trackAdded();
	void trackMoved();
	void trackRemoved();
	void cleared();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKLIST_H
