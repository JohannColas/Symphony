#ifndef TRACKLISTWIDGET_H
#define TRACKLISTWIDGET_H
#include <QMediaObject>
#include <QVariant>
#include <QImage>
#include <QPixmap>
#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QListWidgetItem>
#include "../Commons/Track.h"
#include "../Commons/TrackList.h"
#include "../Commons/Metadata.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
#include <QResizeEvent>
#include <QListWidget>
#include <QScrollBar>

#include <QProcess>

#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackCoverItem
		: public QLabel
{
	Q_OBJECT

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackTitleItem
		: public QLabel
{
	Q_OBJECT
public:
	TrackTitleItem( QWidget* parent = nullptr )
		: QLabel( parent )
	{
		setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackArtistItem
		: public QLabel
{
	Q_OBJECT
public:
	TrackArtistItem( QWidget* parent = nullptr )
		: QLabel( parent )
	{
		setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackItem
		: public QWidget
{
	Q_OBJECT
protected:
	QGridLayout* lay_main = new QGridLayout;
	TrackCoverItem* lb_cover = new TrackCoverItem;
	TrackTitleItem* lb_title = new TrackTitleItem;
	TrackArtistItem* lb_artist = new TrackArtistItem;
	Icons* _icons = 0;
	Track* _track = 0;
	Metadata* _meta = 0;
public:
	TrackItem( QWidget* parent = nullptr )
		: QWidget( parent )
	{
		lay_main->setMargin( 0 );
		//	lay_main->setSpacing( 0 );
		lay_main->setVerticalSpacing( 0 );

		lay_main->addWidget( lb_cover, 0, 0, 2, 1 );
		lay_main->addWidget( lb_title, 0, 1, 1, 1 );
		lay_main->addWidget( lb_artist, 1, 1, 1, 1 );

		setLayout( lay_main );

		//		connect( this, &SongInfoWidget::trackChanged,
		//				 this, &SongInfoWidget::updateWidget );
	}
	TrackItem( Track* track, Icons* icons, QWidget* parent = nullptr )
		: QWidget( parent )
	{
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		lay_main->setMargin( 0 );
		lay_main->setSpacing( 0 );
		lay_main->setVerticalSpacing( 0 );

		lay_main->addWidget( lb_cover, 0, 0, 2, 1 );
		lay_main->addWidget( lb_title, 0, 1, 1, 1 );
		lay_main->addWidget( lb_artist, 1, 1, 1, 1 );

		lb_cover->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );

		setLayout( lay_main );

		//		connect( this, &SongInfoWidget::trackChanged,
		//				 this, &SongInfoWidget::updateWidget );
		_track = track;
		updateIcons( icons );
		updateWidget( track );
	}
	TrackItem( Metadata* meta, Icons* icons, QWidget* parent = nullptr )
		: QWidget( parent )
	{
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		lay_main->setMargin( 0 );
		lay_main->setSpacing( 0 );
		lay_main->setVerticalSpacing( 0 );

		lay_main->addWidget( lb_cover, 0, 0, 2, 1 );
		lay_main->addWidget( lb_title, 0, 1, 1, 1 );
		lay_main->addWidget( lb_artist, 1, 1, 1, 1 );

		lb_cover->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );

		setLayout( lay_main );

		//		connect( this, &SongInfoWidget::trackChanged,
		//				 this, &SongInfoWidget::updateWidget );
		_meta = meta;
		updateIcons( icons );
		updateWidget( meta );
	}

public slots:
	void updateWidget( Track* track )
	{
		QPixmap pix = QPixmap();
		if ( pix.convertFromImage( track->cover() ) )
			lb_cover->setPixmap(
						pix.scaled( sizeHint().height(),
									sizeHint().height(),
									Qt::KeepAspectRatio,
									Qt::SmoothTransformation) );
		else
		{
			if ( _icons )
			{
				QIcon icon = _icons->get("track");
				lb_cover->setPixmap( icon.pixmap( sizeHint().height(),
												  sizeHint().height()) );
			}
		}

		lb_title->setText( track->title() );
		lb_artist->setText( track->artist() );
	}
	void updateWidget( Metadata* meta )
	{
		QPixmap pix = QPixmap();
		if ( pix.convertFromImage( meta->cover() ) )
			lb_cover->setPixmap(
						pix.scaled( sizeHint().height(),
									sizeHint().height(),
									Qt::KeepAspectRatio,
									Qt::SmoothTransformation) );
		else
		{
			if ( _icons )
			{
				QIcon icon = _icons->get("track");
				lb_cover->setPixmap( icon.pixmap( sizeHint().height(),
												  sizeHint().height()) );
			}
		}

		lb_title->setText( meta->title() );
		lb_artist->setText( meta->artist() );
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}

signals:
	void trackChanged();
	void updated();
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackListWidget
		: public QListWidget
{
	Q_OBJECT
private:
	Icons* _icons;

public:
	TrackListWidget( QWidget* parent = nullptr )
		: QListWidget( parent )
	{

	}

public slots:
	void setIcons( Icons* icons )
	{
		_icons = icons;
	}
	void setTrackList( TrackList* tracks )
	{
		clear();
		for ( Track* track : tracks->list() )
		{
			addTrack( track );
		}
		setCurrentRow( tracks->currentTrackIndex() );
	}
	void addTrack( Track* track )
	{
		TrackItem *trackItem = new TrackItem( track, _icons );
		QListWidgetItem *item = new QListWidgetItem;
		addItem( item );
		setItemWidget( item, trackItem );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackListWidget2
		: public QFrame
{
	Q_OBJECT
private:
	Icons* _icons = 0;
	TrackList* _tracks = 0;
	QList<TrackItem*> _items;
	QHBoxLayout* lay_main = new QHBoxLayout;
	QListWidget* _list = new QListWidget;
	QScrollBar* _scroll = new QScrollBar;
	int _begin = 0;


public:
	TrackListWidget2( QWidget* parent = nullptr )
		: QFrame( parent )
	{
		setMinimumWidth( 250 );
		setLayout( lay_main );
		lay_main->addWidget( _list );
		lay_main->addWidget( _scroll );
		_scroll->setOrientation( Qt::Vertical );
		_list->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
		_list->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
		connect( _scroll, &QScrollBar::valueChanged,
				 this, &TrackListWidget2::updateIndex );
	}

public slots:
	void updateIndex( int index )
	{
		qDebug() << _scroll->maximum() << index;
		_begin = index;
		update();
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
	void setTrackList( TrackList* tracks )
	{
		_tracks = tracks;
		_scroll->setMaximum( _tracks->nbTracks() );
		update();
	}
	void update()
	{
		_list->blockSignals(true);
		_list->clear();
		int height = 0;
		int nbItems = 0;
		for ( int it = _begin; it < _tracks->nbTracks(); ++it )
		{
//			QString cmd = "ffmpeg -i \"" + _tracks->list().at(it)->path()  + "\"";
//			QProcess process;
//			process.start(cmd);
//			process.waitForFinished(-1); // will wait forever until finished

//			QString out = process.readAllStandardOutput();
//			QString err = process.readAllStandardError();
//			qDebug().noquote() << cmd;
//			qDebug() << out;
//			qDebug() << err;

//			Metadata meta( _tracks->list().at(it) );
//			TrackItem *trackItem = new TrackItem(&meta, _icons );
			TrackItem *trackItem = new TrackItem(_tracks->list().at(it), _icons );
			QListWidgetItem *item = new QListWidgetItem;
			_list->addItem( item );
			_list->setItemWidget( item, trackItem );
			item->setSizeHint( {_list->sizeHint().width(), trackItem->height() } );
//			qDebug() << meta.title() << height;
			height += trackItem->height();
			if ( height > _list->height() )
			{
				_list->takeItem( nbItems );
				break;
			}
			++nbItems;
		}
		_scroll->setMaximum( _tracks->nbTracks()-nbItems );
//		qDebug() << "----------";

		_list->blockSignals(false);
//		blockSignals(true);
//		for ( auto item : _items )
//			delete item;
//		_items.clear();
//		int height = 0;
//		for ( Track* track : _tracks->list() )
//		{
//			TrackItem *trackItem = new TrackItem( track, _icons, this );

//			trackItem->move( 0, height );
//			_items.append( trackItem );
////			trackItem->setGeometry( 0, height, trackItem->width(), trackItem->height() );
//			qDebug() << track->title() << height;
//			height += trackItem->height();
//			if ( height > this->height() )
//				break;
//		}
//		qDebug() << "----------";
//		blockSignals(false);

	}

	void resizeEvent( QResizeEvent* event ) override
	{
		if ( _tracks )
			update();
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKLISTWIDGET_H
