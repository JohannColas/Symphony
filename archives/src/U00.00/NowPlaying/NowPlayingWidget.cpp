#include "NowPlayingWidget.h"

NowPlayingWidget::NowPlayingWidget( QWidget* parent )
	: QFrame( parent )
{
	lay_main->addWidget( wid_trackInfo, 0, 0 );
	lay_main->addWidget( wid_trackList, 0, 1 );

	wid_trackList->setMaximumWidth( 400 );
	setLayout( lay_main );
}

void NowPlayingWidget::setPlayer( QMediaPlayer* player )
{
	_player = player;
	wid_trackInfo->setPlayer( player );
	connect( _player->playlist(), &QMediaPlaylist::loaded,
			 this, &NowPlayingWidget::updateList );
	updateList();
}

void NowPlayingWidget::setPlaylist( Playlist* /*playlist*/ )
{
//	wid_tracksList->clear();
//	for ( int it = 0; it < playlist->nbTracks(); ++it )
//	{
////		wid_tracksList->addItem( playlist->track(it)->title() + '\n' +
////								 playlist->track(it)->artist() );
////		wid_tracksList->item( it )->setIcon( QIcon( QPixmap::fromImage( playlist->track(it)->cover() ) ) );
////		wid_tracksList->setItemWidget();

//		TitleArtistWidget *myItem = new TitleArtistWidget( playlist->track(it), _icons );
//		QListWidgetItem *item = new QListWidgetItem;//( playlist->track(it)->path() );
////		item->setSizeHint( {0, myItem->sizeHint().height()} );
//		wid_tracksList->addItem( item );
//		wid_tracksList->setItemWidget( item, myItem );
//	}
//	wid_tracksList->setCurrentRow( playlist->currentTrack() );
}

void NowPlayingWidget::setTrackList( TrackList* list )
{
	wid_trackList->setTrackList( list );
}

void NowPlayingWidget::updateList()
{
	if ( _player )
	{
	}
}

void NowPlayingWidget::updateIcons( Icons* icons )
{
	_icons = icons;
	wid_trackInfo->updateIcons( _icons );
	wid_trackList->setIcons( _icons );
}
