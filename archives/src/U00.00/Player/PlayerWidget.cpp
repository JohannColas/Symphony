#include "PlayerWidget.h"

#include <QDebug>
#include <QApplication>
/**********************************************/
/**********************************************/
/* */
PlayerWidget::~PlayerWidget()
{

}
/**********************************************/
/**********************************************/
/* */
PlayerWidget::PlayerWidget( QWidget* parent )
	: QFrame( parent )
{
	//	setFrameShape( QFrame::NoFrame );
	//	QMediaPlaylist* playlist = new QMediaPlaylist;
	//	playlist->addMedia(QUrl::fromLocalFile("/home/johanncolas/Musique/David Bowie - Aladdin Sane.mp3"));
	//	playlist->addMedia(QUrl::fromLocalFile("/home/johanncolas/Musique/Alabina - Alabina.mp3"));
	//	playlist->setCurrentIndex( 1 );


	//	player->setMedia(QUrl::fromLocalFile("/home/johanncolas/Musique/Alabina - Alabina.mp3"));
	//	player->setPlaylist( playlist );
	//	player->setVolume(50);
	//	_track = player;
	//	player->play();
	//	connect( player, &QMediaPlayer::positionChanged,
	//			 this, &PlayerWidget::setPosition );
	//	connect( player, &QMediaPlayer::mediaChanged,
	//			 this, &PlayerWidget::changeMedia );
	//	connect( pb_play, &QPushButton::clicked,
	//			this, &PlayerWidget::playpause );
	//	connect( pb_stop, &QPushButton::clicked,
	//			 player, &QMediaPlayer::stop );
	//	connect( pb_backward, &QPushButton::clicked,
	//			 this, &PlayerWidget::backward );
	//	connect( pb_forward, &QPushButton::clicked,
	//			 player->playlist(), &QMediaPlaylist::next );

	//	connect( player, SIGNAL(metaDataChanged()),
	//			 wid_songInfo, SLOT(updateWidget()) );
	connect( pb_play, &QPushButton::clicked,
			 this, &PlayerWidget::playpause );
	connect( pb_backward, &QPushButton::clicked,
			 this, &PlayerWidget::backward );
	connect( pb_forward, &QPushButton::clicked,
			 this, &PlayerWidget::forward );
	connect( sl_time, &QSlider::sliderMoved,
			 this, &PlayerWidget::setPlayerPos );
	_timer->setSingleShot( true );
	connect( _timer, &QTimer::timeout,
			 this, &PlayerWidget::updatePlayerPos );

	pb_backward->setFlat( true );
	pb_play->setFlat( true );
	pb_stop->setFlat( true );
	pb_forward->setFlat( true );
	sl_time->setOrientation( Qt::Horizontal );
	sl_time->setMinimum( 0 );
	sl_time->setMaximum( 1000 );
	QHBoxLayout* lay_main = new QHBoxLayout;
	lay_main->addWidget( pb_backward );
	lay_main->addWidget( pb_play );
	lay_main->addWidget( pb_stop );
	lay_main->addWidget( pb_forward );
	lay_main->addWidget( wid_songInfo );
	lay_main->addWidget( sl_time );
	lay_main->addItem( new QSpacerItem( 0,
										0,
										QSizePolicy::Expanding )
					   );
	lay_main->addWidget( pb_close );
	connect( pb_close, &QPushButton::clicked,
			 qApp, &QApplication::closeAllWindows );
	setLayout( lay_main );

	setMouseTracking( true );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPlayer( QMediaPlayer* player )
{
	_player = player;
	wid_songInfo->setTrack( _player );
	connect( _player, &QMediaPlayer::positionChanged,
			 this, &PlayerWidget::setPosition );
	connect( _player, &QMediaPlayer::mediaChanged,
			 this, &PlayerWidget::changeMedia );
	connect( pb_stop, &QPushButton::clicked,
			 _player, &QMediaPlayer::stop );
//	connect( pb_forward, &QPushButton::clicked,
//			 _player->playlist(), &QMediaPlaylist::next );

	connect( _player, SIGNAL(metaDataChanged()),
			 wid_songInfo, SLOT(updateWidget()) );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPosition( qint64 pos )
{
	if ( _player )
		if ( _player->duration() )
		{
			int perc = 1000*pos/_player->duration();
			//	qDebug() << pos << player->duration() << perc;
			sl_time->setValue( perc );
		}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPlayerPos( int /*pos*/ )
{
	_timer->start(200);
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::updatePlayerPos()
{
	if ( _player )
	{
		qint64 newpos = sl_time->value()*_player->duration()/1000;
		_player->setPosition( newpos );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::changeMedia()
{

	//	sl_time->setMaximum( player->duration() );
	sl_time->setValue( 0 );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::playpause()
{
	if ( _player )
	{
		if ( _player->state() == QMediaPlayer::PlayingState )
			_player->pause();
		else
			_player->play();
		updatePlayPauseIcon();
	}
}
void PlayerWidget::updatePlayPauseIcon()
{
	if ( _player )
	{
		if ( _player->state() == QMediaPlayer::PlayingState )
			pb_play->setIcon( _icons->get("pause") );
		else
			pb_play->setIcon( _icons->get("play") );
	}
	else
	{
		pb_play->setIcon( _icons->get("play") );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::backward()
{
	if ( _player )
	{
		if ( _player->playlist()->currentIndex() != 0 )
		{
			if ( sl_time->value() < 40 )
				_player->playlist()->previous();
			else
				_player->setPosition( 0 );
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::forward()
{
	if ( _player )
	{
		_player->playlist()->next();
		if ( _player->playlist()->currentIndex() == -1 )
		{
			_player->playlist()->setCurrentIndex( 0 );
			_player->stop();
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::updateIcons( Icons* icons )
{
	_icons = icons;
	pb_backward->setIcon( icons->get("skip_previous") );
	updatePlayPauseIcon();
	pb_stop->setIcon( icons->get("stop") );
	pb_forward->setIcon( icons->get("skip_next") );
	wid_songInfo->updateIcons( _icons );
}
/**********************************************/
/**********************************************/
void PlayerWidget::mouseHoverEvent(QMouseEvent* event)
{
	setCursor( Qt::ArrowCursor );
}
/**********************************************/
/**********************************************/
/* */
