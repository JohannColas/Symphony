#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H

#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QFrame>
#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QProgressBar>
#include <QTimer>
#include <QMouseEvent>
#include "../Commons/Track.h"
#include "../Player/SongInfoWidget.h"
#include "../Commons/Theme.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerButton
		: public QPushButton
{
	Q_OBJECT

public:
	PlayerButton( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerWidget
		: public QFrame
{
	Q_OBJECT
private:
//	QMediaObject* _track;
	QMediaPlayer* _player = 0;
	QTimer* _timer = new QTimer(this);
	PlayerButton* pb_backward = new PlayerButton;
	PlayerButton* pb_play = new PlayerButton;
	PlayerButton* pb_stop = new PlayerButton;
	PlayerButton* pb_forward = new PlayerButton;
	SongInfoWidget* wid_songInfo = new SongInfoWidget;

	QPushButton* pb_close = new QPushButton;
	QSlider* sl_time = new QSlider;
	Icons* _icons = 0;

public:
	~PlayerWidget();
	PlayerWidget( QWidget* parent = nullptr );

public slots:
	void setPlayer( QMediaPlayer* player );
	void setPosition( qint64 pos );
	void setPlayerPos( int pos );
	void updatePlayerPos();
	void changeMedia();
	void playpause();
	void backward();
	void forward();
	void updateIcons( Icons* icons );
	void updatePlayPauseIcon();
	void mouseHoverEvent( QMouseEvent* event );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERWIDGET_H
