#include "Symphony.h"


/**********************************************/
/**********************************************/
/* */
Symphony::~Symphony()
{
	delete _theme;
	delete _icons;
	delete _lang;
}
/**********************************************/
/**********************************************/
/* */
Symphony::Symphony( QWidget *parent )
	: QFrame( parent )
{
//	setWindowFlags( Qt::Window | Qt::FramelessWindowHint );

	setMinimumSize( 800, 500 );

	wid_player->setFixedHeight( 50 );

	wid_nowPlaying->hide();
	wid_nowPlaying->setMouseTracking(true);
	QWidget* wid1 = new QWidget;
	wid1->setStyleSheet( "background:darkgreen;" );
	QWidget* wid2 = new QWidget;
	wid2->setStyleSheet( "background:darkblue;" );
	wid_sidebar->setWidget( 0, wid_nowPlaying );
	wid_sidebar->setMouseTracking(true);

	PlaylistWidget* wid_playlits = new PlaylistWidget;
	TrackListWidget2* wid_trackli = new TrackListWidget2;
//	qDebug() << wid_playlits->list()->list().at(1)->name();
	wid_trackli->setTrackList( wid_playlits->list()->list().at(0) );
	qDebug() << wid_playlits->list()->list().at(0)->track(0)->title();
	wid_trackli->updateIcons( _icons );
//	connect( wid_playlits, &PlaylistWidget::currentTracklistChanged,
//			 wid_trackli, &TrackListWidget::setTrackList );

	QWidget* playliss = new QWidget;
	playliss->hide();
	QGridLayout* lay0 = new QGridLayout;
	lay0->addWidget( wid_playlits, 0, 0 );
	lay0->addWidget( wid_trackli, 0, 1 );
	playliss->setLayout( lay0 );
	playliss->setMouseTracking(true);
	wid_sidebar->setWidget( 5, playliss );

//	connect( wid_libraryList, &LibraryListWidget::currentLibraryChanged,
//			 wid_library, &TrackListWidget::setTrackList );
	QPushButton* pb_updateLibraries = new QPushButton;
	pb_updateLibraries->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Preferred );
	connect( pb_updateLibraries, SIGNAL(clicked()),
			 this, SLOT(updateLibrary()) );
	QWidget* wid_libraries = new QWidget;
	wid_libraries->hide();
	QGridLayout* lay_libraries = new QGridLayout;
	lay_libraries->addWidget( wid_libraryList, 0, 0 );
	lay_libraries->addWidget( pb_updateLibraries, 1, 0 );
	lay_libraries->addWidget( wid_library, 0, 1, 2, 1 );
	wid_libraries->setLayout( lay_libraries );
	wid_libraries->setMouseTracking(true);
	wid_sidebar->setWidget( 6, wid_libraries );

			wid_sidebar->changeCurrentWidget( 0 );
	QGridLayout* lay_main = new QGridLayout;
	lay_main->setMargin( 0 );
	lay_main->setSpacing( 0 );
	lay_main->addWidget( wid_player, 0, 0, 1, 2 );
	lay_main->addWidget( wid_sidebar, 1, 0, 1, 2 );

	setLayout( lay_main );
	// -----------------------
	// -----------------------
	// Themes
	_theme->apply();
	updateIcons();
	updateLang();
	// -----------------------
	// -----------------------
	_tracklist->addTrack( "/home/johanncolas/Musique/David Bowie - Aladdin Sane.mp3" );
	_tracklist->addTrack( "/home/johanncolas/Musique/Alabina - Alabina.mp3" );
	_tracklist->addTrack( "/home/johanncolas/Musique/Travis - Closer.mp3" );



	QMediaPlaylist* now_playlist = new QMediaPlaylist;
	now_playlist->addMedia(QUrl::fromLocalFile("/home/johanncolas/Musique/David Bowie - Aladdin Sane.mp3"));
	now_playlist->addMedia(QUrl::fromLocalFile("/home/johanncolas/Musique/Alabina - Alabina.mp3"));
	now_playlist->setCurrentIndex( 1 );
	now_playlist->addMedia(QUrl::fromLocalFile("/home/johanncolas/Musique/Travis - Closer.mp3"));
	_player->setPlaylist( _tracklist->mediaPlaylist() );
	_player->setVolume(50);
	wid_player->setPlayer( _player );
	wid_nowPlaying->setPlayer( _player );
//	updatePlaylist();
	wid_nowPlaying->updateIcons( _icons );
	updateTrackList();

	setMouseTracking( true );
	installEventFilter(this);

}
/**********************************************/
/**********************************************/
/* */
void Symphony::updatePlaylist()
{
	wid_nowPlaying->setPlaylist( _playlist );
}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateTrackList()
{
	wid_nowPlaying->setTrackList( _tracklist );
}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateIcons()
{
	_icons->update();
	setWindowIcon( _icons->get( "Symphony" ) );
	wid_player->updateIcons( _icons );
	wid_sidebar->updateIcons( _icons );
	wid_nowPlaying->updateIcons( _icons );
}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateLang()
{
	_lang->update();

}

void Symphony::updateLibrary()
{
	wid_libraryList->list()->list().at(0)->update();
	wid_library->setTrackList( wid_libraryList->list()->list().at(0) );
}
/**********************************************/
/**********************************************/
/* */
void Symphony::mousePressEvent( QMouseEvent *event )
{
	 if ( event->button() == Qt::LeftButton )
	 {
		 _lastPos = event->globalPos();
		 isPressedWidget = true;
	 }
	 event->accept();
}
/**********************************************/
/**********************************************/
/* */
void Symphony::mouseDoubleClickEvent(QMouseEvent* event)
{
	if ( event->button() == Qt::LeftButton )
	{
		if ( isMaximized() )
			showNormal();
		else
			showMaximized();
		event->accept();
	}
}
/**********************************************/
/**********************************************/
/* */
void Symphony::mouseReleaseEvent( QMouseEvent *event )
{
	 if ( event->button() == Qt::LeftButton )
	 {
		 isPressedWidget = false;
	 }

	 event->accept();
}
/**********************************************/
/**********************************************/
/* */
void Symphony::mouseMoveEvent( QMouseEvent *event )
{
	if ( isPressedWidget )
	{
		this->move( this->x() + (event->globalX() - _lastPos.x()),
				   this->y() + (event->globalY() - _lastPos.y()) );
		_lastPos = event->globalPos();
	}
	event->accept();
}
/**********************************************/
/**********************************************/
bool Symphony::eventFilter( QObject* /*object*/, QEvent* event )
{
	if (event->type() == QEvent::MouseMove )
	{
	}
	else if (event->type() == QEvent::None )
	{
//		setCursor( Qt::ArrowCursor );
	}
	return false;
}
