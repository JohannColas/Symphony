#ifndef WIDGET_H
#define WIDGET_H

#include <QFrame>
#include "../Commons/Theme.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
#include "../Commons/Playlist.h"
#include "../Commons/TrackListWidget.h"
#include "../Commons/Lang.h"
#include <QListWidget>

#include <QGridLayout>
#include <QSplitter>
#include <QRubberBand>
#include <QMouseEvent>
#include <QCursor>

#include "../Player/PlayerWidget.h"
#include "../Sidebar/SidebarWidget.h"
#include "../NowPlaying/NowPlayingWidget.h"
#include "../Commons/PlaylistsWidget.h"
#include "../Commons/LibraryListWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Symphony
		: public QFrame
{
	Q_OBJECT
private:
	int invisibleBorderSize = 2;
	Playlist* _playlist = new Playlist;
	TrackList* _tracklist = new TrackList;
	QMediaPlayer* _player = new QMediaPlayer;
	bool isPressedWidget;
	QPoint _lastPos;
	NowPlayingWidget* wid_nowPlaying = new NowPlayingWidget;
	LibraryListWidget* wid_libraryList = new LibraryListWidget;
	TrackListWidget2* wid_library = new TrackListWidget2;
protected:
	Theme* _theme = new Theme;
	Icons* _icons = new Icons;
	Lang* _lang = new Lang;
	PlayerWidget* wid_player = new PlayerWidget;
	SidebarWidget* wid_sidebar = new SidebarWidget;

public:
	~Symphony();
	Symphony( QWidget *parent = nullptr );


public slots:
	void updatePlaylist();
	void updateTrackList();
	void updateIcons();
	void updateLang();
	void updateLibrary();
	void mousePressEvent( QMouseEvent* event );
	void mouseDoubleClickEvent( QMouseEvent* event );
	void mouseReleaseEvent( QMouseEvent* event );
	void mouseMoveEvent( QMouseEvent* event );
	bool eventFilter(QObject* object, QEvent* event);
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WIDGET_H
