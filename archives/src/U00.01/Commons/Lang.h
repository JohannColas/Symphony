#ifndef LANG_H
#define LANG_H

#include <QObject>
#include <QMap>
#include <QFile>
#include <QTextStream>

#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Lang
		: public QObject
{
	Q_OBJECT
private:
	QString _selectedLang = "FR-fr";
	QStringList _availableLangs = {""};
	QMap<QString,QString> _lang;

public:
	Lang() {
		_lang["welcome"] = "Welcome";
		_lang["projectExplorer"] = "Project Explorer";
		_lang["settings"] = "Settings";
		_lang["add"] = "Add";
		_lang["new"] = "New";
		_lang["open"] = "Open";
		_lang["delete"] = "Delete";
		_lang["width"] = "Width";
		_lang["height"] = "Height";
		_lang["style"] = "Style";
		_lang["color"] = "Color";
		_lang["cap"] = "Cap";
		_lang["join"] = "Join";
		_lang["background"] = "Background";
		_lang["borders"] = "Borders";
		_lang["graphSets"] = "Graph Settings";
		_lang["layersSets"] = "Layers Settings";
		_lang["axesSets"] = "Axes Settings";
		_lang["curvesSets"] = "Curves Settings";
		_lang["shapesSets"] = "Shapes Settings";
		_lang["graph"] = "Graph";
		_lang["layers"] = "Layers";
		_lang["axes"] = "Axes";
		_lang["curves"] = "Curves";
		_lang["shapes"] = "Shapes";

	}
	QString get( const QString& key ) {
		return _lang[key];
	}
	void setSelectedLang( QString lang ) {
		_selectedLang = lang;
		emit changed();
	}
	QString selectedLang() {
		return _selectedLang;
	}
	QStringList availableLangs() {
		return _availableLangs;
	}

public slots:
	void updateLangsList() {
		// update Available lang
	}
	void update() {
		// update _lang map
		QFile langFile( "./langs/" + selectedLang() );
		if ( langFile.open(QIODevice::ReadOnly) ) {
		   QTextStream in( &langFile );
		   while ( !in.atEnd() ) {
			  QString line = in.readLine();
			  if ( line.size() > 2 ) {
				  if ( line.left( 2 ) != "//" && line.contains("=") ) {
					  QStringList lineSp = line.split("=");
					  if ( lineSp.size() == 2) {
						  QString key = lineSp.at(0);
						  QString value = lineSp.at(1);
						  _lang[key] = value;
					  }
				  }
			  }

		   }
		   langFile.close();
		}
	}


signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LANG_H
