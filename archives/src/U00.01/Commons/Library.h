#ifndef LIBRARY_H
#define LIBRARY_H

#include <QObject>
#include <QSettings>
#include "../Commons/TrackList.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Library
		: public TrackList
{
	Q_OBJECT
private:
	bool _hide = false;

public:
	Library( const QString& name )
		: TrackList( name )
	{
//		update();
	}

public slots:
	void update() override
	{
		QElapsedTimer timer;
		timer.start();
		QSettings _setsFile(
					"./libraries/" + name() + ".slb",
					QSettings::IniFormat );
		setPath( _setsFile.value("path").toString() );
		setHide( _setsFile.value("hide").toInt() );

		QDir dir( path() );
		dir.setSorting( QDir::SortFlag::DirsFirst |
						QDir::SortFlag::Name |
						QDir::IgnoreCase |
						QDir::LocaleAware );
		dir.setFilter( QDir::Files );
		dir.setNameFilters( {"*.mp3", "*.ogg", ".wav", ".flac", ".m4a", ".wma"} );

		for ( QString filename : dir.entryList() )
			addTrack( path() + "/" + filename );

		qDebug() << "Time to get ALL Metadata :" << timer.elapsed() << "milliseconds";
		qDebug() << "---------------";
	}
	void setHide( bool hide )
	{
		_hide = hide;
	}

signals:

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARY_H
