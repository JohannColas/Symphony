#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QObject>
#include <QMediaPlaylist>

#include "../Commons/Track.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Playlist
		: public QObject
{
	Q_OBJECT
private:
	QList<Track*> _tracks;
	int _currentTrack = 0;

public:
	Playlist()
	{

	}
	Track* track( int index ) const
	{
		if ( isIndexValid( index ) )
			return _tracks.at( index );
		return new Track("");
	}
	int currentTrack() const
	{
		return _currentTrack;
	}
	int nbTracks() const
	{
		return _tracks.count();
	}
	bool isIndexValid( int index ) const
	{
		return (index > -1 && index < nbTracks());
	}
	QMediaPlaylist* mediaPlaylist()
	{
		QMediaPlaylist* playlist = new QMediaPlaylist;
		for ( Track* track : _tracks )
		{
			playlist->addMedia( QUrl::fromLocalFile( track->path() ) );
//			track->display();
		}
		return playlist;
	}

public slots:
	void addTrack( const QString& path )
	{
		_tracks.append( new Track(path) );
		emit trackAdded();
	}
	void add( Track* track )
	{
		_tracks.append( track );
		emit trackAdded();
	}
	void move( int from, int to )
	{
		if ( isIndexValid( from ) &&
			 isIndexValid( to ) )
		{
			_tracks.move( from, to );
			emit trackMoved();
		}
	}
	void remove( int index )
	{
		if ( isIndexValid( index ) )
		{
			_tracks.removeAt( index );
			emit trackRemoved();
		}
	}
	void clear()
	{
		_tracks.clear();
		emit cleared();
	}

signals:
	void trackAdded();
	void trackMoved();
	void trackRemoved();
	void cleared();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLIST_H
