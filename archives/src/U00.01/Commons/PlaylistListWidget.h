#ifndef LIBRARYLISTWIDGET_H
#define LIBRARYLISTWIDGET_H

#include <QListWidget>
#include "../Commons/Icons.h"
#include "../Commons/LibraryList.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistListWidget
		: public QListWidget
{
	Q_OBJECT
private:
	Icons* _icons;
	LibraryList* _list = new LibraryList;

public:
	PlaylistListWidget( QWidget* parent = nullptr )
		: QListWidget( parent )
	{
//		setMaximumWidth( 400 );
		setLibraryList( _list );
		updateList();
//		connect( this, &PlaylistWidget::currentRowChanged,
//				 _list, &LibraryList::setCurrentIndex );
		connect( _list, &LibraryList::currentLibraryChanged,
				 this, &PlaylistListWidget::currentLibraryChanged );
	}
	LibraryList* list()
	{
		return _list;
	}

public slots:
	void setLibraryList( LibraryList* list )
	{
		_list = list;
		clear();
		for ( Library* library : _list->list() )
		{
			addLibrary( library );
		}
//		setCurrentRow( _list->currentTracklistIndex() );
	}
	void addLibrary( const QString& name )
	{
		_list->addLibrary( name );
	}
	void addLibrary( Library* library )
	{
//		TrackItem *trackItem = new TrackItem( track, _icons );
		QListWidgetItem *item = new QListWidgetItem( library->name() );
		addItem( item );
//		setItemWidget( item, trackItem );
	}
	void setIcons( Icons* icons )
	{
		_icons = icons;
	}
	void updateList()
	{

	}

signals:
	void currentLibraryChanged( TrackList* library );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARYLISTWIDGET_H
