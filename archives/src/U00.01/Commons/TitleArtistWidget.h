#ifndef TITLEARTISTWIDGET_H
#define TITLEARTISTWIDGET_H

#include <QMediaObject>
#include <QVariant>
#include <QImage>
#include <QPixmap>
#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QListWidgetItem>
#include "../Commons/Track.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"

#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CoverItem
		: public QLabel
{
	Q_OBJECT

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TitleItem
		: public QLabel
{
	Q_OBJECT
public:
	TitleItem( QWidget* parent = nullptr )
		: QLabel( parent )
	{

	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistItem
		: public QLabel
{
	Q_OBJECT
public:
	ArtistItem( QWidget* parent = nullptr )
		: QLabel( parent )
	{

	}

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TitleArtistWidget
		: public QWidget
{
	Q_OBJECT
protected:
	QGridLayout* lay_main = new QGridLayout;
	CoverItem* lb_cover = new CoverItem;
	TitleItem* lb_title = new TitleItem;
	ArtistItem* lb_artist = new ArtistItem;
	Icons* _icons = 0;
	Track* _track;
public:
	TitleArtistWidget( QWidget* parent = nullptr )
		: QWidget( parent )
	{
		lay_main->setMargin( 0 );
		//	lay_main->setSpacing( 0 );
		lay_main->setVerticalSpacing( 0 );

		lay_main->addWidget( lb_cover, 0, 0, 2, 1 );
		lay_main->addWidget( lb_title, 0, 1, 1, 1 );
		lay_main->addWidget( lb_artist, 1, 1, 1, 1 );

		setLayout( lay_main );

		//		connect( this, &SongInfoWidget::trackChanged,
		//				 this, &SongInfoWidget::updateWidget );
	}
	TitleArtistWidget( Track* track, Icons* icons, QWidget* parent = nullptr )
		: QWidget( parent )
	{
		lay_main->setMargin( 0 );
		lay_main->setSpacing( 0 );
		lay_main->setVerticalSpacing( 0 );

		lay_main->addWidget( lb_cover, 0, 0, 2, 1 );
		lay_main->addWidget( lb_title, 0, 1, 1, 1 );
		lay_main->addWidget( lb_artist, 1, 1, 1, 1 );

		lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Preferred );

		setLayout( lay_main );

		//		connect( this, &SongInfoWidget::trackChanged,
		//				 this, &SongInfoWidget::updateWidget );
		_track = track;
		updateIcons( icons );
		updateWidget( track );
	}

public slots:
	void updateWidget( Track* track )
	{
		QPixmap pix = QPixmap();
		if ( pix.convertFromImage( track->cover() ) )
			lb_cover->setPixmap(
						pix.scaled( sizeHint().height(),
									sizeHint().height(),
									Qt::KeepAspectRatio,
									Qt::SmoothTransformation) );
		else
		{
			if ( _icons )
			{
				QIcon icon = _icons->get("track");
				lb_cover->setPixmap( icon.pixmap( sizeHint().height(),
												  sizeHint().height()) );
			}
		}

		lb_title->setText( track->title() );
		lb_artist->setText( track->artist() );
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}

signals:
	void trackChanged();
	void updated();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TITLEARTISTWIDGET_H
