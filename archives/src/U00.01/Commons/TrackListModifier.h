#ifndef TRACKLISTMODIFIER_H
#define TRACKLISTMODIFIER_H

#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>

class TrackListModifierButton
		: public QPushButton
{
	Q_OBJECT
public:
	TrackListModifierButton( QWidget* parent = 0 )
		: QPushButton( parent )
	{
		setFlat( true );
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Preferred );
	}
};


class TrackListModifier
		: public QWidget
{
	Q_OBJECT
private:
	QHBoxLayout* lay_main = new QHBoxLayout;
	QLabel* lb_title = new QLabel;
	TrackListModifierButton* pb_play = new TrackListModifierButton;
	TrackListModifierButton* pb_addToPlayer = new TrackListModifierButton;
	TrackListModifierButton* pb_add = new TrackListModifierButton;
	TrackListModifierButton* pb_update = new TrackListModifierButton;
	TrackListModifierButton* pb_delete = new TrackListModifierButton;

public:
	TrackListModifier( QWidget* parent = 0 )
		: QWidget( parent )
	{
		lay_main->setMargin( 0 );
		lay_main->setSpacing( 0 );
		setLayout( lay_main );

		lay_main->addWidget( lb_title );
		lay_main->addItem( new QSpacerItem( 0, 0, QSizePolicy::Expanding) );
		lay_main->addWidget( pb_play );
		lay_main->addWidget( pb_addToPlayer );
		lay_main->addWidget( pb_add );
		lay_main->addWidget( pb_update );
		lay_main->addWidget( pb_delete );

		connect( pb_update, &TrackListModifierButton::clicked,
				 this, &TrackListModifier::toUpdate );

	}

public slots:
	void updateIcons( Icons* icons )
	{
		pb_play->setIcon( icons->get("play") );
		pb_addToPlayer->setIcon( icons->get("addToPlayer") );
		pb_add->setIcon( icons->get("add") );
		pb_update->setIcon( icons->get("update") );
		pb_delete->setIcon( icons->get("delete") );
	}
	void updateLang( Lang* /*lang*/ )
	{
//		lb_title->setText( );
	}

signals:
	void toPlay();
	void toAddTackList();
	void toAdd();
	void toUpdate();
	void toDelete();
};


#endif // TRACKLISTMODIFIER_H
