#include "LibraryWidget.h"

LibraryWidget::LibraryWidget( QWidget* parent )
	: QFrame( parent )
{

	lay_main->addWidget( wid_libraryModifier, 0, 0 );
	lay_main->addWidget( wid_libraryList, 1, 0 );
	lay_main->addWidget( wid_library, 0, 1, 2, 1 );
	setLayout( lay_main );
//	wid_libraries->setMouseTracking(true);

	connect( wid_libraryList, &LibraryListWidget::currentLibraryChanged,
			 wid_library, &TrackListWidget2::setTrackList );

	connect( wid_library, &TrackListWidget2::currentTrackChanged,
			 this, &LibraryWidget::playTrack );
	connect( wid_library, &TrackListWidget2::sendTrackList,
			 this, &LibraryWidget::receiveTrackList );

	connect( wid_libraryModifier, &TrackListModifier::toUpdate,
			 this, &LibraryWidget::updateLibrary );
}
