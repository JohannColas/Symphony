#ifndef LIBRARYWIDGET_H
#define LIBRARYWIDGET_H

#include <QFrame>
#include <QGridLayout>
#include "../Commons/TrackListModifier.h"
#include "../Commons/LibraryListWidget.h"
#include "../Commons/TrackListWidget.h"

class LibraryWidget
		: public QFrame
{
	Q_OBJECT
private:
	TrackList* _tracklist = 0;
	QGridLayout* lay_main = new QGridLayout;
	TrackListModifier* wid_libraryModifier = new TrackListModifier;
	LibraryListWidget* wid_libraryList = new LibraryListWidget;
	TrackListWidget2* wid_library = new TrackListWidget2;

public:
	LibraryWidget( QWidget* parent = 0 );

public slots:
	void updateIcons( Icons* icons )
	{
		wid_libraryModifier->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
//		lb_title->setText( );
	}
	void setTrackList( TrackList* tracklist = 0 )
	{
		_tracklist = tracklist;
//		wid_library->setTrackList( _tracklist );
	}
	void updateLibrary()
	{
		wid_libraryList->list()->list().at(0)->update();
		wid_library->setTrackList( wid_libraryList->list()->list().at(0) );
	}

signals:
	void playTrack( Track* track );
	void receiveTrackList( TrackList* tracklist );
};

#endif // LIBRARYWIDGET_H
