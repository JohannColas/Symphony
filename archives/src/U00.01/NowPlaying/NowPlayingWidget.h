#ifndef NOWPLAYINGFRAME_H
#define NOWPLAYINGFRAME_H

#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QFrame>
#include <QGridLayout>
#include <QListWidget>

#include "../Commons/TrackInfoWidget.h"
#include "../Commons/TrackListWidget.h"

#include "../Commons/Playlist.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class NowPlayingWidget
		: public QFrame
{
	Q_OBJECT
private:
	QMediaPlayer* _player = 0;
	QGridLayout* lay_main = new QGridLayout;
	TrackInfoWidget* wid_trackInfo = new TrackInfoWidget;
	TrackListWidget* wid_trackList = new TrackListWidget;
	Icons* _icons = 0;

public:
	NowPlayingWidget( QWidget* parent = nullptr );


public slots:
	void setPlayer( QMediaPlayer* player );
	void setPlaylist( Playlist* playlist );
	void setTrackList( TrackList* list );
	void updateList();
	void updateIcons( Icons* icons );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // NOWPLAYINGFRAME_H
