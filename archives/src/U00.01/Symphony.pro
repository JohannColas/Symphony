QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Commons/FrameLess.cpp \
    LibraryWidget/LibraryWidget.cpp \
    NowPlaying/NowPlayingWidget.cpp \
    Player/PlayerWidget.cpp \
    Player/SongInfoWidget.cpp \
    Sidebar/SidebarWidget.cpp \
    main.cpp \
    Symphony.cpp

HEADERS += \
    Commons/FrameLess.h \
    Commons/Icons.h \
    Commons/Lang.h \
    Commons/Library.h \
    Commons/LibraryList.h \
    Commons/LibraryListWidget.h \
    Commons/Metadata.h \
    Commons/Playlist.h \
    Commons/PlaylistListWidget.h \
    Commons/PlaylistsWidget.h \
    Commons/Theme.h \
    Commons/TitleArtistWidget.h \
    Commons/Track.h \
    Commons/TrackInfoWidget.h \
    Commons/TrackList.h \
    Commons/TrackListModifier.h \
    Commons/TrackListWidget.h \
    LibraryWidget/LibraryWidget.h \
    NowPlaying/NowPlayingWidget.h \
    Player/PlayerWidget.h \
    Player/SongInfoWidget.h \
    Sidebar/SidebarWidget.h \
    Symphony.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += \
    /usr/local/include/taglib
DEPENDPATH += /usr/local/include/taglib

LIBS += \
    -L/usr/local/lib \
    -ltag

INCLUDEPATH += \
    /usr/local/include
DEPENDPATH += /usr/local/include
LIBS += \
    -L/usr/local/lib \
    -lz

