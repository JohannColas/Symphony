#ifndef ALBUMSELECTOR_H
#define ALBUMSELECTOR_H

#include <QListWidget>
#include "../Commons/Icons.h"
#include "../Commons/TrackList.h"
#include "../Commons/TracklistList.h"
#include "../Commons/Sorting.h"

class AlbumSelector
		: public QListWidget
{
	Q_OBJECT
private:
	TracklistList* _libraries = 0 ;
	Icons* _icons;

public:
	AlbumSelector( QWidget *parent = 0 )
		: QListWidget( parent )
	{

	}

public slots:
	void setTracklistList( TracklistList* libraries = 0 )
	{
		_libraries = libraries;
		update();
	}
	void update()
	{
		if ( _libraries )
		{
			QStringList albums;
			for ( int it = 0; it < _libraries->count(); ++it )
			{
				QDomDocument document;
				QFile file( "./libraries/" + _libraries->tracklist(it)->name() + ".slb" );
				if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) ) {
					return;
				}
				else
				{
					if ( !document.setContent( &file ) ) {
						file.close();
						return;
					}
					file.close();
					QDomElement el_libray = document.documentElement();
					QDomElement el_artist = el_libray.firstChild().toElement();
					while ( !el_artist.isNull() )
					{
						QDomElement el_album = el_artist.firstChild().toElement();
						while ( !el_album.isNull() )
						{
							if ( el_album.attributeNode("name").value() == "" )
							{
								if ( !albums.contains( "UNKNOWN" ) )
									albums.append( "UNKNOWN" );
							}
							else
							{
								albums.append( el_album.attributeNode("name").value() );
							}
							el_album = el_album.nextSibling().toElement();
						}
						el_artist = el_artist.nextSibling().toElement();
					}
				}
			}
			Sorting::sortAlpha( albums );
			addItems( albums );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}

signals:
};

#endif // ALBUMSELECTOR_H
