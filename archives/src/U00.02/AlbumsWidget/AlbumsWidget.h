#ifndef ALBUMSWIDGET_H
#define ALBUMSWIDGET_H

#include "../Commons/Frame.h"
#include "../Commons/TracklistList.h"
#include "../AlbumsWidget/AlbumSelector.h"
#include "../Commons/TrackListWidget.h"

class AlbumsWidget
		: public Frame
{
	Q_OBJECT
private:
	TracklistList* _libraries = 0;
	AlbumSelector* sel_album = new AlbumSelector;
	TrackListWidget* wid_tracklist = new TrackListWidget;

public:
	AlbumsWidget( QWidget* parent = 0 );

public slots:
	void setTracklistList( TracklistList* libraries )
	{
		_libraries = libraries;
		sel_album->setTracklistList( _libraries );

//		connect( _libraries, &TracklistList::currentTrackListChanged,
//				 wid_tracklist, &TrackListWidget::setTrackList );
	}
	void updateIcons( Icons* /*icons*/ )
	{
//		sel_artist->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
	}

};

#endif // ALBUMSWIDGET_H
