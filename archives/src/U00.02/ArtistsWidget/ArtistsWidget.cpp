#include "ArtistsWidget.h"

ArtistsWidget::ArtistsWidget( QWidget* parent )
	: Frame( parent )
{
	hide();

	addWidget( sel_artist, 0, 0 );
	addWidget( wid_tracklist, 0, 1, 1, 1 );
}
