#ifndef FRAME_H
#define FRAME_H
#include <QFrame>
#include "../Commons/GridLayout.h"
#include "../Commons/TrackList.h"

class Frame
		: public QFrame {
	Q_OBJECT
private:
	GridLayout* lay_main = new GridLayout;

public:
	Frame( QWidget* parent = 0 )
		: QFrame( parent )
	{
		hide();
		setLayout( lay_main );
	}
	void addWidget( QWidget* widget,
					int row, int col,
					int rowSpan = 1, int colSpan = 1 )
	{
		lay_main->addWidget( widget, row, col, rowSpan, colSpan );
	}
	void addSpacer( int row, int col, bool isVertical = true, int rowSpan = 1, int colSpan = 1 )
	{
		QSpacerItem* spacer;
		if ( isVertical )
			spacer  = new QSpacerItem( 0, 0, QSizePolicy::Maximum, QSizePolicy::Expanding );
		else
			spacer  = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Maximum );
		lay_main->addItem( spacer, row, col, rowSpan, colSpan );
	}
	void connectTo( Frame* frame, bool isFrame = true )
	{
		connect( frame, &Frame::trackToPlay,
				 this, &Frame::trackToPlay );
		connect( frame, &Frame::trackToAdd,
				 this, &Frame::trackToAdd );
		if ( isFrame )
		{
			connect( frame, &Frame::tracklistToPlay,
					 this, &Frame::tracklistToPlay );
			connect( frame, &Frame::tracklistToAdd,
					 this, &Frame::tracklistToAdd );
		}
	}


signals:
	void trackToPlay( Track* track );
	void trackToAdd( Track* track );
	void tracklistToPlay( TrackList* tracklist);
	void tracklistToAdd( TrackList* tracklist);
};

#endif // FRAME_H
