#ifndef GRIDLAYOUT_H
#define GRIDLAYOUT_H

#include <QGridLayout>

class GridLayout
		: public QGridLayout
{
	Q_OBJECT
public:
	GridLayout( QWidget* parent = nullptr )
		: QGridLayout( parent )
	{
		setMargin( 0 );
		setSpacing( 0 );
	}
};

#endif // GRIDLAYOUT_H
