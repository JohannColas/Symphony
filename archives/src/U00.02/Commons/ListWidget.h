#ifndef LISTWIDGET_H
#define LISTWIDGET_H

#include "../Commons/Frame.h"
#include <QListWidget>
#include "../Commons/ListWidgetItem.h"
#include <QScrollBar>
#include <QWheelEvent>
#include "../Commons/Icons.h"
#include "../Commons/TrackList.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ListWidget
		: public Frame
{
	Q_OBJECT
private:
	Icons* _icons = 0;
	TrackList* _tracklist = 0;
	QList<ListWidgetItem*> _items;
	QListWidget* _list = new QListWidget;
	QScrollBar* _scroll = new QScrollBar;
	int _begin = 0;


public:
	ListWidget( QWidget* parent = nullptr )
		: Frame( parent )
	{
		setMinimumWidth( 250 );
		addWidget( _list, 0, 0 );
		addWidget( _scroll, 0, 1 );
		_scroll->setOrientation( Qt::Vertical );
		_scroll->hide();
		_list->setFrameShape( QFrame::NoFrame );
		_list->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
		_list->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
		_list->setTextElideMode( Qt::ElideRight );
		connect( _scroll, &QScrollBar::valueChanged,
				 this, &ListWidget::updateIndex );
		connect( _list, &QListWidget::itemDoubleClicked,
				 this, &ListWidget::sendTrackItem );
//		connect( _list, &QListWidget::currentRowChanged,
//				 this, &TrackListWidget2::currentRowChanged );
	}

public slots:
	void addItem( QWidget* widget = 0 )
	{
		QListWidgetItem *item = new QListWidgetItem;
		_list->addItem( item );
		_list->setItemWidget( item, widget );
		item->setSizeHint( {_list->sizeHint().width(), widget->height() } );
	}
	void sendTrack( int index )
	{
		emit currentTrackChanged( _tracklist->track( _begin+index ) );
	}
	void sendTrackItem( QListWidgetItem */*item*/ )
	{
		//emit currentTrackChanged( _tracklist->track( _begin+_list->currentRow() ) );
		_tracklist->setCurrentTrack( _begin+_list->currentRow()  );
		emit sendTrackList( _tracklist );
		emit currentTrackIndexChanged( _begin+_list->currentRow() );
		emit playTrack( _tracklist->track(_begin+_list->currentRow()) );
	}
	void currentRowChanged( int index )
	{
		_tracklist->setCurrentTrack( _begin+index  );
		emit sendTrackList( _tracklist );
		emit playTrack( _tracklist->track(_begin+_list->currentRow()) );
	}
	void updateIndex( int index )
	{
		if ( _tracklist )
		{
			_begin = index;
			_scroll->setValue( _begin );
			update();
		}
	}
	void setIndex( int index )
	{
		if ( _tracklist )
		{
			_list->setCurrentRow( index );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
	void setTrackList( TrackList* tracks )
	{
		_tracklist = tracks;
		_scroll->setValue( 0 );
		_scroll->setMaximum( _tracklist->nbTracks() );
		update();
		connect( tracks, &TrackList::trackAdded,
				 this, &ListWidget::update );
	}
	void update()
	{
		if ( _tracklist )
		{
			_list->blockSignals(true);
			_list->clear();
			int height = 0;
			int nbItems = 0;
			for ( int it = _begin; it < _tracklist->nbTracks(); ++it )
			{
				Metadata meta( _tracklist->list().at(it) );
//				TrackItem *trackItem = new TrackItem(&meta, _icons );
				addItem( );

//				height += trackItem->height();
				if ( height > _list->height() )
				{
					_list->takeItem( nbItems );
					break;
				}
				++nbItems;
			}
			_scroll->setMaximum( _tracklist->nbTracks()-nbItems );
			if ( _scroll->maximum() <= 0 )
				_scroll->hide();
			else
				_scroll->show();

			_list->blockSignals(false);
		}
	}

	void resizeEvent( QResizeEvent* /*event*/ ) override
	{
		if ( _tracklist )
			update();
	}
	void wheelEvent( QWheelEvent* event ) override
	{
		if ( event->delta() > 0 )
		{
			if ( _begin > 0 )
				updateIndex( _begin - 1 );
		}
		else
		{
			if ( _begin + 1 <= _scroll->maximum() )
				updateIndex( _begin + 1 );
		}
		event->accept();
	}

signals:
	void currentTrackChanged( Track* track );
	void playTrack( Track* track );
	void currentTrackIndexChanged( int index );
	void sendTrackList( TrackList* tracklist );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LISTWIDGET_H
