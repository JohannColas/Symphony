#ifndef LISTWIDGETITEM_H
#define LISTWIDGETITEM_H
#include "../Commons/Frame.h"
#include "../Commons/TrackList.h"
#include "../Commons/Metadata.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ListWidgetItem
		: public Frame
{
	Q_OBJECT
protected:
	GridLayout* lay_main = new GridLayout;
	Icons* _icons = 0;
	Track* _track = 0;
	Metadata* _meta = 0;
public:
	ListWidgetItem( QWidget* parent = 0 )
		: Frame( parent )
	{
//		addWidget( lb_cover, 0, 0, 2, 1 );
//		addWidget( lb_title, 0, 1, 1, 1 );
//		addWidget( lb_artist, 1, 1, 1, 1 );
	}
	ListWidgetItem( Metadata* meta,
					Icons* icons,
					QWidget* parent = 0 )
		: Frame( parent )
	{
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );

//		addWidget( lb_cover, 0, 0, 2, 1 );
//		addWidget( lb_title, 0, 1, 1, 1 );
//		addWidget( lb_artist, 1, 1, 1, 1 );

//		lb_cover->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );

		_meta = meta;
		updateIcons( icons );
		updateWidget( meta );
	}

public slots:
	void updateWidget( Metadata* /*meta*/ )
	{
//		QPixmap pix = QPixmap();
//		if ( pix.convertFromImage( meta->cover() ) )
//			lb_cover->setPixmap(
//						pix.scaled( sizeHint().height(),
//									sizeHint().height(),
//									Qt::KeepAspectRatio,
//									Qt::SmoothTransformation) );
//		else
//		{
//			if ( _icons )
//			{
//				QIcon icon = _icons->get("track");
//				lb_cover->setPixmap( icon.pixmap( sizeHint().height(),
//												  sizeHint().height()) );
//			}
//		}

//		lb_title->setText( meta->title() );
//		lb_artist->setText( meta->artist() );
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}

signals:
	void trackChanged();
	void updated();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LISTWIDGETITEM_H
