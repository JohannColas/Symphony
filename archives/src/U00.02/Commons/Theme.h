#ifndef THEME_H
#define THEME_H

#include <QApplication>
#include <QObject>
#include <QFile>
#include <QString>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Theme
		: public QObject
{
	Q_OBJECT
private:
	QString _theme = "Dark Theme";

public:
	QString getCurrentTheme() const {
		return _theme;
	}
	void setCurrentTheme( const QString& value ) {
		_theme = value;
		emit changed();
	}
	void apply() {
		if ( _theme == "Default" )
		{
//			qApp->setStyleSheet( "" );
		}
			else
			{
				QFile file( "themes/" + _theme + "/stylesheet.qss" );
				if ( file.open(QFile::ReadOnly) )
				{
					QString style = QLatin1String( file.readAll() );
					style.remove('\n').remove('\t');
					qApp->setStyleSheet( style );
				}
				file.close();
			}
	}

signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // THEME_H
