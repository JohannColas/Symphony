#ifndef TRACK_H
#define TRACK_H

#include <QObject>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QString>
#include <QImage>
#include <QTime>
#include <QEventLoop>
#include <QFile>
#include <QUrl>
#include <QElapsedTimer>
#include <QProcess>
#include <QFileInfo>
// Taglib
#include <mpegfile.h>
#include <attachedpictureframe.h>
#include <id3v2tag.h>
#include <commentsframe.h>
using namespace TagLib;
#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Track
		: public QObject
{
	Q_OBJECT
	public:
	enum Format
	{
		UNKNOWN,
		MP3,
		OGG,
		WAV,
		FLAC,
		M4A,
		WMA
	};
private:
	QString _path = "Unknown";
	QString _title = "Unknown";
	QString _artist = "Unknown";
	QString _album = "Unknown";
	QDate _date = QDate();
	QString _genre = "Unknown";
	QImage _cover;
	QTime _duration = QTime( 0, 0 );
	int _year = -1;
	int _number = -1;
	QString _albumArtists = "";
	Track::Format _format = Format::UNKNOWN;

public:
	virtual ~Track()
	{

	}
	Track( const QString& path )
	{
		_path = path;
		setType();
//		updateMetadata();
	}
	QString path() const
	{ return _path; }
	QString title() const
	{ return _title; }
	QString artist() const
	{ return _artist; }
	QString album() const
	{ return _album; }
	QDate date() const
	{ return _date; }
	QString genre() const
	{ return _genre; }
	QImage cover() const
	{ return _cover; }
	QTime duration() const
	{ return _duration; }
	int year() const
	{ return _year; }
	int number() const
	{ return _number; }
	QString albumArtists() const
	{ return _albumArtists; }
	Track::Format format() const
	{ return _format; }

public slots:
	void setTitle( QString title )
	{
		if ( title != "" )
			_title = title;
	}
	void setArtist( QString artist )
	{
		if ( artist != "" )
			_artist = artist;
	}
	void setAlbum( QString album )
	{
		if ( album != "" )
			_album = album;
	}
	void setAlbumArtists( QString albumartists )
	{
		if ( albumartists != "" )
			_albumArtists = albumartists;
	}
	void setDate(QDate date )
	{
		if ( date != QDate() )
			_date = date;
	}
	void setGenre( QString genre )
	{
		if ( _genre != "" )
			_genre = genre;
	}
	void setCover( QImage cover )
	{
		if ( cover != QImage() )
			_cover = cover;
	}
	void setDuration( QTime durat )
	{
		_duration = durat;
	}
	void setYear( int year )
	{
		if ( year != 0 )
			_year = year;
	}
	void setNumber( int number )
	{
		if ( number != 0 )
			_number = number;
	}
	void setType()
	{
		int pPos =  path().lastIndexOf('.');
		QString ext = path().right( path().length()-pPos-1 );
		if ( ext.toLower() == "mp3" )
			_format = Format::MP3;
		else if ( ext.toLower() == "ogg" )
			_format = Format::OGG;
		else if ( ext.toLower() == "wav" )
			_format = Format::WAV;
		else if ( ext.toLower() == "flac" )
			_format = Format::FLAC;
		else if ( ext.toLower() == "m4a" )
			_format = Format::M4A;
		else if ( ext.toLower() == "wma" )
			_format = Format::WMA;
	}
	void updateMetadata()
	{
		QElapsedTimer timer;
		timer.start();

		if ( _path != "" )
		{

			MPEG::File file( _path.toUtf8() );
			if ( file.isValid() )
			{
				ID3v2::Tag *tag = file.ID3v2Tag( true );
				if ( tag )
				{
					setAlbum( tag->album().toCString(true) );
					setTitle( tag->title().toCString(true) );
					setArtist( tag->artist().toCString(true) );
					setGenre( tag->genre().toCString(true) );
					setYear( tag->year() );
					setNumber( tag->track());
					QImage image;
					ID3v2::AttachedPictureFrame *frame = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(tag->frameList("APIC").front());
					if ( frame )
					{
						image.loadFromData(
									(const uchar *)frame->picture().data(),
									frame->picture().size());
						setCover( image );
					}

				}
//				else
//				  qDebug() << "file does not have a valid id3v2 tag" << endl;
			}
//			else
//			  qDebug() << "file \"" + _path + "\" invalid" << endl;

//			qDebug() << "Time to get Metadata with TagLib :" << timer.elapsed() << "milliseconds";

		}
	}
	void display( bool hasCover = false )
	{
		qDebug() << "-----------------";
//		qDebug() << "Path : " << path();
		qDebug() << "Title : " << title();
		qDebug() << "Artist : " << artist();
		qDebug() << "Album : " << album();
		qDebug() << "Genre : " << genre();
//		qDebug() << "Album Artists : " << albumArtists();
		qDebug() << "Year : " << year();
//		qDebug() << "Date : " << date();
//		qDebug() << "Duration : " << duration();
		qDebug() << "Number : " << number();
		if ( hasCover )
			qDebug() << "hasCover";
		qDebug() << "-----------------";
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACK_H
