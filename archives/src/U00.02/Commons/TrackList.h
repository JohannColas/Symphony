#ifndef TRACKLIST_H
#define TRACKLIST_H

#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QDirIterator>
#include <QSettings>
#include <QDomDocument>
#include "../Commons/Track.h"
#include "../Commons/Metadata.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackList
		: public QObject
{
	Q_OBJECT
public:
	enum Type
	{
		TRACKLIST,
		PLAYLIST,
		NOWPLAYLIST,
		LIBRARY
	};
private:
	QList<Track*> _list;
	QString _name = "";
	TrackList::Type _type = Type::TRACKLIST;
	QString _path = "";
	bool _hide = false;
	int _currentTrack = 0;

public:
	TrackList()
	{

	}
	TrackList( const QString& name, TrackList::Type type = TrackList::TRACKLIST )
		: _name( name ), _type( type )
	{
		update();
	}
	Track* track( int index ) const
	{
		if ( isIndexValid( index ) )
			return _list.at( index );
		return new Track("");
	}
	QList<Track*> list() const
	{ return _list; }
	QString name() const
	{ return _name; }
	TrackList::Type type() const
	{ return _type; }
	QString path() const
	{ return _path; }
	bool hide() const
	{ return _hide;	}
	int currentTrackIndex() const
	{ return _currentTrack;	}
	Track* currentTrack() const
	{
		if ( isIndexValid( _currentTrack ) )
			return _list.at( _currentTrack );
		return new Track("");
	}
	int nbTracks() const
	{
		return _list.size();
	}
	bool isIndexValid( int index ) const
	{
		return (index > -1 && index < nbTracks());
	}
	QMediaPlaylist* mediaPlaylist()
	{
		QMediaPlaylist* playlist = new QMediaPlaylist;
		for ( Track* track : _list )
		{
			playlist->addMedia( QUrl::fromLocalFile( track->path() ) );
		}
		return playlist;
	}

public slots:
	void setName( const QString& name )
	{ _name = name; }
	void setType( const TrackList::Type& type )
	{ _type = type; }
	void setPath( const QString& path )
	{ _path = path; }
	void setHide( bool hide )
	{ _hide = hide; }
	void setCurrentTrack( int index )
	{
		if ( isIndexValid( index ) )
			_currentTrack = index;
		emit currentTrackIndexChanged( _currentTrack );
		emit currentTrackChanged( _list.at( _currentTrack ) );
	}
	void addTrack( const QString& path )
	{
		_list.append( new Track(path) );
		emit trackAdded();
	}
	void append( Track* track )
	{
		_list.append( track );
	}
	void add( Track* track )
	{
//		_list.append( track );
		*this += track;
		emit trackAdded();
	}
	void add( TrackList* tracklist )
	{
//		QElapsedTimer timer;
//		timer.start();

		*this += tracklist;

//		QString time = QString::number( timer.elapsed() );
//		time.insert( time.length()-3, "s " );
//		time += "ms";
//		qDebug().noquote() << "Time to create MediaPlaylist : " << time;
		emit trackAdded();
	}
	void move( int from, int to )
	{
		if ( isIndexValid( from ) &&
			 isIndexValid( to ) )
		{
			_list.move( from, to );
			emit trackMoved();
		}
	}
	void remove( int index )
	{
		if ( isIndexValid( index ) )
		{
			_list.removeAt( index );
			emit trackRemoved();
		}
	}
	void clear()
	{
		_list.clear();
		emit cleared();
	}
	void update()
	{
		if ( type() == Type::NOWPLAYLIST ||
			 type() == Type::PLAYLIST )
		{
			QString path = "./playlists/" + name() + ".spl";
			QFile file( path );
			if ( file.open(QIODevice::ReadOnly) )
			{
				QTextStream in(&file);
				while ( !in.atEnd() )
				{
					QString line = in.readLine();
					addTrack( line );
				}
			}
			else
				qDebug() << "error Reading file :"
						 << QFileInfo(file).absoluteFilePath();
		}
		else if ( type() == Type::LIBRARY )
		{
			// Create a document to write XML
			QDomDocument libraryXML;
			// Open a file for reading
			QFile file( "./libraries/" + name() + ".slb" );
			if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) ) {
				return;
			}
			else
			{
				if ( !libraryXML.setContent( &file ) ) {
					file.close();
					return;
				}
				file.close();
				// Getting root element
				QDomElement el_libray = libraryXML.documentElement();
				setPath( el_libray.attribute("path") );
				setHide( (el_libray.attribute("hide") == "true") ? true : false );
				QDir dir( path() );
				dir.setSorting( QDir::SortFlag::DirsFirst |
								QDir::SortFlag::Name |
								QDir::IgnoreCase |
								QDir::LocaleAware );
				dir.setFilter( QDir::Files );
				dir.setNameFilters( {"*.mp3", "*.ogg", ".wav", ".flac", ".m4a", ".wma"} );

				for ( QString filename : dir.entryList() )
					addTrack( path() + "/" + filename );
			}
		}
	}
	void updateLibrary()
	{
		QElapsedTimer timer;
		timer.start();
		//Emplacement et nom du fichier
		QString sName = "./libraries/" + name() + ".slb";
		//Création du QDomDoc qui servira de tampon pr les données contenues dans le fichier Xml
		QDomDocument domDoc;
		//Noeud Racine Phonebook
		QDomElement el_library = domDoc.createElement("library");
		el_library.setAttribute( "path", path() );
		el_library.setAttribute( "hide", hide() ? "true" : "false" );
		domDoc.appendChild( el_library );
//		QList<QDomElement> el_artists;
//		QList<QDomElement> el_albums;
		// Parcours de la liste de track
		for( int it = 0 ; it < _list.size() ; ++it )
		{
			Metadata meta( _list.at(it) );
			bool artistExits = false;
			QDomElement el_artist = el_library.firstChildElement();
			while ( !el_artist.isNull() )
			{
				if ( el_artist.attribute("name") == meta.artist() )
				{
					artistExits = true;
					break;
				}
				el_artist = el_artist.nextSiblingElement();
			}
			if ( !artistExits )
			{
				el_artist = domDoc.createElement("artist");
				el_artist.setAttribute( "name", meta.artist() );
				el_library.appendChild( el_artist );
			}
			bool albumExits = false;
			QDomElement el_album = el_artist.firstChildElement();
			while ( !el_album.isNull() )
			{
				if ( el_album.attribute("name") == meta.album() )
				{
					albumExits = true;
					break;
				}
				el_album = el_album.nextSiblingElement();
			}
			if ( !albumExits )
			{
				el_album = domDoc.createElement("album");
				el_album.setAttribute( "name", meta.album() );
				el_artist.appendChild( el_album );
			}
			QDomElement el_track = domDoc.createElement("track");
			el_track.setAttribute( "name", meta.title() );
			el_track.setAttribute( "path", meta.path() );
			el_album.appendChild( el_track );
		}

		//Ecriture des parametres du fichier XMl ..
		QDomProcessingInstruction domInstructions = domDoc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");//encoding=\"ISO-8859-1\"
		//.. en début de fichier
		domDoc.insertBefore( domInstructions, el_library );

		//Creation du fichier XML avec comme parametres
		QFile fFichierXML(sName);
		//Ouverture du fichier
		if( !fFichierXML.open(QFile::WriteOnly) )
		{
			return;
		}
		//Création d'un objet QTextStream avec comme parametre de sortie le fichier Xml
		QTextStream tsOut( &fFichierXML );
		//Ecriture des données du QDomDoc dans le Text Stream avec le parametres d'encodage du fichier et d'indentation (4)
		domDoc.save( tsOut, 4, QDomDocument::EncodingFromTextStream );
		//Fermeture du fichier
		fFichierXML.close();
		QString time = QString::number( timer.elapsed() );
		time.insert( time.length()-3, "s " );
		time += "ms";
		qDebug() << "Time to write Libray : " << time;
	}

signals:
	void trackAdded();
	void trackMoved();
	void trackRemoved();
	void cleared();
	void currentTrackIndexChanged( int index );
	void currentTrackChanged( Track* track );

public:
	// Operators
	TrackList& operator+= ( const TrackList& tracklist )
	{
		for ( int it = 0; it < tracklist.nbTracks(); ++it )
			 this->append( tracklist.track(it) );
		return (*this);
	}
	TrackList& operator+= ( TrackList* tracklist )
	{
		for ( int it = 0; it < tracklist->nbTracks(); ++it )
			 this->append( tracklist->track(it) );
		return (*this);
	}
	TrackList& operator+= ( Track* track )
	{
		this->append( track );
		return (*this);
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKLIST_H
