#ifndef TRACKLISTWIDGET_H
#define TRACKLISTWIDGET_H
#include <QMediaObject>
#include <QVariant>
#include <QImage>
#include <QPixmap>
#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QListWidgetItem>
#include "../Commons/Frame.h"
#include "../Commons/GridLayout.h"
#include "../Commons/Track.h"
#include "../Commons/TrackList.h"
#include "../Commons/Metadata.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
#include <QResizeEvent>
#include <QListWidget>
#include <QScrollBar>

#include <QProcess>

#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackCoverItem
		: public QLabel
{
	Q_OBJECT

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackTitleItem
		: public QLabel
{
	Q_OBJECT
public:
	TrackTitleItem( QWidget* parent = nullptr )
		: QLabel( parent )
	{
		setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackArtistItem
		: public QLabel
{
	Q_OBJECT
public:
	TrackArtistItem( QWidget* parent = nullptr )
		: QLabel( parent )
	{
		setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackItem
		: public QWidget
{
	Q_OBJECT
protected:
	QGridLayout* lay_main = new QGridLayout;
	TrackCoverItem* lb_cover = new TrackCoverItem;
	TrackTitleItem* lb_title = new TrackTitleItem;
	TrackArtistItem* lb_artist = new TrackArtistItem;
	Icons* _icons = 0;
	Track* _track = 0;
	Metadata* _meta = 0;
public:
	TrackItem( QWidget* parent = nullptr )
		: QWidget( parent )
	{
		lay_main->setMargin( 0 );
		//	lay_main->setSpacing( 0 );
		lay_main->setVerticalSpacing( 0 );

		lay_main->addWidget( lb_cover, 0, 0, 2, 1 );
		lay_main->addWidget( lb_title, 0, 1, 1, 1 );
		lay_main->addWidget( lb_artist, 1, 1, 1, 1 );

		setLayout( lay_main );

		//		connect( this, &SongInfoWidget::trackChanged,
		//				 this, &SongInfoWidget::updateWidget );
	}
	TrackItem( Track* track, Icons* icons, QWidget* parent = nullptr )
		: QWidget( parent )
	{
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		lay_main->setMargin( 0 );
		lay_main->setSpacing( 0 );
		lay_main->setVerticalSpacing( 0 );

		lay_main->addWidget( lb_cover, 0, 0, 2, 1 );
		lay_main->addWidget( lb_title, 0, 1, 1, 1 );
		lay_main->addWidget( lb_artist, 1, 1, 1, 1 );

		lb_cover->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );

		setLayout( lay_main );

		//		connect( this, &SongInfoWidget::trackChanged,
		//				 this, &SongInfoWidget::updateWidget );
		_track = track;
		updateIcons( icons );
		updateWidget( track );
	}
	TrackItem( Metadata* meta, Icons* icons, QWidget* parent = nullptr )
		: QWidget( parent )
	{
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		lay_main->setMargin( 0 );
		lay_main->setSpacing( 0 );
		lay_main->setVerticalSpacing( 0 );

		lay_main->addWidget( lb_cover, 0, 0, 2, 1 );
		lay_main->addWidget( lb_title, 0, 1, 1, 1 );
		lay_main->addWidget( lb_artist, 1, 1, 1, 1 );

		lb_cover->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );

		setLayout( lay_main );

		//		connect( this, &SongInfoWidget::trackChanged,
		//				 this, &SongInfoWidget::updateWidget );
		_meta = meta;
		updateIcons( icons );
		updateWidget( meta );
	}

public slots:
	void updateWidget( Track* track )
	{
		QPixmap pix = QPixmap();
		if ( pix.convertFromImage( track->cover() ) )
			lb_cover->setPixmap(
						pix.scaled( sizeHint().height(),
									sizeHint().height(),
									Qt::KeepAspectRatio,
									Qt::SmoothTransformation) );
		else
		{
			if ( _icons )
			{
				QIcon icon = _icons->get("track");
				lb_cover->setPixmap( icon.pixmap( sizeHint().height(),
												  sizeHint().height()) );
			}
		}

		lb_title->setText( track->title() );
		lb_artist->setText( track->artist() );
	}
	void updateWidget( Metadata* meta )
	{
		QPixmap pix = QPixmap();
		if ( pix.convertFromImage( meta->cover() ) )
			lb_cover->setPixmap(
						pix.scaled( sizeHint().height(),
									sizeHint().height(),
									Qt::KeepAspectRatio,
									Qt::SmoothTransformation) );
		else
		{
			if ( _icons )
			{
				QIcon icon = _icons->get("track");
				lb_cover->setPixmap( icon.pixmap( sizeHint().height(),
												  sizeHint().height()) );
			}
		}

		lb_title->setText( meta->title() );
		lb_artist->setText( meta->artist() );
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}

signals:
	void trackChanged();
	void updated();
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackListWidget
		: public Frame
{
	Q_OBJECT
private:
	Icons* _icons = 0;
	TrackList* _tracklist = 0;
	QList<TrackItem*> _items;
	GridLayout* lay_main = new GridLayout;
	QListWidget* _list = new QListWidget;
	QScrollBar* _scroll = new QScrollBar;
	int _begin = 0;


public:
	TrackListWidget( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		setMinimumWidth( 250 );
//		setLayout( lay_main );
		addWidget( _list, 0, 0 );
		addWidget( _scroll, 0, 1 );
		_scroll->setOrientation( Qt::Vertical );
		_scroll->hide();
		_list->setFrameShape( QFrame::NoFrame );
		_list->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
		_list->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
		_list->setTextElideMode( Qt::ElideRight );
		connect( _scroll, &QScrollBar::valueChanged,
				 this, &TrackListWidget::updateIndex );
		connect( _list, &QListWidget::itemDoubleClicked,
				 this, &TrackListWidget::playTrack );
	}

public slots:
	void playTrack( QListWidgetItem */*item*/  )
	{
		emit trackToPlay( _tracklist->track(_begin+_list->currentRow()) );
		emit currentTrackIndexChanged( _begin+_list->currentRow() );
	}
	void currentRowChanged( int index )
	{
		_tracklist->setCurrentTrack( _begin+index  );
	}
	void updateIndex( int index )
	{
		if ( _tracklist )
		{
			_begin = index;
			_scroll->setValue( _begin );
			update();
		}
	}
	void setIndex( int index )
	{
		if ( _tracklist )
		{
			_list->setCurrentRow( index );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
	void setTrackList( TrackList* tracks )
	{
		_tracklist = tracks;
		_scroll->setValue( 0 );
		_scroll->setMaximum( _tracklist->nbTracks() );
		update();
		connect( tracks, &TrackList::trackAdded,
				 this, &TrackListWidget::update );
	}
	void update()
	{
		if ( _tracklist )
		{
			QElapsedTimer timer;
			timer.start();

			_list->blockSignals(true);
			_list->clear();
			int height = 0;
			int nbItems = 0;
			for ( int it = _begin; it < _tracklist->nbTracks(); ++it )
			{
				Metadata meta( _tracklist->list().at(it) );
				TrackItem *trackItem = new TrackItem(&meta, _icons );
				//			TrackItem *trackItem = new TrackItem(_tracks->list().at(it), _icons );
				QListWidgetItem *item = new QListWidgetItem;
				_list->addItem( item );
				_list->setItemWidget( item, trackItem );
				item->setSizeHint( {_list->sizeHint().width(), trackItem->height() } );

				height += trackItem->height();
				if ( height > _list->height() )
				{
					_list->takeItem( nbItems );
					break;
				}
				++nbItems;
			}
			_scroll->setMaximum( _tracklist->nbTracks()-nbItems );
			if ( _scroll->maximum() <= 0 )
				_scroll->hide();
			else
				_scroll->show();
			//		qDebug() << "----------";

			_list->blockSignals(false);
//			qDebug() << "Time to update TrackListWidget :" << timer.elapsed() << "milliseconds";
//			qDebug() << "---------------";
		}
	}

	void resizeEvent( QResizeEvent* /*event*/ ) override
	{
		if ( _tracklist )
			update();
	}
	void wheelEvent( QWheelEvent* event ) override
	{
		if ( event->delta() > 0 )
		{
			if ( _begin > 0 )
				updateIndex( _begin - 1 );
		}
		else
		{
			if ( _begin + 1 <= _scroll->maximum() )
				updateIndex( _begin + 1 );
		}
		event->accept();
	}

signals:
	void currentTrackChanged( Track* track );
//	void playTrack( Track* track );
	void currentTrackIndexChanged( int index );
	void sendTrackList( TrackList* tracklist );
	void trackToPlay( Track* track );
	void trackToAdd( Track* track );
	void tracklistToPlay( TrackList* tracklist);
	void tracklistToAdd( TrackList* tracklist);
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKLISTWIDGET_H
