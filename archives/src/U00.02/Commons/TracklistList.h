#ifndef TRACKLISTLIST_H
#define TRACKLISTLIST_H

#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QDirIterator>
#include <QSettings>
#include <QDomDocument>
#include "../Commons/TrackList.h"
#include "../Commons/Metadata.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TracklistList
		: public QObject
{
	Q_OBJECT
public:
	enum Type
	{
		TRACKLISTLIST,
		PLAYLISTS,
		LIBRARIES
	};
private:
	QList<TrackList*> _list;
	TracklistList::Type _type = Type::TRACKLISTLIST;
	int _currentIndex = -1;

public:
	TracklistList( TracklistList::Type type = TracklistList::TRACKLISTLIST )
		: _type( type )
	{
		update();
	}
	QList<TrackList*> list()
	{
		return _list;
	}
	TracklistList::Type type()
	{
		return _type;
	}
	TrackList* tracklist( int index ) const
	{
		if ( isIndexValid( index ) )
			return _list.at( index );
		return new TrackList("");
	}
	int currentIndex() const
	{
		return _currentIndex;
	}
	TrackList* currentTracklist() const
	{
		if ( isIndexValid( _currentIndex ) )
			return tracklist( currentIndex() );
		return 0;
	}
	TrackList* currentTrackIndex() const
	{
		if ( isIndexValid( _currentIndex ) )
			return _list.at( _currentIndex );
		return 0;
	}
	int count() const
	{
		return _list.size();
	}
	bool isIndexValid( int index ) const
	{
		return ( index > -1 && index < count() );
	}

public slots:
	void setCurrentIndex( int index )
	{
		if ( isIndexValid( index ) )
		{
			_currentIndex = index;
			emit currentIndexChanged( currentIndex() );
			emit currentTrackListChanged( currentTracklist() );
		}
	}
	void addTracklist( const QString& path )
	{
		TrackList* tracklist = 0;
		if ( type() == Type::PLAYLISTS )
		{
			tracklist = new TrackList( path,
									   TrackList::PLAYLIST );
		}
		else if ( type() == Type::LIBRARIES )
		{
			tracklist = new TrackList( path,
									   TrackList::LIBRARY );
		}
		if ( tracklist )
		{
			_list.append( tracklist );
			emit tracklistAdded();
		}
	}
	void add( TrackList* tracklist )
	{
		_list.append( tracklist );
		emit tracklistAdded();
	}
	void move( int from, int to )
	{
		if ( isIndexValid( from ) &&
			 isIndexValid( to ) )
		{
			_list.move( from, to );
			emit tracklistMoved();
		}
	}
	void remove( int index )
	{
		if ( isIndexValid( index ) )
		{
			_list.removeAt( index );
			emit tracklistRemoved();
		}
	}
	void clear()
	{
		_list.clear();
		emit cleared();
	}
	void update()
	{
		QString path = "";
		if ( type() == Type::PLAYLISTS )
		{
			path = "./playlists/#playlists.spls";
		}
		else if ( type() == Type::LIBRARIES )
		{
			path = "./libraries/#libraries.slbs";
		}
		if ( path != "" )
		{
			QFile file( path );
			if ( file.open(QIODevice::ReadOnly) )
			{
				QTextStream in(&file);
				while ( !in.atEnd() )
				{
					QString line = in.readLine();
					if ( line != "" )
						addTracklist( line );
				}
			}
			else
			{
				qDebug() << "error Reading file :"
						 << QFileInfo(file).absoluteFilePath();
			}
		}
	}

signals:
	void tracklistAdded();
	void tracklistMoved();
	void tracklistRemoved();
	void cleared();
	void currentIndexChanged( int index );
	void currentTrackListChanged( TrackList* tracklist );
	void currentTrackChanged( Track* track );

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKLISTLIST_H
