#ifndef LIBRARIESWIDGET_H
#define LIBRARIESWIDGET_H

#include <QFrame>
#include "../Commons/Frame.h"
#include "../Commons/GridLayout.h"
#include "../Commons/TrackListModifier.h"
#include "../LibrariesWidget/LibrarySelector.h"
#include "../Commons/TrackListWidget.h"
#include "../Commons/TracklistList.h"

class LibrariesWidget
		: public Frame
{
	Q_OBJECT
private:
	TrackList* _tracklist = 0;
	TracklistList* _tracklists = 0;
	GridLayout* lay_main = new GridLayout;
	TrackListModifier* wid_libraryModifier = new TrackListModifier;
	LibrarySelector* sel_library = new LibrarySelector;
	TrackListWidget* wid_library = new TrackListWidget;

public:
	LibrariesWidget( QWidget* parent = 0 );

public slots:
	void updateIcons( Icons* icons )
	{
		wid_libraryModifier->updateIcons( icons );
		wid_library->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
//		lb_title->setText( );
	}
	void setTrackList( TrackList* tracklist = 0 )
	{
		_tracklist = tracklist;
//		wid_library->setTrackList( _tracklist );
	}
	void setTracklistList( TracklistList* tracklists = 0 )
	{
		_tracklists = tracklists;
		sel_library->setTracklistList( _tracklists );

		connect( tracklists, &TracklistList::currentTrackListChanged,
				 wid_library, &TrackListWidget::setTrackList );

//		wid_library->setTrackList( _tracklists->tracklist(0) );
//		wid_library->setTrackList( _tracklist );
	}
	void playLibrary()
	{
		if ( _tracklists->currentTracklist() )
			emit tracklistToPlay( _tracklists->currentTracklist() );
	}
	void addLibrary()
	{
		if ( _tracklists->currentTracklist() )
			emit tracklistToAdd( _tracklists->currentTracklist() );
	}
	void updateLibrary()
	{
//		_tracklists->
	}
	void deleteLibrary()
	{
//		_tracklists->tracklist(0)->updateLibrary();
	}

signals:
//	void playTrack( Track* track );
//	void receiveTrackList( TrackList* tracklist );
};

#endif // LIBRARIESWIDGET_H
