#ifndef LIBRARYLISTWIDGET_H
#define LIBRARYLISTWIDGET_H

#include <QListWidget>
#include "../Commons/Icons.h"
#include "../Commons/TracklistList.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LibrarySelector
		: public QListWidget
{
	Q_OBJECT
private:
	Icons* _icons;
//	LibraryList* _list = new LibraryList;

public:
	LibrarySelector( QWidget* parent = nullptr )
		: QListWidget( parent )
	{
//		setMaximumWidth( 400 );
//		setLibraryList( _list );
		updateList();
//		connect( this, &PlaylistWidget::currentRowChanged,
//				 _list, &LibraryList::setCurrentIndex );
//		connect( _list, &LibraryList::currentLibraryChanged,
//				 this, &LibrarySelector::currentLibraryChanged );
	}
//	TracklistList* list()
//	{
//		return _list;
//	}

public slots:
	void setTracklistList( TracklistList* list )
	{
//		_list = list;
		connect( this, &LibrarySelector::currentRowChanged,
				 list, &TracklistList::setCurrentIndex );
		clear();
		for ( TrackList* library : list->list() )
		{
			addLibrary( library );
		}
//		setCurrentRow( _list->currentTracklistIndex() );
//		setCurrentRow( list->currentIndex() );
	}
	void addLibrary( const QString& /*name*/ )
	{
//		_list->addLibrary( name );
	}
	void addLibrary( TrackList* library )
	{
//		TrackItem *trackItem = new TrackItem( track, _icons );
		QListWidgetItem *item = new QListWidgetItem( library->name() );
		addItem( item );
//		setItemWidget( item, trackItem );
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
	void updateList()
	{

	}

signals:
	void currentLibraryChanged( TrackList* library );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARYLISTWIDGET_H
