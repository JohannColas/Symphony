#include "NowPlayingWidget.h"

NowPlayingWidget::NowPlayingWidget( QWidget* parent )
	: QFrame( parent )
{
	lay_main->addWidget( wid_trackInfo, 0, 0 );
	lay_main->addWidget( wid_trackList, 0, 1 );

	wid_trackList->setMaximumWidth( 400 );
	setLayout( lay_main );

	connect( wid_trackList, &TrackListWidget::currentTrackIndexChanged,
			 this, &NowPlayingWidget::currentTrackIndexChanged );
}

void NowPlayingWidget::setPlayer( QMediaPlayer* player )
{
	_player = player;
	if ( _player )
	{
		wid_trackInfo->setPlayer( player );
		connect( _player->playlist(), &QMediaPlaylist::loaded,
			 this, &NowPlayingWidget::updateList );
	}
	updateList();
}

void NowPlayingWidget::setPlaylist( TrackList* playlist )
{
	wid_trackList->setTrackList( playlist );
	wid_trackInfo->setTrack( playlist->track(0) );
	connect( playlist, &TrackList::currentTrackIndexChanged,
			 wid_trackList, &TrackListWidget::setIndex );
	connect( playlist, &TrackList::currentTrackChanged,
			 wid_trackInfo, &TrackInfoWidget::setTrack );
}

void NowPlayingWidget::setTrackList( TrackList* playlist )
{
	wid_trackList->setTrackList( playlist );
	wid_trackInfo->setTrack( playlist->track(0) );
	connect( playlist, &TrackList::currentTrackIndexChanged,
			 wid_trackList, &TrackListWidget::setIndex );
	connect( playlist, &TrackList::currentTrackChanged,
			 wid_trackInfo, &TrackInfoWidget::setTrack );
}

void NowPlayingWidget::updateList()
{
	wid_trackList->update();
}

void NowPlayingWidget::updateIcons( Icons* icons )
{
	_icons = icons;
	wid_trackInfo->updateIcons( _icons );
	wid_trackList->updateIcons( _icons );
}
