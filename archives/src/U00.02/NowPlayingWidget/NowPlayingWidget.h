#ifndef NOWPLAYINGFRAME_H
#define NOWPLAYINGFRAME_H

#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QFrame>
#include "../Commons/GridLayout.h"
#include <QListWidget>

#include "../Commons/TrackInfoWidget.h"
#include "../Commons/TrackListWidget.h"

#include "../Commons/Icons.h"
#include "../Commons/Lang.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class NowPlayingWidget
		: public QFrame
{
	Q_OBJECT
private:
	QMediaPlayer* _player = 0;
	GridLayout* lay_main = new GridLayout;
	TrackInfoWidget* wid_trackInfo = new TrackInfoWidget;
	TrackListWidget* wid_trackList = new TrackListWidget;
	Icons* _icons = 0;

public:
	NowPlayingWidget( QWidget* parent = nullptr );


public slots:
	void setPlayer( QMediaPlayer* player );
	void setPlaylist( TrackList* playlist );
	void setTrackList( TrackList* list );
	void updateList();
	void updateIcons( Icons* icons );

signals:
	void currentTrackIndexChanged( int index );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // NOWPLAYINGFRAME_H
