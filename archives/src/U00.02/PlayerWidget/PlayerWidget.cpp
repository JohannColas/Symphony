#include "PlayerWidget.h"

#include <QDebug>
#include <QApplication>
#include <QTime>
/**********************************************/
/**********************************************/
/* */
PlayerWidget::~PlayerWidget()
{

}
/**********************************************/
/**********************************************/
/* */
PlayerWidget::PlayerWidget( QWidget* parent )
	: QFrame( parent )
{
	wid_songInfo->setTrack( _player );
	connect( _player, &QMediaPlayer::positionChanged,
			 this, &PlayerWidget::setPosition );
	connect( _player, &QMediaPlayer::mediaChanged,
			 this, &PlayerWidget::changeMedia );
	connect( _player, &QMediaPlayer::mediaStatusChanged,
			 this, &PlayerWidget::onMediaStatusChanged );
	connect( _player, SIGNAL(metaDataChanged()),
			 wid_songInfo, SLOT(updateWidget()) );
	connect( pb_play, &QPushButton::clicked,
			 this, &PlayerWidget::playpause );
	connect( pb_stop, &QPushButton::clicked,
			 _player, &QMediaPlayer::stop );
	connect( pb_backward, &QPushButton::clicked,
			 this, &PlayerWidget::backward );
	connect( pb_forward, &QPushButton::clicked,
			 this, &PlayerWidget::forward );
	connect( sl_time, &QSlider::sliderMoved,
			 this, &PlayerWidget::setPlayerPos );
	_timer->setSingleShot( true );
	connect( _timer, &QTimer::timeout,
			 this, &PlayerWidget::updatePlayerPos );

	pb_backward->setFlat( true );
	pb_play->setFlat( true );
	pb_stop->setFlat( true );
	pb_forward->setFlat( true );
	sl_time->setOrientation( Qt::Horizontal );
	sl_time->setMinimum( 0 );
	sl_time->setMaximum( 1000 );
	QHBoxLayout* lay_main = new QHBoxLayout;
	lay_main->addWidget( pb_backward );
	lay_main->addWidget( pb_play );
	lay_main->addWidget( pb_stop );
	lay_main->addWidget( pb_forward );
	lay_main->addWidget( wid_songInfo );


	QHBoxLayout* hlay = new QHBoxLayout;
	hlay->setMargin(0);
	hlay->setSpacing(0);
	hlay->addItem( new QSpacerItem(0,0,QSizePolicy::Expanding) );
	hlay->addWidget( lb_time );
	QVBoxLayout* vlay = new QVBoxLayout;
	vlay->setMargin(0);
	vlay->setSpacing(0);
	vlay->addLayout( hlay );
	vlay->addItem( new QSpacerItem(0,0,QSizePolicy::Maximum,QSizePolicy::Expanding) );
	vlay->addWidget( lb_time );
	vlay->addWidget( sl_time );
	lay_main->addLayout( vlay );

	lay_main->addItem( new QSpacerItem( 0,
										0,
										QSizePolicy::Expanding )
					   );
	lay_main->addWidget( pb_close );
	connect( pb_close, &QPushButton::clicked,
			 qApp, &QApplication::closeAllWindows );
	setLayout( lay_main );

	setMouseTracking( true );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::playTrack( Track* track )
{
	_nowPlaylist->add( track );
	emit _nowPlaylist->trackAdded();
	_player->setPlaylist( _nowPlaylist->mediaPlaylist() );
	_player->playlist()->setCurrentIndex( _nowPlaylist->nbTracks() -1 );
	_player->play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::addTrack( Track* track )
{
	_nowPlaylist->add( track );
	emit _nowPlaylist->trackAdded();
	_player->setPlaylist( _nowPlaylist->mediaPlaylist() );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::playTracklist( TrackList* tracklist )
{
	_nowPlaylist->clear();
	_nowPlaylist->add( tracklist );
	_player->setPlaylist( _nowPlaylist->mediaPlaylist() );
	_player->playlist()->setCurrentIndex( 0 );
	_player->play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::addTracklist( TrackList* tracklist )
{
	_nowPlaylist->add( tracklist );
	_player->setPlaylist( _nowPlaylist->mediaPlaylist() );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::receiveTrackList( TrackList* tracklist )
{
	_player->setPlaylist( tracklist->mediaPlaylist() );
	_player->playlist()->setCurrentIndex( tracklist->currentTrackIndex() );
	_player->play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPlaylistIndex( int index )
{
	_player->playlist()->setCurrentIndex( index );
	_player->play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setTrackList( TrackList* playlist )
{
	_nowPlaylist = playlist;
	_player->setPlaylist( playlist->mediaPlaylist() );
	_player->playlist()->setCurrentIndex( 0 );
	connect( _player->playlist(), &QMediaPlaylist::currentIndexChanged,
			 playlist, &TrackList::setCurrentTrack );
	playlistChanged( playlist );

}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPosition( qint64 pos )
{
	if ( _player->duration() )
	{
		int perc = 1000*pos/_player->duration();
		//	qDebug() << pos << player->duration() << perc;
		sl_time->setValue( perc );
		QString curT = QTime( 0, 0, 0 ).addMSecs( pos ).toString();
		if( curT.left(2) == "00" )
			curT = curT.mid( 3 );
		QString durT = QTime( 0, 0, 0).addMSecs( _player->duration() ).toString();
		if( durT.left(2) == "00" )
			durT = durT.mid( 3 );
		lb_time->setText( curT + " / " + durT );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPlayerPos( int /*pos*/ )
{
	_timer->start(200);
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::updatePlayerPos()
{
	qint64 newpos = sl_time->value()*_player->duration()/1000;
	_player->setPosition( newpos );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::changeMedia()
{

	//	sl_time->setMaximum( player->duration() );
	sl_time->setValue( 0 );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::playpause()
{
	if ( _player->state() == QMediaPlayer::PlayingState )
		_player->pause();
	else
		_player->play();
	updatePlayPauseIcon();
}
void PlayerWidget::updatePlayPauseIcon()
{
	if ( _icons )
	{
		if ( _player->state() == QMediaPlayer::PlayingState )
			pb_play->setIcon( _icons->get("pause") );
		else
			pb_play->setIcon( _icons->get("play") );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::backward()
{
	if ( _player->playlist()->currentIndex() != 0 )
	{
		if ( sl_time->value() < 40 )
			_player->playlist()->previous();
		else
			_player->setPosition( 0 );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::forward()
{
	_player->playlist()->next();
	if ( _player->playlist()->currentIndex() == -1 )
	{
		_player->playlist()->setCurrentIndex( 0 );
		_player->stop();
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::onMediaStatusChanged()
{
	updatePlayPauseIcon();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::updateIcons( Icons* icons )
{
	_icons = icons;
	pb_backward->setIcon( icons->get("skip_previous") );
	updatePlayPauseIcon();
	pb_stop->setIcon( icons->get("stop") );
	pb_forward->setIcon( icons->get("skip_next") );
	wid_songInfo->updateIcons( _icons );
}
/**********************************************/
/**********************************************/
void PlayerWidget::mouseHoverEvent( QMouseEvent* /*event*/ )
{
	setCursor( Qt::ArrowCursor );
}

void PlayerWidget::connectTo( Frame* frame )
{
	connect( frame, &Frame::trackToPlay,
			 this, &PlayerWidget::playTrack );
	connect( frame, &Frame::trackToAdd,
			 this, &PlayerWidget::addTrack );
	connect( frame, &Frame::tracklistToPlay,
			 this, &PlayerWidget::playTracklist );
	connect( frame, &Frame::tracklistToAdd,
			 this, &PlayerWidget::addTracklist );

}
/**********************************************/
/**********************************************/
/* */
