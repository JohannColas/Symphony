#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H

#include <QElapsedTimer>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QFrame>
#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QProgressBar>
#include <QTimer>
#include <QMouseEvent>
#include "../Commons/Frame.h"
#include "../Commons/Track.h"
#include "../Commons/TrackList.h"
#include "../PlayerWidget/SongInfoWidget.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerButton
		: public QPushButton
{
	Q_OBJECT

public:
	PlayerButton( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerWidget
		: public QFrame
{
	Q_OBJECT
private:
	TrackList* _nowPlaylist = 0;
	QMediaPlayer* _player = new QMediaPlayer;
	QTimer* _timer = new QTimer(this);
	// Player Widgets
	PlayerButton* pb_play = new PlayerButton;
	PlayerButton* pb_stop = new PlayerButton;
	PlayerButton* pb_backward = new PlayerButton;
	PlayerButton* pb_forward = new PlayerButton;
	SongInfoWidget* wid_songInfo = new SongInfoWidget;
	QSlider* sl_time = new QSlider;
	QLabel* lb_time = new QLabel;
	// System Button
	QPushButton* pb_close = new QPushButton;
	Icons* _icons = 0;

public:
	~PlayerWidget();
	PlayerWidget( QWidget* parent = nullptr );

public slots:
	void playTrack( Track* track );
	void addTrack( Track* track );
	void playTracklist( TrackList* tracklist );
	void addTracklist( TrackList* tracklist );
	void receiveTrackList( TrackList* tracklist );
	void setPlaylistIndex( int index );
	void setTrackList( TrackList* playlist );
	void setPosition( qint64 pos );
	void setPlayerPos( int pos );
	void updatePlayerPos();
	void changeMedia();
	void playpause();
	void backward();
	void forward();
	void updateIcons( Icons* icons );
	void updatePlayPauseIcon();
	void mouseHoverEvent( QMouseEvent* event );
	void connectTo( Frame* frame );

	void onMediaStatusChanged();

signals:
	void playlistChanged( TrackList* list );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERWIDGET_H
