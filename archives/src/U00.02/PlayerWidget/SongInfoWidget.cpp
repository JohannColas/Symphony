#include "SongInfoWidget.h"
/**********************************************/
/**********************************************/
/* */
SongInfoWidget::SongInfoWidget( QWidget* parent )
	: QWidget( parent )
{
	lay_main->setMargin( 0 );
//	lay_main->setSpacing( 0 );
	lay_main->setVerticalSpacing( 0 );

	lay_main->addWidget( lb_cover, 0, 0, 2, 1 );
	lay_main->addWidget( lb_title, 0, 1, 1, 1 );
	lay_main->addWidget( lb_artist, 1, 1, 1, 1 );

	setLayout( lay_main );

	connect( this, &SongInfoWidget::trackChanged,
			 this, &SongInfoWidget::updateWidget );
}
/**********************************************/
/**********************************************/
