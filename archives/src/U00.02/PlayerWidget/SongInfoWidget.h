#ifndef SONGINFOWIDGET_H
#define SONGINFOWIDGET_H

#include <QMediaObject>
#include <QVariant>
#include <QImage>
#include <QPixmap>
#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include "../Commons/Track.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"

#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SongCoverLabel
		: public QLabel
{
	Q_OBJECT

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SongTitleLabel
		: public QLabel
{
	Q_OBJECT
public:
	SongTitleLabel( QWidget* parent = nullptr )
		: QLabel( parent )
	{

	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SongArtistLabel
		: public QLabel
{
	Q_OBJECT
public:
	SongArtistLabel( QWidget* parent = nullptr )
		: QLabel( parent )
	{

	}

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SongInfoWidget
		: public QWidget
{
	Q_OBJECT
protected:
	QMediaObject* _player;
	QGridLayout* lay_main = new QGridLayout;
	SongCoverLabel* lb_cover = new SongCoverLabel;
	SongTitleLabel* lb_title = new SongTitleLabel;
	SongArtistLabel* lb_artist = new SongArtistLabel;
	Icons* _icons = 0;
public:
	SongInfoWidget( QWidget* parent = nullptr );

public slots:
	void setTrack( QMediaObject* player )
	{
		_player = player;
		emit trackChanged();
	}
	void updateWidget()
	{
			if ( _player )
			{
				QImage cover = _player->metaData("CoverArtImage").value<QImage>();
				QPixmap pix = QPixmap();
				if ( pix.convertFromImage( cover ) )
					lb_cover->setPixmap( pix.scaled( lb_cover->width(), lb_cover->height(), Qt::KeepAspectRatio, Qt::SmoothTransformation) );
				else
				{
					if ( _icons )
					{
						QIcon icon = _icons->get("track");
						lb_cover->setPixmap( icon.pixmap(lb_cover->width(), lb_cover->height()) );
					}
				}

				lb_title->setText( _player->metaData("Title").toString() );
				lb_artist->setText( _player->metaData("ContributingArtist").toString() );
			}
		}
		void updateIcons( Icons* icons )
		{
			_icons = icons;
		}

signals:
	void trackChanged();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SONGINFOWIDGET_H
