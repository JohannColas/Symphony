#ifndef PLAYLISTSELECTOR_H
#define PLAYLISTSELECTOR_H

#include <QListWidget>
#include "../Commons/Icons.h"
#include "../PlaylistsWidget/PlaylistList.h"
#include "../Commons/TracklistList.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistSelector
		: public QListWidget
{
	Q_OBJECT
private:
	Icons* _icons;
	PlaylistList* _playlists = new PlaylistList;

public:
	PlaylistSelector( QWidget* parent = nullptr )
		: QListWidget( parent )
	{
//		setMaximumWidth( 400 );
		setPlaylists( _playlists );
		connect( this, &PlaylistSelector::currentRowChanged,
				 _playlists, &PlaylistList::setCurrentIndex );
		connect( _playlists, &PlaylistList::currentTracklistChanged,
				 this, &PlaylistSelector::currentTracklistChanged );
	}
	PlaylistList* list()
	{
		return _playlists;
	}

public slots:
	void addPlaylist( const QString& /*name*/ )
	{

	}
	void addPlaylist( TrackList* tracklist )
	{
		//		TrackItem *trackItem = new TrackItem( track, _icons );
				QListWidgetItem *item = new QListWidgetItem( tracklist->name() );
				addItem( item );
		//		setItemWidget( item, trackItem );
	}
	void setPlaylists( PlaylistList* playlists )
	{
		_playlists = playlists;
		clear();
		for ( TrackList* tracklist : playlists->list() )
		{
			addTracklist( tracklist );
		}
		setCurrentRow( playlists->currentTracklistIndex() );
	}
	void setTracklistList( TracklistList* list )
	{
//		_list = list;
		connect( this, &PlaylistSelector::currentRowChanged,
				 list, &TracklistList::setCurrentIndex );
		clear();
		for ( TrackList* tracklist : list->list() )
		{
			addPlaylist( tracklist );
		}
//		setCurrentRow( _list->currentTracklistIndex() );
//		setCurrentRow( list->currentIndex() );
	}
	void addTracklist( TrackList* tracklist )
	{
//		TrackItem *trackItem = new TrackItem( track, _icons );
		QListWidgetItem *item = new QListWidgetItem( tracklist->name() );
		addItem( item );
//		setItemWidget( item, trackItem );
	}
	void setIcons( Icons* icons )
	{
		_icons = icons;
	}
	void updateList()
	{

	}

signals:
	void currentTracklistChanged( TrackList* tracklist );
};
/**********************************************/
/**********************************************/
/**********************************************/

#endif // PLAYLISTSELECTOR_H
