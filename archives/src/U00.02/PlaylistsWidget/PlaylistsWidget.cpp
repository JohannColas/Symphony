#include "PlaylistsWidget.h"

PlaylistsWidget::PlaylistsWidget( QWidget* parent )
	: Frame( parent )
{
	hide();

	addWidget( wid_playlistModifier, 0, 0 );
	addWidget( sel_playlist, 1, 0 );
	addWidget( wid_playlist, 0, 1, 2, 1 );

	connect( sel_playlist, &PlaylistSelector::currentTracklistChanged,
			 wid_playlist, &TrackListWidget::setTrackList );

	//	connect( wid_libraryModifier, &TrackListModifier::toNew,
	//			 this, &LibrariesWidget::addNew );
		connect( wid_playlistModifier, &TrackListModifier::toPlay,
				 this, &PlaylistsWidget::playPlaylist );
		connect( wid_playlistModifier, &TrackListModifier::toAdd,
				 this, &PlaylistsWidget::addPlaylist );
		connect( wid_playlistModifier, &TrackListModifier::toDelete,
				 this, &PlaylistsWidget::deletePlaylist );

}

