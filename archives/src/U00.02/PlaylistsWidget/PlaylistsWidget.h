#ifndef PLAYLISTSWIDGET_H
#define PLAYLISTSWIDGET_H

#include "../Commons/Frame.h"
#include "../Commons/GridLayout.h"
#include "../Commons/TracklistList.h"
#include "../Commons/TrackListModifier.h"
#include "../PlaylistsWidget/PlaylistSelector.h"
#include "../Commons/TrackListWidget.h"

class PlaylistsWidget
		: public Frame
{
	Q_OBJECT
private:
	TracklistList* _tracklists = 0;
	TrackListModifier* wid_playlistModifier = new TrackListModifier;
	PlaylistSelector* sel_playlist = new PlaylistSelector;
	TrackListWidget* wid_playlist = new TrackListWidget;

public:
	PlaylistsWidget( QWidget* parent = 0 );

public slots:
	void updateIcons( Icons* icons )
	{
		wid_playlistModifier->updateIcons( icons );
		wid_playlist->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
//		lb_title->setText( );
	}
	void setTracklistList( TracklistList* tracklists = 0 )
	{
		_tracklists = tracklists;
		sel_playlist->setTracklistList( tracklists );

		connect( tracklists, &TracklistList::currentTrackListChanged,
				 wid_playlist, &TrackListWidget::setTrackList );
	}
	void playPlaylist()
	{
		if ( _tracklists->currentTracklist() )
			emit tracklistToPlay( _tracklists->currentTracklist() );
	}
	void addPlaylist()
	{
		if ( _tracklists->currentTracklist() )
			emit tracklistToAdd( _tracklists->currentTracklist() );
	}
	void deletePlaylist()
	{
//		_tracklists->tracklist(0)->updateLibrary();
	}

signals:
};

#endif // PLAYLISTSWIDGET_H
