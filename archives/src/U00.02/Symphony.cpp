#include "Symphony.h"


/**********************************************/
/**********************************************/
/* */
Symphony::~Symphony()
{
	delete _theme;
	delete _icons;
	delete _lang;
}
/**********************************************/
/**********************************************/
/* */
Symphony::Symphony( QWidget *parent )
	: QFrame( parent )
{
	// Window Geometry
	setMinimumSize( 800, 500 );
	wid_player->setFixedHeight( 50 );

	// Adding TrackLists to Widgets
	wid_player->setTrackList( _nowPlaylist );
	wid_nowPlaying->setTrackList( _nowPlaylist );
	wid_artists->setTracklistList( _libraries );
	wid_albums->setTracklistList( _libraries );
//	wid_genres->setTracklistList( _libraries );
//	wid_tracks->setTracklistList( _libraries );
	wid_playlists->setTracklistList( _playlists );
	wid_libraries->setTracklistList( _libraries );
//	wid_radios->setTracklistList( _radios );
//	wid_podcasts->setTracklistList( _podcasts );

	// Adding Widgets to SidebarWidget
	wid_sidebar->setWidget( 0, wid_nowPlaying );
	wid_sidebar->setWidget( 1, wid_artists );
	wid_sidebar->setWidget( 2, wid_albums );
	wid_sidebar->setWidget( 3, wid_genres );
	wid_sidebar->setWidget( 4, wid_tracks );
	wid_sidebar->setWidget( 5, wid_playlists );
	wid_sidebar->setWidget( 6, wid_libraries );
	wid_sidebar->setWidget( 7, wid_radios );
	wid_sidebar->setWidget( 8, wid_podcasts );
	wid_sidebar->changeCurrentWidget( 0 );

	// Connections between widgets
	connect( wid_nowPlaying, &NowPlayingWidget::currentTrackIndexChanged,
			 wid_player, &PlayerWidget::setPlaylistIndex );
	connect( wid_player, &PlayerWidget::playlistChanged,
			 wid_nowPlaying, &NowPlayingWidget::setPlaylist );
	wid_player->connectTo( wid_playlists );
	wid_player->connectTo( wid_libraries );


	QGridLayout* lay_main = new QGridLayout;
	lay_main->setMargin( 0 );
	lay_main->setSpacing( 0 );
	lay_main->addWidget( wid_player, 0, 0, 1, 2 );
	lay_main->addWidget( wid_sidebar, 1, 0, 1, 2 );

	setLayout( lay_main );
	// -----------------------
	// -----------------------
	// Themes
	_theme->apply();
	updateIcons();
	updateLang();
	// -----------------------
	// -----------------------
	setMouseTracking( true );
	installEventFilter(this);
	// Update Sidebar size
	wid_sidebar->setTextVisibility( true );

}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateSettings()
{
//	_settings->update();
//	wid_player->updateIcons( _settings );
//	wid_sidebar->updateIcons( _settings );
//	wid_nowPlaying->updateIcons( _settings );
//	wid_libraries->updateIcons( _settings );
}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateIcons()
{
	_icons->update();
	setWindowIcon( _icons->get( "Symphony" ) );
	wid_player->updateIcons( _icons );
	wid_sidebar->updateIcons( _icons );
	wid_nowPlaying->updateIcons( _icons );
	wid_playlists->updateIcons( _icons );
	wid_libraries->updateIcons( _icons );
}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateLang()
{
	_lang->update();

}
/**********************************************/
/**********************************************/
/* */
void Symphony::mousePressEvent( QMouseEvent *event )
{
	 if ( event->button() == Qt::LeftButton )
	 {
		 _lastPos = event->globalPos();
		 isPressedWidget = true;
	 }
	 event->accept();
}
/**********************************************/
/**********************************************/
/* */
void Symphony::mouseDoubleClickEvent(QMouseEvent* event)
{
	if ( event->button() == Qt::LeftButton )
	{
		if ( isMaximized() )
			showNormal();
		else
			showMaximized();
		event->accept();
	}
}
/**********************************************/
/**********************************************/
/* */
void Symphony::mouseReleaseEvent( QMouseEvent *event )
{
	 if ( event->button() == Qt::LeftButton )
	 {
		 isPressedWidget = false;
	 }

	 event->accept();
}
/**********************************************/
/**********************************************/
/* */
void Symphony::mouseMoveEvent( QMouseEvent *event )
{
	if ( isPressedWidget )
	{
		this->move( this->x() + (event->globalX() - _lastPos.x()),
				   this->y() + (event->globalY() - _lastPos.y()) );
		_lastPos = event->globalPos();
	}
	event->accept();
}
/**********************************************/
/**********************************************/
bool Symphony::eventFilter( QObject* /*object*/, QEvent* event )
{
	if (event->type() == QEvent::MouseMove )
	{
	}
	else if (event->type() == QEvent::None )
	{
//		setCursor( Qt::ArrowCursor );
	}
	return false;
}

void Symphony::closeEvent( QCloseEvent* event )
{

	QFrame::closeEvent( event );
}
