#ifndef WIDGET_H
#define WIDGET_H

#include <QFrame>
#include "../Commons/Theme.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
#include "../Commons/TrackListWidget.h"
#include "../Commons/TrackList.h"
#include "../Commons/TracklistList.h"
#include "../Commons/Lang.h"
#include <QListWidget>

#include <QGridLayout>
#include <QSplitter>
#include <QRubberBand>
#include <QMouseEvent>
#include <QCursor>

#include "../PlayerWidget/PlayerWidget.h"
#include "../SidebarWidget/SidebarWidget.h"
#include "../NowPlayingWidget/NowPlayingWidget.h"
#include "../ArtistsWidget/ArtistsWidget.h"
#include "../AlbumsWidget/AlbumsWidget.h"
#include "../GenresWidget/GenresWidget.h"
#include "../TracksWidget/TracksWidget.h"
#include "../PlaylistsWidget/PlaylistsWidget.h"
#include "../LibrariesWidget/LibrariesWidget.h"
#include "../RadiosWidget/RadiosWidget.h"
#include "../PodcastsWidget/PodcastsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Symphony
		: public QFrame
{
	Q_OBJECT
private:
	// Window Parameters
	int invisibleBorderSize = 2;
	bool isPressedWidget;
	QPoint _lastPos;
//	Playlist* _playlist = new Playlist;
	// Initialise the TrackList
	TrackList* _nowPlaylist = new TrackList( "#NowPlaylist", TrackList::NOWPLAYLIST );
	TracklistList* _playlists = new TracklistList( TracklistList::PLAYLISTS );
	TracklistList* _libraries = new TracklistList( TracklistList::LIBRARIES );
//	QMediaPlayer* _player = new QMediaPlayer;
	// Initialise the Widgets
	NowPlayingWidget* wid_nowPlaying = new NowPlayingWidget;
	ArtistsWidget* wid_artists = new ArtistsWidget;
	AlbumsWidget* wid_albums = new AlbumsWidget;
	GenresWidget* wid_genres = new GenresWidget;
	TracksWidget* wid_tracks = new TracksWidget;
	PlaylistsWidget* wid_playlists = new PlaylistsWidget;
	LibrariesWidget* wid_libraries = new LibrariesWidget;
	RadiosWidget* wid_radios = new RadiosWidget;
	PodcastsWidget* wid_podcasts = new PodcastsWidget;
protected:
	Theme* _theme = new Theme;
	Icons* _icons = new Icons;
	Lang* _lang = new Lang;
	PlayerWidget* wid_player = new PlayerWidget;
	SidebarWidget* wid_sidebar = new SidebarWidget;

public:
	~Symphony();
	Symphony( QWidget *parent = nullptr );


public slots:
	void updateSettings();
	void updateIcons();
	void updateLang();
	void mousePressEvent( QMouseEvent* event );
	void mouseDoubleClickEvent( QMouseEvent* event );
	void mouseReleaseEvent( QMouseEvent* event );
	void mouseMoveEvent( QMouseEvent* event );
	bool eventFilter(QObject* object, QEvent* event);
	void closeEvent( QCloseEvent* event );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WIDGET_H
