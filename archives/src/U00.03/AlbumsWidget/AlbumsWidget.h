#ifndef ALBUMSWIDGET_H
#define ALBUMSWIDGET_H
/**********************************************/
#include "../Commons/Frame.h"
#include "../Commons/TracklistList.h"
#include "../AlbumsWidget/AlbumSelector.h"
#include "../TracksWidget/TrackSelector.h"
#include "../Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumsWidget
		: public Frame
{
	Q_OBJECT
private:
	TracklistList* _libraries = 0;
	AlbumSelector* sel_album = new AlbumSelector;
	TrackSelector* wid_tracklist = new TrackSelector;

public:
	AlbumsWidget( QWidget* parent = 0 );

public slots:
	void setTracklistList( TracklistList* libraries )
	{
		_libraries = libraries;
		sel_album->setTracklistList( _libraries );

//		connect( _libraries, &TracklistList::currentTrackListChanged,
//				 wid_tracklist, &TrackListWidget::setTrackList );
	}
	void updateIcons( Icons* /*icons*/ )
	{
//		sel_artist->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMSWIDGET_H
