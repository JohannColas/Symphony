#ifndef ARTISTSELECTOR_H
#define ARTISTSELECTOR_H
/**********************************************/
#include <QListWidget>
#include "../Commons/Icons.h"
#include "../Commons/TrackList.h"
#include "../Commons/TracklistList.h"
#include "../Commons/Sorting.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistSelector
		: public QListWidget
{
	Q_OBJECT
private:
	TracklistList* _libraries = 0;
	Icons* _icons;

public:
	ArtistSelector( QWidget *parent = 0 )
		: QListWidget( parent )
	{

	}

public slots:
	void setTracklistList( TracklistList* libraries = 0 )
	{
		_libraries = libraries;
		update();
	}
	void update()
	{
		if ( _libraries )
		{
			QStringList artists;
			for ( int it = 0; it < _libraries->count(); ++it )
			{
				QDomDocument document;
				QFile file( "./libraries/" + _libraries->tracklist(it)->name() + ".slb" );
				if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) ) {
					return;
				}
				else
				{
					if ( !document.setContent( &file ) ) {
						file.close();
						return;
					}
					file.close();
					QDomElement el_libray = document.documentElement();
					QDomElement el_artist = el_libray.firstChild().toElement();
					while ( !el_artist.isNull() )
					{
						if ( el_artist.attributeNode("name").value() == "" )
						{
							if ( !artists.contains( "UNKNOWN" ) )
								artists.append( "UNKNOWN" );
						}
						else
						{
							artists.append( el_artist.attributeNode("name").value() );
						}
						el_artist = el_artist.nextSibling().toElement();
					}
				}
			}
			Sorting::sortAlpha( artists );
			addItems( artists );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}

signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTSELECTOR_H
