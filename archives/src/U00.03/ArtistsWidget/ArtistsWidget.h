#ifndef ARTISTSWIDGET_H
#define ARTISTSWIDGET_H
/**********************************************/
#include "../Commons/Frame.h"
#include "../Commons/TracklistList.h"
#include "../ArtistsWidget/ArtistSelector.h"
#include "../TracksWidget/TrackSelector.h"
#include "../Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistsWidget
		: public Frame
{
	Q_OBJECT
private:
	TracklistList* _libraries = 0;
	ArtistSelector* sel_artist = new ArtistSelector;
	TrackSelector* wid_tracklist = new TrackSelector;

public:
	ArtistsWidget( QWidget* parent = 0 );

public slots:
	void setTracklistList( TracklistList* libraries = 0 )
	{
		_libraries = libraries;
		sel_artist->setTracklistList( _libraries );

//		connect( _libraries, &TracklistList::currentTrackListChanged,
//				 wid_tracklist, &TrackListWidget::setTrackList );
	}
	void updateIcons( Icons* /*icons*/ )
	{
//		sel_artist->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTSWIDGET_H
