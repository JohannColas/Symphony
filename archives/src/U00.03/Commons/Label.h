#ifndef LABEL_H
#define LABEL_H
/**********************************************/
#include <QLabel>
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Label
		: public QLabel
{
	Q_OBJECT
private:
	QString _text = "";

public:
	Label( const QString& text, QWidget* parent = 0 )
		: QLabel( text, parent )
	{
		_text = text;
		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}
	Label( QWidget* parent = 0 )
		: QLabel( parent )
	{
		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}
	void setText( const QString& text )
	{
		_text = text;
		QLabel::setText( _text );
	}
	void update()
	{
		if ( sizeHint().width() > width() )
		{
			QFontMetrics metrix( font() );
			int w = width() - 30;
			QString clippedText = metrix.elidedText( _text, Qt::ElideRight, w );
			QLabel::setText( clippedText );
		}
	}
	void paintEvent( QPaintEvent* event ) override
	{
		update();
//		qDebug() << text() << width() << sizeHint().width();
		QLabel::paintEvent( event );
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LABEL_H
