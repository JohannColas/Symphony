#ifndef LISTITEM_H
#define LISTITEM_H
/**********************************************/
#include <QPushButton>
#include <QMouseEvent>
#include "../Commons/Layouts.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ListItem
		: public QPushButton
{
	Q_OBJECT
protected:
	GridLayout* lay_main = new GridLayout;

public:
	ListItem( QWidget* parent = 0 )
		: QPushButton( parent )
	{
		show();
		setLayout( lay_main );
		setFlat( true );
		setCheckable( true );
		setSizePolicy( QSizePolicy::Preferred,
					   QSizePolicy::Maximum );
	}
	void addWidget( QWidget* widget,
					int row, int col,
					int rowSpan = 1, int colSpan = 1 )
	{
		lay_main->addWidget( widget, row, col, rowSpan, colSpan );
	}
	void mousePressEvent( QMouseEvent* event ) override
	{
		setChecked( !isChecked() );
		event->ignore();
	}
	void enterEvent( QEvent* event ) override
	{
		event->ignore();
	}
	void leaveEvent( QEvent* event ) override
	{
		event->ignore();
	}

signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LISTITEM_H
