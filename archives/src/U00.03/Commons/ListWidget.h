#ifndef LISTWIDGET_H
#define LISTWIDGET_H
/**********************************************/
#include "../Commons/Frame.h"
#include <QListWidget>
#include "../Commons/ListItem.h"
#include <QScrollBar>
#include <QWheelEvent>
#include "../Commons/Icons.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ListWidget
		: public Frame
{
	Q_OBJECT
protected:
	Icons* _icons = 0;
	Frame* _container = new Frame;
	int _nbItems = 0;
	QList<QWidget*> _sections;
	QList<ListItem*> _items;
//	QListWidget* _list = new QListWidget;
	QScrollBar* _scroll = new QScrollBar(this);
	int _begin = 0;
	int _index = -1;


public:
	ListWidget( QWidget* parent = 0 )
		: Frame( parent )
	{
		// Set Widget
		show();
		setMinimumWidth( 250 );
		installEventFilter( this );

		// Settings QListWidget
//		_list->setFrameShape( QFrame::NoFrame );
//		_list->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
//		_list->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
//		_list->setSelectionMode( QAbstractItemView::ExtendedSelection );
//		_list->setTextElideMode( Qt::ElideRight );
//		_container->show();

		// Settings ScrollBar
		_scroll->setOrientation( Qt::Vertical );
		_scroll->hide();

		// Adding the widgets
//		addWidget( _list, 0, 0 );
//		addWidget( _container, 0, 0 );
//		addWidget( _scroll, 0, 1 );
		// Connections
		connect( _scroll, &QScrollBar::valueChanged,
				 this, &ListWidget::onScrollbarChanged );
//		connect( _list, &QListWidget::currentRowChanged,
//				 this, &TrackListWidget2::currentRowChanged );
		// Context Menu
		setContextMenuPolicy( Qt::CustomContextMenu );
		setSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Preferred );
//		connect( this, &ListWidget::customContextMenuRequested,
//				 this, &ListWidget::showContextMenu );
	}
	int count()
	{
		return _items.size();
	}
	int index()
	{
		return _index;
	}
	bool isIndexValid( int index )
	{
		return ( 0 < index && index < count() );
	}

public slots:
	void setIndex( int index )
	{
		_begin = index;
	}
	void onScrollbarChanged( int index )
	{
		setIndex( index );
		update();
	}
	virtual void showContextMenu( const QPoint& /*pos*/ )
	{

	}
	void clear()
	{
//		_container->removeWidgets();
		for ( QWidget* section : _sections )
			delete section;
		_sections.clear();
		for ( ListItem* item : _items )
			delete item;
		_items.clear();
		_container->removeSpacers();
		_nbItems = 0;
	}
	virtual void addItem( ListItem* item = 0 )
	{
		_container->addWidget( item, _nbItems, 0 );
		_items.append( item );
		++_nbItems;
	}
	void addSpacer()
	{
		_container->addSpacer( _nbItems, 0 );
	}
	virtual void update()
	{
	}
	void updateScrollbar()
	{
		_scroll->setValue( _begin );
	}
	void updateScrollbarGeometry()
	{
		int x = width() - contentsMargins().right() - _scroll->sizeHint().width();
		int y = contentsMargins().top();
		int h = height() - contentsMargins().top() - contentsMargins().bottom();
		_scroll->setGeometry( x, y, _scroll->sizeHint().width(), h );
		_scroll->raise();
	}
	bool eventFilter( QObject* obj, QEvent* event ) override
	{
		// Mouse Wheel Event
		if ( event->type() == QEvent::Wheel )
		{
			qDebug().noquote() << "Wheel Event";
			QWheelEvent* wheel = static_cast<QWheelEvent*>(event);
			if ( wheel->delta() > 0 )
			{
				if ( _begin > 0 )
				{
					setIndex( _begin - 1 );
					if ( _scroll->isVisible() )
						_scroll->setValue( _begin );
					update();
				}
			}
			else
			{
				if ( _begin + 1 <= _scroll->maximum() )
				{
					setIndex( _begin + 1 );
					if ( _scroll->isVisible() )
						_scroll->setValue( _begin );
					update();
				}
			}
			qDebug().noquote() << "   Wheel Delta :" << wheel->delta();
//			qDebug().noquote() << "   Wheel Angle Delta :" << wheel->angleDelta();
//			qDebug().noquote() << "   Wheel Pixel Delta :" << wheel->pixelDelta();
		}
		// Mouse Press Event
		else if ( event->type() == QEvent::MouseButtonPress )
		{
			qDebug().noquote() << "Press Event";
			QMouseEvent* mouse = static_cast<QMouseEvent*>(event);
			if ( mouse->button() == Qt::LeftButton ||
				 mouse->button() == Qt::RightButton )
			{
				if ( mouse->modifiers() == Qt::ControlModifier )
				{
					_index = getIndexByPos(mouse->pos());
					qDebug().noquote() << "   CTRL Modifiers - Mouse pos : " << mouse->pos();
					emit indexToAdd( _begin+_index );
				}
				else if ( mouse->modifiers() == Qt::ShiftModifier &&
						  _index != -1 )
				{
					qDebug().noquote() << "   SHIFT Modifiers - Mouse pos : " << mouse->globalPos();
					int newIndex = getIndexByPos(mouse->pos());
					setCheckedItems( _index, newIndex, true );
					emit indexesToAdd( _begin+_index, _begin+newIndex );
					_index = newIndex;
				}
				else
				{
					_index = getIndexByPos(mouse->pos());
					setItemsChecked( false, _index );
					if ( _index != -1 )
						emit indexToChange( _begin+_index );
					else
						emit toReset();
				}
			}
		}
		// Mouse Release Event
		else if ( event->type() == QEvent::MouseButtonRelease )
		{
			qDebug().noquote() << "Release Event";
			QMouseEvent* mouse = static_cast<QMouseEvent*>(event);
			if ( mouse->button() == Qt::RightButton )
			{
				showContextMenu( mouse->globalPos() );
				qDebug().noquote() << "   Mouse pos : " << mouse->globalPos();
			}
		}
		// Mouse Double Clicked Event
		else if ( event->type() == QEvent::MouseButtonDblClick )
		{
			qDebug().noquote() << "Double Clicked Event";
			QMouseEvent* mouse = static_cast<QMouseEvent*>(event);
			if ( mouse->button() == Qt::LeftButton )
			{
				if ( index() != -1 )
					emit indexToPlay(  _begin+index() );
			}
		}
		else if ( event->type() == QEvent::Resize ) {
			update();
		}
		return Frame::eventFilter( obj, event );
	}
	ListItem* getItemByPos( const QPoint& pos )
	{
		int index = getIndexByPos( pos );
		return ( index == -1 ) ? 0 : _items.at(index);
	}
	int getIndexByPos( const QPoint& pos )
	{
		for ( int it = 0; it < count(); ++it )
		{
			if ( _items.at(it)->geometry().contains( pos ) )
			{
				return it;
			}
		}
		return -1;
	}
	void setItemsChecked( bool checked = false, int except = -1 )
	{
		for ( int it = 0; it < count(); ++it )
		{
			if ( it != except )
				_items.at(it)->setChecked( checked );
		}
	}
	void setCheckedItems( int begin, int end, bool checked = false )
	{
		if ( isIndexValid(begin) && isIndexValid(end) )
		{
			int itBeg = begin, itEnd = end;
			if ( itBeg > itEnd )
			{
				itBeg = end;
				itEnd = begin;
			}
			for ( int it = itBeg; it <= itEnd; ++it )
			{
				_items.at(it)->setChecked( checked );
			}
		}
	}

signals:
	void indexToChange( int index );
	void indexToAdd( int index );
	void indexesToAdd( int begin, int end );
	void indexToPlay( int index );
	void toReset();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LISTWIDGET_H
