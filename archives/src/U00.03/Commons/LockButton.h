#ifndef LOCKBUTTON_H
#define LOCKBUTTON_H
/**********************************************/
#include <QPushButton>
#include <QDebug>
#include <QMouseEvent>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LockButton
		: public QPushButton
{
	Q_OBJECT

public:
	LockButton( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setFlat( true );
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	}
public slots:
	void mousePressEvent( QMouseEvent* event ) override
	{
		event->ignore();
	}
	void mouseDoubleClickEvent( QMouseEvent* event ) override
	{
		event->ignore();
	}
	void mouseReleaseEvent( QMouseEvent* event ) override
	{
		event->ignore();
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LOCKBUTTON_H
