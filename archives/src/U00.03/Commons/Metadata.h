#ifndef METADATA_H
#define METADATA_H
/**********************************************/
#include <QObject>
#include <QString>
#include <QImage>
#include "../Commons/Track.h"
/**********************************************/
// Taglib
#include <mpegfile.h>
#include <id3v2tag.h>
#include <audioproperties.h>
#include <commentsframe.h>
using namespace TagLib;
#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Metadata
		: public QObject
{
	Q_OBJECT
private:
	Track* _track = 0;
	QString _path = "Unknown";
	QString _title = "Unknown";
	QString _artist = "Unknown";
	QString _album = "Unknown";
	QDate _date = QDate();
	QString _genre = "Unknown";
	QImage _cover;
	int _duration = -1;// = QTime( 0, 0 );
	int _year = -1;
	int _number = -1;
	QString _albumArtists = "";
	int _bitrate = -1;

public:
	Metadata( Track* track = 0 )
	{
		if ( track )
		{
			_track = track;
			_path = track->path();
			update();
		}
	}
	QString path() const
	{ return _path; }
	QString title() const
	{ return _title; }
	QString artist() const
	{ return _artist; }
	QString album() const
	{ return _album; }
	QDate date() const
	{ return _date; }
	QString genre() const
	{ return _genre; }
	QImage cover() const
	{ return _cover; }
	int duration() const
	{ return _duration; }
	int year() const
	{ return _year; }
	int number() const
	{ return _number; }
	int bitrate() const
	{ return _bitrate; }
	QString albumArtists() const
	{ return _albumArtists; }

public slots:
	void setTitle( QString title )
	{ _title = title; }
	void setArtist( QString artist )
	{ _artist = artist; }
	void setAlbum( QString album )
	{ _album = album; }
	void setAlbumArtists( QString albumartists )
	{ _albumArtists = albumartists; }
	void setDate( QDate date )
	{ _date = date; }
	void setGenre( QString genre )
	{ _genre = genre; }
	void setCover( QImage cover )
	{ _cover = cover; }
	void setDuration( int durat )
	{ _duration = durat; }
	void setYear( int year )
	{ _year = year; }
	void setNumber( int number )
	{ _number = number; }
	void setBitrate( int rate )
	{ _bitrate = rate; }
	void update()
	{
		if ( _path != "" )
		{
			if ( _track->format() == Track::MP3 )
			{
				MPEG::File file( _path.toUtf8() );
				if ( file.isValid() )
				{
					ID3v2::Tag *tag = file.ID3v2Tag( true );
					if ( tag )
					{
						setAlbum( tag->album().toCString(true) );
						setTitle( tag->title().toCString(true) );
						setArtist( tag->artist().toCString(true) );
						setGenre( tag->genre().toCString(true) );
						setYear( tag->year() );
						setNumber( tag->track());
						setDuration( file.audioProperties()->lengthInSeconds() );
						setBitrate( file.audioProperties()->bitrate() );
						QImage image;
						ID3v2::AttachedPictureFrame *frame = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(tag->frameList("APIC").front());
						if ( frame )
						{
							image.loadFromData(
										(const uchar *)frame->picture().data(),
										frame->picture().size());
							setCover( image );
						}

					}
	//				else
	//				  qDebug() << "file does not have a valid id3v2 tag" << endl;
				}
			}
			else if ( _track->format() == Track::OGG )
			{

			}
		}
	}
	void display()
	{
		qDebug() << "-----------------";
		qDebug() << "Path : " << path();
		qDebug() << "Title : " << title();
		qDebug() << "Artist : " << artist();
		qDebug() << "Album : " << album();
		qDebug() << "Genre : " << genre();
		qDebug() << "Album Artists : " << albumArtists();
		qDebug() << "Year : " << year();
		qDebug() << "Date : " << date();
		qDebug() << "Duration : " << duration();
		qDebug() << "Number : " << number();
		qDebug() << "-----------------";
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // METADATA_H
