#ifndef TRACKLISTMODIFIER_H
#define TRACKLISTMODIFIER_H
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackListModifierButton
		: public QPushButton
{
	Q_OBJECT
public:
	TrackListModifierButton( QWidget* parent = 0 )
		: QPushButton( parent )
	{
		setFlat( true );
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Preferred );
	}
};


class TrackListModifier
		: public QWidget
{
	Q_OBJECT
private:
	QHBoxLayout* lay_main = new QHBoxLayout;
	QLabel* lb_title = new QLabel;
	TrackListModifierButton* pb_new = new TrackListModifierButton;
	TrackListModifierButton* pb_play = new TrackListModifierButton;
	TrackListModifierButton* pb_add = new TrackListModifierButton;
	TrackListModifierButton* pb_update = new TrackListModifierButton;
	TrackListModifierButton* pb_delete = new TrackListModifierButton;

public:
	TrackListModifier( QWidget* parent = 0 )
		: QWidget( parent )
	{
		setSizePolicy( QSizePolicy::Expanding,
								QSizePolicy::Maximum );

		lay_main->setMargin( 0 );
		lay_main->setSpacing( 0 );
		setLayout( lay_main );

		lay_main->addWidget( lb_title );
		lay_main->addItem( new QSpacerItem( 0, 0, QSizePolicy::Expanding) );
		lay_main->addWidget( pb_new );
		lay_main->addWidget( pb_play );
		lay_main->addWidget( pb_add );
		lay_main->addWidget( pb_update );
		lay_main->addWidget( pb_delete );

		// Connections
		connect( pb_new, &TrackListModifierButton::clicked,
				 this, &TrackListModifier::toNew );
		connect( pb_play, &TrackListModifierButton::clicked,
				 this, &TrackListModifier::toPlay );
		connect( pb_add, &TrackListModifierButton::clicked,
				 this, &TrackListModifier::toAdd );
		connect( pb_update, &TrackListModifierButton::clicked,
				 this, &TrackListModifier::toUpdate );
		connect( pb_delete, &TrackListModifierButton::clicked,
				 this, &TrackListModifier::toDelete );

	}

public slots:
	void updateIcons( Icons* icons )
	{
		pb_add->setIcon( icons->get("new") );
		pb_play->setIcon( icons->get("play") );
		pb_add->setIcon( icons->get("addToPlayer") );
		pb_update->setIcon( icons->get("update") );
		pb_delete->setIcon( icons->get("delete") );
	}
	void updateLang( Lang* /*lang*/ )
	{
//		lb_title->setText( );
	}

signals:
	void toNew();
	void toPlay();
	void toAdd();
	void toUpdate();
	void toDelete();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKLISTMODIFIER_H
