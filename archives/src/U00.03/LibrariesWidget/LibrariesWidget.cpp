#include "LibrariesWidget.h"
/**********************************************/
/**********************************************/
/* */
LibrariesWidget::LibrariesWidget( QWidget* parent )
	: Frame( parent )
{
//	hide();
	// Adding the widgets
	addWidget( wid_libraryModifier, 0, 0 );
	addWidget( sel_library, 1, 0 );
	addWidget( wid_library, 0, 1, 2, 1 );


	connect( wid_libraryModifier, &TrackListModifier::toPlay,
			 this, &LibrariesWidget::playLibrary );
	connect( wid_libraryModifier, &TrackListModifier::toAdd,
			 this, &LibrariesWidget::addLibrary );
	connect( wid_libraryModifier, &TrackListModifier::toUpdate,
			 this, &LibrariesWidget::updateLibrary );
//	connect( wid_libraryModifier, &TrackListModifier::toDelete,
//			 this, &LibrariesWidget::deleteLibrary );

	connect( sel_library, &LibrarySelector::indexToChange,
			 this, &LibrariesWidget::showLibrary );
	connect( sel_library, &LibrarySelector::toReset,
			 wid_library, &TrackSelector::reset );
}
/**********************************************/
/**********************************************/
