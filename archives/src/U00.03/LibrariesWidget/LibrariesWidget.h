#ifndef LIBRARIESWIDGET_H
#define LIBRARIESWIDGET_H
/**********************************************/
#include "../Commons/Frame.h"
#include "../Commons/TrackListModifier.h"
#include "../LibrariesWidget/LibrarySelector.h"
#include "../TracksWidget/TrackSelector.h"
#include "../Commons/TracklistList.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LibrariesWidget
		: public Frame
{
	Q_OBJECT
private:
	TracklistList* _libraries = 0;
	TrackListModifier* wid_libraryModifier = new TrackListModifier;
	LibrarySelector* sel_library = new LibrarySelector;
	TrackSelector* wid_library = new TrackSelector;

public:
	LibrariesWidget( QWidget* parent = 0 );

public slots:
	void updateIcons( Icons* icons )
	{
		wid_libraryModifier->updateIcons( icons );
		sel_library->updateIcons( icons );
		wid_library->updateIcons( icons );
//		wid_library->setMaximumWidth( 400 );
	}
	void updateLang( Lang* /*lang*/ )
	{
//		lb_title->setText( );
	}
	void setTracklistList( TracklistList* libraries = 0 )
	{
		_libraries = libraries;
		sel_library->setLibraries( _libraries );

//		connect( _libraries, &TracklistList::currentTrackListChanged,
//				 wid_library, &TrackListSelector::setTrackList );
//		connect( _libraries, &TracklistList::changed,
//				 wid_library, &TrackListSelector::reset );
		connect( wid_libraryModifier, &TrackListModifier::toNew,
				 _libraries, &TracklistList::addNew );
		connect( wid_libraryModifier, &TrackListModifier::toDelete,
				 _libraries, &TracklistList::remove );
		connect( sel_library, &LibrarySelector::renamed,
				 _libraries, &TracklistList::rename );
		connect( sel_library, &LibrarySelector::deleted,
				 _libraries, &TracklistList::remove );

		connect( _libraries, &TracklistList::currentTrackListChanged,
				 wid_library, &TrackSelector::setTrackList );
	}
	void showLibrary( int index )
	{
		if ( index != -1 )
			wid_library->setTrackList( _libraries->tracklist(index) );
	}
	void newLibrary()
	{
		_libraries->addNew();
	}
	void playLibrary()
	{
//		if ( _libraries->currentTracklist() )
//			emit tracklistToPlay( _libraries->currentTracklist() );
	}
	void addLibrary()
	{
//		if ( _libraries->currentTracklist() )
//			emit tracklistToAdd( _libraries->currentTracklist() );
	}
	void updateLibrary()
	{
//		_tracklists->
	}
	void deleteLibrary()
	{
		_libraries->remove();
	}

signals:
//	void playTrack( Track* track );
//	void receiveTrackList( TrackList* tracklist );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARIESWIDGET_H
