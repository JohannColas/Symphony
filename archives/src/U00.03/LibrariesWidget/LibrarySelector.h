#ifndef LIBRARYLISTWIDGET_H
#define LIBRARYLISTWIDGET_H
/**********************************************/
#include <QInputDialog>
#include <QListWidget>
#include <QMenu>
#include "../Commons/ListWidget.h"
#include "../LibrariesWidget/LibraryItem.h"
#include "../Commons/Icons.h"
#include "../Commons/TracklistList.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LibrarySelector
		: public ListWidget
{
	Q_OBJECT
private:
	Icons* _icons = 0;
	TracklistList* _libraries = 0;

public:
	LibrarySelector( QWidget* parent = nullptr )
		: ListWidget( parent )
	{
//		setMaximumWidth( 400 );
	}
	TrackList* currentTrackList()
	{
		if ( _libraries && index() != -1 )
			return _libraries->tracklist( index() );
		return 0;
	}

public slots:
	void setLibraries( TracklistList* playlists )
	{
		_libraries = playlists;
		connect( _libraries, &TracklistList::changed,
				 this, &LibrarySelector::update );
		update();
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		update();
	}
	void update() override
	{
		if ( _libraries )
		{
			clear();
			int x = contentsMargins().left();
			int w = width()
					- contentsMargins().left()
					- contentsMargins().right();
			int totheight = contentsMargins().top();
			QIcon icon = _icons ? _icons->get("library") : QIcon();
			for ( int it = 0; it < _libraries->count(); ++it )
			{
				LibraryItem* item = new LibraryItem( _libraries->tracklist(it)->name(), icon, this );
				item->setGeometry( x, totheight,
										w,
										item->sizeHint().height() );
				_items.append( item );
				totheight += item->sizeHint().height();
				++_nbItems;
				if ( totheight > height() )
				{
					break;
				}
			}
		}
	}
	void showContextMenu( const QPoint& pos ) override
	{
		QMenu* menu = new QMenu( this );
		QAction* ac_rename = menu->addAction( "Rename" );
		QAction* ac_delete = menu->addAction( "Delete" );
		QAction* action = menu->exec( pos );
		if ( action == ac_rename )
		{
			QString text =  QInputDialog::getText( this, "New name", "New name:", QLineEdit::Normal, _libraries->tracklist(index())->name() );

			if ( text != "" )
			{
				_libraries->tracklist(index())->setName( text );
				emit renamed( text );
			}
		}
		else if ( action == ac_delete )
		{
			emit deleted();
		}
	}

signals:
	void currentTracklistChanged( TrackList* tracklist );
	void renamed( const QString& name );
	void deleted();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARYLISTWIDGET_H
