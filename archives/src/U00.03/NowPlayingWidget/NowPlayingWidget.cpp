#include "NowPlayingWidget.h"
/**********************************************/
/**********************************************/
/* */
NowPlayingWidget::NowPlayingWidget( QWidget* parent )
	: Frame( parent )
{
	addWidget( wid_trackInfo, 0, 0 );
	addWidget( wid_trackList, 0, 1 );

	wid_trackList->setMaximumWidth( 400 );
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::setPlayer( QMediaPlayer* /*player*/ )
{
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::setPlaylist( TrackList* playlist )
{
	_nowPlaylist = playlist;
	wid_trackList->setTrackList( _nowPlaylist );
	wid_trackInfo->setTrack( _nowPlaylist->track(0) );
//	connect( _nowPlaylist, &TrackList2::currentTrackIndexChanged,
//			 wid_trackList, &TrackSelector::setIndex );
	connect( _nowPlaylist, &TrackList::currentTrackChanged,
			 wid_trackInfo, &TrackWidget::setTrack );
	connect( _nowPlaylist, &TrackList::changed,
			 this, &NowPlayingWidget::update );
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::setTrackList( TrackList* playlist )
{
	_nowPlaylist = playlist;
	wid_trackList->setTrackList( _nowPlaylist );
	wid_trackInfo->setTrack( _nowPlaylist->track(0) );
//	connect( _nowPlaylist, &TrackList::currentTrackIndexChanged,
//			 wid_trackList, &TrackSelector::setIndex );
	connect( _nowPlaylist, &TrackList::currentTrackChanged,
			 wid_trackInfo, &TrackWidget::setTrack );
	connect( _nowPlaylist, &TrackList::changed,
			 this, &NowPlayingWidget::update );
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::updateList()
{
	wid_trackList->update();
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::update()
{
	wid_trackList->setIndex( _nowPlaylist->currentTrackIndex() );
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::updateIcons( Icons* icons )
{
	_icons = icons;
	wid_trackInfo->updateIcons( _icons );
	wid_trackList->updateIcons( _icons );
}
/**********************************************/
/**********************************************/
