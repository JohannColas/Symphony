#include "PlayerWidget.h"
/**********************************************/
#include <QDebug>
#include <QApplication>
#include <QTime>
/**********************************************/
/**********************************************/
/* */
PlayerWidget::~PlayerWidget()
{

}
/**********************************************/
/**********************************************/
/* */
PlayerWidget::PlayerWidget( QWidget* parent )
	: QFrame( parent )
{
	setSizePolicy( QSizePolicy::Expanding,
							QSizePolicy::Maximum );

	wid_songInfo->setTrack( _player );
	connect( _player, &QMediaPlayer::positionChanged,
			 this, &PlayerWidget::setPosition );
	connect( _player, &QMediaPlayer::mediaChanged,
			 this, &PlayerWidget::changeMedia );
	connect( _player, &QMediaPlayer::mediaStatusChanged,
			 this, &PlayerWidget::onMediaStatusChanged );
	connect( _player, SIGNAL(metaDataChanged()),
			 wid_songInfo, SLOT(updateWidget()) );
	connect( pb_play, &QPushButton::clicked,
			 this, &PlayerWidget::playpause );
	connect( pb_stop, &QPushButton::clicked,
			 _player, &QMediaPlayer::stop );
	connect( pb_backward, &QPushButton::clicked,
			 this, &PlayerWidget::backward );
	connect( pb_forward, &QPushButton::clicked,
			 this, &PlayerWidget::forward );
	connect( sl_time, &QSlider::sliderMoved,
			 this, &PlayerWidget::setPlayerPos );
	_timer->setSingleShot( true );
	connect( _timer, &QTimer::timeout,
			 this, &PlayerWidget::updatePlayerPos );

	pb_backward->setFlat( true );
	pb_play->setFlat( true );
	pb_stop->setFlat( true );
	pb_forward->setFlat( true );
	sl_time->setOrientation( Qt::Horizontal );
	sl_time->setMinimum( 0 );
	sl_time->setMaximum( 1000 );
	QHBoxLayout* lay_main = new QHBoxLayout;
	lay_main->addWidget( pb_backward );
	lay_main->addWidget( pb_play );
	lay_main->addWidget( pb_stop );
	lay_main->addWidget( pb_forward );
	lay_main->addWidget( wid_songInfo );


	GridLayout* lay_timer = new GridLayout;
	lay_timer->addSpacer( 0, 0, true, 1, 2 );
	lay_timer->addSpacer( 1, 0, false );
	lay_timer->addWidget( lb_time, 1, 1 );
	lay_timer->addWidget( sl_time, 2, 0, 1, 2 );
	lay_timer->addSpacer( 3, 0, true, 1, 2 );
	lb_time->setSizePolicy( QSizePolicy::Maximum,
							QSizePolicy::Maximum );
	sl_time->setMinimumWidth( 200 );

	lay_main->addLayout( lay_timer );

	lay_main->addSpacing( 10 );
	lay_main->addWidget( pb_repeat );
	lay_main->addWidget( pb_shuffle );
	lay_main->addWidget( pb_equalizer );
	lay_main->addWidget( pb_volume );

	lay_main->addItem( new QSpacerItem( 0,
										0,
										QSizePolicy::Expanding )
					   );

	lay_main->addWidget( pb_settings );
	lay_main->addSpacing( 10 );
	GridLayout* lay_system = new GridLayout;
	lay_system->addWidget( pb_minimize, 0, 0 );
	lay_system->addWidget( pb_maximize, 0, 1 );
	lay_system->addWidget( pb_close, 0, 2 );
//	lay_system->addSpacer( 1, 0, true, 1, 3 );
	lay_main->addLayout( lay_system );

	connect( pb_minimize, &QPushButton::clicked,
			 this, &PlayerWidget::onMinimize );
	connect( pb_maximize, &QPushButton::clicked,
			 this, &PlayerWidget::onMaximize );
	connect( pb_close, &QPushButton::clicked,
			 qApp, &QApplication::closeAllWindows );
	setLayout( lay_main );

	setMouseTracking( true );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::playTrack( Track* track )
{
	_nowPlaylist->add( track );
	emit _nowPlaylist->trackAdded();
	_player->setPlaylist( _nowPlaylist->mediaPlaylist() );
	_player->playlist()->setCurrentIndex( _nowPlaylist->count() -1 );
	_player->play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::addTrack( Track* track )
{
	_nowPlaylist->add( track );
	emit _nowPlaylist->trackAdded();
	_player->setPlaylist( _nowPlaylist->mediaPlaylist() );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::playTracklist( TrackList* tracklist )
{
	_nowPlaylist->clear();
	_nowPlaylist->add( tracklist );
	_player->setPlaylist( _nowPlaylist->mediaPlaylist() );
	_player->playlist()->setCurrentIndex( 0 );
	_player->play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::addTracklist( TrackList* tracklist )
{
	_nowPlaylist->add( tracklist );
	_player->setPlaylist( _nowPlaylist->mediaPlaylist() );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::receiveTrackList( TrackList* tracklist )
{
	_player->setPlaylist( tracklist->mediaPlaylist() );
	_player->playlist()->setCurrentIndex( tracklist->currentTrackIndex() );
	_player->play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPlaylistIndex( int index )
{
	_player->playlist()->setCurrentIndex( index );
	_player->play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setTrackList( TrackList* playlist )
{
	_nowPlaylist = playlist;
	_player->setPlaylist( playlist->mediaPlaylist() );
	_player->playlist()->setCurrentIndex( 0 );
	connect( _player->playlist(), &QMediaPlaylist::currentIndexChanged,
			 playlist, &TrackList::setCurrentTrack );
	playlistChanged( playlist );

}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPosition( qint64 pos )
{
	if ( _player->duration() )
	{
		int perc = 1000*pos/_player->duration();
		//	qDebug() << pos << player->duration() << perc;
		sl_time->setValue( perc );
		QString curT = QTime( 0, 0, 0 ).addMSecs( pos ).toString();
		if( curT.left(2) == "00" )
			curT = curT.mid( 3 );
		QString durT = QTime( 0, 0, 0).addMSecs( _player->duration() ).toString();
		if( durT.left(2) == "00" )
			durT = durT.mid( 3 );
		lb_time->setText( curT + " / " + durT );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPlayerPos( int /*pos*/ )
{
	_timer->start(200);
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::updatePlayerPos()
{
	qint64 newpos = sl_time->value()*_player->duration()/1000;
	_player->setPosition( newpos );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::changeMedia()
{

	//	sl_time->setMaximum( player->duration() );
	sl_time->setValue( 0 );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::playpause()
{
	if ( _player->state() == QMediaPlayer::PlayingState )
		_player->pause();
	else
		_player->play();
	updatePlayPauseIcon();
}
void PlayerWidget::updatePlayPauseIcon()
{
	if ( _icons )
	{
		if ( _player->state() == QMediaPlayer::PlayingState )
			pb_play->setIcon( _icons->get("pause") );
		else
			pb_play->setIcon( _icons->get("play") );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::backward()
{
	if ( _player->playlist()->currentIndex() != 0 )
	{
		if ( sl_time->value() < 40 )
			_player->playlist()->previous();
		else
			_player->setPosition( 0 );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::forward()
{
	_player->playlist()->next();
	if ( _player->playlist()->currentIndex() == -1 )
	{
		_player->playlist()->setCurrentIndex( 0 );
		_player->stop();
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::onMediaStatusChanged()
{
	updatePlayPauseIcon();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::updateIcons( Icons* icons )
{
	_icons = icons;
	pb_backward->setIcon( icons->get("skip_previous") );
	updatePlayPauseIcon();
	pb_stop->setIcon( icons->get("stop") );
	pb_forward->setIcon( icons->get("skip_next") );
	wid_songInfo->updateIcons( _icons );
	pb_repeat->setIcon( icons->get("repeat") );
	pb_shuffle->setIcon( icons->get("shuffle") );
	pb_equalizer->setIcon( icons->get("equalizer") );
	pb_volume->setIcon( icons->get("volume") );
	pb_settings->setIcon( icons->get("settings") );
	pb_minimize->setIcon( icons->get("minimise") );
	pb_maximize->setIcon( icons->get("maximise") );
	pb_close->setIcon( icons->get("close") );
}
/**********************************************/
/**********************************************/
void PlayerWidget::mouseDoubleClickEvent( QMouseEvent* event )
{
	if ( event->button() == Qt::LeftButton )
		onMaximize();
	event->ignore();
}

void PlayerWidget::connectTo( Frame* frame )
{
	connect( frame, &Frame::trackToPlay,
			 this, &PlayerWidget::playTrack );
	connect( frame, &Frame::trackToAdd,
			 this, &PlayerWidget::addTrack );
	connect( frame, &Frame::tracklistToPlay,
			 this, &PlayerWidget::playTracklist );
	connect( frame, &Frame::tracklistToAdd,
			 this, &PlayerWidget::addTracklist );

}
/**********************************************/
/**********************************************/
