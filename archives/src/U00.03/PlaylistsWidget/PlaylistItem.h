#ifndef PLAYLISTITEM_H
#define PLAYLISTITEM_H
/**********************************************/
#include "../Commons/ListItem.h"
#include "../Commons/Icons.h"
#include "../Commons/TrackList.h"
/**********************************************/
#include <QLabel>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistItemIcon
		: public QLabel
{

public:
	PlaylistItemIcon( QWidget* parent = 0 )
		: QLabel( parent )
	{
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	}
};
class PlaylistItemName
		: public QLabel
{
};

class PlaylistItem
		: public ListItem
{
	Q_OBJECT
private:
//	PlaylistItemIcon* lb_icon = new PlaylistItemIcon;
//	PlaylistItemName* lb_name = new PlaylistItemName;

public:
	PlaylistItem( const QString& name, QIcon icon, QWidget* parent = 0 )
		: ListItem( parent )
	{
		// Adding the widgets
//		addWidget( lb_icon, 0, 0 );
//		addWidget( lb_name, 0, 1 );

//		lb_name->setText( name );
		setText( name );
		setIcon( icon );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTITEM_H
