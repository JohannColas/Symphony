#ifndef SIDEBARWIDGET_H
#define SIDEBARWIDGET_H
/**********************************************/
#include <QWidget>
#include "../Commons/Theme.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
#include <QMouseEvent>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QScrollArea>
#include <QScrollBar>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include <QMenu>
#include <QAction>
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SidebarIcon
		: public QPushButton
{
	Q_OBJECT
public:
	SidebarIcon( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setFlat( true );
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	}
	void mouseEnterEvent( QMouseEvent* event ) { event->ignore() ; }
	void mousePressEvent( QMouseEvent* event ) { event->ignore() ; }
	void mouseMoveEvent( QMouseEvent* event ) { event->ignore() ; }
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SidebarLabel
		: public QPushButton
{
	Q_OBJECT
public:
	SidebarLabel( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setFlat( true );
		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}
	void mouseEnterEvent( QMouseEvent* event ) { event->ignore() ; }
	void mousePressEvent( QMouseEvent* event ) { event->ignore() ; }
	void mouseMoveEvent( QMouseEvent* event ) { event->ignore() ; }
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SidebarItem
		: public QWidget
{
	Q_OBJECT
private:
	SidebarIcon* wid_icon = new SidebarIcon;
	SidebarLabel* wid_label = new SidebarLabel;
	QHBoxLayout* lay_main = new QHBoxLayout;

public:
	SidebarItem( QWidget* parent = nullptr )
		: QWidget( parent )
	{
		lay_main->setMargin( 0 );
		lay_main->setSpacing( 0 );
		lay_main->addWidget( wid_icon );
		lay_main->addWidget( wid_label );
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		setLayout( lay_main );

//		setMouseTracking( false );
	}

public slots:
	void setText( const QString& text )
	{
		wid_label->setText( text );
	}
	void setIcon( const QIcon& icon )
	{
		wid_icon->setIcon( icon );
	}
	void setLabelVisible( bool visible )
	{
		wid_label->setVisible( visible );
	}
	void mouseEnterEvent( QMouseEvent* event ) { event->ignore() ; }
	void mousePressEvent( QMouseEvent* event ) { event->ignore() ; }
	void mouseMoveEvent( QMouseEvent* event ) { event->ignore() ; }
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SidebarButton
		: public QPushButton
{
	Q_OBJECT
public:
	SidebarButton( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setFlat( true );
		setMouseTracking( true );
		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SidebarFrame
		: public QFrame
{
	Q_OBJECT
public:
	SidebarFrame( QWidget* parent = nullptr )
		: QFrame( parent )
	{
		setMouseTracking( true );
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Preferred );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SidebarWidget
		: public QWidget
{
	Q_OBJECT
private:
	QMenu* pop_options = new QMenu;
	QAction*  ac_textVisibility = new QAction("Text visible");
	QAction*  ac_nowPlaying = new QAction("Now Playing");
	QAction*  ac_artists = new QAction("Artists");
	QAction*  ac_albums = new QAction("Albums");
	QAction*  ac_tracks = new QAction("Tracks");
	QAction*  ac_genres = new QAction("Genres");
	QAction*  ac_playlists = new QAction("Playlists");
	QAction*  ac_libraries = new QAction("Libraries");
	QAction*  ac_radio = new QAction("Radio");
	QAction*  ac_podcats = new QAction("Podcasts");
	SidebarFrame* _sidebar = new SidebarFrame;
	QListWidget* wid_list = new QListWidget;
	QGridLayout* lay_main = new QGridLayout;
	QList<QWidget*> _widgets;
	SidebarButton* pb_options = new SidebarButton;
	int _selectedTab = -1;
	/*---------------------------*/
	/*---------------------------*/
public:
	~SidebarWidget();
	SidebarWidget( QWidget* parent = nullptr );
	/*---------------------------*/
	void addItem( const QString& name, QWidget* widget = new QWidget );
	void setWidget( int index, QWidget* widget = new QWidget );
	/*---------------------------*/
	int count();
	SidebarItem* sidebarItem( int index )
	{
		return static_cast<SidebarItem*>(wid_list->itemWidget( wid_list->item( index ) ));
	}
	/*---------------------------*/
	/*---------------------------*/
public slots:
	void changeCurrentWidget( int index );
	void updateIcons( Icons* icons );
	void setTextVisibility( bool check );
	void setNowPlayingVisible( bool check );
	void setArtistsVisible( bool check );
	void setAlbumsVisible( bool check );
	void setTracksVisible( bool check );
	void setGenresVisible( bool check );
	void setPlaylistsVisible( bool check );
	void setLibrariesVisible( bool check );
	void setRadioVisible( bool check );
	void setPodcastsVisible( bool check );
	/*---------------------------*/
	/*---------------------------*/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SIDEBARWIDGET_H
