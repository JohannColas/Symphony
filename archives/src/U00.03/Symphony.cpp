#include "Symphony.h"
/**********************************************/
/**********************************************/
/* */
Symphony::~Symphony()
{
	delete _theme;
	delete _icons;
	delete _lang;
}
/**********************************************/
/**********************************************/
/* */
Symphony::Symphony( QWidget *parent )
	: Frame( parent )
{
	// Window Geometry
	setMinimumSize( 800, 500 );
	wid_player->setMinimumHeight( 50 );
	addWidget( wid_player, 0, 0, 1, 2 );
	addWidget( wid_sidebar, 1, 0, 1, 2 );

	// Adding TrackLists to Widgets
	wid_player->setTrackList( _nowPlaylist );
	wid_nowPlaying->setTrackList( _nowPlaylist );
	wid_artists->setTracklistList( _libraries );
	wid_albums->setTracklistList( _libraries );
//	wid_genres->setTracklistList( _libraries );
//	wid_tracks->setTracklistList( _libraries );
	wid_playlists->setTracklistList( _playlists );
	wid_libraries->setTracklistList( _libraries );
//	wid_radios->setTracklistList( _radios );
//	wid_podcasts->setTracklistList( _podcasts );

	// Adding Widgets to SidebarWidget
	wid_sidebar->setWidget( 0, wid_nowPlaying );
	wid_sidebar->setWidget( 1, wid_artists );
	wid_sidebar->setWidget( 2, wid_albums );
	wid_sidebar->setWidget( 3, wid_genres );
	wid_sidebar->setWidget( 4, wid_tracks );
	wid_sidebar->setWidget( 5, wid_playlists );
	wid_sidebar->setWidget( 6, wid_libraries );
	wid_sidebar->setWidget( 7, wid_radios );
	wid_sidebar->setWidget( 8, wid_podcasts );
	wid_sidebar->changeCurrentWidget( 0 );

	// Connections between widgets
	connect( wid_nowPlaying, &NowPlayingWidget::currentTrackIndexChanged,
			 wid_player, &PlayerWidget::setPlaylistIndex );
	connect( wid_player, &PlayerWidget::playlistChanged,
			 wid_nowPlaying, &NowPlayingWidget::setPlaylist );
	connect( wid_player, &PlayerWidget::onMinimize ,
			 this, &Symphony::showMinimized );
	connect( wid_player, &PlayerWidget::onMaximize,
			 this, &Symphony::maximise );
	wid_player->connectTo( wid_playlists );
	wid_player->connectTo( wid_libraries );



	// -----------------------
	// -----------------------
	// Themes
	_theme->apply();
	updateIcons();
	updateLang();
	// -----------------------
	// -----------------------
	setMouseTracking( true );
//	installEventFilter(this);
	// Update Sidebar size
	wid_sidebar->setTextVisibility( true );

}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateSettings()
{
//	_settings->update();
//	wid_player->updateIcons( _settings );
//	wid_sidebar->updateIcons( _settings );
//	wid_nowPlaying->updateIcons( _settings );
//	wid_libraries->updateIcons( _settings );
}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateIcons()
{
	_icons->update();
	setWindowIcon( _icons->get( "Symphony" ) );
	wid_player->updateIcons( _icons );
	wid_sidebar->updateIcons( _icons );
	wid_nowPlaying->updateIcons( _icons );
	wid_playlists->updateIcons( _icons );
	wid_libraries->updateIcons( _icons );
}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateLang()
{
	_lang->update();

}
/**********************************************/
/**********************************************/
/* */
void Symphony::closeEvent( QCloseEvent* event )
{

	QFrame::closeEvent( event );
}
/**********************************************/
/**********************************************/
