QT       += core gui multimedia xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    PodcastsWidget/PodcastsWidget.cpp \
    RadiosWidget/RadiosWidget.cpp \
    AlbumsWidget/AlbumsWidget.cpp \
    ArtistsWidget/ArtistSelector.cpp \
    ArtistsWidget/ArtistsWidget.cpp \
    Commons/FrameLess.cpp \
    GenresWidget/GenresWidget.cpp \
    LibrariesWidget/LibrariesWidget.cpp \
    NowPlayingWidget/NowPlayingWidget.cpp \
    PlayerWidget/PlayerWidget.cpp \
    PlayerWidget/SongInfoWidget.cpp \
    PlaylistsWidget/PlaylistsWidget.cpp \
    SidebarWidget/SidebarWidget.cpp \
    TracksWidget/TracksWidget.cpp \
    main.cpp \
    Symphony.cpp

HEADERS += \
    AlbumsWidget/AlbumSelector.h \
    AlbumsWidget/AlbumWidget.h \
    ArtistsWidget/ArtistWidget.h \
    Commons/Frame.h \
    Commons/Label.h \
    Commons/Layouts.h \
    Commons/ListItem.h \
    Commons/ListWidget.h \
    Commons/LockButton.h \
    Commons/Settings.h \
    Commons/Sorting.h \
    Commons/TracklistList.h \
    GenresWidget/GenreSelector.h \
    GenresWidget/GenreWidget.h \
    LibrariesWidget/LibraryItem.h \
    LibrariesWidget/LibraryListItem.h \
    LyricsWidget/LyricsWidget.h \
    PlaylistsWidget/PlaylistItem.h \
    PodcastsWidget/PodcastSelector.h \
    PodcastsWidget/PodcastsWidget.h \
    RadiosWidget/RadioSelector.h \
    RadiosWidget/RadiosWidget.h \
    AlbumsWidget/AlbumsWidget.h \
    ArtistsWidget/ArtistsWidget.h \
    ArtistsWidget/ArtistSelector.h \
    Commons/FrameLess.h \
    Commons/GridLayout.h \
    Commons/Icons.h \
    Commons/Lang.h \
    Commons/Metadata.h \
    Commons/Theme.h \
    Commons/Track.h \
    Commons/TrackList.h \
    Commons/TrackListModifier.h \
    GenresWidget/GenresWidget.h \
    LibrariesWidget/LibrariesWidget.h \
    LibrariesWidget/LibraryList.h \
    LibrariesWidget/LibrarySelector.h \
    NowPlayingWidget/NowPlayingWidget.h \
    PlayerWidget/PlayerWidget.h \
    PlayerWidget/SongInfoWidget.h \
    PlaylistsWidget/PlaylistList.h \
    PlaylistsWidget/PlaylistSelector.h \
    PlaylistsWidget/PlaylistsWidget.h \
    SidebarWidget/SidebarWidget.h \
    Symphony.h \
    TracksWidget/TrackItem.h \
    TracksWidget/TrackSelector.h \
    TracksWidget/TrackWidget.h \
    TracksWidget/TracksWidget.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += \
    /usr/local/include/taglib
DEPENDPATH += /usr/local/include/taglib

LIBS += \
    -L/usr/local/lib \
    -ltag

INCLUDEPATH += \
    /usr/local/include
DEPENDPATH += /usr/local/include
LIBS += \
    -L/usr/local/lib \
    -lz

