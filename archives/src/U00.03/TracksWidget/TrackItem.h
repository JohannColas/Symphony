#ifndef TRACKITEM_H
#define TRACKITEM_H
/**********************************************/
#include "../Commons/ListItem.h"
#include "../Commons/Label.h"
#include "../Commons/Track.h"
#include "../Commons/Metadata.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackCoverItem
		: public QPushButton
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	TrackCoverItem( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setFlat( true );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setPixmap( const QPixmap& pix )
	{
		setIcon( pix );
	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
class TrackTitleItem
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	TrackTitleItem( const QString& text, QWidget* parent = nullptr )
		: Label( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
class TrackArtistItem
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	TrackArtistItem( const QString& text, QWidget* parent = nullptr )
		: Label( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
class TrackAlbumItem
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	TrackAlbumItem( const QString& text, QWidget* parent = nullptr )
		: Label( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
class TrackGenreItem
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	TrackGenreItem( const QString& text, QWidget* parent = nullptr )
		: Label( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
class TrackDurationItem
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	TrackDurationItem( int duration, QWidget* parent = nullptr )
		: Label( parent )
	{
		QString durT = QTime( 0, 0, 0 ).addSecs(duration).toString();
		if( durT.left(2) == "00" )
			durT = durT.mid( 3 );
		setText( durT );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
class TrackNumberItem
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	TrackNumberItem( int number, QWidget* parent = nullptr )
		: Label( parent )
	{
		setText( QString::number(number) );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackItem
		: public ListItem
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	TrackItem( const Metadata& meta, const QString& _options, const QIcon& icon, QWidget* parent = nullptr )
		: ListItem( parent )
	{
		QStringList colOpts = _options.split(':');
		int itCol = 0;
		for ( QString col : colOpts )
		{
			int itRow = 0;
			if ( col == "--" )
			{
				lay_main->addSpacer( itRow, itCol, false );
			}
			else
			{
				QStringList rowOpts = col.split('/');
				GridLayout* lay_col = new GridLayout;
				for ( QString row : rowOpts )
				{
					if ( row == "||" )
					{
						lay_col->addSpacer( itRow, 0, true );
					}
					else if ( row == "title" )
					{
						lay_col->addWidget( new TrackTitleItem( meta.title() ), itRow, 0 );
					}
					else if ( row == "artist" )
					{
						lay_col->addWidget( new TrackArtistItem( meta.artist() ), itRow, 0 );
					}
					else if ( row == "album" )
					{
						lay_col->addWidget( new TrackAlbumItem( meta.album() ), itRow, 0 );
					}
					else if ( row == "Genre" )
					{
						lay_col->addWidget( new TrackGenreItem( meta.genre() ), itRow, 0 );
					}
					else if ( row == "number" )
					{
						lay_col->addWidget( new TrackNumberItem( meta.number() ), itRow, 0 );
					}
					else if ( row == "cover" )
					{
						TrackCoverItem* cover = new TrackCoverItem;
						QPixmap pix = QPixmap();
						if ( pix.convertFromImage( meta.cover() ) )
							cover->setPixmap( pix );
						else
						{
							cover->setIcon( icon );
						}
						lay_col->addWidget( cover, itRow, 0 );
					}
					else if ( row == "duration" )
					{
						int dur = meta.duration();
						if ( dur == -1 )
							dur = 0;
						lay_col->addWidget( new TrackDurationItem( dur ), itRow, 0 );
					}
					++itRow;
				}
				lay_main->addLayout( lay_col, 0, itCol );
			}
			++itCol;
		}
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setOptions( const QString& options )
	{
		_options = options;
	}
	void update()
	{

	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKITEM_H
