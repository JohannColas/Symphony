#ifndef TRACKSELECTOR_H
#define TRACKSELECTOR_H
/**********************************************/
#include <QInputDialog>
#include <QListWidget>
#include <QMenu>
#include "../Commons/ListWidget.h"
#include "../TracksWidget/TrackItem.h"
#include "../Commons/Icons.h"
#include "../Commons/TrackList.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackSelector
		: public ListWidget
{
	Q_OBJECT
private:
	Icons* _icons = 0;
	TrackList* _tracklist = 0;
	QList<QWidget*> _widgets;

public:
	TrackSelector( QWidget* parent = nullptr )
		: ListWidget( parent )
	{
//		setMaximumWidth( 400 );
//		setStyleSheet( "background:blue;");
	}
	Track* currentTrack()
	{
		if ( _tracklist && index() != -1 )
			return _tracklist->track( index() );
		return 0;
	}

public slots:
	void reset()
	{

	}
	void setTrackList( TrackList* tracklist )
	{
		_tracklist = tracklist;
		connect( _tracklist, &TrackList::changed,
				 this, &TrackSelector::update );
		update();
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		update();
	}
	void update() override
	{
		if ( _tracklist )
		{
			clear();
			for ( QWidget* widget : _widgets )
			{
				delete widget;
			}
			_widgets.clear();
//			qDebug() << contentsMargins();
			int x = contentsMargins().left();// 0;
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();// 0;
			int nbItems = 0;
			for ( int it = _begin; it < _tracklist->count(); ++it )
			{
				Metadata meta( _tracklist->track(it) );
				QString options = "cover:||/title/artist/||:--:duration";
				options = "cover:title/artist:--:duration";
//				options = "cover:||/title/artist/album/duration/||:--";
				QIcon icon = QIcon();
				if ( _icons )
					icon = _icons->get("track");
				TrackItem *trackItem = new TrackItem( meta, options, icon, this );
				trackItem->setGeometry( x, totheight,
										w,
										trackItem->sizeHint().height() );
				_items.append( trackItem );

				totheight += trackItem->sizeHint().height();
				if ( totheight > height() )
				{
					break;
				}
				++nbItems;
			}
			_scroll->setMaximum( _tracklist->count()-nbItems );
			if ( _scroll->maximum() <= 0 )
				_scroll->hide();
			else
			{
				updateScrollbarGeometry();
				_scroll->show();
			}
		}
	}
	void showContextMenu( const QPoint& pos ) override
	{
		QMenu* menu = new QMenu( this );
		QAction* ac_play = menu->addAction( "Play Now" );
		QAction* ac_add = menu->addAction( "Add to Current Playlist" );
		QAction* ac_addToPlaylist = menu->addAction( "Add to a Playlist" );
		QAction* action = menu->exec( pos );
		if ( action == ac_play )
		{
		}
		else if ( action == ac_add )
		{
		}
		else if ( action == ac_addToPlaylist )
		{
		}
	}

signals:
	void currentTracklistChanged( TrackList* tracklist );
	void renamed( const QString& name );
	void deleted();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKSELECTOR_H
