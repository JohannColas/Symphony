#ifndef TRACKWIDGET_H
#define TRACKWIDGET_H
/**********************************************/
#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QMediaPlayer>
#include "../Commons/Metadata.h"
#include "../Commons/Track.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
#include "../Commons/LockButton.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackCover
		: public LockButton
{
	Q_OBJECT

public:
	TrackCover( QWidget* parent = nullptr )
		: LockButton( parent )
	{
		setFlat( true );
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackTitle
		: public QPushButton
{
	Q_OBJECT

public:
	TrackTitle( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setFlat( true );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackArtist
		: public QPushButton
{
	Q_OBJECT

public:
	TrackArtist( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setFlat( true );
	}
};
class TrackA
		: public QPushButton
{
	Q_OBJECT

public:
	TrackA( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setFlat( true );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackWidget
		: public QFrame
{
	Q_OBJECT
protected:
	QMediaPlayer* _player = 0;
	QGridLayout* lay_main = new QGridLayout;
	TrackCover* lb_cover = new TrackCover;
	TrackTitle* lb_title = new TrackTitle;
	TrackArtist* lb_artist = new TrackArtist;
	TrackA* label = new TrackA;
	Icons* _icons = 0;
	//	QLabel* lb_Title = new QLabel;
	//	QLabel* lb_Title = new QLabel;
	//	QLabel* lb_Title = new QLabel;
public:
	~TrackWidget()
	{
		delete lay_main;
		delete lb_title;
		delete lb_artist;
	}
	TrackWidget( QWidget* parent = nullptr )
		: QFrame( parent )
	{
		lay_main->addWidget( lb_cover, 0, 0, Qt::AlignCenter );
		lay_main->addWidget( lb_title, 1, 0, Qt::AlignCenter );
		lay_main->addWidget( lb_artist, 2, 0, Qt::AlignCenter );
		lay_main->addWidget( label, 4, 0, Qt::AlignCenter );
		lay_main->addItem(
					new QSpacerItem( 0,
									 0,
									 QSizePolicy::Preferred,
									 QSizePolicy::Expanding),
					3, 0 );

		lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );

		setLayout( lay_main );
	}

public slots:
	void setPlayer( QMediaPlayer* player )
	{
		_player = player;
		connect( player, SIGNAL(metaDataChanged()),
				 this, SLOT(update()) );
	}
	void setTrack( Track* track )
	{
		Metadata meta( track );
		QPixmap pix = QPixmap();

		if ( pix.convertFromImage( meta.cover() ) )
			lb_cover->setIcon( pix );
		else
		{
			if ( _icons )
			{
				lb_cover->setIcon( _icons->get("track") );
			}
		}

		lb_title->setText( meta.title() );
		lb_artist->setText( meta.artist() );

		QIcon icon( pix );
		label->setIcon( icon );
//		label->setIconSize( {150, 150} );
	}
	void update()
	{
		if ( _player )
		{
			QImage cover = _player->metaData("CoverArtImage").value<QImage>();
			QPixmap pix = QPixmap();
			if ( pix.convertFromImage( cover ) )
				lb_cover->setIcon( pix );
			else
			{
				if ( _icons )
				{
					lb_cover->setIcon( _icons->get("track") );
				}
			}

			lb_title->setText( _player->metaData("Title").toString() );
			lb_artist->setText( _player->metaData("ContributingArtist").toString() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKWIDGET_H
