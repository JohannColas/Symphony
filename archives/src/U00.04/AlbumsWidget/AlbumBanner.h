#ifndef ALBUMBANNER_H
#define ALBUMBANNER_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/Frame.h"
/**********************************************/
#include "../Commons/Album.h"
/**********************************************/
#include "../BasicWidgets/AlbumLabel.h"
#include "../BasicWidgets/ThumbImage.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumBanner
		: public Frame
{
	Q_OBJECT
protected:
	Album* _album = 0;
	ThumbImage* lb_cover = new ThumbImage;
	AlbumLabel* lb_title = new AlbumLabel;
	Label* lb_info = new Label;
	Icons* _icons = 0;

public:
	AlbumBanner( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( lb_cover, 0, 0, 2 );
		addWidget( lb_title, 0, 1 );
		addWidget( lb_info, 1, 1 );
		lb_title->setText("----");
		lb_info->setText("----");

		lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}

public slots:
	void setAlbum( Album* album )
	{
		_album = album;
		update();
	}
	void update()
	{
		if ( _album )
		{
			QString title = _album->title();
			if ( title == "" )
				title = "[UNKNOWN]";
			lb_title->setText( title );
			QPixmap pix = QPixmap();
			if ( pix.convertFromImage( QImage() ) )
				lb_cover->setIcon( pix );
			else
			{
				if ( _icons )
				{
					lb_cover->setIcon( _icons->get("album") );
				}
			}
			QString infos;
			int nbTracks = _album->nbTracks();
			if ( nbTracks == 1 )
				infos = "1 track";
			else
				infos = QString::number( nbTracks ) + " tracks";
			lb_info->setText( infos );
			//
			resize( width(), sizeHint().height() );
			repaint();
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMBANNER_H
