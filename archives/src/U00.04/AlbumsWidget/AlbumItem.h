#ifndef ALBUMITEM_H
#define ALBUMITEM_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/ListItem.h"
/**********************************************/
#include "../Commons/Album.h"
/**********************************************/
#include "../BasicWidgets/AlbumLabel.h"
#include "../BasicWidgets/ArtistLabel.h"
#include "../BasicWidgets/ThumbImage.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumItem
		: public ListItem
{
	Q_OBJECT
protected:
	Album* _album = 0;
	ThumbImage* lb_cover = new ThumbImage;
	AlbumLabel* lb_title = new AlbumLabel;
	//	ArtistLabel* lb_artist = new ArtistLabel;
	Label* lb_info = new Label;
	Icons* _icons = 0;

public:
	AlbumItem( Album* /*album*/, Icons* /*icons*/, QWidget* parent = nullptr )
		: ListItem( parent )
	{
		_type = ListItem::ALBUMITEM;
		show();
		addWidget( lb_cover, 0, 0, 2 );
		addWidget( lb_title, 0, 1 );
		//		addWidget( lb_artist, 1, 1 );
		addWidget( lb_info, 1, 1 );
		lb_title->setText("----");
		lb_info->setText("----");


		lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}
	AlbumItem( QWidget* parent = nullptr )
		: ListItem( parent )
	{
		_type = ListItem::ALBUMITEM;
		show();
		addWidget( lb_cover, 0, 0, 2 );
		addWidget( lb_title, 0, 1 );
		//		addWidget( lb_artist, 1, 1 );
		addWidget( lb_info, 1, 1 );
		lb_title->setText("----");
		lb_info->setText("----");


		lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}
	Album* album()
	{
		return _album;
	}

public slots:
	void setAlbum( Album* album )
	{
		_album = album;
		update();
	}
	void update()
	{
		if ( _album )
		{
			lay_main->setContentsMargins( contentsMargins() );
			QString title = _album->title();
			if ( title == "" )
				title = "[UNKNOWN]";
			lb_title->setText( title );
			QPixmap pix = QPixmap();
			if ( pix.convertFromImage( QImage() ) )
				lb_cover->setIcon( pix );
			else
			{
				if ( _icons )
				{
					lb_cover->setIcon( _icons->get("album") );
				}
			}
			QString infos;
			if ( _album->nbTracks() == 1 )
				infos = "1 track";
			else
				infos = QString::number( _album->nbTracks() ) + " tracks";
			lb_info->setText( infos );

		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}

public slots:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMITEM_H
