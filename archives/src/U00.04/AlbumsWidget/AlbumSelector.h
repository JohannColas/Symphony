#ifndef ALBUMSELECTOR_H
#define ALBUMSELECTOR_H
/**********************************************/
#include "../BasicWidgets/ListWidget.h"
#include "../Commons/Icons.h"
#include "../Commons/TrackList.h"
#include "../Commons/Sorting.h"
#include "../LibrariesWidget/Libraries.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumSelector
		: public ListWidget
{
	Q_OBJECT
private:
	Libraries* _libraries = 0;
	QList<Album*> _albums;
	Icons* _icons = 0;

public:
	AlbumSelector( QWidget *parent = 0 )
		: ListWidget( parent )
	{
		connect( this, &AlbumSelector::indexToChange,
				 this, &AlbumSelector::sendAlbum );
		setMinimumWidth(300);
	}

public slots:
	void sendAlbum( int index )
	{
		if ( isIndexValid(index) )
			emit albumChanged( _items.at(index)->text() );
	}
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
		_albums = _libraries->albums();
		update();
	}
	void update()
	{
		if ( _libraries )
		{
			clear();
			int x = contentsMargins().left();
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();
			int nbItems = 0;
			for ( int it = _begin; it < _albums.size(); ++it )
			{
				QIcon icon = QIcon();
				if ( _icons )
					icon = _icons->get("album");
				ListItem *item = new ListItem( this );
				QString name = _albums.at( it )->title();
				if ( name == "" )
					name = "[UNKNOWN]";
				item->setText( name );
				item->setIcon( icon );
				item->setGeometry( x, totheight,
								   w,
								   item->sizeHint().height() );
				// Adding the Item to AlbumSelector
				addItem( item );

				totheight += item->sizeHint().height();
				if ( totheight > height() )
				{
					break;
				}
				++nbItems;
			}
			updateScrollbar( _albums.size() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}

signals:
	void albumChanged( const QString& album );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMSELECTOR_H
