#ifndef ALBUMTRACKSWIDGET_H
#define ALBUMTRACKSWIDGET_H
#include <QMenu>
/**********************************************/
#include "../BasicWidgets/ListWidget.h"
#include "../Commons/Icons.h"
#include "../Commons/TrackList.h"
#include "../Commons/Sorting.h"
#include "../Commons/Album.h"
/**********************************************/
#include "../BasicWidgets/TrackItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumTracksWidget
		: public ListWidget
{
	Q_OBJECT
private:
	Album* _album = 0;
	Icons* _icons = 0;

public:
	AlbumTracksWidget( QWidget *parent = 0 )
		: ListWidget( parent )
	{
//		connect( this, &AlbumTracksWidget::indexToChange,
//				 this, &AlbumTracksWidget::sendAlbum );
		setMinimumWidth(300);
		connect( this, &AlbumTracksWidget::indexToPlay,
				 this, &AlbumTracksWidget::toPlay );
	}

public slots:
	void sendAlbum( int index )
	{
		qDebug() << _begin << index;
		if ( isIndexValid(index) )
			emit albumChanged( _items.at(index)->text() );
	}
	void setAlbum( Album* album = 0 )
	{
		if ( album )
		{
			_album = album;
			update();
		}
	}
	void update() override
	{
		if ( _album )
		{
			clear();
			int x = contentsMargins().left();
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();
			int nbItems = 0;
			int it = 0;
			for ( Track* track : _album->tracks() )
			{
				++it;
				if ( it >= _begin )
				{
//					Metadata meta(track);
					QString options = "number:cover:||/title/artist/||:--:duration";
					TrackItem* item = new TrackItem(track, options, _icons->get("track"), this);
					// Adding the Item to AlbumTracksWidget
					addItem( item );
					item->setGeometry( x, totheight,
									   w,
									   item->sizeHint().height() );
					totheight += item->sizeHint().height();
					if ( totheight > height() )
					{
						break;
					}
					++nbItems;
				}
			}
			updateScrollbar( _album->nbTracks() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
	//
	void toPlay( int index )
	{
		if ( isIndexValid(index-_begin) )
		{
			TrackItem* item = static_cast<TrackItem*>(_items.at(index-_begin));
			emit trackToPlay( item->track() );
		}
	}
	void showContextMenu( const QPoint& pos ) override
	{
		int index = getIndexByPos( pos );
		TrackItem* item = 0;
		if ( isIndexValid(index) )
		{
			item = static_cast<TrackItem*>( _items.at( index ) );
		}
		QMenu* menu = new QMenu( this );
		QAction* ac_play = menu->addAction( "Play Now" );
		QAction* ac_add = menu->addAction( "Add to Current Playlist" );
		QAction* ac_addToPlaylist = menu->addAction( "Add to a Playlist" );
		QAction* action = menu->exec( this->mapToGlobal(pos) );
		if ( action == ac_play )
		{
			if ( item )
				emit trackToPlay( item->track() );
		}
		else if ( action == ac_add )
		{
			if ( item )
				emit trackToAdd( item->track() );
		}
		else if ( action == ac_addToPlaylist )
		{
			emit tracklistToAdd( _album->toTracklist()->getTracklistByIndexes(_selection) );
		}
	}

signals:
	void albumChanged( const QString& album );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMTRACKSWIDGET_H
