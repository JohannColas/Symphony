#ifndef ALBUMWIDGET_H
#define ALBUMWIDGET_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/Frame.h"
//#include <QLabel>
//#include <QPushButton>
#include "../BasicWidgets/ListWidget.h"
//#include "../BasicWidgets/LockButton.h"
//#include "../Commons/Metadata.h"
//#include "../Commons/Track.h"
/**********************************************/
#include "../AlbumsWidget/AlbumItem.h"
#include "../AlbumsWidget/AlbumBanner.h"
#include "../AlbumsWidget/AlbumTracksWidget.h"
/**********************************************/
//#include "../Commons/TracklistList.h"
#include "../LibrariesWidget/Libraries.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumWidget
		: public Frame
{
	Q_OBJECT
protected:
	Libraries* _libraries = 0;
	Album* _album = 0;
	AlbumBanner* inf_album = new AlbumBanner;
//	ListWidget* wid_tracks = new ListWidget;
	AlbumTracksWidget* wid_tracks = new AlbumTracksWidget;
	Icons* _icons = 0;

public:
	AlbumWidget( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( inf_album, 0, 0 );
		addWidget( wid_tracks, 1, 0 );

//		connect( wid_tracks, &AlbumTracksWidget::trackToPlay,
//				 this, &AlbumWidget::trackToPlay );
//		connect( wid_tracks, &AlbumTracksWidget::tracklistToPlay,
//				 this, &AlbumWidget::tracklistToPlay );
		connectTo( wid_tracks );
	}

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
	}
	void setAlbum( const QString& /*album*/ )
	{
		update();
	}
	void setAlbum( Album* album )
	{
		_album = album;
		inf_album->setAlbum( album );
		wid_tracks->setAlbum( album );
		update();
	}
	void setArtistByIndex( int index )
	{
		QList<Album*> albums = _libraries->albums();
		if ( -1 < index && index < albums.size() )
		{
			setAlbum( albums.at(index) );
		}
	}
	void update()
	{
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		inf_album->updateIcons( icons );
		wid_tracks->updateIcons( icons );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMWIDGET_H
