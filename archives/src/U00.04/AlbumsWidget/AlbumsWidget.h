#ifndef ALBUMSWIDGET_H
#define ALBUMSWIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include "../Commons/TracklistList.h"
#include "../AlbumsWidget/AlbumSelector.h"
#include "../AlbumsWidget/AlbumWidget.h"
#include "../Commons/Lang.h"
#include "../LibrariesWidget/Libraries.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumsWidget
		: public Frame
{
	Q_OBJECT
private:
	Libraries* _libraries = 0;
	AlbumSelector* sel_album = new AlbumSelector;
	AlbumWidget* wid_album = new AlbumWidget;

public:
	AlbumsWidget( QWidget* parent = 0 );

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
		sel_album->setLibraries( _libraries );
		wid_album->setLibraries( _libraries );
		connect( sel_album, &AlbumSelector::indexToChange,
				 wid_album, &AlbumWidget::setArtistByIndex );
	}
	void updateIcons( Icons* icons )
	{
		sel_album->updateIcons( icons );
		wid_album->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMSWIDGET_H
