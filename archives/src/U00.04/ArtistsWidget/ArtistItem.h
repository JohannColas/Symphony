#ifndef ARTISTITEM_H
#define ARTISTITEM_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/Frame.h"
/**********************************************/
#include "../Commons/Artist.h"
/**********************************************/
#include "../BasicWidgets/ArtistLabel.h"
#include "../BasicWidgets/ThumbImage.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistItem
		: public Frame
{
	Q_OBJECT
protected:
	Artist* _artist = 0;
	ThumbImage* lb_cover = new ThumbImage;
	ArtistLabel* lb_name = new ArtistLabel;
	Label* lb_info = new Label;
	Icons* _icons = 0;

public:
	ArtistItem( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( lb_cover, 0, 0, 2 );
		addWidget( lb_name, 0, 1 );
		addWidget( lb_info, 1, 1 );
		lb_name->setText("----");
		lb_info->setText("----");

		lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}

public slots:
	//	void setLibraries( Libraries* libraries = 0 )
	//	{
	//		_libraries = libraries;
	//	}
	void setArtist( Artist* artist )
	{
		_artist = artist;
		update();
	}
	//	void setArtistByIndex( int index )
	//	{
	//		if ( -1 < index && index < _libraries->artists().size() )
	//		{
	//			setArtist( _libraries->artists().at(index) );
	//		}
	//	}
	void update()
	{
		if ( _artist )
		{
			QString name = _artist->name();
			if ( name == "" )
				name = "[UNKNOWN]";
			lb_name->setText( name );
			QPixmap pix = QPixmap();
			if ( pix.convertFromImage( QImage() ) )
				lb_cover->setIcon( pix );
			else
			{
				if ( _icons )
				{
					lb_cover->setIcon( _icons->get("artist") );
				}
			}
			QString infos;
			if ( _artist->nbAlbums() == 1 )
				infos = "1 album - ";
			else
				infos = QString::number( _artist->nbAlbums() ) + " albums - ";
			int nbTracks = 0;
			for ( Album* album : _artist->albums() )
				nbTracks += album->nbTracks();
			if ( nbTracks == 1 )
				infos += "1 track";
			else
				infos += QString::number( nbTracks ) + " tracks";
			lb_info->setText( infos );
			//
			resize( width(), sizeHint().height() );
			repaint();
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTITEM_H
