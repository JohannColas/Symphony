#ifndef ARTISTTRACKSWIDGET_H
#define ARTISTTRACKSWIDGET_H
#include <QMenu>
/**********************************************/
#include "../BasicWidgets/ListWidget.h"
#include "../Commons/Icons.h"
#include "../Commons/TrackList.h"
#include "../Commons/TracklistList.h"
#include "../Commons/Sorting.h"
#include "../Commons/Artist.h"
/**********************************************/
#include "../AlbumsWidget/AlbumItem.h"
#include "../BasicWidgets/TrackItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistTracksWidget
		: public ListWidget
{
	Q_OBJECT
private:
	Artist* _artist = 0;
	TracklistList* _playlists = 0;
	Icons* _icons = 0;

public:
	ArtistTracksWidget( QWidget *parent = 0 )
		: ListWidget( parent )
	{
		setMinimumWidth(300);
		connect( this, &ArtistTracksWidget::indexToPlay,
				 this, &ArtistTracksWidget::toPlay );
	}
	void setPlaylists( TracklistList* playlists )
	{
		_playlists = playlists;
	}

public slots:
	void setArtist( Artist* artist = 0 )
	{
		if ( artist )
		{
			_artist = artist;
			update();
		}
	}
	void update() override
	{
		if ( _artist )
		{
			clear();
			int x = contentsMargins().left();
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();
			int nbItems = 0;
			int it = 0;
			for ( Album* album : _artist->albums() )
			{
				++it;
				if ( it >= _begin )
				{
					AlbumItem* it_Album = new AlbumItem( this );
					it_Album->updateIcons( _icons );
					it_Album->setAlbum( album );
					//
					addItem( it_Album );
					it_Album->setGeometry( x, totheight,
									   w,
									   it_Album->sizeHint().height() );
					totheight += it_Album->sizeHint().height();
					if ( totheight > height() )
					{
						break;
					}
					++nbItems;
				}
				for ( Track* track : album->tracks() )
				{
					++it;
					if ( it >= _begin )
					{
//						Metadata meta(track);
						QString options = "number:title:--:duration";
						TrackItem* item = new TrackItem(track, options, _icons->get("track"), this);
						addItem( item );
						// Adding the Item to ArtistTracksSelector
						item->setGeometry( x, totheight,
										   w,
										   item->sizeHint().height() );
						totheight += item->sizeHint().height();
						if ( totheight > height() )
						{
							break;
						}
						++nbItems;
					}
				}
			}
			updateScrollbar( _artist->nbLines() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
	//
	void toPlay( int index )
	{
		if ( isIndexValid(index-_begin) )
		{
			if ( _items.at(index-_begin)->type() == ListItem::ALBUMITEM )
			{
				AlbumItem* item = static_cast<AlbumItem*>(_items.at(index-_begin));
				emit tracklistToPlay( item->album()->toTracklist() );
			}
			else
			{
				TrackItem* item = static_cast<TrackItem*>(_items.at(index-_begin));
				emit trackToPlay( item->track() );
			}
		}
	}
	//
	void showContextMenu( const QPoint& pos ) override
	{
		int index = getIndexByPos( pos );
//		TrackItem* item = 0;
//		if ( isIndexValid(index) )
		{
//			item = static_cast<TrackItem*>( _items.at( index ) );
			QMenu* menu = new QMenu( this );
			QAction* ac_play = menu->addAction( "Play Now" );
			QAction* ac_add = menu->addAction( "Add to Current Playlist" );
			QMenu* ac_addToPlaylist = menu->addMenu( "Add to a Playlist" );
			QList<QAction*> playlists;
			for ( TrackList* elm : _playlists->list() )
			{
				QAction* playlist = ac_addToPlaylist->addAction( elm->name() );
				playlists.append( playlist );
			}
			QAction* action = menu->exec( this->mapToGlobal(pos) );
			if ( action == ac_play )
			{
				emitPlayOrAdd( index, true );
			}
			else if ( action == ac_add )
			{
				emitPlayOrAdd( index, false );
			}
			for ( int it = 0; it < playlists.size(); ++it )
			{
				if ( action == playlists.at(it) )
				{
					QList<Track*> list;
					int lt = 0;
					for ( Album* album : _artist->albums() )
					{
						++lt;
						bool addAll = false;
						if ( _selection.contains(lt) )
							addAll = true;
						for ( Track* track : album->tracks() )
						{
							++lt;
							if ( _selection.contains(lt)
								 || addAll )
							{
								list.append( track );
							}
						}
					}
					TrackList* tltmp = _playlists->tracklist(it);
					if ( tltmp )
						tltmp->add(list);
					break;
				}
			}
		}
	}
	void emitPlayOrAdd( int index, bool toPlay = false )
	{
		if ( isIndexValid(index) )
		{
			ListItem::Type type = _items.at(index)->type();
			if ( type == ListItem::ALBUMITEM )
			{
				AlbumItem* item = static_cast<AlbumItem*>(_items.at(index));
				if ( toPlay )
					emit tracklistToPlay( item->album()->toTracklist() );
				else
					emit tracklistToAdd( item->album()->toTracklist() );
			}
			else if ( type == ListItem::TRACKITEM )
			{
				TrackItem* item = static_cast<TrackItem*>(_items.at(index));
				if ( toPlay )
					emit trackToPlay( item->track() );
				else
					emit trackToAdd( item->track() );
			}
		}
	}

signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTTRACKSWIDGET_H
