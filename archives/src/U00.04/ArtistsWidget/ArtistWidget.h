#ifndef ARTISTWIDGET_H
#define ARTISTWIDGET_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/Frame.h"
//#include <QLabel>
//#include <QPushButton>
//#include "../BasicWidgets/ListWidget.h"
//#include "../BasicWidgets/LockButton.h"
//#include "../Commons/Metadata.h"
//#include "../Commons/Track.h"
/**********************************************/
#include "../ArtistsWidget/ArtistItem.h"
#include "../ArtistsWidget/ArtistBanner.h"
#include "../ArtistsWidget/ArtistTracksWidget.h"
/**********************************************/
//#include "../Commons/TracklistList.h"
#include "../LibrariesWidget/Libraries.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistWidget
		: public Frame
{
	Q_OBJECT
protected:
	Libraries* _libraries = 0;
	Artist* _artist = 0;
	ArtistBanner* inf_artist = new ArtistBanner;
//	ListWidget* wid_tracks = new ListWidget;
	ArtistTracksWidget* wid_tracks = new ArtistTracksWidget;
	Icons* _icons = 0;

public:
	ArtistWidget( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( inf_artist, 0, 0 );
		addWidget( wid_tracks, 1, 0 );
		connectTo( wid_tracks );
	}
	void setPlaylists( TracklistList* playlists )
	{
		wid_tracks->setPlaylists( playlists );
	}

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
	}
	void setArtist( const QString& /*artist*/ )
	{
		update();
	}
	void setArtist( Artist* artist )
	{
		_artist = artist;
		inf_artist->setArtist( artist );
		wid_tracks->setArtist( artist );
		update();
	}
	void setArtistByIndex( int index )
	{
		if ( -1 < index && index < _libraries->artists().size() )
		{
			setArtist( _libraries->artists().at(index) );
		}
	}
	void update()
	{
//		wid_tracks->setStyleSheet("background:red;");
//		wid_tracks->clear();
//		int x = wid_tracks->contentsMargins().left();
//		int w = wid_tracks->width()
//				- wid_tracks->contentsMargins().left()
//				- wid_tracks->contentsMargins().right();
//		int totheight = wid_tracks->contentsMargins().top();
//		for ( Album* album : _artist->albums() )
//		{
//			ListItem* item = new ListItem(wid_tracks);
//			QString title = album->title();
//			if ( title == "" )
//				title = "[UNKNOWN]";
//			item->setText( title );
//			wid_tracks->addItem( item );
//			item->setGeometry( x, totheight,
//									w,
//									item->sizeHint().height() );
//			totheight += item->sizeHint().height();
//			if ( totheight > wid_tracks->height() )
//			{
//				break;
//			}
//			for ( Track* track : album->tracks() )
//			{
//				ListItem* item = new ListItem(wid_tracks);
//				QString title = track->title();
//				if ( title == "" )
//					title = "[UNKNOWN]";
//				item->setText( title );
//				wid_tracks->addItem( item );
//				item->setGeometry( x, totheight,
//										w,
//										item->sizeHint().height() );
//				totheight += item->sizeHint().height();
//				if ( totheight > wid_tracks->height() )
//				{
//					break;
//				}
//			}
//		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		inf_artist->updateIcons( icons );
		wid_tracks->updateIcons( icons );
	}
	//
//	void sendTrackToPlay( int index )
//	{
//		if ( index != -1 )
//			emit toPlay( wid_tracks->track(index) );
//	}

signals:
	void trackToPlay( Track* track );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTWIDGET_H
