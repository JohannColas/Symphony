#ifndef ARTISTLABEL_H
#define ARTISTLABEL_H
/**********************************************/
#include "../BasicWidgets/Label.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistLabel
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	ArtistLabel( QWidget* parent = 0 )
		: Label( parent )
	{
	}
	ArtistLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTLABEL_H
