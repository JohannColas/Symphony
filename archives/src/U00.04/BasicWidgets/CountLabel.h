#ifndef COUNTLABEL_H
#define COUNTLABEL_H
/**********************************************/
#include "../BasicWidgets/Label.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CountLabel
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	CountLabel( QWidget* parent = 0 )
		: Label( parent )
	{
	}
	CountLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
	}
	CountLabel( int count, QWidget* parent = 0 )
		: Label( parent )
	{
		setText( QString::number( count ) );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COUNTLABEL_H
