#ifndef COVERIMAGE_H
#define COVERIMAGE_H
/**********************************************/
#include "../BasicWidgets/LockButton.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CoverImage
		: public LockButton
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	CoverImage( QWidget* parent = 0 )
		: LockButton( parent )
	{
		setFlat( true );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setPixmap( const QPixmap& pix )
	{
		setIcon( pix );
	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COVERIMAGE_H
