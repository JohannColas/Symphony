#ifndef FRAME_H
#define FRAME_H
/**********************************************/
#include <QFrame>
#include <QStyle>
#include "../BasicWidgets/Layouts.h"
#include "../Commons/TrackList.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Frame
		: public QFrame {
	Q_OBJECT
protected:
	GridLayout* lay_main = new GridLayout;
	QList<QWidget*> _widgets;
	QList<QSpacerItem*> _spacers;

public:
	Frame( QWidget* parent = 0 )
		: QFrame( parent )
	{
		hide();
		setLayout( lay_main );
	}
	void addWidget( QWidget* widget,
					int row, int col,
					int rowSpan = 1, int colSpan = 1 )
	{
		lay_main->addWidget( widget, row, col, rowSpan, colSpan );
		_widgets.append( widget );
	}
	void addLayout( QLayout* layout,
					int row, int col,
					int rowSpan = 1, int colSpan = 1 )
	{
		lay_main->addLayout( layout, row, col, rowSpan, colSpan );
//		_widgets.append( widget );
	}
	void addSpace( int size, int row, int col, bool isVertical = true, int rowSpan = 1, int colSpan = 1 )
	{
		QSpacerItem* spacer;
		if ( isVertical )
			spacer  = new QSpacerItem( 0, size );
		else
			spacer  = new QSpacerItem( size, 0 );
		lay_main->addItem( spacer, row, col, rowSpan, colSpan );
		_spacers.append( spacer );
	}
	void setAlignment( QWidget* widget, Qt::Alignment align = Qt::AlignLeft )
	{
		lay_main->setAlignment( widget, align );
	}
	void removeWidget( QWidget* widget )
	{
		lay_main->removeWidget( widget );
	}
	void removeWidgets()
	{
		for ( QWidget* wid : _widgets )
		{
			removeWidget( wid );
			delete wid;
		}
		_widgets.clear();
	}
	void removeSpacers()
	{
		for ( QSpacerItem* spacer : _spacers )
		{
			lay_main->removeItem( spacer );
			delete spacer;
		}
		_spacers.clear();
	}
	void addSpacer( int row, int col, bool isVertical = true, int rowSpan = 1, int colSpan = 1 )
	{
		QSpacerItem* spacer;
		if ( isVertical )
			spacer  = new QSpacerItem( 0, 0, QSizePolicy::Maximum, QSizePolicy::Expanding );
		else
			spacer  = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Maximum );
		lay_main->addItem( spacer, row, col, rowSpan, colSpan );
		_spacers.append( spacer );
	}
	void connectTo( Frame* frame, bool isFrame = true )
	{
		connect( frame, &Frame::trackToPlay,
				 this, &Frame::trackToPlay );
		connect( frame, &Frame::trackToAdd,
				 this, &Frame::trackToAdd );
		if ( isFrame )
		{
			connect( frame, &Frame::tracklistToPlay,
					 this, &Frame::tracklistToPlay );
			connect( frame, &Frame::tracklistToAdd,
					 this, &Frame::tracklistToAdd );
		}
	}


signals:
	void trackToPlay( Track* track );
	void trackToAdd( Track* track );
	void tracklistToPlay( TrackList* tracklist);
	void tracklistToAdd( TrackList* tracklist);
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FRAME_H
