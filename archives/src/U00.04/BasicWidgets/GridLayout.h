//#ifndef GRIDLAYOUT_H
//#define GRIDLAYOUT_H
///**********************************************/
//#include <QGridLayout>
///**********************************************/
///**********************************************/
///**********************************************/
///*
// *
// * */
//class GridLayout
//		: public QGridLayout
//{
//	Q_OBJECT
//public:
//	GridLayout( QWidget* parent = nullptr )
//		: QGridLayout( parent )
//	{
//		setMargin( 0 );
//		setSpacing( 0 );
//	}
//	void addSpacer( int row, int col,
//					bool isVertical = true,
//					int rowSpan = 1, int colSpan = 1 )
//	{
//		QSpacerItem* spacer = 0;
//		if ( isVertical )
//			spacer = new QSpacerItem( 0, 0,
//									  QSizePolicy::Maximum,
//									  QSizePolicy::Expanding );
//		else
//			spacer = new QSpacerItem( 0, 0,
//									  QSizePolicy::Expanding,
//									  QSizePolicy::Maximum );
//		if ( spacer )
//			addItem( spacer, row, col, rowSpan, colSpan );
//	}
//	void addSpacer( int row, int col,
//					int width = 0, int height = 0,
//					int rowSpan = 1, int colSpan = 1  )
//	{
//		QSpacerItem* spacer = new QSpacerItem( width, height,
//									  QSizePolicy::Maximum,
//									  QSizePolicy::Maximum );
//		if ( spacer )
//			addItem( spacer, row, col, rowSpan, colSpan );
//	}
//};
///**********************************************/
///**********************************************/
///**********************************************/
//#endif // GRIDLAYOUT_H
