#ifndef LIBRARYLABEL_H
#define LIBRARYLABEL_H
/**********************************************/
#include <QLineEdit>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LibraryLabel
		: public QLineEdit
{
	Q_OBJECT
protected:
	/* ------------------------ */
	/* ------------------------ */
public:
	LibraryLabel( QWidget* parent = 0 )
		: QLineEdit( parent )
	{
	}
	LibraryLabel( const QString& text, QWidget* parent = 0 )
		: QLineEdit( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARYLABEL_H
