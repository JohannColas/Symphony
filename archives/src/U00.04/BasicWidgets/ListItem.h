#ifndef LISTITEM_H
#define LISTITEM_H
/**********************************************/
#include <QPushButton>
#include <QMouseEvent>
#include <QStyleOptionButton>
#include "../BasicWidgets/Frame.h"
#include "../BasicWidgets/Layouts.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ListItem
		: public QPushButton
//		: public Frame
{
	Q_OBJECT
public:
	enum Type
	{
		UNKNOWNITEM,
		TRACKITEM,
		ARTISTITEM,
		ALBUMITEM
	};
protected:
	GridLayout* lay_main = new GridLayout;
	Type _type = Type::UNKNOWNITEM;

public:
	ListItem( QWidget* parent = 0 )
		: QPushButton( parent )
	{
		show();
		setLayout( lay_main );
		setFlat( true );
		setCheckable( true );

		setSizePolicy( QSizePolicy::Preferred,
					   QSizePolicy::Maximum );
	}
	void addWidget( QWidget* widget,
					int row, int col,
					int rowSpan = 1, int colSpan = 1 )
	{
		lay_main->addWidget( widget, row, col, rowSpan, colSpan );
	}
	void mousePressEvent( QMouseEvent* event ) override
	{
		if ( event->button() == Qt::LeftButton )
			setChecked( !isChecked() );
		event->ignore();
	}
	void enterEvent( QEvent* event ) override
	{
		event->ignore();
	}
	void leaveEvent( QEvent* event ) override
	{
		event->ignore();
	}
	void paintEvent( QPaintEvent* event ) override
	{
		  QPushButton::paintEvent(event);
		  QStyleOptionButton option;
		  option.initFrom(this);
		  QRect contRec = style()->subElementRect(QStyle::SE_PushButtonFocusRect, &option, this);
		  QRect rec = rect();
		  int left = contRec.x();
		  int right = rec.width() - left - contRec.width();
		  int top = contRec.y();
		  int bottom = rec.height() - top - contRec.height();
		  setContentsMargins( left, top, right, bottom );
	}
	void setType( ListItem::Type type )
	{
		_type = type;
	}
	ListItem::Type type()
	{
		return _type;
	}

signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LISTITEM_H
