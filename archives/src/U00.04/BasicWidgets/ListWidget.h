#ifndef LISTWIDGET_H
#define LISTWIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include <QListWidget>
#include "../BasicWidgets/ListItem.h"
#include <QScrollBar>
#include <QWheelEvent>
#include "../Commons/Icons.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ListWidget
		: public Frame
{
	Q_OBJECT
protected:
	Icons* _icons = 0;
	Frame* _container = new Frame;
	int _nbItems = 0;
	int _elementSize = 0;
	QList<QWidget*> _sections;
	QList<ListItem*> _items;
	QScrollBar* _scroll = new QScrollBar(this);
	int _begin = 0;
	int _index = -1;
	QList<int> _selection;
	bool _avoidRightClick = false;
	bool _multipleSelectable = true;


public:
	ListWidget( QWidget* parent = 0 )
		: Frame( parent )
	{
		// Set Widget
		show();
		setMinimumWidth( 250 );
		installEventFilter( this );

		// Settings ScrollBar
		_scroll->setOrientation( Qt::Vertical );
		_scroll->hide();

		// Connections
		connect( _scroll, &QScrollBar::valueChanged,
				 this, &ListWidget::onScrollbarChanged );
		// Context Menu
		setContextMenuPolicy( Qt::CustomContextMenu );
		setSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Preferred );
	}
	int count()
	{
		return _items.size();
	}
	int index()
	{
		return _index;
	}
	bool isIndexValid( int index )
	{
		return ( _begin-1 < index && index < _begin+count() );
	}

public slots:
	void allowRightClick()
	{
		_avoidRightClick = false;
	}
	void avoidRightClick()
	{
		_avoidRightClick = true;
	}
	void allowMultipleSelection()
	{
		_multipleSelectable = true;
	}
	void avoidMultipleSelection()
	{
		_multipleSelectable = false;
	}
	void setBegin( int index )
	{
		_begin = index;
	}
	void onScrollbarChanged( int index )
	{
		setBegin( index );
		update();
	}
	virtual void showContextMenu( const QPoint& /*pos*/ )
	{

	}
	void clear()
	{
		for ( QWidget* section : _sections )
			delete section;
		_sections.clear();
		for ( ListItem* item : _items )
			delete item;
		_items.clear();
		_container->removeSpacers();
		_nbItems = 0;
	}
	virtual void addItem( ListItem* item = 0 )
	{
		_items.append( item );
		++_nbItems;
	}
	void addSpacer()
	{
		_container->addSpacer( _nbItems, 0 );
	}
	virtual void update()
	{
	}
	void updateScrollbar()
	{
		_scroll->setValue( _begin );
	}
	void updateScrollbar( int size )
	{
		_scroll->blockSignals( true );
		int nbItems = this->count();
		if ( nbItems == size )
		{
			_scroll->setMaximum(0);
			_scroll->hide();
		}
		else
		{
			if ( _begin + nbItems + 2 < size )
				_scroll->setMaximum( size-nbItems+2 );
			updateScrollbarGeometry();
			_scroll->show();
		}
		_scroll->blockSignals( false );
	}
	void updateScrollbarGeometry()
	{
		int x = width() - contentsMargins().right() - _scroll->sizeHint().width();
		int y = contentsMargins().top();
		int h = height() - contentsMargins().top() - contentsMargins().bottom();
		_scroll->setGeometry( x, y, _scroll->sizeHint().width(), h );
		_scroll->raise();
	}
	bool eventFilter( QObject* obj, QEvent* event ) override
	{
		// Mouse Wheel Event
		if ( event->type() == QEvent::Wheel )
		{
//			qDebug().noquote() << "Wheel Event";
			QWheelEvent* wheel = static_cast<QWheelEvent*>(event);
			int index = _begin - ( 45*wheel->delta()/360 );
			if ( index < 0 )
				index = 0;
			else if ( index > _scroll->maximum() )
				index = _scroll->maximum();

			setBegin( index );
			if ( _scroll->isVisible() )
			{
				_scroll->blockSignals( true );
				_scroll->setValue( index );
				_scroll->blockSignals( false );
			}
			update();
			checkItems();
		}
		// Mouse Press Event
		else if ( event->type() == QEvent::MouseButtonPress )
		{
//			qDebug().noquote() << "Press Event";
			QMouseEvent* mouse = static_cast<QMouseEvent*>(event);
			if ( mouse->button() == Qt::LeftButton )
			{
				if ( mouse->modifiers() == Qt::ControlModifier &&
					 _multipleSelectable )
				{
					_index = getIndexByPos(mouse->pos());
//					qDebug().noquote() << "   CTRL Modifiers - Mouse pos : " << mouse->pos();
					addToSelection( _index );
					emit indexToAdd( _index );
				}
				else if ( mouse->modifiers() == Qt::ShiftModifier &&
						  _multipleSelectable &&
						  _index != -1 )
				{
//					qDebug().noquote() << "   SHIFT Modifiers - Mouse pos : " << mouse->globalPos();
					int newIndex = getIndexByPos( mouse->pos() );
//					setCheckedItems( _index, newIndex, true );
					addToSelection( _index, newIndex );
					emit indexesToAdd( _index, newIndex );
					_index = newIndex;
				}
				else
				{
					_index = getIndexByPos( mouse->pos() );
					_selection.clear();
					addToSelection( _index );
//					setItemsChecked( false, _index );
					if ( _index != -1 )
						emit indexToChange( _index );
					else
						emit toReset();
				}
				checkItems();
			}
			else if ( mouse->button() == Qt::RightButton )
			{

			}
		}
		// Mouse Release Event
		else if ( event->type() == QEvent::MouseButtonRelease )
		{
//			qDebug().noquote() << "Release Event";
			QMouseEvent* mouse = static_cast<QMouseEvent*>(event);
			if ( mouse->button() == Qt::RightButton &&
				 !_avoidRightClick )
			{
				showContextMenu( mouse->pos() );
//				qDebug().noquote() << "   Mouse pos : " << mouse->globalPos();
			}
		}
		// Mouse Double Clicked Event
		else if ( event->type() == QEvent::MouseButtonDblClick )
		{
//			qDebug().noquote() << "Double Clicked Event";
			QMouseEvent* mouse = static_cast<QMouseEvent*>(event);
			if ( mouse->button() == Qt::LeftButton )
			{
				if ( index() != -1 )
					emit indexToPlay(  index() );
			}
		}
		else if ( event->type() == QEvent::Resize )
		{
			update();
		}
		return Frame::eventFilter( obj, event );
	}
	ListItem* getItemByPos( const QPoint& pos )
	{
		int index = getIndexByPos( pos )-_begin;
		return ( index == -1 ) ? 0 : _items.at(index);
	}
	int getIndexByPos( const QPoint& pos )
	{
		for ( int it = 0; it < count(); ++it )
		{
			if ( _items.at(it)->geometry().contains( pos ) )
			{
				return _begin+it;
			}
		}
		return -1;
	}
	void setItemsChecked( bool checked = false, int except = -1 )
	{
		for ( int it = 0; it < count(); ++it )
		{
			if ( it != except )
				_items.at(it)->setChecked( checked );
		}
	}
	void setCheckedItems( int begin, int end, bool checked = false )
	{
		if ( isIndexValid(begin) && isIndexValid(end) )
		{
			int itBeg = begin, itEnd = end;
			if ( itBeg > itEnd )
			{
				itBeg = end;
				itEnd = begin;
			}
			for ( int it = itBeg; it <= itEnd; ++it )
			{
				_items.at(it)->setChecked( checked );
			}
		}
	}
	void addToSelection( int index, int indexTo = -1 )
	{
		if ( indexTo == -1 )
		{
			if ( !_selection.contains( index) )
				_selection.append( index );
		}
		else
		{
			int itBeg = index, itEnd = indexTo;
			if ( itBeg > itEnd )
			{
				itBeg = indexTo;
				itEnd = index;
			}
			for ( int it = itBeg; it < itEnd+1; ++it )
			{
				addToSelection( it );
			}
		}
	}
	void checkItems()
	{
		for ( int it = 0; it < count(); ++it )
		{
			if ( _selection.contains( _begin+it ) )
				_items.at(it)->setChecked( true );
			else
				_items.at(it)->setChecked( false );
		}
	}

signals:
	void indexChanged( int index );
	void indexToChange( int index );
	void indexToAdd( int index );
	void indexesToAdd( int begin, int end );
	void indexToPlay( int index );
	void toReset();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LISTWIDGET_H
