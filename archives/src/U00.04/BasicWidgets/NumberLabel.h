#ifndef NUMBERLABEL_H
#define NUMBERLABEL_H
/**********************************************/
#include "../BasicWidgets/Label.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class NumberLabel
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	NumberLabel( QWidget* parent = 0 )
		: Label( parent )
	{
	}
	NumberLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
	}
	NumberLabel( int number, QWidget* parent = 0 )
		: Label( parent )
	{
		if ( number > 0 )
			setText( QString::number(number) );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // NUMBERLABEL_H
