#ifndef PATHBUTTON_H
#define PATHBUTTON_H
/**********************************************/
#include <QPushButton>
#include <QFileDialog>
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PathButton
		: public QPushButton
{
	Q_OBJECT
protected:
	QString _text = "";

public:
	PathButton( QWidget* parent = 0 )
		: QPushButton( parent )
	{
		setFlat( true );
		setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
		connect( this, &QPushButton::clicked,
				 this, &PathButton::selectPath );
	}
	QString text() const
	{
		return _text;
	}

public slots:
	void setText( const QString& text )
	{
		_text = text;
		QPushButton::setText( _text );
	}
	void selectPath()
	{
		QString path = text();
		if ( !QDir(path).exists() )
			path = QDir::homePath();
		QString newpath = QFileDialog::getExistingDirectory(
							  this,
							  tr("Open Directory"),
							  path,
							  QFileDialog::ShowDirsOnly |
							  QFileDialog::DontResolveSymlinks
							  );
		if ( newpath != "" )
		{
			setText( newpath );
			emit pathChanged( newpath );
		}
	}
	void update()
	{
		if ( sizeHint().width() > width() )
		{
			QFontMetrics metrix( font() );
			int w = width() - 30;
			QString clippedText = metrix.elidedText( _text, Qt::ElideMiddle, w );
			QPushButton::setText( clippedText );
		}
	}
	void paintEvent( QPaintEvent* event ) override
	{
		update();
//		qDebug() << text() << width() << sizeHint().width();
		QPushButton::paintEvent( event );
	}

signals:
	void pathChanged( const QString& path );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PATHBUTTON_H
