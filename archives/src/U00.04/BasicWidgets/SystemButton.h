#ifndef SYSTEMBUTTON_H
#define SYSTEMBUTTON_H
/**********************************************/
#include <QPushButton>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SystemButton
		: public QPushButton
{
	Q_OBJECT

public:
	SystemButton( QWidget* parent = 0 )
		: QPushButton( parent )
	{
		setFlat( true );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SYSTEMBUTTON_H
