#ifndef TRACKITEM_H
#define TRACKITEM_H
/**********************************************/
#include "../BasicWidgets/ListItem.h"
#include "../BasicWidgets/CoverImage.h"
#include "../BasicWidgets/TitleLabel.h"
#include "../BasicWidgets/ArtistLabel.h"
#include "../BasicWidgets/AlbumLabel.h"
#include "../BasicWidgets/GenreLabel.h"
#include "../BasicWidgets/DurationLabel.h"
#include "../BasicWidgets/NumberLabel.h"
#include "../Commons/Track.h"
#include "../Commons/Metadata.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackItem
		: public ListItem
{
	Q_OBJECT
protected:
	QString _options = "";
	Track* _track = 0;
	/* ------------------------ */
	/* ------------------------ */
public:
	TrackItem( Track* track, const QString& _options, const QIcon& icon, QWidget* parent = 0 )
		: ListItem( parent )
	{
		_type = ListItem::TRACKITEM;
		_track = track;
		Metadata meta(track);
		QStringList colOpts = _options.split(':');
		int itCol = 0;
		for ( QString col : colOpts )
		{
			int itRow = 0;
			if ( col == "--" )
			{
				lay_main->addSpacer( itRow, itCol, false );
			}
			else
			{
				QStringList rowOpts = col.split('/');
				GridLayout* lay_col = new GridLayout;
				for ( QString row : rowOpts )
				{
					if ( row == "||" )
					{
						lay_col->addSpacer( itRow, 0, true );
					}
					else if ( row == "title" )
					{
						lay_col->addWidget( new TitleLabel( meta.title() ), itRow, 0 );
					}
					else if ( row == "artist" )
					{
						lay_col->addWidget( new ArtistLabel( meta.artist() ), itRow, 0 );
					}
					else if ( row == "album" )
					{
						lay_col->addWidget( new AlbumLabel( meta.album() ), itRow, 0 );
					}
					else if ( row == "Genre" )
					{
						lay_col->addWidget( new GenreLabel( meta.genre() ), itRow, 0 );
					}
					else if ( row == "number" )
					{

						lay_col->addWidget( new NumberLabel( meta.number() ), itRow, 0 );
					}
					else if ( row == "cover" )
					{
						CoverImage* cover = new CoverImage;
						QPixmap pix = QPixmap();
						if ( pix.convertFromImage( meta.cover() ) )
							cover->setPixmap( pix );
						else
						{
							cover->setIcon( icon );
						}
						lay_col->addWidget( cover, itRow, 0 );
					}
					else if ( row == "duration" )
					{
						int dur = meta.duration();
						if ( dur == -1 )
							dur = 0;
						lay_col->addWidget( new DurationLabel( dur ), itRow, 0 );
					}
					++itRow;
				}
				lay_main->addLayout( lay_col, 0, itCol );
			}
			++itCol;
		}
	}
	Track* track()
	{
		return _track;
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setOptions( const QString& options )
	{
		_options = options;
	}
	void update()
	{

	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKITEM_H
