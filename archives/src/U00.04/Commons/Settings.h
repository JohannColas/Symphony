#ifndef SETTINGS_H
#define SETTINGS_H
/**********************************************/
#include <QFile>
#include <QIcon>
#include <QSettings>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Settings
		: public QObject
{
	Q_OBJECT
private:
	QString _path = "./settings.ini";
	QString _icons = "";

public:
	Settings()
	{

	}
	void readSettings()
	{
		QSettings setsFile( _path, QSettings::IniFormat );
//		setXXXX( setsFile->value("xxxx").toYYY() );
		_icons = setsFile.value("iconsPack").toString();
	}
	void saveSettings()
	{
		QFile file( _path );
		file.remove();

		QSettings setsFile( _path, QSettings::IniFormat );
//		setsFile.setValue("xxxx", getXXXX() );
//		setsFile->beginGroup("curves");
//		setsFile->beginWriteArray("curve");
//		for ( int it = 0; it < curvesSettings.size(); ++it ) {
//			setsFile->setArrayIndex(it);
//			curvesSettings.at(it)->saveSettings( setsFile );
//		}
//		setsFile->endArray();
//		setsFile->endGroup();
	}
	QIcon icon( const QString& name )
	{
		QIcon icontmp;
		if ( _icons == "" )
			icontmp = QIcon( "./icons/default/" + name + ".svg" );
		else
			icontmp = QIcon( "./icons/" + _icons + "/" + name + ".svg" );
		return icontmp;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGS_H
