#ifndef SORTING_H
#define SORTING_H
/**********************************************/
#include <QStringList>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
struct Sorting
{
	static inline void sortAlpha( QStringList& list )
	{
		std::sort( list.begin(), list.end(), compareNames );
	}
	static inline bool compareNames( const QString& s1, const QString& s2 )
	{
		return ( clean(s1) < clean(s2) );
	}
	static inline QString clean( const QString& str )
	{
		QString temp = str;
		if ( temp.left(4) == "The " )
			temp = temp.mid( 4 );
		temp.replace("É", "E").replace("é", "e");
		return temp.toLower();
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SORTING_H
