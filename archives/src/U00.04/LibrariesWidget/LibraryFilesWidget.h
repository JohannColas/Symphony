#ifndef LIBRARYFILESWIDGET_H
#define LIBRARYFILESWIDGET_H
/**********************************************/
#include <QInputDialog>
#include <QListWidget>
#include <QMenu>
#include "../BasicWidgets/ListWidget.h"
#include "../Commons/Icons.h"
#include "../LibrariesWidget/Libraries.h"
#include "../BasicWidgets/TrackItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class FileSelector
		: public ListWidget
{
	Q_OBJECT
private:
	Icons* _icons = 0;
	Library* _library = 0;
	QStringList _filesFilters = {"*.mp3", "*.ogg", ".wav", ".flac", ".m4a", ".wma"};
	QStringList _files;

public:
	FileSelector( QWidget* parent = nullptr )
		: ListWidget( parent )
	{
		//		setMaximumWidth( 400 );
		//		setStyleSheet( "background:blue;");
	}
	Track* currentTrack()
	{
		//		if ( _tracklist && index() != -1 )
		//			return _tracklist->track( index() );
		return 0;
	}

public slots:
	void reset()
	{
		_begin = 0;
		getFiles();
		update();
	}
	void setLibrary( Library* library = 0 )
	{
		_library = library;
		//		connect( _libraries, &Libraries::changed,
		//				 this, &FileSelector::update );
		getFiles();
	}
	void getFiles()
	{
		_files.clear();
		QDir dir( _library->path() );
		dir.setSorting( QDir::SortFlag::Name |
						QDir::IgnoreCase |
						QDir::LocaleAware );
		dir.setFilter( QDir::Files );
		dir.setNameFilters( _filesFilters );
		_files = dir.entryList();
		update();
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		update();
	}
	void update() override
	{
		clear();
		int x = contentsMargins().left();
		int w = width() - contentsMargins().left() - contentsMargins().right();
		int totheight = contentsMargins().top();
		int nbItems = 0;
		for ( int it = _begin; it < _files.size(); ++it )
		{
			ListItem *item = new ListItem( this );
			item->setGeometry( x, totheight,
									w,
									item->sizeHint().height() );
			item->setText( _files.at(it) );
			// Adding the Item to LibraryFilesWidget
			addItem( item );

			totheight += item->sizeHint().height();
			if ( totheight > height() )
			{
				break;
			}
			++nbItems;
		}
		updateScrollbar( _files.size() );
	}
	void showContextMenu( const QPoint& pos ) override
	{
		QMenu* menu = new QMenu( this );
		QAction* ac_play = menu->addAction( "Play Now" );
		QAction* ac_add = menu->addAction( "Add to Current Playlist" );
		QAction* ac_addToPlaylist = menu->addAction( "Add to a Playlist" );
		QAction* action = menu->exec( pos );
		if ( action == ac_play )
		{
		}
		else if ( action == ac_add )
		{
		}
		else if ( action == ac_addToPlaylist )
		{
		}
	}

signals:
	void currentTracklistChanged( TrackList* tracklist );
	void renamed( const QString& name );
	void deleted();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARYFILESWIDGET_H
