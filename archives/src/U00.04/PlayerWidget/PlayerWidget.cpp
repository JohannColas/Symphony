#include "PlayerWidget.h"
/**********************************************/
#include <QDebug>
#include <QApplication>
#include <QTime>
/**********************************************/
/**********************************************/
/* */
PlayerWidget::~PlayerWidget()
{

}
/**********************************************/
/**********************************************/
/* */
PlayerWidget::PlayerWidget( QWidget* parent )
	: Frame( parent )
{
	show();
	setSizePolicy( QSizePolicy::Expanding,
				   QSizePolicy::Maximum );

	wid_songInfo->setTrack( _player );
	connect( _player, &QMediaPlayer::positionChanged,
			 this, &PlayerWidget::changePosition );
	connect( _player, &QMediaPlayer::mediaStatusChanged,
			 this, &PlayerWidget::onMediaStatusChanged );
	connect( pb_play, &QPushButton::clicked,
			 this, &PlayerWidget::playpause );
	connect( pb_stop, &QPushButton::clicked,
			 _player, &QMediaPlayer::stop );
	connect( pb_backward, &QPushButton::clicked,
			 this, &PlayerWidget::backward );
	connect( pb_forward, &QPushButton::clicked,
			 this, &PlayerWidget::forward );
	connect( pb_repeat, &QPushButton::clicked,
			 this, &PlayerWidget::repeat );
	connect( pb_shuffle, &QPushButton::clicked,
			 this, &PlayerWidget::shuffle );
	connect( sl_time, &QSlider::sliderMoved,
			 this, &PlayerWidget::setPlayerPos );
	_timer->setSingleShot( true );
	connect( _timer, &QTimer::timeout,
			 this, &PlayerWidget::updatePlayerPos );

	pb_backward->setFlat( true );
	pb_play->setFlat( true );
	pb_stop->setFlat( true );
	pb_forward->setFlat( true );
	sl_time->setOrientation( Qt::Horizontal );
	sl_time->setMinimum( 0 );
	sl_time->setMaximum( 1000 );
	// Adding Player Buttons
	addWidget( pb_backward, 0, 0 );
	addWidget( pb_play, 0, 1 );
	addWidget( pb_stop, 0, 2 );
	addWidget( pb_forward, 0, 3 );
	// Adding Track Info Widget
	GridLayout* lay_track = new GridLayout;
	lay_track->addWidget( img_cover, 0, 0, 4 );
	lay_track->addSpacer( 0, 1 );
	lay_track->addWidget( lb_title, 1, 1 );
	lay_track->addWidget( lb_artist, 2, 1 );
	lay_track->addSpacer( 3, 1 );
	lay_track->addSpace( 10, 0, 2, false, 4 );
	addLayout( lay_track, 0, 4 );
	// Adding Timer Widget
	GridLayout* lay_timer = new GridLayout;
	lay_timer->addSpacer( 0, 0, true, 1, 2 );
	lay_timer->addSpacer( 1, 0, false );
	lay_timer->addWidget( lb_time, 1, 1 );
	lay_timer->addWidget( sl_time, 2, 0, 1, 2 );
	lay_timer->addSpacer( 3, 0, true, 1, 2 );
	lb_time->setSizePolicy( QSizePolicy::Maximum,
							QSizePolicy::Maximum );
	sl_time->setMinimumWidth( 200 );
	addSpace( 10, 0, 5, false );
	lay_main->addLayout( lay_timer, 0, 6 );

	addSpace( 10, 0, 7, false );

	lay_main->addWidget( pb_repeat, 0, 8 );
	lay_main->addWidget( pb_shuffle, 0, 9 );
	lay_main->addWidget( pb_equalizer, 0, 10 );
	lay_main->addWidget( pb_volume, 0, 11 );

	addSpacer( 0, 12, false );

	lay_main->addWidget( pb_settings, 0, 13 );
	addSpace( 10, 0, 14, false );
	GridLayout* lay_system = new GridLayout;
	lay_system->addWidget( pb_minimize, 0, 0 );
	lay_system->addWidget( pb_maximize, 0, 1 );
	lay_system->addWidget( pb_close, 0, 2 );
	lay_main->addLayout( lay_system, 0, 15 );

	connect( pb_minimize, &QPushButton::clicked,
			 this, &PlayerWidget::onMinimize );
	connect( pb_maximize, &QPushButton::clicked,
			 this, &PlayerWidget::onMaximize );
	connect( pb_close, &QPushButton::clicked,
			 qApp, &QApplication::closeAllWindows );

	setMouseTracking( true );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::randomize()
{
	_listOrder.clear();
	for ( int it = 0; it < _nowPlaylist->count(); ++it )
	{
		_listOrder.append( it );
	}
	std::srand(std::time(nullptr));
	std::random_shuffle( _listOrder.begin(), _listOrder.end() );
	if ( _index != -1 && _listOrder.contains(_index) )
	{
		while ( _listOrder.first() != _index )
		{
			_listOrder.append( _listOrder.takeFirst() );
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::playTrack( Track* track )
{
	_nowPlaylist->clear();
	_nowPlaylist->add( track );
	_index = 0;
	setTrack();
	play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::addTrack( Track* track )
{
	_nowPlaylist->add( track );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::playTracklist( TrackList* tracklist )
{
	_nowPlaylist->clear();
	_nowPlaylist->add( tracklist );
	_index = 0;
	setTrack();
	play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::addTracklist( TrackList* tracklist )
{
	_nowPlaylist->add( tracklist );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::receiveTrackList( TrackList* tracklist )
{
	_nowPlaylist->clear();
	_nowPlaylist->add( tracklist );
	_index = 0;
	setTrack();
	play();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPlaylistIndex( int index )
{
	_index = index;
	if ( _isShuffle )
		_index = _listOrder.indexOf( index );
	setTrack();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setTrackList( TrackList* playlist )
{
	_nowPlaylist = playlist;
	_index = 0;
	setTrack();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setPlayerPos( int pos )
{
	_newPos = pos;
	_timer->start(200);
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::updatePlayerPos()
{
	if ( _newPos != -1 )
	{
		_player->setPosition( 1000*_newPos );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::setTrack()
{
	int index = _index;
	if ( _isShuffle )
		index = _listOrder.at( index );
	Track* track = _nowPlaylist->track(index);
	_player->setMedia( QUrl::fromLocalFile( track->path() ) );
	updateTrackInfo( track );
	sl_time->setValue(0);
	sl_time->setMaximum( _duration );
	if ( _isPlaying )
		play();
	updatePlayPauseIcon();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::play()
{
	_player->play();
	_isPlaying = true;
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::pause()
{
	_player->pause();
	_isPlaying = false;
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::playpause()
{
	if ( _player->state() == QMediaPlayer::PlayingState )
		pause();
	else
		play();
	// Update Play/Pause Icon
	updatePlayPauseIcon();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::stop()
{
	_player->stop();
	_isPlaying = false;
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::backward()
{
	if ( sl_time->value() < 40 )
	{
		--_index;
		if ( _index == -1 )
			_index = _nowPlaylist->count() - 1;
		setTrack();
	}
	else
		_player->setPosition( 0 );
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::forward()
{
	++_index;
	if ( _index == _nowPlaylist->count() )
	{
		_index = 0;
		if ( _repeat == Repeat::NOREPEAT )
		{
			stop();
		}
	}
	setTrack();
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::repeat()
{
	if ( _repeat == Repeat::NOREPEAT )
	{
		_repeat = Repeat::REPEATALL;
		if ( _icons )
			pb_repeat->setIcon( _icons->get("repeat_all") );
		qDebug() << "Repeat All";
	}
	else if ( _repeat == Repeat::REPEATALL )
	{
		_repeat = Repeat::REPEATONE;
		if ( _icons )
			pb_repeat->setIcon( _icons->get("repeat_one") );
		qDebug() << "Repeat One";
	}
	else if ( _repeat == Repeat::REPEATONE )
	{
		_repeat = Repeat::NOREPEAT;
		if ( _icons )
			pb_repeat->setIcon( _icons->get("no_repeat") );
		qDebug() << "No Repeat";
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::shuffle()
{
	_isShuffle = !_isShuffle;
	if ( _isShuffle )
	{
		randomize();
		if ( _icons )
			pb_shuffle->setIcon( _icons->get("shuffle") );
	}
	else
	{
		_index = _listOrder.at(_index);
		_listOrder.clear();
		if ( _icons )
			pb_shuffle->setIcon( _icons->get("no_shuffle") );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::changePosition( qint64 pos )
{
	//	if ( _player->duration() )
	{
		int perc = _duration ? (0.001*pos)/_duration : 0;
		sl_time->setValue( perc );
		QString curT = QTime( 0, 0, 0 ).addMSecs( pos ).toString();
		if( curT.left(2) == "00" )
			curT = curT.mid( 3 );
		QString durT = QTime( 0, 0, 0).addSecs( _duration ).toString();
		if( durT.left(2) == "00" )
			durT = durT.mid( 3 );
		lb_time->setText( curT + " / " + durT );
		sl_time->setValue(0.001*pos);
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::onMediaStatusChanged()
{
	updatePlayPauseIcon();
	if ( _player->mediaStatus() == QMediaPlayer::EndOfMedia )
	{
		if ( _repeat == Repeat::REPEATONE )
		{
			_player->setPosition(0);
			play();
		}
		else
			forward();
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::updateTrackInfo( Track* track )
{
	if ( track )
	{
		Metadata meta( track );
		QPixmap pix = QPixmap();
		if ( pix.convertFromImage( meta.cover() ) )
			img_cover->setPixmap( pix );
		else
		{
			if ( _icons )
				img_cover->setIcon( _icons->get("track") );
		}
		lb_title->setText( meta.title() );
		lb_artist->setText( meta.artist() );
		_duration = meta.duration();
		changePosition(0);
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::updateIcons( Icons* icons )
{
	_icons = icons;
	pb_backward->setIcon( icons->get("skip_previous") );
	updatePlayPauseIcon();
	pb_stop->setIcon( icons->get("stop") );
	pb_forward->setIcon( icons->get("skip_next") );
	wid_songInfo->updateIcons( _icons );
	pb_repeat->setIcon( icons->get("no_repeat") );
	pb_shuffle->setIcon( icons->get("no_shuffle") );
	pb_equalizer->setIcon( icons->get("equalizer") );
	pb_volume->setIcon( icons->get("volume") );
	pb_settings->setIcon( icons->get("settings") );
	pb_minimize->setIcon( icons->get("minimise") );
	pb_maximize->setIcon( icons->get("maximise") );
	pb_close->setIcon( icons->get("close") );
}
/**********************************************/
/**********************************************/
void PlayerWidget::updatePlayPauseIcon()
{
	if ( _icons )
	{
		if ( _player->state() == QMediaPlayer::PlayingState )
			pb_play->setIcon( _icons->get("pause") );
		else
			pb_play->setIcon( _icons->get("play") );
	}
}
/**********************************************/
/**********************************************/
/* */
void PlayerWidget::mousePressEvent( QMouseEvent* event )
{
	if ( event->button() == Qt::LeftButton )
	{
		_movable = true;
		emit moveWindow( event->globalPos() );
	}
	event->ignore();
}
/**********************************************/
/**********************************************/
void PlayerWidget::mouseReleaseEvent( QMouseEvent* event )
{
	if ( event->button() == Qt::LeftButton )
	{
		_movable = false;
		emit moveWindow( {-1,0} );
	}
	event->ignore();
}
/**********************************************/
/**********************************************/
void PlayerWidget::mouseMoveEvent( QMouseEvent* event )
{
	if ( _movable )
		emit moveWindow( event->globalPos() );
	event->ignore();
}
/**********************************************/
/**********************************************/
void PlayerWidget::mouseDoubleClickEvent( QMouseEvent* event )
{
	if ( event->button() == Qt::LeftButton )
		onMaximize();
	event->ignore();
}
/**********************************************/
/**********************************************/
void PlayerWidget::connectTo( Frame* frame )
{
	connect( frame, &Frame::trackToPlay,
			 this, &PlayerWidget::playTrack );
	connect( frame, &Frame::trackToAdd,
			 this, &PlayerWidget::addTrack );
	connect( frame, &Frame::tracklistToPlay,
			 this, &PlayerWidget::playTracklist );
	connect( frame, &Frame::tracklistToAdd,
			 this, &PlayerWidget::addTracklist );
}
/**********************************************/
/**********************************************/
