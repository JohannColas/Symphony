#ifndef WIDGET_H
#define WIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include "../Commons/Settings.h"
#include "../Commons/Theme.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
#include "../Commons/TrackList.h"
#include "../Commons/TracklistList.h"
#include "../LibrariesWidget/Libraries.h"
#include <QListWidget>
/**********************************************/
#include "../PlayerWidget/PlayerWidget.h"
#include "../SidebarWidget/SidebarWidget.h"
#include "../NowPlayingWidget/NowPlayingWidget.h"
#include "../ArtistsWidget/ArtistsWidget.h"
#include "../AlbumsWidget/AlbumsWidget.h"
#include "../GenresWidget/GenresWidget.h"
#include "../TracksWidget/TracksWidget.h"
#include "../PlaylistsWidget/PlaylistsWidget.h"
#include "../LibrariesWidget/LibrariesWidget.h"
#include "../RadiosWidget/RadiosWidget.h"
#include "../PodcastsWidget/PodcastsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Symphony
		: public Frame
{
	Q_OBJECT
private:
	// Window Parameters
	int invisibleBorderSize = 2;
	bool isPressedWidget;
	QPoint _lastPos = {-1,0};
	// Initialise the TrackList
	TrackList* _nowPlaylist = new TrackList( "#NowPlaylist", TrackList::NOWPLAYLIST );
	TracklistList* _playlists = new TracklistList( TracklistList::PLAYLISTS );
//	TracklistList* _libraries = new TracklistList( TracklistList::LIBRARIES );
	Libraries* _libraries = new Libraries;
	// Initialise the Widgets
	NowPlayingWidget* wid_nowPlaying = new NowPlayingWidget;
	ArtistsWidget* wid_artists = new ArtistsWidget;
	AlbumsWidget* wid_albums = new AlbumsWidget;
	GenresWidget* wid_genres = new GenresWidget;
	TracksWidget* wid_tracks = new TracksWidget;
	PlaylistsWidget* wid_playlists = new PlaylistsWidget;
	LibrariesWidget* wid_libraries = new LibrariesWidget;
	RadiosWidget* wid_radios = new RadiosWidget;
	PodcastsWidget* wid_podcasts = new PodcastsWidget;
protected:
	Theme* _theme = new Theme;
	Icons* _icons = new Icons;
	Lang* _lang = new Lang;
	PlayerWidget* wid_player = new PlayerWidget;
	SidebarWidget* wid_sidebar = new SidebarWidget;

public:
	~Symphony();
	Symphony( QWidget *parent = nullptr );


public slots:
	void updateSettings();
	void updateIcons();
	void updateLang();
	void move( const QPoint& pos );
	void closeEvent( QCloseEvent* event );
	void maximise()
	{
		isMaximized() ? showNormal() : showMaximized();
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WIDGET_H
