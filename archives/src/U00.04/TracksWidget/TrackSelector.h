#ifndef TRACKSELECTOR_H
#define TRACKSELECTOR_H
/**********************************************/
#include <QInputDialog>
#include <QListWidget>
#include <QMenu>
#include "../BasicWidgets/ListWidget.h"
#include "../Commons/Icons.h"
#include "../Commons/TrackList.h"
#include "../BasicWidgets/TrackItem.h"
#include "../LibrariesWidget/Libraries.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackSelector
		: public ListWidget
{
	Q_OBJECT
private:
	Icons* _icons = 0;
	TrackList* _tracklist = 0;

public:
	TrackSelector( QWidget* parent = nullptr )
		: ListWidget( parent )
	{
//		setMaximumWidth( 400 );
//		setStyleSheet( "background:blue;");
		connect( this, &TrackSelector::indexToChange,
				 this, &TrackSelector::indexChange );

	}
	Track* currentTrack()
	{
		if ( _tracklist && index() != -1 )
			return _tracklist->track( index() );
		return 0;
	}

public slots:
	void connectTrackList()
	{
		connect( _tracklist, &TrackList::changed,
				 this, &TrackSelector::update );
	}
	void disconnectTrackList()
	{
		if ( _tracklist )
		{
			disconnect( _tracklist, &TrackList::changed,
						this, &TrackSelector::update );
		}
	}
	void reset()
	{

	}
	void setTrackList( TrackList* tracklist )
	{
		disconnectTrackList();
		_tracklist = tracklist;
		connectTrackList();
		update();
	}
	void setLibraries( Libraries* libraries = 0 )
	{
		if ( libraries )
		{
			setTrackList( libraries->toTracklist() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		update();
	}
	void update() override
	{
		if ( _tracklist )
		{
			clear();
			int x = contentsMargins().left();
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();
			int nbItems = 0;
			for ( int it = _begin; it < _tracklist->count(); ++it )
			{
//				Metadata meta( _tracklist->track(it) );
				QString options = "cover:||/title/artist/||:--:duration";
				options = "cover:title/artist:--:duration";
//				options = "cover:||/title/artist/album/duration/||:--";
				QIcon icon = QIcon();
				if ( _icons )
					icon = _icons->get("track");
				TrackItem* trackItem = new TrackItem( _tracklist->track(it), options, icon, this );
				if ( it == _tracklist->currentTrackIndex() )
					trackItem->setChecked( true );
				trackItem->setGeometry( x, totheight,
										w,
										trackItem->sizeHint().height() );
				// Adding the Item to TrackSelector
				addItem( trackItem );
				totheight += trackItem->sizeHint().height();
				if ( totheight > height() )
				{
					break;
				}
				++nbItems;
			}
			updateScrollbar( _tracklist->count() );
		}
	}
	void showContextMenu( const QPoint& pos ) override
	{
		int index = getIndexByPos( pos );
		TrackItem* item = 0;
		if ( isIndexValid(index) )
		{
			item = static_cast<TrackItem*>( _items.at( index ) );
		}
		QMenu* menu = new QMenu( this );
		QAction* ac_play = menu->addAction( "Play Now" );
		QAction* ac_add = menu->addAction( "Add to Current Playlist" );
		QAction* ac_addToPlaylist = menu->addAction( "Add to a Playlist" );
		QAction* action = menu->exec( this->mapToGlobal(pos) );
		if ( action == ac_play )
		{
			if ( item )
				emit trackToPlay( item->track() );
		}
		else if ( action == ac_add )
		{
			if ( item )
				emit trackToAdd( item->track() );
		}
		else if ( action == ac_addToPlaylist )
		{
			emit tracklistToAdd( _tracklist->getTracklistByIndexes(_selection) );
		}
	}
	void indexChange( int index )
	{
		emit trackChanged( _tracklist->track(index) );
	}

signals:
	void trackChanged( Track* track );
	void currentTracklistChanged( TrackList* tracklist );
	void renamed( const QString& name );
	void deleted();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKSELECTOR_H
