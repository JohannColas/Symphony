#ifndef TRACKSWIDGET_H
#define TRACKSWIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include "../Commons/TracklistList.h"
#include "../TracksWidget/TrackSelector.h"
#include "../TracksWidget/TrackWidget.h"
#include "../Commons/Lang.h"
#include "../LibrariesWidget/Libraries.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TracksWidget
		: public Frame
{
	Q_OBJECT
private:
	Libraries* _libraries = 0;
	TrackSelector* sel_track = new TrackSelector;
	TrackWidget* wid_track = new TrackWidget;

public:
	TracksWidget( QWidget* parent = 0 )
		: Frame( parent )
	{
		addWidget( sel_track, 0, 0 );
		addWidget( wid_track, 0, 1 );

		connect( sel_track, &TrackSelector::trackChanged,
				 wid_track, &TrackWidget::setTrack );

	//	connect( sel_artist, &ArtistSelector::artistChanged,
	//			 wid_artist, &ArtistWidget::setArtist );
//		connect( wid_track, &TrackWidget::trackToPlay,
//				 this, &TracksWidget::trackToPlay );
//		connect( wid_track, &TrackWidget::tracklistToPlay,
//				 this, &TracksWidget::tracklistToPlay );
		connectTo( sel_track );
	}

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
		sel_track->setLibraries( _libraries );
		wid_track->setLibraries( _libraries );
//		connect( sel_track, &TrackSelector::indexToChange,
//				 wid_track, &TrackWidget::setArtistByIndex );
	}
	void updateIcons( Icons* icons )
	{
		sel_track->updateIcons( icons );
		wid_track->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
	}
	//
	void sendTrackToPlay( Track* track )
	{
		emit trackToPlay( track );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKSWIDGET_H
