#ifndef ALBUMBANNER_H
#define ALBUMBANNER_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/Frame.h"
/**********************************************/
#include "../Commons/Album.h"
#include "../Commons/AlbumInfo.h"
/**********************************************/
#include "../BasicWidgets/AlbumLabel.h"
#include "../BasicWidgets/ThumbImage.h"
#include "../BasicWidgets/ArtistLabel.h"
#include "../BasicWidgets/CountLabel.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"

#include <QTimer>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumBanner
		: public Frame
{
	Q_OBJECT
protected:
	Album* _album = 0;
	AlbumInfo _albumInfo;
	ThumbImage* lb_cover = new ThumbImage;
	AlbumLabel* lb_title = new AlbumLabel;
	ArtistLabel* lb_artist = new ArtistLabel;
	CountLabel* lb_nbTracks = new CountLabel;
	Icons* _icons = 0;
	QTimer* _timer = new QTimer;

public:
	~AlbumBanner()
	{
		delete _timer;
		_timer = 0;
	}
	AlbumBanner( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( lb_cover, 0, 0, 2 );
		addWidget( lb_title, 0, 1, 1, 3 );
//		HorizontalLayout* line2 = new HorizontalLayout;
//		line2->addWidget( lb_artist );
//		line2->addSpacer(10);
//		line2->addWidget( lb_nbTracks );
//		addLayout( line2, 1, 1 );
		addWidget( lb_artist, 1, 1 );
		addSpace( 10, 1, 2, false );
		addWidget( lb_nbTracks, 1, 3 );
		lb_title->setText("----");
		lb_artist->setText("----");
		lb_nbTracks->setText("");
		lb_nbTracks->fitWidthToContent();
//		lb_artist->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
//		lb_artist->setStyleSheet("background:red;");
		//		lb_artist->allowElide();
		lb_nbTracks->avoidElide();
//		lb_nbTracks->setStyleSheet("background:yellow;");
//		lb_nbTracks->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );

		lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );

		_timer->setSingleShot(true);
		connect( _timer, &QTimer::timeout,
				 this, &AlbumBanner::updateThumb );
	}


public slots:
	void setAlbum( Album* album )
	{
		_album = album;
		update();
	}
	void update()
	{
		if ( _album )
		{
			_albumInfo.setAlbum(_album);
			QString title = _album->title();
			if ( title == "" )
				title = "[UNKNOWN]";
			lb_title->setText( title );
//			QPixmap pix = QPixmap();
//			if ( pix.convertFromImage( QImage() ) )
//				lb_cover->setIcon( pix );
//			else
//			{
//				if ( _icons )
//				{
//					lb_cover->setIcon( _icons->get("album") );
//				}
//			}
//				_timer->start( 50 );

			QString artist = _album->artist();
			if ( artist == "" )
				artist = "[UNKNOWN]";//"[UNKNOWN]---------sgjlj igidqhioghqioqd\nsd udghud hgudg dguh dghd gu\nfskldgjsdg--------------";
			lb_artist->setText( artist );
			lb_artist->setStyleSheet( "background:red;" );
			int nb = _album->nbTracks();
			QString number = QString::number(nb);
			if ( nb < 2 )
				number += " track" ;
			else
				number += " tracks" ;
			lb_nbTracks->setText( number );
			//
			resize( width(), sizeHint().height() );
			repaint();
		}
	}
	void updateThumb()
	{
		if ( _icons )
		{
			lb_cover->setIcon( _icons->get("album") );
		}
		if ( _album )
		{
			 QPixmap pix = _albumInfo.thumb();
			 if (  !pix.isNull() )
				 lb_cover->setIcon( pix );
			 else
			 {
				 Network* network = new Network;
				 connect( network, &Network::downloaded,
						  this, &AlbumBanner::setThumb );
				 network->download( _albumInfo.thumbUrl() );
			 }
		}
	}
	void setThumb( const QByteArray& data )
	{
		if ( data.length() > 0 )
		{
			QFile file( _albumInfo.path()+"/thumb.png" );
			file.open( QIODevice::WriteOnly );
			file.write( data );
			file.close();
			QPixmap pixmap;
			pixmap.loadFromData(data);
			lb_cover->setIcon( pixmap );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMBANNER_H
