#ifndef ALBUMITEM_H
#define ALBUMITEM_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/ListItem.h"
/**********************************************/
#include "../Commons/Album.h"
#include "../Commons/AlbumInfo.h"
/**********************************************/
#include "../BasicWidgets/AlbumLabel.h"
#include "../BasicWidgets/ArtistLabel.h"
#include "../BasicWidgets/ThumbImage.h"
#include "../BasicWidgets/CountLabel.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"

#include <QTimer>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumItem
		: public ListItem
{
	Q_OBJECT
protected:
	Album* _album = 0;
	AlbumInfo _albumInfo;
	ThumbImage* thumb = new ThumbImage;
	QTimer* timer = new QTimer;

public:
	~AlbumItem()
	{
//		emit deleted();
		delete thumb;
		delete timer;
		thumb = 0;
	}
	AlbumItem( Album* album, const QIcon& icon, QWidget* parent = 0, const QString& options = "" )
		: ListItem( parent )
	{
		_album = album;
		GridLayout* lay_col = new GridLayout;
		thumb->setIcon( icon );
		_albumInfo.setAlbum( _album );
		updateThumb();
//		timer->setSingleShot( true );
//		connect( timer, &QTimer::timeout,
//				 this, &AlbumItem::updateThumb );
//		timer->start(50);
//		QPixmap pix = QPixmap();
//		if ( pix.convertFromImage( QImage() ) )
//			thumb->setIcon( pix );
//		else
//		{
//			thumb->setIcon( icon );
//		}
		QString title = album->title();
		if ( title == "" )
			title = "[UNKNOWN]";
		if ( options == "" )
		{
			lay_col->addWidget( thumb , 0, 0 );
			lay_col->addWidget( new AlbumLabel( title ), 0, 1 );
		}
		else
		{
			lay_col->addWidget( thumb , 0, 0, 2 );
			lay_col->addWidget( new AlbumLabel( title ), 0, 1 );
			if ( options.contains("number") )
			{
				int nb = album->nbTracks();
				QString number = QString::number(nb);
				if ( nb < 2 )
					number += " track" ;
				else
					number += " tracks" ;
				lay_col->addWidget( new CountLabel( number ), 1, 1 );
			}
			else if ( options.contains("artist") )
			{
				QString artist = album->artist();
				if ( artist == "" )
					artist = "[UNKNOWN]";
				lay_col->addWidget( new ArtistLabel( artist ), 1, 1 );
			}
		}
		lay_main->addLayout( lay_col, 0, 0 );

		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}

public slots:
	void updateThumb()
	{
		if ( _album )
		{
//			QElapsedTimer timer;
//			timer.start();
//			AlbumInfo info( _album );
			QPixmap pix = _albumInfo.thumb();
			if ( !pix.isNull() )
			{
				thumb->setIcon( pix );
			}
			else
			{
				Network* network = new Network;
				connect( network, &Network::downloaded,
						 this, &AlbumItem::setThumb );
				network->download( _albumInfo.thumbUrl() );
			}
		}
	}
	void setThumb( const QByteArray& data )
	{
		if ( data.length() > 0 )
		{
			QFile file( _albumInfo.path()+"/thumb.png" );
			file.open( QIODevice::WriteOnly );
			file.write( data );
			file.close();
			QPixmap pixmap;
			pixmap.loadFromData(data);
			thumb->setIcon( pixmap );
		}
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMITEM_H
