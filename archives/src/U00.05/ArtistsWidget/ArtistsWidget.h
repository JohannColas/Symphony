#ifndef ARTISTSWIDGET_H
#define ARTISTSWIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include "../Commons/TracklistList.h"
#include "../ArtistsWidget/ArtistSelector.h"
#include "../ArtistsWidget/ArtistWidget.h"
#include "../Commons/Lang.h"
#include "../LibrariesWidget/Libraries.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistsWidget
		: public Frame
{
	Q_OBJECT
private:
	Libraries* _libraries = 0;
	ArtistSelector* sel_artist = new ArtistSelector;
	ArtistWidget* wid_artist = new ArtistWidget;

public:
	ArtistsWidget( QWidget* parent = 0 )
		: Frame( parent )
	{
		hide();

		addWidget( sel_artist, 0, 0 );
		addWidget( wid_artist, 0, 1 );

	//	connect( sel_artist, &ArtistSelector::artistChanged,
	//			 wid_artist, &ArtistWidget::setArtist );
		connectTo( wid_artist );
	}
	void setPlaylists( TracklistList* playlists )
	{
		wid_artist->setPlaylists( playlists );
	}

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
		sel_artist->setLibraries( _libraries );
		wid_artist->setLibraries( _libraries );
		connect( sel_artist, &ArtistSelector::indexToChange,
				 wid_artist, &ArtistWidget::setArtistByIndex );
	}
	void updateIcons( Icons* icons )
	{
		sel_artist->updateIcons( icons );
		wid_artist->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
	}
	//
	void sendTrackToPlay( Track* track )
	{
		emit trackToPlay( track );
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTSWIDGET_H
