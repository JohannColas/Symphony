#ifndef GENRELABEL_H
#define GENRELABEL_H
/**********************************************/
#include "../BasicWidgets/Label.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GenreLabel
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	GenreLabel( QWidget* parent = 0 )
		: Label( parent )
	{
	}
	GenreLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GENRELABEL_H
