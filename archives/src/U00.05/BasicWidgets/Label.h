#ifndef LABEL_H
#define LABEL_H
/**********************************************/
#include <QFrame>
#include <QLabel>
#include <QDebug>
#include <QPainter>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Label
		: public QLabel
{
	Q_OBJECT
private:
	QString _text = "";
	bool _elide = true;
	bool _multiLine = false;
	bool _fitWidthToContent = false;
	bool _adaptWidthToContent = false;
	bool _fitHeightToContent = true;

public:
	Label( const QString& text, QWidget* parent = 0 )
		: QLabel( parent )
	{
		setText( text );
		setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
	}
	Label( QWidget* parent = 0 )
		: QLabel( parent )
	{
		setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
	}
	const QString& text() const
	{
		return _text;
	}
	bool isElided() const
	{
		return _elide;
	}

public slots:
	void fixWidth( int width )
	{
		_fitWidthToContent = true;
		setMaximumWidth( width );
//		setFixedWidth( width );
	}
	void fixHeight( int height )
	{
		_fitHeightToContent = false;
		setFixedHeight( height );
	}
	void setText( const QString& text )
	{
		_text = text;
//		QLabel::setText( _text );
		repaint();
	}
	void allowElide()
	{
		_elide = true;
		unfitToContent();
	}
	void avoidElide()
	{
		_elide = false;
//		repaint();
	}
	void allowMultiLine()
	{
		_multiLine = true;
	}
	void avoidMultiLine()
	{
		_multiLine = false;
	}
	void fitToContent()
	{
		_fitWidthToContent = true;
		_fitHeightToContent = true;
		_elide = false;
//		repaint();
	}
	void fitWidthToContent()
	{
		_fitWidthToContent = true;
		_elide = false;
//		repaint();
	}
	void adaptWidthToContent()
	{
		_adaptWidthToContent = true;
//		_elide = false;
//		repaint();
	}
	void fitHeightToContent()
	{
		_fitHeightToContent = true;
		_elide = false;
//		repaint();
	}
	void unfitToContent()
	{
		_fitWidthToContent = false;
//		repaint();
	}
	void update()
	{
		if ( /*(sizeHint().width() > width()) &&*/
			 _elide )
		{
//			QFontMetrics metrix( font() );
//			int w = contentsRect().width();
//			QString clippedText = metrix.elidedText( _text, Qt::ElideRight, w );
////			qDebug() << text() << w << metrix.boundingRect(clippedText).width() << sizeHint().width() << width();
//			QLabel::setText( clippedText );
		}
	}

private:
	void paintEvent( QPaintEvent* event ) override
	{
//		qDebug() << QLabel::text() << width() << sizeHint().width();
//		update();
//		qDebug() << QLabel::text() << width() << sizeHint().width();
//		qDebug() << "------";
//		QLabel::paintEvent( event );

		QFrame::paintEvent( event );

		QPainter painter( this );
		QFontMetrics metrix = painter.fontMetrics();
		int _width = 0;
		int _height = contentsMargins().top() +
					  contentsMargins().bottom();
		int x = contentsMargins().left();
		int y = metrix.ascent() + contentsMargins().top();
		int maxWidth = width();
		if ( maxWidth > maximumWidth() )
			maxWidth = maximumWidth();
		maxWidth -= (contentsMargins().left()+contentsMargins().right());
		QStringList lines = _text.split("\n");
		for ( QString line : lines )
		{
			// Elide line
			if ( _elide && !_fitWidthToContent )
				line = metrix.elidedText( line, Qt::ElideRight, maxWidth );
			// Getting the maximum line width
			if ( _width < metrix.boundingRect( line ).width() )
				_width = metrix.boundingRect( line ).width();

			painter.drawText( x, y, line );

			y += metrix.height();
			_height += metrix.height();
			if ( !_multiLine )
				break;
		}
		if ( _fitWidthToContent )
		{
			_width += contentsMargins().left() + contentsMargins().right();
			setFixedWidth( _width );
		}
		if ( _fitHeightToContent )
			setFixedHeight( _height );

	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LABEL_H
