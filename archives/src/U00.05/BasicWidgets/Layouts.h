#ifndef LAYOUTS_H
#define LAYOUTS_H
/**********************************************/
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GridLayout
		: public QGridLayout
{
	Q_OBJECT
public:
	GridLayout( QWidget* parent = nullptr )
		: QGridLayout( parent )
	{
		setMargin( 0 );
		setSpacing( 0 );
	}
	void addWidget( QWidget* widget,
					int row, int col,
					int rowSpan = 1, int colSpan = 1 )
	{
		QGridLayout::addWidget( widget, row, col, rowSpan, colSpan );
	}
	void addLayout( QLayout* layout,
					int row, int col,
					int rowSpan = 1, int colSpan = 1 )
	{
		QGridLayout::addLayout( layout, row, col, rowSpan, colSpan );
	}
	void addSpace( int size, int row, int col, bool isVertical = true, int rowSpan = 1, int colSpan = 1 )
	{
		QSpacerItem* spacer;
		if ( isVertical )
			spacer  = new QSpacerItem( 0, size );
		else
			spacer  = new QSpacerItem( size, 0 );
		addItem( spacer, row, col, rowSpan, colSpan );
	}
	void addSpacer( int row, int col,
					bool isVertical = true,
					int rowSpan = 1, int colSpan = 1 )
	{
		QSpacerItem* spacer = 0;
		if ( isVertical )
			spacer = new QSpacerItem( 0, 0,
									  QSizePolicy::Maximum,
									  QSizePolicy::Expanding );
		else
			spacer = new QSpacerItem( 0, 0,
									  QSizePolicy::Expanding,
									  QSizePolicy::Maximum );
		if ( spacer )
			addItem( spacer, row, col, rowSpan, colSpan );
	}
//	void addSpacer( int row, int col,
//					int width = 0, int height = 0,
//					int rowSpan = 1, int colSpan = 1  )
//	{
//		QSpacerItem* spacer = new QSpacerItem( width, height,
//											   QSizePolicy::Maximum,
//											   QSizePolicy::Maximum );
//		if ( spacer )
//			addItem( spacer, row, col, rowSpan, colSpan );
//	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class VerticalLayout
		: public QVBoxLayout
{
	Q_OBJECT
public:
	VerticalLayout( QWidget* parent = nullptr )
		: QVBoxLayout( parent )
	{
		setMargin( 0 );
		setSpacing( 0 );
	}
	void addSpacer()
	{
		addItem( new QSpacerItem( 0, 0,
								  QSizePolicy::Maximum,
								  QSizePolicy::Expanding ) );
	}
	void addSpacer( int height )
	{
		addItem( new QSpacerItem( 0, height,
								  QSizePolicy::Maximum,
								  QSizePolicy::Maximum ) );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class HorizontalLayout
		: public QHBoxLayout
{
	Q_OBJECT
public:
	HorizontalLayout( QWidget* parent = nullptr )
		: QHBoxLayout( parent )
	{
		setMargin( 0 );
		setSpacing( 0 );
	}
	void addSpacer()
	{
		addItem( new QSpacerItem( 0, 0,
								  QSizePolicy::Expanding,
								  QSizePolicy::Maximum ) );
	}
	void addSpacer( int width )
	{
		addItem( new QSpacerItem( width, 0,
								  QSizePolicy::Maximum,
								  QSizePolicy::Maximum ) );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LAYOUTS_H
