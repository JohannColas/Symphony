#ifndef THUMBIMAGE_H
#define THUMBIMAGE_H
/**********************************************/
#include "../BasicWidgets/LockButton.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ThumbImage
		: public LockButton
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	ThumbImage( QWidget* parent = 0 )
		: LockButton( parent )
	{
		setFlat( true );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setPixmap( const QPixmap& pix )
	{
		setIcon( pix );
	}
	void setIcon( const QIcon& icon )
	{
		LockButton::setIcon( icon );
	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // THUMBIMAGE_H
