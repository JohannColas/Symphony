#ifndef TITLEWIDGET_H
#define TITLEWIDGET_H
/**********************************************/
#include "../BasicWidgets/Label.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TitleLabel
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	TitleLabel( QWidget* parent = 0 )
		: Label( parent )
	{
	}
	TitleLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TITLEWIDGET_H
