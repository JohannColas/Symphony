#ifndef ARTISTINFO_H
#define ARTISTINFO_H

#include <QApplication>
//#include <QNetworkAccessManager>
//#include <QNetworkReply>
//#include <QNetworkRequest>
#include <QPixmap>
#include <QString>

#include "../Commons/Artist.h"
#include "../Commons/Network.h"
#include "../Commons/Json.h"


class ArtistInfo
		: public QObject
{
	Q_OBJECT
public:
	enum Gender
	{
		MALE,
		FEMALE,
		NONE
	};

protected:
	Artist* _artist = 0;
	QString _path = "";
	QString _name = "";
	QString _biography = "";
	int _formedYear = -1;
	QString _website = "";
	Gender _gender = Gender::NONE;
	int _nbMembers = -1;
	QString _thumb = "";
	QString _fanart = "";
	QString _country = "";

public:
	ArtistInfo( Artist* artist = 0 )
	{
		setArtist( artist );
	}
	void setArtist( Artist* artist )
	{
		if ( artist )
		{
			_artist = artist;
			_path = "./libraries/artists/"+_artist->name();
			QDir dir;
			if ( !dir.exists( _path ) ) dir.mkpath( _path );
			update();
		}
	}
	const QString& biography()
	{
		return _biography;
	}
	const QString& name()
	{
		return _name;
	}
	const QString& path()
	{
		return _path;
	}
	int formedYear()
	{
		return _formedYear;
	}
	const QString& website()
	{
		return _website;
	}
	const Gender& gender()
	{
		return _gender;
	}
	int nbMembers()
	{
		return _nbMembers;
	}
	QPixmap thumb()
	{
		return pixmap( "thumb" );
	}
	QString thumbUrl()
	{
		return _thumb;
	}
	QPixmap fanart()
	{
//		qDebug() << _fanart;
		return pixmap( "fanart" );
	}
	const QString& country()
	{
		return _country;
	}
	//public slots:
	void update()
	{
		if ( _artist )
		{
			QFile file( _path+"/artist.ats" );
			if ( file.open( QIODevice::ReadOnly) )
			{
				read();
			}
			else
			{
				QString url = "https://www.theaudiodb.com/api/v1/json/1/search.php?s="+_artist->name();
				url = url.replace(" ", "%20");

				Network *network = new Network;
				connect( network, &Network::downloaded,
						 this, &ArtistInfo::readJson );
				network->download( url );

//				QNetworkRequest request = QNetworkRequest( QUrl( url ) );
////				request.setHeader( QNetworkRequest::ContentTypeHeader, "application/json" );

//				QNetworkAccessManager nam;
//				QNetworkReply *reply = nam.get( request );
//				while ( !reply->isFinished() )
//				{
//					qApp->processEvents();
//				}
//				reply->deleteLater();
//				QByteArray rep_data = reply->readAll();
//				QJsonDocument jsonDoc = QJsonDocument::fromJson( rep_data );
//				QJsonObject jsonObject = jsonDoc.object();
//				QVariantMap jsonMap = jsonObject.toVariantMap();

//				for (auto elm : jsonMap.keys() )
//				{
//					if ( elm == "artists" )
//					{
//						QJsonArray artistArray = jsonMap[elm].toJsonArray();
////						for ( auto elm : artistArray )
//						{
//							QVariantMap artistMap = artistArray[0].toObject().toVariantMap();
//							for (auto elm : artistMap.keys() )
//							{
////								qDebug() << elm;
//								if ( elm == "artist" )
//								{
//									_name = artistMap[elm].toString();
//								}
//								else if ( elm == "intBornYear" )
//								{
//									_formedYear = artistMap[elm].toInt();
//								}
//								else if ( elm == "intMembers" )
//								{
//									_nbMembers = artistMap[elm].toInt();
//								}
//								else if ( elm == "strArtistFanart" )
//								{
//									_fanart = artistMap[elm].toString();
//								}
//								else if ( elm == "strArtistThumb" )
//								{
//									_thumb = artistMap[elm].toString();
//								}
//								else if ( elm == "strBiographyFR" )
//								{
//									_biography = artistMap[elm].toString();
//								}
//								else if ( elm == "strCountryCode" )
//								{
//									_country = artistMap[elm].toString();
//								}
//								else if ( elm == "strGender" )
//								{
//									QString gender = artistMap[elm].toString();
//									if ( gender == "Male" )
//										_gender = Gender::MALE;
//									else if ( gender == "Female" )
//										_gender = Gender::MALE;
//								}
//								else if ( elm == "strWebsite" )
//								{
//									_website = artistMap[elm].toString();
//								}
//							}
//						}
//					}
//				}
//				save();
			}
		}
	}
public slots:
	void read()
	{
		QSettings setsFile( _path+"/artist.ats", QSettings::IniFormat );
		_name = setsFile.value("name").toString();
		_biography = setsFile.value("biography").toString();
		_formedYear = setsFile.value("formedYear").toInt();
		_website = setsFile.value("website").toString();
		_gender = (Gender)setsFile.value("gender").toInt();
		_nbMembers = setsFile.value("nbMembers").toInt();
		_thumb = setsFile.value("thumb").toString();
		_fanart = setsFile.value("fanart").toString();
		_country = setsFile.value("country").toString();
	}
	void readJson( const QByteArray& data )
	{
		QJsonDocument jsonDoc = QJsonDocument::fromJson( data );
		QJsonObject jsonObject = jsonDoc.object();
		QVariantMap jsonMap = jsonObject.toVariantMap();

		for (auto elm : jsonMap.keys() )
		{
			if ( elm == "artists" )
			{
				QJsonArray artistArray = jsonMap[elm].toJsonArray();
//						for ( auto elm : artistArray )
				{
					QVariantMap artistMap = artistArray[0].toObject().toVariantMap();
					for (auto elm : artistMap.keys() )
					{
//								qDebug() << elm;
						if ( elm == "artist" )
						{
							_name = artistMap[elm].toString();
						}
						else if ( elm == "intBornYear" )
						{
							_formedYear = artistMap[elm].toInt();
						}
						else if ( elm == "intMembers" )
						{
							_nbMembers = artistMap[elm].toInt();
						}
						else if ( elm == "strArtistFanart" )
						{
							_fanart = artistMap[elm].toString();
							Network* network = new Network;
							connect( network, &Network::downloaded,
									 this, &ArtistInfo::saveFanart );
							network->download( _fanart );
						}
						else if ( elm == "strArtistThumb" )
						{
							_thumb = artistMap[elm].toString();
							Network* network = new Network;
							connect( network, &Network::downloaded,
									 this, &ArtistInfo::saveThumb );
							network->download( _thumb );
						}
						else if ( elm == "strBiographyFR" )
						{
							_biography = artistMap[elm].toString();
						}
						else if ( elm == "strCountryCode" )
						{
							_country = artistMap[elm].toString();
						}
						else if ( elm == "strGender" )
						{
							QString gender = artistMap[elm].toString();
							if ( gender == "Male" )
								_gender = Gender::MALE;
							else if ( gender == "Female" )
								_gender = Gender::MALE;
						}
						else if ( elm == "strWebsite" )
						{
							_website = artistMap[elm].toString();
						}
					}
				}
			}
		}
		emit readJsonEnded();
		save();
	}
	void save()
	{
		QFile file( _path );
		file.remove();

		QSettings setsFile( _path+"/artist.ats", QSettings::IniFormat );
		setsFile.setValue("name", _name );
		setsFile.setValue("biography", _biography );
		setsFile.setValue("formedYear", _formedYear );
		setsFile.setValue("website", _website );
		setsFile.setValue("gender", (int)_gender );
		setsFile.setValue("nbMembers", _nbMembers );
		setsFile.setValue("thumb", _thumb );
		setsFile.setValue("fanart", _fanart );
		setsFile.setValue("country", _country );
	}
	const QPixmap pixmap( const QString& filePath )
	{
		QPixmap pixmap;
		pixmap.load( _path+"/"+filePath+".png" );
		return pixmap;
	}
	void getPixmap( const QByteArray& data )
	{
		QPixmap pixmap;
		if ( data.length() > 0 )
		{
//			QFile file( _path+"/"+filePath+".png" );
//			file.open( QIODevice::WriteOnly );
//			file.write( data );
//			file.close();
//			pixmap.loadFromData(data);
		}
		emit pixmapDownload( pixmap );
	}
	void saveThumb( const QByteArray& data )
	{
		if ( data.length() > 0 )
		{
			QFile file( _path+"/thumb.png" );
			if ( !file.exists() )
			{
				file.open( QIODevice::WriteOnly );
				file.write( data );
				file.close();
			}
		}
	}
	void saveFanart( const QByteArray& data )
	{
		if ( data.length() > 0 )
		{
			QFile file( _path+"/fanart.png" );
			if ( !file.exists() )
			{
				file.open( QIODevice::WriteOnly );
				file.write( data );
				file.close();
			}
		}
	}


signals:
	void readJsonEnded();
	void pixmapDownload( const QPixmap& pixmap );
};

#endif // ARTISTINFO_H
