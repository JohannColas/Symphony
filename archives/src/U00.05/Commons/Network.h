#ifndef NETWORK_H
#define NETWORK_H

#include <QApplication>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <QFuture>
#include <QtConcurrent>

class NetworkThread
		: public QThread
{
	Q_OBJECT
	QUrl _url;
	void run() override
	{
		QNetworkRequest request = QNetworkRequest( _url );
		QNetworkAccessManager nam;
		QNetworkReply* _reply = nam.get( request );
		while ( !_reply->isFinished() )
		{
			qApp->processEvents();
		}
		_reply->deleteLater();
		emit downloaded( _reply->readAll() );
	}

public:
	NetworkThread( const QUrl& url )
	{
		_url = url;
		connect( this, &NetworkThread::finished,
				 this, &QObject::deleteLater );
	}
signals:
	void downloaded( const QByteArray& download );
};

class Network
		: public QObject
{
	Q_OBJECT
	QNetworkReply *_reply = 0;

public:
	void download( const QUrl& url )
	{
		NetworkThread *thread = new NetworkThread( url );
		connect( thread, &NetworkThread::downloaded,
				 this, &Network::downloaded );
		thread->start();
	}

public slots:
	void replyFinished()
	{
		if ( _reply )
		{
			emit downloaded( _reply->readAll() );
			_reply->deleteLater();
		}
	}

signals:
	void finished();
	void downloaded( const QByteArray& download );
};

#endif // NETWORK_H
