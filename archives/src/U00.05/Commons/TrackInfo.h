#ifndef TRACKINFO_H
#define TRACKINFO_H

#include <QApplication>
#include <QDir>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QPixmap>
#include <QSettings>
#include <QString>

#include "../Commons/Track.h"
#include "../Commons/Metadata.h"
#include "../Commons/Network.h"


class TrackInfo
		: public QObject
//		: public QSettings
{
	Q_OBJECT
public:
	enum Gender
	{
		MALE,
		FEMALE,
		NONE
	};

protected:
	Metadata _track;
	QString _path = "";
	QString _title = "";
	QString _artist = "";
	QString _album = "";
	QString _description = "";
	int _releaseYear = -1;
	int _number = -1;
	QString _genre = "";
	QString _style = "";
	QString _cover = "";
	bool _stop = false;

public:
	TrackInfo( Track* track = 0 )
	{
		setTrack( track );
	}
	void setTrack( Track* track )
	{
		if ( track )
		{
			_track.setTrack( track );
			_path = "./libraries/tracks/"+_track.artist()+"-"+_track.title();
			QDir dir;
			if ( !dir.exists( _path ) ) dir.mkpath( _path );
			update();
		}
	}
	const QString& path()
	{
		return _path;
	}
	const QString& description()
	{
		return _description;
	}
	const QString& title()
	{
		return _title;
	}
	const QString& artist()
	{
		return _artist;
	}
	const QString& album()
	{
		return _album;
	}
	int releaseYear()
	{
		return _releaseYear;
	}
	int number()
	{
		return _number;
	}
	const QString& genre()
	{
		return _genre;
	}
	const QString& style()
	{
		return _style;
	}
	const QString& coverUrl()
	{
		return _cover;
	}
	QPixmap cover()
	{
		return pixmap( "cover" );
	}
	void stop()
	{
		_stop = true;
	}
	//public slots:
	void update()
	{
//		if ( _track )
		{
			QFile file( _path+"/track.trk" );
			if ( file.open( QIODevice::ReadOnly) )
			{
				read();
			}
			else
			{
//				Metadata meta(_track);
				QString url = "https://www.theaudiodb.com/api/v1/json/1/searchtrack.php?s="+_track.artist()+"&t="+_track.title();
				url = url.replace(" ", "%20");

				Network *network = new Network;
				connect( network, &Network::downloaded,
						 this, &TrackInfo::readJson );
				network->download( url );

//				QNetworkRequest request = QNetworkRequest( QUrl( url ) );
//				request.setHeader( QNetworkRequest::ContentTypeHeader, "application/json" );

//				QNetworkAccessManager nam;
//				QNetworkReply *reply = nam.get( request );
//				while ( !reply->isFinished() )
//				{
//					if ( _stop )
//						return;
//					qApp->processEvents();
//				}
//				reply->deleteLater();
//				QByteArray rep_data = reply->readAll();
			}
		}
	}
private:
	void read()
	{
		QSettings setsFile( _path+"/track.trk", QSettings::IniFormat );
		_title = setsFile.value("title").toString();
		_artist = setsFile.value("artist").toString();
		_album = setsFile.value("album").toString();
		_number = setsFile.value("number").toInt();
		_description = setsFile.value("description").toString();
		_releaseYear = setsFile.value("releaseYear").toInt();
		_genre = setsFile.value("genre").toString();
		_style = setsFile.value("style").toString();
		_cover = setsFile.value("cover").toString();
	}
	void readJson( const QByteArray& data )
	{
		QJsonDocument jsonDoc = QJsonDocument::fromJson( data );
		QJsonObject jsonObject = jsonDoc.object();
		QVariantMap jsonMap = jsonObject.toVariantMap();

		for (auto elm : jsonMap.keys() )
		{
			if ( _stop )
				return;
			if ( elm == "track" )
			{
				QJsonArray artistArray = jsonMap[elm].toJsonArray();
				//						for ( auto elm : artistArray )
				{
					QVariantMap artistMap = artistArray[0].toObject().toVariantMap();
					for (auto elm : artistMap.keys() )
					{
						if ( _stop )
							return;
						//								qDebug() << elm;
						if ( elm == "strTrack" )
						{
							_title = artistMap[elm].toString();
						}
						else if ( elm == "strArtist" )
						{
							_artist = artistMap[elm].toString();
						}
						else if ( elm == "strAlbum" )
						{
							_album = artistMap[elm].toString();
						}
						else if ( elm == "intYearReleased" )
						{
							_releaseYear = artistMap[elm].toInt();
						}
						else if ( elm == "intTrackNumber" )
						{
							_number = artistMap[elm].toInt();
						}
						else if ( elm == "strTrackThumb" )
						{
							_cover = artistMap[elm].toString();
						}
						else if ( elm == "strDescriptionFR" )
						{
							_description = artistMap[elm].toString();
						}
						else if ( elm == "strGenre" )
						{
							_genre = artistMap[elm].toString();
						}
						else if ( elm == "strStyle" )
						{
							_style = artistMap[elm].toString();
						}
					}
				}
			}
		}
		emit readJsonEnded();
		save();
	}
	void save()
	{
		QFile file( _path );
		file.remove();

		QSettings setsFile( _path+"/track.trk", QSettings::IniFormat );
		setsFile.setValue("title", _title );
		setsFile.setValue("artist", _artist );
		setsFile.setValue("album", _album );
		setsFile.setValue("number", _number );
		setsFile.setValue("description", _description );
		setsFile.setValue("releaseYear", _releaseYear );
		setsFile.setValue("genre", _genre );
		setsFile.setValue("style", _style );
		setsFile.setValue("cover", _cover );
	}
	const QPixmap pixmap( const QString& filePath )
	{
		QPixmap pixmap;
		pixmap.load( _path+"/"+filePath+".png" );
		return pixmap;
	}
	void saveCover( const QByteArray& data )
	{
		if ( data.length() > 0 )
		{
			QFile file( _path+"/cover.png" );
			if ( !file.exists() )
			{
				file.open( QIODevice::WriteOnly );
				file.write( data );
				file.close();
			}
		}
	}

signals:
	void readJsonEnded();
	void pixmapDownload( const QPixmap& pixmap );
};

#endif // TRACKINFO_H
