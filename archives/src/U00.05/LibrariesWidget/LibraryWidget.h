#ifndef LIBRARYWIDGET_H
#define LIBRARYWIDGET_H

/**********************************************//**********************************************/
#include "../BasicWidgets/Frame.h"
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include "../LibrariesWidget/LibraryFilesWidget.h"
#include "../BasicWidgets/LockButton.h"
#include "../Commons/Metadata.h"
#include "../Commons/Track.h"
/**********************************************/
#include "../BasicWidgets/PathButton.h"
#include "../BasicWidgets/LibraryLabel.h"
/**********************************************/
#include "../Commons/TracklistList.h"
#include "../LibrariesWidget/Libraries.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LibraryWidget
		: public Frame
{
	Q_OBJECT
protected:
	Libraries* _libraries = 0;
	Library* _library = 0;
	LibraryLabel* lb_name = new LibraryLabel;
	PathButton* bt_path = new PathButton;
	QCheckBox* ck_hide = new QCheckBox;
	FileSelector* sel_files = new FileSelector;
	Icons* _icons = 0;

public:
	LibraryWidget( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( lb_name, 0, 0, 1, 2 );
		addWidget( bt_path, 1, 0 );
		addWidget( ck_hide, 1, 1 );
		addWidget( sel_files, 2, 0, 1, 2 );
		bt_path->hide();
		ck_hide->hide();
		ck_hide->setText( "hide" );
		setSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Preferred );
	}
	void setPlaylists( TracklistList* playlists )
	{
		sel_files->setPlaylists( playlists );
	}

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
	}
	void setLibrary( Library* library = 0 )
	{
		bt_path->show();
		ck_hide->show();
		disconnection();
		_library = library;
		lb_name->setText( _library->name() );
		QString path = _library->path();
		if ( path == "" )
			path = "[No Path]";
		bt_path->setText( path );
		ck_hide->setChecked( _library->hide() );
		sel_files->setLibrary( _library );
		connection();
	}
	void connection()
	{
		connect( lb_name, &LibraryLabel::editingFinished,
				 this, &LibraryWidget::changeName );
		connect( bt_path, &PathButton::pathChanged,
				 _library, &Library::setPath );
		connect( ck_hide, &QCheckBox::toggled,
				 _library, &Library::setHide );
		connect( _library, &Library::changed,
				 sel_files, &FileSelector::reset );
	}
	void disconnection()
	{
		if ( _library )
		{
			disconnect( lb_name, &LibraryLabel::editingFinished,
						this, &LibraryWidget::changeName );
			disconnect( bt_path, &PathButton::pathChanged,
						_library, &Library::setPath );
			disconnect( ck_hide, &QCheckBox::toggled,
						_library, &Library::setHide );
			disconnect( _library, &Library::changed,
						sel_files, &FileSelector::reset );
		}
	}
	void changeName()
	{
		_library->setName( lb_name->text() );
	}
	void update()
	{
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARYWIDGET_H
