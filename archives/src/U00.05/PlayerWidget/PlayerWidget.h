#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H
/**********************************************/
#include <QElapsedTimer>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QFrame>
#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QProgressBar>
#include <QTimer>
#include <QMouseEvent>
#include "../BasicWidgets/Frame.h"
#include "../Commons/Track.h"
#include "../Commons/TrackList.h"
#include "../PlayerWidget/SongInfoWidget.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
#include "../BasicWidgets/PlayerButton.h"
#include "../BasicWidgets/PlayerOptionButton.h"
#include "../BasicWidgets/AppButton.h"
#include "../BasicWidgets/SystemButton.h"
#include "../BasicWidgets/CoverImage.h"
#include "../BasicWidgets/TitleLabel.h"
#include "../BasicWidgets/ArtistLabel.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerWidget
		: public Frame
{
	Q_OBJECT
public:
	enum Repeat {
		NOREPEAT,
		REPEATALL,
		REPEATONE
	};

private:
	bool _movable = false;
	TrackList* _nowPlaylist = 0;
	QMediaPlayer* _player = new QMediaPlayer;
	QTimer* _timer = new QTimer(this);
	// Player Widgets
	PlayerButton* pb_play = new PlayerButton;
	bool _isPlaying = false;
	PlayerButton* pb_stop = new PlayerButton;
	PlayerButton* pb_backward = new PlayerButton;
	PlayerButton* pb_forward = new PlayerButton;
	PlayerOptionButton* pb_repeat = new PlayerOptionButton;
	Repeat _repeat = Repeat::NOREPEAT;
	PlayerOptionButton* pb_shuffle = new PlayerOptionButton;
	bool _isShuffle = false;
	PlayerOptionButton* pb_equalizer = new PlayerOptionButton;
	PlayerOptionButton* pb_volume = new PlayerOptionButton;
	//
	CoverImage* img_cover = new CoverImage;
	TitleLabel* lb_title = new TitleLabel;
	ArtistLabel* lb_artist = new ArtistLabel;
	SongInfoWidget* wid_songInfo = new SongInfoWidget;
	QSlider* sl_time = new QSlider;
	QLabel* lb_time = new QLabel;
	int _newPos = -1;
	int _duration = -1;
	// Application Button
	AppButton* pb_settings = new AppButton;
	// System Button
	SystemButton* pb_minimize = new SystemButton;
	SystemButton* pb_maximize = new SystemButton;
	SystemButton* pb_close = new SystemButton;
	Icons* _icons = 0;
	QList<int> _listOrder;
	int _index = 0;

public:
	~PlayerWidget();
	PlayerWidget( QWidget* parent = nullptr );
	void randomize();

public slots:
	void playTrack( Track* track );
	void addTrack( Track* track );
	void playTracklist( TrackList* tracklist );
	void addTracklist( TrackList* tracklist );
	void insertTracklist( TrackList* tracklist );
	void receiveTrackList( TrackList* tracklist );
	void setPlaylistIndex( int index );
	void setTrackList( TrackList* playlist );
	//
	void setTrack();
	void play();
	void pause();
	void playpause();
	void stop();
	void backward();
	void forward();
	void repeat();
	void shuffle();
	void changePosition( qint64 pos );
	void setPlayerPos( int pos );
	void updatePlayerPos();
	void updateIcons( Icons* icons );
	void updatePlayPauseIcon();
	void mousePressEvent( QMouseEvent* event ) override;
	void mouseReleaseEvent( QMouseEvent* event ) override;
	void mouseMoveEvent( QMouseEvent* event ) override;
	void mouseDoubleClickEvent( QMouseEvent* event ) override;
	void connectTo( Frame* frame );
	void onMediaStatusChanged();
	void updateTrackInfo( Track* track );

signals:
	void indexChanged();
	void playlistChanged( TrackList* list );
	void onMinimize();
	void onMaximize();
	void moveWindow( const QPoint& pos );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERWIDGET_H
