#include "PlaylistsWidget.h"
/**********************************************/
/**********************************************/
/* */
PlaylistsWidget::PlaylistsWidget( QWidget* parent )
	: Frame( parent )
{
	hide();
	addWidget( wid_playlistModifier, 0, 0 );
	addWidget( sel_playlist, 1, 0 );
	VerticalLayout* lay_right = new VerticalLayout;
	lay_right->addWidget( ban_playlist );
	lay_right->addWidget( wid_playlist );
	addLayout( lay_right, 0, 1, 2, 1 );
//	addWidget( wid_playlist, 0, 1, 2, 1 );

	//	connect( wid_libraryModifier, &TrackListModifier::toNew,
	//			 this, &LibrariesWidget::addNew );
		connect( wid_playlistModifier, &TrackListModifier::toPlay,
				 this, &PlaylistsWidget::playPlaylist );
		connect( wid_playlistModifier, &TrackListModifier::toAdd,
				 this, &PlaylistsWidget::addPlaylist );
		connect( wid_playlistModifier, &TrackListModifier::toDelete,
				 this, &PlaylistsWidget::deletePlaylist );

		connect( sel_playlist, &PlaylistSelector::indexToChange,
				 this, &PlaylistsWidget::showPlaylist );
//		connect( wid_playlist, &PlaylistSelector::indexToPlay,
//				 this, &PlaylistsWidget::sendTrackToPlay );
		connectTo( wid_playlist );
		connect( sel_playlist, &PlaylistSelector::toReset,
				 wid_playlist, &TrackSelector::reset );
}
/**********************************************/
/**********************************************/
