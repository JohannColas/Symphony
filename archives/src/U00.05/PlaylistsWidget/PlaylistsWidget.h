#ifndef PLAYLISTSWIDGET_H
#define PLAYLISTSWIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include "../Commons/TracklistList.h"
#include "../Commons/TrackListModifier.h"
#include "../PlaylistsWidget/PlaylistSelector.h"
#include "../PlaylistsWidget/PlaylistBanner.h"
#include "../TracksWidget/TrackSelector.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistsWidget
		: public Frame
{
	Q_OBJECT
private:
	TracklistList* _playlists = 0;
	TrackListModifier* wid_playlistModifier = new TrackListModifier;
	PlaylistSelector* sel_playlist = new PlaylistSelector;
	PlaylistBanner* ban_playlist = new PlaylistBanner;
	TrackSelector* wid_playlist = new TrackSelector;

public:
	PlaylistsWidget( QWidget* parent = 0 );

public slots:
	void updateIcons( Icons* icons )
	{
		wid_playlistModifier->updateIcons( icons );
		sel_playlist->updateIcons( icons );
		wid_playlist->updateIcons( icons );
		wid_playlist->allowRemove();
	}
	void updateLang( Lang* /*lang*/ )
	{
//		lb_title->setText( );
	}
	void setPlaylists( TracklistList* playlists = 0 )
	{
		_playlists = playlists;
		sel_playlist->setPlaylists( playlists );
		wid_playlist->setPlaylists( playlists );

		connect( playlists, &TracklistList::currentTrackListChanged,
				 wid_playlist, &TrackSelector::setTrackList );
//		connect( playlists, &TracklistList::currentTrackListChanged,
//				 wid_playlist, &TrackListSelector::setTrackList );
	}
	void showPlaylist( int index )
	{
		if ( index != -1 )
		{
			wid_playlist->setTrackList( _playlists->tracklist(index) );
			ban_playlist->setPlaylist( _playlists->tracklist(index) );
		}
	}
	void sendTrackToPlay( int index )
	{
		if ( index != -1 )
			emit trackToPlay( sel_playlist->currentTrackList()->track(index) );
	}
	void playPlaylist()
	{
		if ( _playlists->currentTracklist() )
			emit tracklistToPlay( _playlists->currentTracklist() );
	}
	void addPlaylist()
	{
		if ( _playlists->currentTracklist() )
			emit tracklistToAdd( _playlists->currentTracklist() );
	}
	void deletePlaylist()
	{
//		_tracklists->tracklist(0)->updateLibrary();
	}

signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTSWIDGET_H
