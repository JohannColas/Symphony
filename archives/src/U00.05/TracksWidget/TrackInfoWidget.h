#ifndef TRACKINFOWIDGET_H
#define TRACKINFOWIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
/**********************************************/
#include "../Commons/Track.h"
#include "../Commons/Metadata.h"
#include "../Commons/TrackInfo.h"
/**********************************************/
#include "../BasicWidgets/TextWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackInfoWidget
		: public Frame
{
	Q_OBJECT
protected:
	TextWidget* wid_description = new TextWidget;


public:
	TrackInfoWidget( QWidget* parent = 0 )
		: Frame( parent )
	{
		addWidget( wid_description, 0, 0 );
	}

public slots:
	void setTrack( Track* track = 0 )
	{
		if ( track )
		{
			TrackInfo trackInfo( track );
			wid_description->setText( trackInfo.album() );
		}
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKINFOWIDGET_H
