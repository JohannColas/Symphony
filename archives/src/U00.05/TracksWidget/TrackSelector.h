#ifndef TRACKSELECTOR_H
#define TRACKSELECTOR_H
/**********************************************/
#include <QInputDialog>
#include <QListWidget>
#include <QMenu>
#include "../BasicWidgets/ListWidget.h"
#include "../Commons/Icons.h"
#include "../Commons/TrackList.h"
#include "../Commons/TracklistList.h"
#include "../BasicWidgets/TrackItem.h"
#include "../LibrariesWidget/Libraries.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackSelector
		: public ListWidget
{
	Q_OBJECT
private:
	Icons* _icons = 0;
	TrackList* _tracklist = 0;
	TracklistList* _playlists = 0;

public:
	TrackSelector( QWidget* parent = nullptr )
		: ListWidget( parent )
	{
//		setMaximumWidth( 400 );
//		setStyleSheet( "background:blue;");
		connect( this, &TrackSelector::indexToChange,
				 this, &TrackSelector::indexChange );

	}
	void setPlaylists( TracklistList* playlists )
	{
		_playlists = playlists;
	}
	Track* currentTrack()
	{
		if ( _tracklist && index() != -1 )
			return _tracklist->track( index() );
		return 0;
	}

public slots:
	void connectTrackList()
	{
		connect( _tracklist, &TrackList::changed,
				 this, &TrackSelector::update );
		if ( _tracklist->type() == TrackList::PLAYLIST )
		{
			connect( this, &TrackSelector::removeAll,
					 _tracklist, &TrackList::removeAll );
			connect( this, &TrackSelector::tracklistToRemove,
					 _tracklist, &TrackList::remove );
		}
	}
	void disconnectTrackList()
	{
		if ( _tracklist )
		{
			disconnect( _tracklist, &TrackList::changed,
						this, &TrackSelector::update );
		}
	}
	void reset()
	{

	}
	void setTrackList( TrackList* tracklist )
	{
		disconnectTrackList();
		_tracklist = tracklist;
		connectTrackList();
		update();
	}
	void setLibraries( Libraries* libraries = 0 )
	{
		if ( libraries )
		{
			setTrackList( libraries->toTracklist() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		update();
	}
	void update() override
	{
		if ( _tracklist )
		{
			clear();
			int x = contentsMargins().left();
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();
			int nbItems = 0;
			for ( int it = _begin; it < _tracklist->count(); ++it )
			{
//				Metadata meta( _tracklist->track(it) );
				QString options = "cover:||/title/artist/||:--:duration";
				options = "cover:title/artist:--:duration";
//				options = "cover:||/title/artist/album/duration/||:--";
				QIcon icon = QIcon();
				if ( _icons )
					icon = _icons->get("track");
				TrackItem* trackItem = new TrackItem( _tracklist->track(it), options, icon, this );
				if ( it == _tracklist->currentTrackIndex() )
					trackItem->setChecked( true );
				trackItem->setGeometry( x, totheight,
										w,
										trackItem->sizeHint().height() );
				// Adding the Item to TrackSelector
				addItem( trackItem );
				totheight += trackItem->sizeHint().height();
				if ( totheight > height() )
				{
					break;
				}
				++nbItems;
			}
			updateScrollbar( _tracklist->count() );
		}
	}
	void showContextMenu( const QPoint& pos ) override
	{
		//int index = getIndexByPos( pos );
		QMenu* menu = new QMenu( this );
		// Adding Play/Add/Insert Options
		QAction* ac_play = menu->addAction( "Play Now (and remove Current Playlist)" );
		QAction* ac_add = menu->addAction( "Add to Current Playlist" );
		//QAction* ac_add = menu->addAction( "Add and Play to Current Playlist" );
		QAction* ac_insert = menu->addAction( "Insert after the Current Track" );
		menu->addSeparator();
		// Adding Playlists Menu
		QMenu* ac_addToPlaylist = menu->addMenu( "Add to a Playlist" );
		QList<QAction*> playlists;
		for ( TrackList* elm : _playlists->list() )
		{
			QAction* playlist = ac_addToPlaylist->addAction( elm->name() );
			playlists.append( playlist );
		}
		// Adding Remove Options
		QAction* ac_remove;
		QAction* ac_removeall;
		if ( _allowRemove )
		{
			menu->addSeparator();
			ac_remove = menu->addAction( "Remove" );
			ac_removeall = menu->addAction( "Remove All" );
		}
		QAction* action = menu->exec( this->mapToGlobal(pos) );
		if ( action == ac_play )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToPlay( tltmp );
		}
		else if ( action == ac_add )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToAdd( tltmp );
		}
		else if ( action == ac_insert )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToInsert( tltmp );
		}
		else if ( action == ac_remove )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToRemove( tltmp );
		}
		else if ( action == ac_removeall )
		{
			emit removeAll();
		}
		for ( int it = 0; it < playlists.size(); ++it )
		{
			if ( action == playlists.at(it) )
			{
				TrackList* tltmp = selectedTracks();
				TrackList* pltmp = _playlists->tracklist(it);
				if ( tltmp && pltmp )
					pltmp->add(tltmp);
				break;
			}
		}
	}
	void indexChange( int index )
	{
		emit trackChanged( _tracklist->track(index) );
	}
	TrackList* selectedTracks()
	{
		TrackList* list = new TrackList;
		int lt = 0;
		for ( Track* track : _tracklist->list() )
		{
			if ( _selection.contains(lt) )
			{
				list->append( track );
				if ( _selection.size() == 1 )
					break;
			}
			++lt;
		}
		if ( list->count() == 0 )
		{
			delete list;
			return 0;
		}
		return list;
	}

signals:
	void trackChanged( Track* track );
	void currentTracklistChanged( TrackList* tracklist );
	void renamed( const QString& name );
	void deleted();
	void removeAll();
	void tracklistToRemove( TrackList* tracklist );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKSELECTOR_H
