#ifndef TRACKWIDGET_H
#define TRACKWIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QMediaPlayer>
#include "../Commons/Metadata.h"
#include "../Commons/Track.h"
#include "../LibrariesWidget/Libraries.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
#include "../TracksWidget/TrackBanner.h"
#include "../TracksWidget/TrackInfoWidget.h"
#include "../BasicWidgets/CoverImage.h"
#include "../BasicWidgets/TitleLabel.h"
#include "../BasicWidgets/ArtistLabel.h"
#include "../BasicWidgets/TabWidget.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackWidget
		: public Frame
{
	Q_OBJECT
protected:
	TrackList* _tracklist = 0;
	QMediaPlayer* _player = 0;
	TrackBanner* ban_track = new TrackBanner;
	CoverImage* lb_cover = new CoverImage;
	TitleLabel* lb_title = new TitleLabel;
	ArtistLabel* lb_artist = new ArtistLabel;
	TabWidget* tab_menu = new TabWidget;
	TrackInfoWidget* inf_track = new TrackInfoWidget;
//	TrackA* label = new TrackA;
	Icons* _icons = 0;
	//	QLabel* lb_Title = new QLabel;
	//	QLabel* lb_Title = new QLabel;
	//	QLabel* lb_Title = new QLabel;
public:
	~TrackWidget()
	{
		delete lb_title;
		delete lb_artist;
	}
	TrackWidget( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
//		addWidget( lb_cover, 0, 0 );
//		setAlignment( lb_cover, Qt::AlignCenter );
//		addWidget( lb_title, 1, 0 );
//		setAlignment( lb_title, Qt::AlignCenter );
//		addWidget( lb_artist, 2, 0 );
//		setAlignment( lb_artist, Qt::AlignCenter );
////		lay_main->addWidget( label, 4, 0, Qt::AlignCenter );
////		addSpacer( 3, 0 );
//		addWidget( tab_menu, 3, 0 );
		addWidget( ban_track, 0, 0 );
		addWidget( tab_menu, 1, 0 );
		tab_menu->addTab( "Info", inf_track );
		tab_menu->addTab( "Autres" );
		tab_menu->changeTab(0);

		lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );

	}

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		if ( libraries )
		{
			_tracklist = libraries->toTracklist();
		}
	}
	void setPlayer( QMediaPlayer* player )
	{
		_player = player;
		connect( player, SIGNAL(metaDataChanged()),
				 this, SLOT(update()) );
	}
	void setTrack( Track* track )
	{
//		Metadata meta( track );
//		QPixmap pix = QPixmap();

//		if ( pix.convertFromImage( meta.cover() ) )
//			lb_cover->setIcon( pix );
//		else
//		{
//			if ( _icons )
//			{
//				lb_cover->setIcon( _icons->get("track") );
//			}
//		}

//		lb_title->setText( meta.title() );
//		lb_artist->setText( meta.artist() );
		ban_track->setTrack( track );
		inf_track->setTrack( track );
//		QIcon icon( pix );
//		label->setIcon( icon );
//		label->setIconSize( {150, 150} );
	}
	void update()
	{
		if ( _player )
		{
			QImage cover = _player->metaData("CoverArtImage").value<QImage>();
			QPixmap pix = QPixmap();
			if ( pix.convertFromImage( cover ) )
				lb_cover->setIcon( pix );
			else
			{
				if ( _icons )
				{
					lb_cover->setIcon( _icons->get("track") );
				}
			}

			lb_title->setText( _player->metaData("Title").toString() );
			lb_artist->setText( _player->metaData("ContributingArtist").toString() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		ban_track->updateIcons( _icons );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKWIDGET_H
