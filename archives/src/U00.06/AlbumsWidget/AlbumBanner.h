#ifndef ALBUMBANNER_H
#define ALBUMBANNER_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/Frame.h"
/**********************************************/
#include "../Commons/Album.h"
#include "../Commons/AlbumInfo.h"
/**********************************************/
#include "../BasicWidgets/AlbumLabel.h"
#include "../BasicWidgets/ThumbImage.h"
#include "../BasicWidgets/ArtistLabel.h"
#include "../BasicWidgets/CountLabel.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"

#include <QTimer>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumBanner
		: public Frame
{
	Q_OBJECT
protected:
	Album* _album = 0;
	AlbumInfo _albumInfo;
	ThumbImage* lb_cover = new ThumbImage;
	AlbumLabel* lb_title = new AlbumLabel;
	ArtistLabel* lb_artist = new ArtistLabel;
	CountLabel* lb_nbTracks = new CountLabel;
	Icons* _icons = 0;

public:
	~AlbumBanner()
	{
	}
	AlbumBanner( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( lb_cover, 0, 0, 2 );
		addWidget( lb_title, 0, 1, 1, 3 );
		addWidget( lb_artist, 1, 1 );
		addSpace( 10, 1, 2, false );
		addWidget( lb_nbTracks, 1, 3 );
		lb_title->setText("----");
		lb_artist->setText("----");
		lb_nbTracks->setText("");
		lb_nbTracks->fitWidthToContent();
		lb_nbTracks->avoidElide();

		lb_cover->setSizePolicy( QSizePolicy::Maximum,
								 QSizePolicy::Maximum );
		setSizePolicy( QSizePolicy::Preferred,
					   QSizePolicy::Maximum );
	}


public slots:
	void setAlbum( Album* album )
	{
		_album = album;
		update();
	}
	void update()
	{
		if ( _album )
		{
			_albumInfo.setAlbum(_album);
			QString title = _album->title();
			if ( title == "" )
				title = "[UNKNOWN]";
			lb_title->setText( title );

			QString artist = _album->artist();
			if ( artist == "" )
				artist = "[UNKNOWN]";
			lb_artist->setText( artist );
			int nb = _album->nbTracks();
			QString number = QString::number(nb);
			if ( nb < 2 )
				number += " track" ;
			else
				number += " tracks" ;
			lb_nbTracks->setText( number );
			//
			resize( width(), sizeHint().height() );
			repaint();
		}
	}
	void updateThumb()
	{
		// Applying default icon
		if ( _icons )
		{
			lb_cover->setIcon( _icons->get("album") );
		}
		if ( _album )
		{
			 QPixmap pix = _albumInfo.thumb();
			 // Applying album icon
			 if ( !pix.isNull() )
				 lb_cover->setIcon( pix );
			 // If the file doesn't exist, get it from Network
			 // (using setThumb)
			 else
			 {
				 Network* network = new Network;
				 connect( network, &Network::downloaded,
						  this, &AlbumBanner::setThumb );
				 network->download( _albumInfo.thumbUrl() );
			 }
		}
	}
	void setThumb( const QByteArray& data )
	{
		if ( data.length() > 0 )
		{
			QFile file( _albumInfo.path()+"/thumb.png" );
			file.open( QIODevice::WriteOnly );
			file.write( data );
			file.close();
			QPixmap pixmap;
			pixmap.loadFromData(data);
			lb_cover->setIcon( pixmap );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMBANNER_H
