#ifndef ALBUMWIDGET_H
#define ALBUMWIDGET_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include "../BasicWidgets/TabWidget.h"
/**********************************************/
#include "../AlbumsWidget/AlbumItem.h"
#include "../AlbumsWidget/AlbumBanner.h"
#include "../AlbumsWidget/AlbumInfoWidget.h"
#include "../AlbumsWidget/AlbumTracksWidget.h"
/**********************************************/
#include "../Commons/Libraries.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumWidget
		: public Frame
{
	Q_OBJECT
protected:
	Libraries* _libraries = 0;
	Album* _album = 0;
	AlbumBanner* inf_album = new AlbumBanner;
	TabWidget* tab_menu = new TabWidget;
	AlbumInfoWidget* info_album = new AlbumInfoWidget;
	AlbumTracksWidget* wid_tracks = new AlbumTracksWidget;
	Icons* _icons = 0;

public:
	AlbumWidget( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( inf_album, 0, 0 );
		addWidget( tab_menu, 1, 0 );
		tab_menu->addTab( "Tracks", wid_tracks );
		tab_menu->addTab( "Info", info_album );
		tab_menu->changeTab( 0 );
		connectTo( wid_tracks );
	}
	void setPlaylists( Playlists* playlists )
	{
		wid_tracks->setPlaylists( playlists );
	}

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
	}
	void setAlbum( const QString& /*album*/ )
	{
		update();
	}
	void setAlbum( Album* album )
	{
		_album = album;
		inf_album->setAlbum( album );
		wid_tracks->setAlbum( album );
		info_album->setAlbumInfo( new AlbumInfo(album) );
		update();
	}
	void setArtistByIndex( int index )
	{
		QList<Album*> albums = _libraries->albums();
		if ( -1 < index && index < albums.size() )
		{
			setAlbum( albums.at(index) );
		}
	}
	void update()
	{
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		inf_album->updateIcons( icons );
		wid_tracks->updateIcons( icons );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMWIDGET_H
