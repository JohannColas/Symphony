#ifndef ARTISTBANNER_H
#define ARTISTBANNER_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/Frame.h"
/**********************************************/
#include "../Commons/Artist.h"
#include "../Commons/ArtistInfo.h"
/**********************************************/
#include "../BasicWidgets/ArtistLabel.h"
#include "../BasicWidgets/ThumbImage.h"
#include "../BasicWidgets/CountLabel.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
#include "../Commons/Network.h"

#include <QTimer>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistBanner
		: public Frame
{
	Q_OBJECT
protected:
	Artist* _artist = 0;
	ArtistInfo _artistInfo;
	ThumbImage* lb_cover = new ThumbImage;
	ArtistLabel* lb_name = new ArtistLabel;
	CountLabel* lb_nbTracks = new CountLabel;
	Icons* _icons = 0;

public:
	~ArtistBanner()
	{
	}
	ArtistBanner( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( lb_cover, 0, 0, 2 );
		addWidget( lb_name, 0, 1 );
		addWidget( lb_nbTracks, 1, 1 );
		lb_name->setText("----");
		lb_nbTracks->setText("----");

		lb_cover->setSizePolicy( QSizePolicy::Maximum,
								 QSizePolicy::Maximum );
		setSizePolicy( QSizePolicy::Preferred,
					   QSizePolicy::Maximum );
	}

public slots:
	void setArtist( Artist* artist )
	{
		_artist = artist;
		update();
	}
	void update()
	{
		if ( _artist )
		{
			_artistInfo.setArtist(_artist);
			QString name = _artist->name();
			if ( name == "" )
				name = "[UNKNOWN]";
			lb_name->setText( name );
			updateThumb();
			QString infos;
			if ( _artist->nbAlbums() == 1 )
				infos = "1 album - ";
			else
				infos = QString::number( _artist->nbAlbums() ) + " albums - ";
			int nbTracks = 0;
			for ( Album* album : _artist->albums() )
				nbTracks += album->nbTracks();
			if ( nbTracks == 1 )
				infos += "1 track";
			else
				infos += QString::number( nbTracks ) + " tracks";
			lb_nbTracks->setText( infos );
			//
			resize( width(), sizeHint().height() );
			repaint();
		}
	}
	void updateThumb()
	{
		if ( _icons )
		{
			lb_cover->setIcon( _icons->get("artist") );
		}
		QPixmap pixmap;
		if ( _artist )
		{
			pixmap = _artistInfo.thumb();
			if (  !pixmap.isNull() )
			{
				lb_cover->setIcon( pixmap );
			}
			else
			{
				Network* network = new Network;
				connect( network, &Network::downloaded,
						 this, &ArtistBanner::setThumb );
				network->download( _artistInfo.thumbUrl() );
			}
		}
	}
	void setThumb( const QByteArray& data )
	{
		if ( data.length() > 0 )
		{
			QFile file( _artistInfo.path()+"/thumb.png" );
			file.open( QIODevice::WriteOnly );
			file.write( data );
			file.close();
			QPixmap pixmap;
			pixmap.loadFromData(data);
			lb_cover->setIcon( pixmap );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTBANNER_H
