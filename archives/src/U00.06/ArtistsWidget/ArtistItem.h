#ifndef ARTISTITEM_H
#define ARTISTITEM_H
/**********************************************/
/**********************************************/
#include "../BasicWidgets/ListItem.h"
//#include <QFuture>
//#include <QThread>
#include <QTimer>
/**********************************************/
#include "../Commons/Artist.h"
#include "../Commons/ArtistInfo.h"
/**********************************************/
#include "../BasicWidgets/ArtistLabel.h"
#include "../BasicWidgets/ThumbImage.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistItem
		: public ListItem
{
	Q_OBJECT
protected:
	Artist* _artist = 0;
	ArtistInfo _artistInfo;
	ThumbImage* thumb = new ThumbImage(this);


public:
	~ArtistItem()
	{
	}
	ArtistItem( Artist* artist, QIcon icon, QWidget* parent = 0 )
		: ListItem( parent )
	{
		_artist = artist;
		GridLayout* lay_col = new GridLayout;
		thumb->setIcon( icon );
		_artistInfo.setArtist( _artist );
		updateThumb();

		lay_col->addWidget( thumb , 0, 0 );
		QString name = artist->name();
		if ( name == "" )
			name = "[UNKNOWN]";
		lay_col->addWidget( new ArtistLabel( name ), 0, 1 );
		lay_main->addLayout( lay_col, 0, 0 );

		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}

public slots:
	void updateThumb()
	{
		QPixmap pixmap;
		if ( _artist )
		{
			pixmap = _artistInfo.thumb();
			if (  !pixmap.isNull() )
			{
				thumb->setIcon( pixmap );
			}
			else
			{
				Network* network = new Network;
				connect( network, &Network::downloaded,
						 this, &ArtistItem::setThumb );
				network->download( _artistInfo.thumbUrl() );
			}
		}
	}
	void setThumb( const QByteArray& data )
	{
		if ( data.length() > 0 )
		{
			QFile file( _artistInfo.path()+"/thumb.png" );
			file.open( QIODevice::WriteOnly );
			file.write( data );
			file.close();
			QPixmap pixmap;
			pixmap.loadFromData(data);
			thumb->setIcon( pixmap );
		}
	}

private:

signals:

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTITEM_H

