#ifndef ARTISTSELECTOR_H
#define ARTISTSELECTOR_H
/**********************************************/
#include "../BasicWidgets/ListWidget.h"
#include "../Commons/Icons.h"
//#include "../Commons/TrackList.h"
//#include "../Commons/Sorting.h"
#include "../Commons/Libraries.h"
#include "../ArtistsWidget/ArtistItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistSelector
		: public ListWidget
{
	Q_OBJECT
private:
	Libraries* _libraries = 0;
	QList<Artist*> _artists;
	Icons* _icons = 0;

public:
	ArtistSelector( QWidget *parent = 0 )
		: ListWidget( parent )
	{
		connect( this, &ArtistSelector::indexToChange,
				 this, &ArtistSelector::sendArtist );
		setMinimumWidth(300);
	}

public slots:
	void sendArtist( int index )
	{
		if ( isIndexValid(index) )
			emit artistChanged( _items.at(index-_begin)->text() );
	}
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
		_artists = _libraries->artists();

		update();
	}
	void update()
	{
		if ( _libraries )
		{
			clear();

			int x = contentsMargins().left();
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();
			int nbItems = 0;
			for ( int it = _begin; it < _artists.size(); ++it )
			{
				QIcon icon = QIcon();
				if ( _icons )
					icon = _icons->get("artist");
				ArtistItem* item = 0;
				item = new ArtistItem( _artists.at( it ), icon, this );
				item->setGeometry( x, totheight,
								   w,
								   item->sizeHint().height() );
				// Adding the Item to ArtistSelector
				addItem( item );

				totheight += item->sizeHint().height();
				if ( totheight > height() )
				{
					break;
				}
				++nbItems;
			}
			updateScrollbar( _artists.size() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}

signals:
	void artistChanged( const QString& artist );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTSELECTOR_H
