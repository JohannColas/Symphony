#ifndef LAYOUTS_H
#define LAYOUTS_H
/**********************************************/
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QWidget>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GridLayout
		: public QGridLayout
{
	Q_OBJECT
public:
	GridLayout( QWidget* parent = nullptr )
		: QGridLayout( parent )
	{
		setMargin( 0 );
		setSpacing( 0 );
	}
	void addWidget( QWidget* widget,
					int row, int col,
					int rowSpan = 1, int colSpan = 1 )
	{
		QGridLayout::addWidget( widget, row, col, rowSpan, colSpan );
	}
	void addLayout( QLayout* layout,
					int row, int col,
					int rowSpan = 1, int colSpan = 1 )
	{
		QGridLayout::addLayout( layout, row, col, rowSpan, colSpan );
	}
	void addSpace( int size, int row, int col, bool isVertical = true, int rowSpan = 1, int colSpan = 1 )
	{
		QSpacerItem* spacer;
		if ( isVertical )
			spacer  = new QSpacerItem( 0, size );
		else
			spacer  = new QSpacerItem( size, 0 );
		addItem( spacer, row, col, rowSpan, colSpan );
	}
	void addSpacer( int row, int col,
					bool isVertical = true,
					int rowSpan = 1, int colSpan = 1 )
	{
		QSpacerItem* spacer = 0;
		if ( isVertical )
			spacer = new QSpacerItem( 0, 0,
									  QSizePolicy::Maximum,
									  QSizePolicy::Expanding );
		else
			spacer = new QSpacerItem( 0, 0,
									  QSizePolicy::Expanding,
									  QSizePolicy::Maximum );
		if ( spacer )
			addItem( spacer, row, col, rowSpan, colSpan );
	}
	//	void addSpacer( int row, int col,
	//					int width = 0, int height = 0,
	//					int rowSpan = 1, int colSpan = 1  )
	//	{
	//		QSpacerItem* spacer = new QSpacerItem( width, height,
	//											   QSizePolicy::Maximum,
	//											   QSizePolicy::Maximum );
	//		if ( spacer )
	//			addItem( spacer, row, col, rowSpan, colSpan );
	//	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class VerticalLayout
		: public QVBoxLayout
{
	Q_OBJECT
public:
	VerticalLayout( QWidget* parent = nullptr )
		: QVBoxLayout( parent )
	{
		setMargin( 0 );
		setSpacing( 0 );
	}
	void addSpacer()
	{
		addItem( new QSpacerItem( 0, 0,
								  QSizePolicy::Maximum,
								  QSizePolicy::Expanding ) );
	}
	void addSpacer( int height )
	{
		addItem( new QSpacerItem( 0, height,
								  QSizePolicy::Maximum,
								  QSizePolicy::Maximum ) );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class HorizontalLayout
		: public QHBoxLayout
{
	Q_OBJECT
public:
	HorizontalLayout( QWidget* parent = nullptr )
		: QHBoxLayout( parent )
	{
		setMargin( 0 );
		setSpacing( 0 );
	}
	void addSpacer()
	{
		addItem( new QSpacerItem( 0, 0,
								  QSizePolicy::Expanding,
								  QSizePolicy::Maximum ) );
	}
	void addSpacer( int width )
	{
		addItem( new QSpacerItem( width, 0,
								  QSizePolicy::Maximum,
								  QSizePolicy::Maximum ) );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class FormLayout
		: public QFormLayout
{
	Q_OBJECT
public:
	FormLayout( QWidget* parent = nullptr )
		: QFormLayout( parent )
	{
		setMargin( 0 );
		setSpacing( 0 );
		setLabelAlignment( Qt::AlignRight );
		setFieldGrowthPolicy( QFormLayout::FieldsStayAtSizeHint );
	}
	//	void addSpacer()
	//	{
	//		addItem( new QSpacerItem( 0, 0,
	//								  QSizePolicy::Expanding,
	//								  QSizePolicy::Maximum ) );
	//	}
	//	void addSpacer( int width )
	//	{
	//		addItem( new QSpacerItem( width, 0,
	//								  QSizePolicy::Maximum,
	//								  QSizePolicy::Maximum ) );
	//	}
	void addLine( QWidget* label, QWidget* wid1, QWidget* wid2 )
	{
		//		QWidget* wid0 = new QWidget;
		GridLayout* lay0 = new GridLayout;
		lay0->setMargin( 0 );
		lay0->addWidget( wid1, 0, 0 );
		lay0->addWidget( wid2, 0, 1 );
		//		wid0->setLayout( lay0 );
		addRow( label, lay0 );
	}
	void addLine( QWidget* label, QWidget* wid1, QWidget* wid2, QWidget* wid3  )
	{
		//		QWidget* wid0 = new QWidget;
		GridLayout* lay0 = new GridLayout;
		lay0->setMargin( 0 );
		lay0->addWidget( wid1, 0, 0 );
		lay0->addWidget( wid2, 0, 1 );
		lay0->addWidget( wid3, 0, 2 );
		//		wid0->setLayout( lay0 );
		addRow( label, lay0 );
	}
	void addFullLine( QWidget* wid0, QWidget* wid1, QWidget* wid2 )
	{
		QWidget* widA = new QWidget;
		GridLayout* lay0 = new GridLayout;
		lay0->setMargin( 0 );
		lay0->addWidget( wid0, 0, 0 );
		lay0->addWidget( wid1, 0, 1 );
		lay0->addWidget( wid2, 0, 2 );
		widA->setLayout( lay0 );
		addRow( widA );
	}
	void addFullLine( QWidget* wid0, QWidget* wid1, QWidget* wid2, QWidget* wid3 )
	{
		QWidget* widA = new QWidget;
		GridLayout* lay0 = new GridLayout;
		lay0->setMargin( 0 );
		lay0->addWidget( wid0, 0, 0 );
		lay0->addWidget( wid1, 0, 1 );
		lay0->addWidget( wid2, 0, 2 );
		lay0->addWidget( wid3, 0, 3 );
		widA->setLayout( lay0 );
		addRow( widA );
	}
	void addFullLine( QList<QWidget*> widsList )
	{
		QWidget* widA = new QWidget;
		GridLayout* lay0 = new GridLayout;
		lay0->setMargin( 0 );
		int it = 0;
		for ( ; it < widsList.size(); ++it )
		{
			lay0->addWidget( widsList.at( it ), 0, it );
		}
		lay0->addItem( new QSpacerItem(0, 0, QSizePolicy::Expanding), 0, it );
		widA->setLayout( lay0 );
		addRow( widA );
	}
	void addFullLine( QWidget* wi0, QList<QWidget*> widsList )
	{
		QWidget* widA = new QWidget;
		GridLayout* lay0 = new GridLayout;
		lay0->setMargin( 0 );
		int it = 0;
		for ( ; it < widsList.size(); ++it )
		{
			lay0->addWidget( widsList.at( it ), 0, it );
		}
		lay0->addItem( new QSpacerItem(0, 0, QSizePolicy::Expanding), 0, it );
		widA->setLayout( lay0 );
		addRow( wi0, widA );
	}
	void addLineField( QWidget* wid1, QWidget* wid2 )
	{
		//		QWidget* wid0 = new QWidget;
		GridLayout* lay0 = new GridLayout;
		lay0->setMargin( 0 );
		lay0->addWidget( wid1, 0, 0 );
		lay0->addWidget( wid2, 0, 1 );
		//		wid0->setLayout( lay0 );
		addRow( new QWidget, lay0 );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LAYOUTS_H
