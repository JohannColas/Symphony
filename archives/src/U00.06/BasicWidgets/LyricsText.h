#ifndef LYRICSTEXT_H
#define LYRICSTEXT_H
/**********************************************/
#include "../BasicWidgets/TextWidget.h"
/**********************************************/
#include "../Commons/Track.h"
#include "../Commons/TrackInfo.h"
#include "../Commons/Metadata.h"
/**********************************************/
#include "../Commons/Network.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LyricsText
		: public TextWidget
{
	Q_OBJECT
public:
	~LyricsText()
	{

	}
	LyricsText( QWidget* parent = 0 )
		: TextWidget( parent )
	{
		setAlignment( Qt::AlignHCenter );
	}

public slots:
	void setTrack( Track* track = 0 )
	{
		if ( track )
		{
//			setText( "" );
			TrackInfo inf( track );
//			if ( inf.lyrics() != "" )
				setText( inf.lyrics() );
//			Network* network = new Network;
//			connect( network, &Network::downloaded,
//					 this, &LyricsText::setLyrics );
//			Metadata meta(track);
//			network->download( QString("http://api.chartlyrics.com/apiv1.asmx/SearchLyric?artist="+meta.artist()+"&song="+meta.title()) );
		}
	}
	void setLyrics( const QByteArray& data )
	{
		setText( data );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LYRICSTEXT_H
