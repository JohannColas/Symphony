#ifndef PLAYEROPTIONBUTTON_H
#define PLAYEROPTIONBUTTON_H
/**********************************************/
#include <QPushButton>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerOptionButton
		: public QPushButton
{
	Q_OBJECT

public:
	PlayerOptionButton( QWidget* parent = 0 )
		: QPushButton( parent )
	{
		setFlat( true );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYEROPTIONBUTTON_H
