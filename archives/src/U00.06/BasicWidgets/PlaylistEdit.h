#ifndef PLAYLISTEDIT_H
#define PLAYLISTEDIT_H
/**********************************************/
//#include "../BasicWidgets/Label.h"
#include <QLineEdit>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistEdit
		: public QLineEdit
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	PlaylistEdit( QWidget* parent = 0 )
		: QLineEdit( parent )
	{
	}
	PlaylistEdit( const QString& text, QWidget* parent = 0 )
		: QLineEdit( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTEDIT_H
