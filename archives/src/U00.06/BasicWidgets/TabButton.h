#ifndef TABBUTTON_H
#define TABBUTTON_H

#include <QPushButton>
#include <QKeyEvent>
#include <QFontMetrics>
#include <QDebug>
#include <QStyleOption>
#include <QPainter>
#include <QStyle>
#include <QLayout>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TabButton
		: public QPushButton
{
	Q_OBJECT
private:
	int index = 0;
	QString _text;

public:
	TabButton( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setSizePolicy( QSizePolicy::Maximum,
					   QSizePolicy::Preferred );
		setIconSize( {16, 16} );
	}
	TabButton( const QString& text, QWidget* parent = nullptr )
		: QPushButton( text, parent )
	{
		setSizePolicy( QSizePolicy::Maximum,
					   QSizePolicy::Preferred );
		setIconSize( {16, 16} );
		connect( this, &TabButton::clicked,
				 this, &TabButton::onClicked );

//		QFontMetrics metrics = this->fontMetrics();
//		this->setMaximumWidth( metrics.horizontalAdvance(this->text())+10 );
//		this->setMinimumWidth( metrics.horizontalAdvance(this->text())+10 );
	}

	void setSVG( const QString& path )
	{
		setIcon( QIcon( path ) );
	}
	int getIndex() const {
		return index;
	}
	void hideText() {
		QPushButton::setText( "" );
	}
	void showText() {
		QPushButton::setText( _text );
	}
	void setText( const QString& text ) {
		_text = text;
		showText();
	}
	QSize sizeHint() const override {
		if ( layout() ) {
			// the button now has a layout, return its size hint
			return layout()->sizeHint();
		}
		// no layout set, return the result of the default implementation
		return QPushButton::sizeHint();
	}


public slots:
	void setIndex( int ind ) {
		index = ind;
	}
	void onClicked()
	{

//		qDebug() << sizeHint() << size();
//		setChecked( !isChecked() );
//		repaint();
//		adjustSize();
//		update();
//		updateGeometry();
//		qDebug() << sizeHint() << size();
//		qDebug() << "---------";
//		emit released( text() );
		emit simpleClick();
		emit sendIndex( getIndex() );
	}
	void mouseReleaseEvent( QMouseEvent* event ) override
	{
		if ( event->button() == Qt::LeftButton ) {
//			emit released( text() );
//			qDebug() << sizeHint() << size();
//			repaint();
//			update();
//			updateGeometry();
//			qDebug() << sizeHint() << size();
//			qDebug() << "---------";
//			emit sendIndex( index );
//			emit simpleClick();
		}
		QPushButton::mouseReleaseEvent( event );
	}

protected:
	void paintEvent( QPaintEvent *event ) override {
		QPushButton::paintEvent( event );
	}

signals:
//	void released( const QString& text );
	void sendIndex( int index );
//	void repainted();
	void simpleClick();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TABBUTTON_H
