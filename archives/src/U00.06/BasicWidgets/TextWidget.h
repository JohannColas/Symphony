#ifndef TEXTWIDGET_H
#define TEXTWIDGET_H
/**********************************************/
#include <QTextEdit>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TextWidget
		: public QTextEdit
{
	Q_OBJECT
public:
	~TextWidget()
	{

	}
	TextWidget( QWidget* parent = 0 )
		: QTextEdit( parent )
	{

	}
	void mergeFormatOnWordOrSelection( const QTextCharFormat &format )
	{
		QTextCursor cursor = textCursor();
		if (!cursor.hasSelection())
			cursor.select(QTextCursor::WordUnderCursor);
		cursor.mergeCharFormat(format);
		mergeCurrentCharFormat(format);
	}

public slots:
	void changeSelectionFamily(const QString &f)
	{
		QTextCharFormat fmt;
		fmt.setFontFamily(f);
		mergeFormatOnWordOrSelection(fmt);
	}
	void changeSelectionSize( int size )
	{
		if ( size > 0) {
			QTextCharFormat fmt;
			fmt.setFontPointSize(size);
			mergeFormatOnWordOrSelection(fmt);
		}
	}
	void changeSelectionColor( const QColor& color )
	{
		QTextCharFormat fmt;
		fmt.setForeground( color );
		mergeFormatOnWordOrSelection( fmt );
	}
	void changeSelectionItalic( bool italic )
	{
		QTextCharFormat fmt;
		fmt.setFontItalic( italic );
		mergeFormatOnWordOrSelection(fmt);
	}
	void changeSelectionBold( bool bold )
	{
		QTextCharFormat fmt;
		fmt.setFontWeight( bold ? QFont::Bold : QFont::Normal );
		mergeFormatOnWordOrSelection(fmt);
	}
	void changeSelectionUnderline( bool underl )
	{
		QTextCharFormat fmt;
		fmt.setFontUnderline( underl );
		mergeFormatOnWordOrSelection(fmt);
	}
	void changeSelectionExponant( bool expon )
	{
		QTextCharFormat fmt;
		fmt.setVerticalAlignment( expon ? QTextCharFormat::AlignSuperScript : QTextCharFormat::AlignNormal );
		mergeFormatOnWordOrSelection(fmt);
	}
	void changeSelectionIndice( bool indic )
	{
		QTextCharFormat fmt;
		fmt.setVerticalAlignment( indic ? QTextCharFormat::AlignSubScript : QTextCharFormat::AlignNormal );
		mergeFormatOnWordOrSelection(fmt);
	}
	void changeSelectionAlign( const Qt::Alignment& align )
	{
		setAlignment( align );
	}
	void changeSelectionCap( const QFont::Capitalization& cap )
	{
		QTextCharFormat fmt;
		fmt.setFontCapitalization( cap );
		mergeFormatOnWordOrSelection(fmt);
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TEXTWIDGET_H
