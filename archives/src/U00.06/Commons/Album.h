#ifndef ALBUM_H
#define ALBUM_H
/**********************************************/
#include <QObject>
#include "../Commons/TrackList.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Album
		: public QObject
{
	Q_OBJECT
protected:
	QString _title = "";
	QString _artist = "";
	QList<Track*> _tracks;

public:
	Album( const QString& title, const QString& artist = "" )
		: _title( title ), _artist( artist )
	{

	}
	QString title() const
	{
		return _title;
	}
	QString artist() const
	{
		return _artist;
	}
	int nbTracks()
	{
		return _tracks.size();
	}
	QList<Track*> tracks()
	{
		return _tracks;
	}
	TrackList* toTracklist()
	{
		TrackList* tracklist = new TrackList;
		tracklist->add( tracks() );
		return tracklist;
	}
	void append( Album* album = 0 )
	{
		if ( album )
			for ( Track* track : album->tracks() )
				addTrack( track );
	}
	void addTrack( Track* track )
	{
		_tracks.append( track );
	}
	void addTrack( const QString& title )
	{
		_tracks.append( new Track(title) );
	}
	bool removeTrack( const QString& title )
	{
		for ( Track* track : _tracks )
		{
			if ( track->title() == title )
			{
				_tracks.removeOne( track );
				return true;
			}
		}
		return false;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUM_H
