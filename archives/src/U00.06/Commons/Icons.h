#ifndef ICONS_H
#define ICONS_H
/**********************************************/
#include <QObject>
#include <QMap>
#include <QIcon>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Icons
		: public QObject
{
	Q_OBJECT
private:
	QString _iconPack = "Dark Theme";
	QMap<QString, QIcon> _icons;

public:
	QIcon getIcon( const QString& name )
	{
		return QIcon( "./icons/" + _iconPack + "/" + name +".svg" );
	}
	QIcon get( const QString& name )
	{
		return QIcon( "./icons/" + _iconPack + "/" + name + ".svg" );
	}
	void update() {
		_icons.insert( "Symphony",
					   QIcon( "./icons/" + _iconPack + "/Symphony.svg" ) );
	}
	QString getCurrentIconPack() const {
		return _iconPack;
	}
	void setCurrentIconPack( const QString& value )	{
		_iconPack = value;
		update();
		emit changed();
	}

signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ICONS_H
