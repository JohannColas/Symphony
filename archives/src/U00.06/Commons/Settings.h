#ifndef SETTINGS_H
#define SETTINGS_H
/**********************************************/
#include <QApplication>
#include <QFile>
#include <QIcon>
#include <QSettings>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Settings
		: public QObject
{
	Q_OBJECT
private:
	QString _path = "./settings.ini";
	QString _theme = "";
	QString _icons = "";

public:
	Settings()
	{

	}
	void readSettings()
	{
		QSettings setsFile( _path, QSettings::IniFormat );
		_icons = setsFile.value("iconsPack").toString();
	}
	void saveSettings()
	{
		QFile file( _path );
		file.remove();

		QSettings setsFile( _path, QSettings::IniFormat );
		setsFile.setValue("iconsPack", _icons );
		//		setsFile->beginGroup("curves");
		//		setsFile->beginWriteArray("curve");
		//		for ( int it = 0; it < curvesSettings.size(); ++it ) {
		//			setsFile->setArrayIndex(it);
		//			curvesSettings.at(it)->saveSettings( setsFile );
		//		}
		//		setsFile->endArray();
		//		setsFile->endGroup();
	}
	QIcon icon( const QString& name )
	{
		QIcon icontmp = QIcon();
		if ( _icons == "" )
			icontmp = QIcon( "./icons/default/" + name + ".svg" );
		else
			icontmp = QIcon( "./icons/" + _icons + "/" + name + ".svg" );
		return icontmp;
	}
	void applyTheme() {
		if ( _theme == "" )
		{
			qApp->setStyleSheet( "" );
		}
		else
		{
			QFile file( "themes/" + _theme + "/stylesheet.qss" );
			if ( file.open(QFile::ReadOnly) )
			{
				QString style = QLatin1String( file.readAll() );
				style.remove('\n').remove('\t');
				qApp->setStyleSheet( style );
			}
			file.close();
		}
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGS_H
