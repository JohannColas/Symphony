#ifndef LIBRARIESWIDGET_H
#define LIBRARIESWIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include "../Commons/TrackListModifier.h"
#include "../LibrariesWidget/LibrarySelector.h"
#include "../LibrariesWidget/LibraryWidget.h"
#include "../TracksWidget/TrackSelector.h"
#include "../Commons/Playlists.h"
#include "../Commons/Libraries.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LibrariesWidget
		: public Frame
{
	Q_OBJECT
private:
	Libraries* _libraries = 0;
	TrackListModifier* wid_libraryModifier = new TrackListModifier;
	LibrarySelector* sel_library = new LibrarySelector;
	LibraryWidget* wid_library = new LibraryWidget;

public:
	LibrariesWidget( QWidget* parent = 0 )
		: Frame( parent )
	{
		//	hide();
		// Adding the widgets
		addWidget( wid_libraryModifier, 0, 0 );
		addWidget( sel_library, 1, 0 );
		addWidget( wid_library, 0, 1, 2, 1 );


		connect( wid_libraryModifier, &TrackListModifier::toPlay,
				 this, &LibrariesWidget::playLibrary );
		connect( wid_libraryModifier, &TrackListModifier::toAdd,
				 this, &LibrariesWidget::addLibrary );

		connect( sel_library, &LibrarySelector::indexToChange,
				 this, &LibrariesWidget::showLibrary );
	}
	void setPlaylists( Playlists* playlists )
	{
		wid_library->setPlaylists( playlists );
	}

public slots:
	void updateIcons( Icons* icons )
	{
		wid_libraryModifier->updateIcons( icons );
		sel_library->updateIcons( icons );
		wid_library->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
//		lb_title->setText( );
	}
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
		sel_library->setLibraries( _libraries );

		connect( wid_libraryModifier, &TrackListModifier::toNew,
				 _libraries, &Libraries::addNew );
		connect( wid_libraryModifier, &TrackListModifier::toUpdate,
				 _libraries, &Libraries::updateLibrary );
	}
	void showLibrary( int index )
	{
		if ( index != -1 )
			wid_library->setLibrary( _libraries->at( index ) );
	}
	void newLibrary()
	{
	}
	void playLibrary()
	{
	}
	void addLibrary()
	{
	}
	void updateLibrary()
	{
		_libraries->updateLibrary();
	}
	void deleteLibrary()
	{
	}

signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARIESWIDGET_H
