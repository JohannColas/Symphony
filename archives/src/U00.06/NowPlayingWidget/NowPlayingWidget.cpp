#include "NowPlayingWidget.h"
/**********************************************/
/**********************************************/
/* */
NowPlayingWidget::NowPlayingWidget( QWidget* parent )
	: Frame( parent )
{
	addWidget( wid_trackInfo, 0, 0 );
	addWidget( wid_trackList, 0, 1 );
	wid_trackList->allowRemove();

	wid_trackList->setMaximumWidth( 400 );
	wid_trackList->avoidMultipleSelection();
	wid_trackList->avoidRightClick();
	connect( wid_trackList, &TrackSelector::indexToPlay,
			 this, &NowPlayingWidget::indexChanged );
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::setPlayer( QMediaPlayer* /*player*/ )
{
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::setPlaylist( TrackList* playlist )
{
	_nowPlaylist = playlist;
	wid_trackList->setTrackList( _nowPlaylist );
	wid_trackInfo->setTrack( _nowPlaylist->track(0) );
	connect( wid_trackList, &TrackSelector::trackChanged,
			 wid_trackInfo, &TrackWidget::setTrack );
	connect( _nowPlaylist, &TrackList::changed,
			 this, &NowPlayingWidget::update );
	connect( _nowPlaylist, &TrackList::trackAdded,
			 this, &NowPlayingWidget::updateList );
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::setTrackList( TrackList* playlist )
{
	_nowPlaylist = playlist;
	wid_trackList->setTrackList( _nowPlaylist );
	wid_trackInfo->setTrack( _nowPlaylist->track(0) );
	connect( wid_trackList, &TrackSelector::trackChanged,
			 wid_trackInfo, &TrackWidget::setTrack );
	connect( _nowPlaylist, &TrackList::changed,
			 this, &NowPlayingWidget::update );
	connect( _nowPlaylist, &TrackList::trackAdded,
			 this, &NowPlayingWidget::updateList );
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::updateList()
{
	wid_trackList->update();
	wid_trackInfo->setTrack( _nowPlaylist->currentTrack() );
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::update()
{
	wid_trackList->setBegin( _nowPlaylist->currentTrackIndex() );
}
/**********************************************/
/**********************************************/
/* */
void NowPlayingWidget::updateIcons( Icons* icons )
{
	_icons = icons;
	wid_trackInfo->updateIcons( _icons );
	wid_trackList->updateIcons( _icons );
}
/**********************************************/
/**********************************************/
