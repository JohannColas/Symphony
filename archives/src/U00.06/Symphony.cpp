#include "Symphony.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
/**********************************************/
/**********************************************/
/* */
Symphony::~Symphony()
{
	delete _theme;
	delete _icons;
	delete _lang;
}
/**********************************************/
/**********************************************/
/* */
Symphony::Symphony( QWidget *parent )
	: Frame( parent )
{
	// Window Geometry
	setMinimumSize( 800, 500 );
	wid_player->setMinimumHeight( 50 );
	addWidget( wid_player, 0, 0, 1, 2 );
	addWidget( wid_sidebar, 1, 0, 1, 2 );

	// Adding TrackLists to Widgets
	wid_player->setTrackList( _nowPlaylist );
	wid_nowPlaying->setTrackList( _nowPlaylist );
	wid_artists->setLibraries( _libraries );
	wid_albums->setLibraries( _libraries );
	wid_tracks->setLibraries( _libraries );
	wid_genres->setLibraries( _libraries );
	wid_libraries->setLibraries( _libraries );

	wid_playlists->setPlaylists( _playlists );
	wid_nowPlaying->setPlaylists( _playlists );
	wid_artists->setPlaylists( _playlists );
	wid_albums->setPlaylists( _playlists );
//	wid_genres->setPlaylists( _playlists );
	wid_tracks->setPlaylists( _playlists );
	wid_libraries->setPlaylists( _playlists );
//	wid_radios->setTracklistList( _radios );
//	wid_podcasts->setTracklistList( _podcasts );


	// Adding Widgets to SidebarWidget
	wid_sidebar->setWidget( 0, wid_nowPlaying );
	wid_sidebar->setWidget( 1, wid_artists );
	wid_sidebar->setWidget( 2, wid_albums );
	wid_sidebar->setWidget( 3, wid_genres );
	wid_sidebar->setWidget( 4, wid_tracks );
	wid_sidebar->setWidget( 5, wid_playlists );
	wid_sidebar->setWidget( 6, wid_libraries );
	wid_sidebar->setWidget( 7, wid_radios );
	wid_sidebar->setWidget( 8, wid_podcasts );
	wid_sidebar->setWidget( 9, wid_settings );
//	wid_sidebar->setWidget( 10, wid_lyrics );
	wid_sidebar->changeCurrentWidget( 0 );

	// Connections between widgets
	connect( wid_nowPlaying, &NowPlayingWidget::indexChanged,
			 wid_player, &PlayerWidget::setPlaylistIndex );
	connect( wid_player, &PlayerWidget::playlistChanged,
			 wid_nowPlaying, &NowPlayingWidget::setPlaylist );
	connect( wid_player, &PlayerWidget::onMinimize ,
			 this, &Symphony::showMinimized );
	connect( wid_player, &PlayerWidget::onMaximize,
			 this, &Symphony::maximise );
	connect( wid_player, &PlayerWidget::moveWindow,
			 this, &Symphony::move );
	connect( wid_player, &PlayerWidget::showSettings,
			 this, &Symphony::showSettings );
	wid_player->connectTo( wid_artists );
	wid_player->connectTo( wid_albums );
	wid_player->connectTo( wid_tracks );
	wid_player->connectTo( wid_playlists );
	wid_player->connectTo( wid_libraries );


	// -----------------------
	// -----------------------
	// Themes
	_theme->apply();
	updateIcons();
	updateLang();
	// -----------------------
	// -----------------------
	setMouseTracking( true );
	// Update Sidebar size
	wid_sidebar->setTextVisibility( true );

}
/**********************************************/
/**********************************************/
/* */
void Symphony::showSettings()
{
	if ( wid_sidebar->selectedTab() != 9 )
	{
		_oldWidget = wid_sidebar->selectedTab();
		wid_sidebar->changeCurrentWidget( 9 );
	}
	else
	{
		wid_sidebar->changeCurrentWidget( _oldWidget );
	}
}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateSettings()
{
//	_settings->update();
//	wid_player->updateIcons( _settings );
//	wid_sidebar->updateIcons( _settings );
//	wid_nowPlaying->updateIcons( _settings );
//	wid_libraries->updateIcons( _settings );
}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateIcons()
{
	_icons->update();
	setWindowIcon( _icons->get( "Symphony" ) );
	wid_player->updateIcons( _icons );
	wid_sidebar->updateIcons( _icons );
	wid_nowPlaying->updateIcons( _icons );
	wid_artists->updateIcons( _icons );
	wid_albums->updateIcons( _icons );
	wid_tracks->updateIcons( _icons );
	wid_playlists->updateIcons( _icons );
	wid_libraries->updateIcons( _icons );
}
/**********************************************/
/**********************************************/
/* */
void Symphony::updateLang()
{
	_lang->update();

}
/**********************************************/
/**********************************************/
/* */
void Symphony::move( const QPoint& pos )
{
	if ( _lastPos.x() != -1 && pos.x() != -1 && !isMaximized() )
	{
		Frame::move( this->pos() - _lastPos + pos );
	}
	_lastPos = pos;
}
/**********************************************/
/**********************************************/
/* */
void Symphony::closeEvent( QCloseEvent* event )
{
	QFrame::closeEvent( event );
	qDebug().noquote() << "";
	qDebug().noquote() << "--------------";
	qDebug().noquote() << "Saving ...";
	QElapsedTimer timer;
	timer.start();
	_nowPlaylist->save();
	for ( TrackList* playlist : _playlists->list() )
		playlist->save();
	_libraries->saveLibraries();
	QString time = QString::number( timer.elapsed() );
	time.insert( time.length()-3, "s " );
	time += "ms";
	qDebug().noquote() << "Time to save Libraries & Playlists : " + time;
	qDebug().noquote() << "--------------";
}
/**********************************************/
/**********************************************/
