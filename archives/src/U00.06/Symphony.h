#ifndef WIDGET_H
#define WIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include "../Commons/Settings.h"
#include "../Commons/Theme.h"
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
#include "../Commons/TrackList.h"
#include "../Commons/Playlists.h"
#include "../Commons/Libraries.h"
#include <QListWidget>
/**********************************************/
#include "../PlayerWidget/PlayerWidget.h"
#include "../SidebarWidget/SidebarWidget.h"
#include "../NowPlayingWidget/NowPlayingWidget.h"
#include "../ArtistsWidget/ArtistsWidget.h"
#include "../AlbumsWidget/AlbumsWidget.h"
#include "../GenresWidget/GenresWidget.h"
#include "../TracksWidget/TracksWidget.h"
#include "../PlaylistsWidget/PlaylistsWidget.h"
#include "../LibrariesWidget/LibrariesWidget.h"
#include "../RadiosWidget/RadiosWidget.h"
#include "../PodcastsWidget/PodcastsWidget.h"
#include "../SettingsWidget/SettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Symphony
		: public Frame
{
	Q_OBJECT
protected:
	// Window Parameters
	int invisibleBorderSize = 2;
	bool isPressedWidget;
	QPoint _lastPos = {-1,0};
	int _oldWidget = 0;
	// Initialise the TrackList
	TrackList* _nowPlaylist = new TrackList( "#NowPlaylist", TrackList::NOWPLAYLIST );
	Playlists* _playlists = new Playlists;
	Libraries* _libraries = new Libraries;
//	Radios* _radios = new Radios;
//	Podcasts* _radios = new Podcasts;
	// Initialise the Widgets
	PlayerWidget* wid_player = new PlayerWidget;
	SidebarWidget* wid_sidebar = new SidebarWidget;
	NowPlayingWidget* wid_nowPlaying = new NowPlayingWidget;
	ArtistsWidget* wid_artists = new ArtistsWidget;
	AlbumsWidget* wid_albums = new AlbumsWidget;
	GenresWidget* wid_genres = new GenresWidget;
	TracksWidget* wid_tracks = new TracksWidget;
	PlaylistsWidget* wid_playlists = new PlaylistsWidget;
	LibrariesWidget* wid_libraries = new LibrariesWidget;
	RadiosWidget* wid_radios = new RadiosWidget;
	PodcastsWidget* wid_podcasts = new PodcastsWidget;
	SettingsWidget* wid_settings = new SettingsWidget;
	// Application Settings
	Theme* _theme = new Theme;
	Icons* _icons = new Icons;
	Lang* _lang = new Lang;

public:
	~Symphony();
	Symphony( QWidget *parent = nullptr );


public slots:
	void showSettings();
	void updateSettings();
	void updateIcons();
	void updateLang();
	void move( const QPoint& pos );
	void closeEvent( QCloseEvent* event );
	void maximise()
	{
		isMaximized() ? showNormal() : showMaximized();
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WIDGET_H
