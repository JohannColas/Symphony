#ifndef TRACKWIDGET_H
#define TRACKWIDGET_H
/**********************************************/
#include "../BasicWidgets/Frame.h"
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QMediaPlayer>
#include "../Commons/Metadata.h"
#include "../Commons/Track.h"
#include "../Commons/Libraries.h"
/**********************************************/
#include "../Commons/Icons.h"
#include "../Commons/Lang.h"
/**********************************************/
#include "../TracksWidget/TrackBanner.h"
#include "../TracksWidget/TrackInfoWidget.h"
#include "../BasicWidgets/CoverImage.h"
#include "../BasicWidgets/TitleLabel.h"
#include "../BasicWidgets/ArtistLabel.h"
#include "../BasicWidgets/TabWidget.h"
#include "../BasicWidgets/LyricsText.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackWidget
		: public Frame
{
	Q_OBJECT
protected:
	TrackList* _tracklist = 0;
	QMediaPlayer* _player = 0;
	TrackBanner* ban_track = new TrackBanner;
	TabWidget* tab_menu = new TabWidget;
	TrackInfoWidget* inf_track = new TrackInfoWidget;
	LyricsText* txt_lyrics = new LyricsText;
	Icons* _icons = 0;
public:
	~TrackWidget()
	{
	}
	TrackWidget( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( ban_track, 0, 0 );
		addWidget( tab_menu, 1, 0 );
		tab_menu->addTab( "Info", inf_track );
		tab_menu->addTab( "Lyrics", txt_lyrics );
		tab_menu->changeTab(0);

		Network* network = new Network;
		connect( network, &Network::downloaded,
				 inf_track, &TrackInfoWidget::showDownload );
		network->download( QString("https://en.wikipedia.org/wiki/ID3#ID3v2") );

	}

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		if ( libraries )
		{
			_tracklist = libraries->toTracklist();
		}
	}
	void setPlayer( QMediaPlayer* player )
	{
		_player = player;
		connect( player, SIGNAL(metaDataChanged()),
				 this, SLOT(update()) );
	}
	void setTrack( Track* track )
	{
		ban_track->setTrack( track );
		inf_track->setTrack( track );
		txt_lyrics->setTrack( track );
	}
	void update()
	{
		if ( _player )
		{
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		ban_track->updateIcons( _icons );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKWIDGET_H
