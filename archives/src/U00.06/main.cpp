#include "Symphony.h"
#include "FrameLess.h"
/**********************************************/
#include <QApplication>
/**********************************************/
/**********************************************/
/* */
int main( int argc, char *argv[] )
{
	QApplication a( argc, argv );
	Symphony w;
	FrameLess frameless(&w);
	w.show();
	return a.exec();
}
/**********************************************/
/**********************************************/
