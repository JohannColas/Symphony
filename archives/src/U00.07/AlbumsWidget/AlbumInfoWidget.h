#ifndef ALBUMINFOWIDGET_H
#define ALBUMINFOWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "BasicWidgets/TextWidget.h"
#include "BasicWidgets/WebsiteLabel.h"
/**********************************************/
#include "Commons/AlbumInfo.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumInfoWidget
		: public Frame
{
	Q_OBJECT
private:
	TextWidget* txt_description = new TextWidget;
//	WebsiteLabel* lb_website = new WebsiteLabel;


public:
	AlbumInfoWidget( QWidget* parent = 0 )
		: Frame( parent )
	{

		addWidget( txt_description, 0, 0 );
//		addWidget( lb_website, 1, 0 );
		txt_description->setAlignment( Qt::AlignJustify );
//		lb_website->setTextFormat( Qt::RichText );
//		lb_website->setTextInteractionFlags( Qt::TextBrowserInteraction );
//		lb_website->setOpenExternalLinks(true);
	}

public slots:
	void setAlbumInfo( AlbumInfo* info = 0 )
	{
		if ( info )
		{
			txt_description->setHtml( info->description() );
//			lb_website->setText( info->website() );
		}
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMINFOWIDGET_H
