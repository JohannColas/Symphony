#ifndef ALBUMTRACKSWIDGET_H
#define ALBUMTRACKSWIDGET_H
#include <QMenu>
/**********************************************/
#include "BasicWidgets/ListWidget.h"
#include "Commons/Icons.h"
#include "Commons/TrackList.h"
#include "Commons/Playlists.h"
#include "Commons/Sorting.h"
#include "Commons/Album.h"
/**********************************************/
#include "BasicWidgets/TrackItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumTracksWidget
		: public ListWidget
{
	Q_OBJECT
private:
	Album* _album = 0;
	Playlists* _playlists = 0;
	Icons* _icons = 0;

public:
	AlbumTracksWidget( QWidget *parent = 0 )
		: ListWidget( parent )
	{
		setMinimumWidth(300);
		connect( this, &AlbumTracksWidget::indexToPlay,
				 this, &AlbumTracksWidget::toPlay );
	}
	void setPlaylists( Playlists* playlists )
	{
		_playlists = playlists;
	}

public slots:
	void sendAlbum( int index )
	{
		if ( isIndexValid(index) )
			emit albumChanged( _items.at(index)->text() );
	}
	void setAlbum( Album* album = 0 )
	{
		if ( album )
		{
			_album = album;
			update();
		}
	}
	void update() override
	{
		if ( _album )
		{
			clear();
			int x = contentsMargins().left();
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();
			int nbItems = 0;
			int it = 0;
			for ( Track* track : _album->tracks() )
			{
				++it;
				if ( it >= _begin )
				{
					QString options = "number:cover:||/title/artist/||:--:duration";
					TrackItem* item = new TrackItem(track, options, _icons->get("track"), this);
					// Adding the Item to AlbumTracksWidget
					addItem( item );
					item->setGeometry( x, totheight,
									   w,
									   item->sizeHint().height() );
					totheight += item->sizeHint().height();
					if ( totheight > height() )
					{
						break;
					}
					++nbItems;
				}
			}
			updateScrollbar( _album->nbTracks() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
	//
	void toPlay( int /*index*/ )
	{
		TrackList* tltmp = selectedTracks();
		if ( tltmp )
			emit tracklistToPlay( tltmp );
	}
	void showContextMenu( const QPoint& pos ) override
	{
		QMenu* menu = new QMenu( this );
		QAction* ac_play = menu->addAction( "Play Now (and remove Current Playlist)" );
		QAction* ac_add = menu->addAction( "Add to Current Playlist" );
		//QAction* ac_add = menu->addAction( "Add and Play to Current Playlist" );
		QAction* ac_insert = menu->addAction( "Insert after the Current Track" );
		menu->addSeparator();
		QMenu* ac_addToPlaylist = menu->addMenu( "Add to a Playlist" );
		QList<QAction*> playlists;
		for ( TrackList* elm : _playlists->list() )
		{
			QAction* playlist = ac_addToPlaylist->addAction( elm->name() );
			playlists.append( playlist );
		}
		QAction* action = menu->exec( this->mapToGlobal(pos) );
		if ( action == ac_play )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToPlay( tltmp );
		}
		else if ( action == ac_add )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToAdd( tltmp );
		}
		else if ( action == ac_insert )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToInsert( tltmp );
		}
		for ( int it = 0; it < playlists.size(); ++it )
		{
			if ( action == playlists.at(it) )
			{
				TrackList* tltmp = selectedTracks();
				TrackList* pltmp = _playlists->tracklist(it);
				if ( tltmp && pltmp )
					pltmp->add(tltmp);
				break;
			}
		}
	}
	TrackList* selectedTracks()
	{
		TrackList* list = new TrackList;
		int lt = 0;
		for ( Track* track : _album->tracks() )
		{
			if ( _selection.contains(lt) )
			{
				list->append( track );
				if ( _selection.size() == 1 )
					break;
			}
			++lt;
		}
		if ( list->count() == 0 )
		{
			delete list;
			return 0;
		}
		return list;
	}

signals:
	void albumChanged( const QString& album );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMTRACKSWIDGET_H
