#ifndef ARTISTLABEL_H
#define ARTISTLABEL_H
/**********************************************/
#include "BasicWidgets/Label.h"
#include "BasicWidgets/LockButton.h"
#include "BasicWidgets/ElideLabel.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistLabel
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	ArtistLabel( QWidget* parent = 0 )
		: Label( parent )
	{
		allowElide();
	}
	ArtistLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
		allowElide();
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTLABEL_H
