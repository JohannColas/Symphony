#ifndef FANARTIMAGE_H
#define FANARTIMAGE_H
/**********************************************/
#include "BasicWidgets/LockButton.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class FanartImage
		: public LockButton
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	FanartImage( QWidget* parent = 0 )
		: LockButton( parent )
	{
		setFlat( true );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setPixmap( const QPixmap& pix )
	{
		setIcon( pix );
	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FANARTIMAGE_H
