#ifndef LABEL_H
#define LABEL_H
/**********************************************/
#include <QFrame>
#include <QLabel>
#include <QDebug>
#include <QPainter>
#include <QTimer>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Label
		: public QLabel
{
	Q_OBJECT
private:
	QString _text = "";
	bool _elide = true;
	bool _multiLine = false;
	bool _fitWidthToContent = false;
	bool _adaptWidthToContent = false;
	bool _fitHeightToContent = true;
	QTimer* _timer = new QTimer(this);
	int _txtBegin = 0;
	bool _isElide = false;

public:
	Label( const QString& text, QWidget* parent = 0 )
		: QLabel( parent )
	{
		setText( text );
		init();
	}
	Label( QWidget* parent = 0 )
		: QLabel( parent )
	{
		init();
	}
	void init()
	{
		setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
		connect( _timer, &QTimer::timeout,
				 this, &Label::onTimerOut );
	}
	const QString& text() const
	{
		return _text;
	}
	bool isElided() const
	{
		return _elide;
	}

public slots:
	void fixWidth( int width )
	{
		_fitWidthToContent = true;
		setMaximumWidth( width );
	}
	void fixHeight( int height )
	{
		_fitHeightToContent = false;
		setFixedHeight( height );
	}
	void setText( const QString& text )
	{
		_text = text;
		repaint();
	}
	void allowElide()
	{
		_elide = true;
		unfitToContent();
	}
	void avoidElide()
	{
		_elide = false;
	}
	void allowMultiLine()
	{
		_multiLine = true;
	}
	void avoidMultiLine()
	{
		_multiLine = false;
	}
	void fitToContent()
	{
		_fitWidthToContent = true;
		_fitHeightToContent = true;
		_elide = false;
	}
	void fitWidthToContent()
	{
		_fitWidthToContent = true;
		_elide = false;
	}
	void adaptWidthToContent()
	{
		_adaptWidthToContent = true;
	}
	void fitHeightToContent()
	{
		_fitHeightToContent = true;
		_elide = false;
	}
	void unfitToContent()
	{
		_fitWidthToContent = false;
	}
	void update()
	{
	}

private:
	void paintEvent( QPaintEvent* event ) override
	{
		QFrame::paintEvent( event );

		QPainter painter( this );
		QFontMetrics metrix = painter.fontMetrics();
		int _width = 0;
		int _height = contentsMargins().top() +
					  contentsMargins().bottom();
		int x = contentsMargins().left();
		int y = metrix.ascent() + contentsMargins().top();
		int maxWidth = width();
		if ( maxWidth > maximumWidth() )
			maxWidth = maximumWidth();
		maxWidth -= (contentsMargins().left()+contentsMargins().right());
		QStringList lines = _text.split("\n");
		for ( QString line : lines )
		{
			// Elide line
			if ( _elide && !_fitWidthToContent )
			{
				QString line3 =  metrix.elidedText( line, Qt::ElideRight, maxWidth );
				if ( line3 == line )
					_isElide = false;
				else
					_isElide = true;
				QString line2 = line.remove(0, _txtBegin );

				line = metrix.elidedText( line2, Qt::ElideRight, maxWidth );
				if ( line.length() < 5 )
				{
					_txtBegin = 0;
				}
			}
			// Getting the maximum line width
			if ( _width < metrix.boundingRect( line ).width() )
				_width = metrix.boundingRect( line ).width();

			if ( alignment() == Qt::AlignRight )
			{
				x = maxWidth - metrix.boundingRect( line ).width();
			}
			painter.drawText( x, y, line );

			y += metrix.height();
			_height += metrix.height();
			if ( !_multiLine )
				break;
		}
		if ( _fitWidthToContent )
		{
			_width += contentsMargins().left() + contentsMargins().right();
			setFixedWidth( _width );
		}
		if ( _fitHeightToContent )
			setFixedHeight( _height );

	}
	void onTimerOut()
	{
		_timer->start( 200 );
		repaint();
		++_txtBegin;
//		if ( _txtBegin == 1 )
//			_timer->start( 600 );
//		else
//			_timer->start( 200 );
	}
	void enterEvent( QEvent* event ) override
	{
		if ( _isElide )
			_timer->start( 600 );
		QLabel::enterEvent( event );
	}
	void leaveEvent( QEvent* event ) override
	{
		_timer->stop();
		_txtBegin = 0;
		QLabel::leaveEvent( event );
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LABEL_H
