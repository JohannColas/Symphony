#ifndef TABWIDGET_H
#define TABWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include <QScrollArea>
#include <QScrollBar>
/**********************************************/
#include "BasicWidgets/TabButton.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TabWidget
				: public Frame
{
		Q_OBJECT
		Q_PROPERTY( Qt::Alignment halign READ getHalign WRITE setHalign )
		Q_PROPERTY( int pos READ getTabsPos WRITE setTabsPos )
public:
		enum Pos {
			TOP,
			RIGHT,
			BOTTOM,
			LEFT
	};
private:
//	GridLayout* layMain = new GridLayout;
	GridLayout* layTabs = nullptr;
//	QTabBar* tab = new QTabBar;
	QList<TabButton*> _tabs;
	QList<QWidget*> _widgets;
	int _nbTabs = 0;
	int _selectedTab = -1;
	TabWidget::Pos _pos = TabWidget::TOP;
	Qt::Alignment _Halign = Qt::AlignCenter;
	Qt::Alignment _Valign = Qt::AlignTop;


public:
	TabWidget( QWidget* parent = nullptr )
			: Frame( parent )
	{
		show();
		setFrameShape( QFrame::NoFrame );
		setMinimumWidth( 250 );
		changeTab( 0 );
		setSizePolicy( QSizePolicy::Preferred,
					   QSizePolicy::Expanding );
//		setLayout( layMain );
//		layMain->setSpacing(0);
//		layMain->setMargin(0);
	}
	void setHalign( Qt::Alignment align ) {
			_Halign = align;
	}
	Qt::Alignment getHalign() {
			return 0;
	}
	void setValign( const Qt::Alignment& align ) {
			_Valign = align;
	}
	Qt::Alignment getValign() {
			return _Valign;
	}
	void setTabsPos( const TabWidget::Pos& pos )
	{
		_pos = pos;
//		update();
	}
	void setTabsPos( int pos )
	{
		if ( pos == 1 )
			_pos = TabWidget::TOP;
		else if ( pos == 2 )
			_pos = TabWidget::RIGHT;
		else if ( pos == 3 )
			_pos = TabWidget::BOTTOM;
		else if ( pos == 4 )
			_pos = TabWidget::LEFT;
		update();
	}
	TabWidget::Pos getTabsPos()
	{
			return _pos;
	}
	void setAlignment( Qt::Alignment halign = Qt::AlignLeft,
					   Qt::Alignment valign = Qt::AlignBottom )
	{
			_Halign = halign;
		_Valign = valign;
	}
	void addTab( const QString& name, QWidget* widget = 0 )
	{
		TabButton* newTab = new TabButton( name );
		newTab->setCheckable( true );
		newTab->setChecked( false );
		newTab->setSizePolicy( QSizePolicy::Maximum,
							   QSizePolicy::Maximum );
		newTab->setIndex( _nbTabs );
		connect( newTab, &TabButton::sendIndex,
						 this, &TabWidget::changeTab );
//		connect( newTab, &PushButton::simpleClick,
//				 this, &TabWidget::update );
		_tabs.append( newTab );
		if ( widget )
			_widgets.append( widget );
		else
		{
			QWidget* wid = new QWidget;
			wid->setStyleSheet( "background:rgb("+QString::number(_nbTabs*25) +","+ QString::number(_nbTabs*30) +","+ QString::number((_nbTabs+1)*40) +");" );
			wid->setSizePolicy( QSizePolicy::Preferred,
								QSizePolicy::Expanding );
			_widgets.append( wid );
		}
		++_nbTabs;
		update();
	}
	void setTabName( int index, const QString& name )
	{
			if ( index > -1 && index < _nbTabs ) {
				_tabs.at( index )->setText( name );
			update();
		}
	}
	void setWidget( int index, QWidget* widget )
	{
			if ( index < _nbTabs ) {
				_widgets.replace( index, widget );
		}
	}
	void clearWidget()
	{
			lay_main->removeItem( layTabs );
		if ( layTabs != nullptr )
				delete layTabs;
	}

public slots:
	void update()
	{
		clearWidget();
		// Adding the tabs
		Qt::Alignment align = _Halign | _Valign;
		layTabs = new GridLayout;
		layTabs->setMargin( 0 );
		layTabs->setSpacing( 0 );
		layTabs->setContentsMargins(0,0,0,0);
		if ( _pos == TabWidget::LEFT || _pos == TabWidget::RIGHT ) {
			int it = 0, gap = 0, maxWidth = 0;
			int totHeight = 0;
			if ( _Valign != Qt::AlignTop )
			{
//				QSpacerItem* spacer = new QSpacerItem( 0,0, QSizePolicy::Minimum, QSizePolicy::Expanding );
//				layTabs->addItem( spacer, it, 0 );
				layTabs->addSpacer( it, 0, true );
				gap = 1;
			}
			for ( ; it < _nbTabs; ++it )
			{
				layTabs->addWidget( _tabs.at(it), it+gap, 0, 1, 1 );
				layTabs->setAlignment( _tabs.at(it), align );
				maxWidth = qMax( maxWidth, _tabs.at(it)->sizeHint().width() );
				totHeight += _tabs.at(it)->sizeHint().height();
			}
			if ( _Valign != Qt::AlignBottom )
			{
//				QSpacerItem* spacer = new QSpacerItem( 0,0, QSizePolicy::Minimum, QSizePolicy::Expanding );
//				layTabs->addItem( spacer, it+gap, 0 );
				layTabs->addSpacer( it+gap, 0 );
			}
			// ---------
			setMinimumWidth( maxWidth + 200 );
			//
			QWidget* widTabs = new QWidget;
//			widTabs->setStyleSheet( qApp->styleSheet() );
			widTabs->setLayout( layTabs );
			widTabs->setMinimumHeight( totHeight );
			widTabs->setFixedWidth( maxWidth );
			//
			QScrollArea* scroll = new QScrollArea;
//			scroll->setStyleSheet("background:transparent;border-color:transparent;");
			scroll->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Expanding );
			scroll->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
			scroll->setWidget( widTabs );
			//
			int col = 0;
			if ( _pos == TabWidget::RIGHT )
					col = 1;
			// Add
			addWidget( scroll, 0, col );

			int scrollBarWidth = scroll->verticalScrollBar()->sizeHint().width();
			int scrollHeight = lay_main->itemAtPosition(0,col)->geometry().height();

			if ( scrollHeight < totHeight ) {
					maxWidth += scrollBarWidth;
			}
			else {
					widTabs->setFixedHeight( scrollHeight );
				scroll->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
			}
			for ( int it = 0; it < layTabs->count(); ++it )
			{
				QLayoutItem* const item = layTabs->itemAt(it);
				if( dynamic_cast<QWidgetItem *>(item) )
					item->widget()->setFixedWidth( maxWidth );
			}

//			lay_main->itemAtPosition(0,col)->widget()->setFixedWidth(maxWidth);
		}
		else {
				QVBoxLayout* vLayout = new QVBoxLayout;
//			QFrame* hFrame = new QFrame;
//			hFrame->setFrameShape(QFrame::HLine);
//			if ( _pos == TabWidget::BOTTOM )
//				vLayout->addWidget( hFrame );
			QHBoxLayout* newHLay = getNewHLayout();
			vLayout->addLayout( newHLay );
			int totWidth = 0, totHeight = 0, tmpheight = 0;
			for ( int it = 0; it < _nbTabs; ++it ) {
					totWidth += _tabs.at(it)->sizeHint().width();
//				totWidth += _tabs.at(it)->width();
//				qDebug() << _tabs.at(it)->width() << _tabs.at(it)->sizeHint().width();
				if ( totWidth > width()-2*lay_main->margin() ) {
						totWidth = _tabs.at(it)->sizeHint().width();
//					totWidth = _tabs.at(it)->width();
					if ( _Halign != Qt::AlignRight ) {
							QSpacerItem* spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding );
						newHLay->addItem( spacer );
					}
					newHLay = getNewHLayout();
					vLayout->addLayout( newHLay );
					totHeight += tmpheight;
					tmpheight = 0;
				}
				tmpheight = qMax( tmpheight, _tabs.at(it)->sizeHint().height() );
				newHLay->addWidget( _tabs.at(it) );
				if ( it+1 == _nbTabs && _Halign != Qt::AlignRight ) {
						QSpacerItem* spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding );
					newHLay->addItem( spacer );
				}
			}
			QFrame* hFrame = new QFrame;
			hFrame->setFrameShape(QFrame::HLine);
			int row = 0;
			if ( _pos == TabWidget::BOTTOM ) {
					row = 2;
				addWidget( hFrame, row-1, 0 );
			}
			else
				addWidget( hFrame, 1, 0 );

			lay_main->addLayout( vLayout, row, 0 );
		}
		// Adding the widgets
		for ( int it = 0; it < _nbTabs; ++it ) {
				if ( _pos == TabWidget::LEFT ) {
					addWidget( _widgets.at(it), 0, 1 );
			}
			else if ( _pos == TabWidget::RIGHT ) {
					addWidget( _widgets.at(it), 0, 0 );
			}
			else if ( _pos == TabWidget::TOP ) {
					addWidget( _widgets.at(it), 2, 0 );
			}
			else {
					addWidget( _widgets.at(it), 0, 0 );
			}
			_widgets.at(it)->hide();
		}
		changeTab( _selectedTab );
	}
	void changeTab( int index )
	{
			if ( index > -1 && index < _nbTabs )
		{
				if ( _selectedTab > -1 && _selectedTab < _nbTabs ) {
					_tabs.at( _selectedTab )->setChecked( false );
				_widgets.at( _selectedTab )->hide();
			}
			if ( index > -1 && index < _nbTabs ) {
					_selectedTab = index;
				_tabs.at( index )->setChecked( true );
				_tabs.at( index )->repaint();
				_tabs.at( index )->updateGeometry();

//				qDebug() << _tabs.at( index )->text()
//						 << _tabs.at(index)->size()
//						 << _tabs.at(index)->sizeHint();
//				qDebug() << "-----";
				_widgets.at( index )->show();
			}
		}
	}
	void resizeEvent( QResizeEvent *event ) override {
			update();
		QWidget::resizeEvent( event );
	}

private:
	QHBoxLayout* getNewHLayout() {
			QHBoxLayout* lay = new QHBoxLayout;
		lay->setMargin(0);
		lay->setSpacing(0);
		lay->activate();
		if ( _Halign != Qt::AlignLeft ) {
				QSpacerItem* spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding );
			lay->addItem( spacer );
		}
		return lay;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TABWIDGET_H
