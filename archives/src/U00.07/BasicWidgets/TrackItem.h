#ifndef TRACKITEM_H
#define TRACKITEM_H
/**********************************************/
#include "BasicWidgets/ListItem.h"
#include "BasicWidgets/CoverImage.h"
#include "BasicWidgets/TitleLabel.h"
#include "BasicWidgets/ArtistLabel.h"
#include "BasicWidgets/AlbumLabel.h"
#include "BasicWidgets/GenreLabel.h"
#include "BasicWidgets/DurationLabel.h"
#include "BasicWidgets/NumberLabel.h"
#include "Commons/Track.h"
#include "Commons/TrackInfo.h"
#include "Commons/Metadata.h"

#include <QTimer>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackItem
		: public ListItem
{
	Q_OBJECT
protected:
	QString _options = "";
	Track* _track = 0;
	CoverImage* cover = 0;
	TrackInfo info;
	Metadata meta;
	/* ------------------------ */
	/* ------------------------ */
public:
	~TrackItem()
	{
	}
	TrackItem( Track* track, const QString& _options, const QIcon& icon, QWidget* parent = 0 )
		: ListItem( parent )
	{
		_type = ListItem::TRACKITEM;
		_track = track;
		meta.setTrack(track);
		info.setTrack( track );
		QStringList colOpts = _options.split(':');
		int itCol = 0;
		for ( QString col : colOpts )
		{
			int itRow = 0;
			if ( col == "--" )
			{
//				lay_main->addSpacer( itRow, itCol, false );
			}
			else
			{
				QStringList rowOpts = col.split('/');
				GridLayout* lay_col = new GridLayout;
				for ( QString row : rowOpts )
				{
					if ( row == "||" )
					{
//						lay_col->addSpacer( itRow, 0, true );
					}
					else if ( row == "title" )
					{
						QString strtitle = info.title();
						if ( strtitle == "" )
							strtitle = meta.title();
						if ( strtitle == "" )
                            strtitle = QUrl(track->path()).fileName();
						TitleLabel* title = new TitleLabel( strtitle );
						title->allowElide();
						lay_col->addWidget( title, itRow, 0 );
					}
					else if ( row == "artist" )
					{
						QString str = info.artist();
						if ( str == "" )
							str = meta.artist();
						if ( str == "" )
							str = "[UNKNOWN]";
						ArtistLabel* label = new ArtistLabel( str );
						label->allowElide();
						lay_col->addWidget( label, itRow, 0 );
					}
					else if ( row == "album" )
					{
						QString str = info.album();
						if ( str == "" )
							str = meta.album();
						if ( str == "" )
							str = "[UNKNOWN]";
						lay_col->addWidget( new AlbumLabel( str ), itRow, 0 );
					}
					else if ( row == "Genre" )
					{
						lay_col->addWidget( new GenreLabel( info.genre() ), itRow, 0 );
					}
					else if ( row == "number" )
					{
						NumberLabel* lb_numb = new NumberLabel( info.number() );
						lb_numb->setFixedWidth( 25 );
						lb_numb->setAlignment( Qt::AlignRight );
						lay_col->addWidget( lb_numb, itRow, 0 );

					}
					else if ( row == "cover" )
					{
						cover = new CoverImage;
						cover->setIcon( icon );
						updateCover();
						lay_col->addWidget( cover, itRow, 0 );
					}
					else if ( row == "duration" )
					{
						int dur = meta.duration();
						if ( dur == -1 )
							dur = 0;
						DurationLabel* lb_dur = new DurationLabel( dur );
						lb_dur->fitWidthToContent();
						lay_col->addWidget( lb_dur, itRow, 0 );
					}
					++itRow;
				}
				lay_main->addLayout( lay_col, 0, itCol );
			}
			++itCol;
		}
	}
	Track* track()
	{
		return _track;
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setOptions( const QString& options )
	{
		_options = options;
	}
	void update()
	{

	}
	void updateCover()
	{
		if ( _track && cover )
		{
			QPixmap pix = info.cover();
			if (  !pix.isNull() )
			{
				cover->setIcon( pix );
			}
			else
            {/*
				Network* network = new Network;
				connect( network, &Network::downloaded,
						 this, &TrackItem::setCover );
                network->download( info.coverUrl() );*/
			}
		}
	}
	void setCover( const QByteArray& data )
	{
		QPixmap pixmap;
		if ( data.length() > 0 )
		{
			QFile file( info.path()+"/cover.png" );
			file.open( QIODevice::WriteOnly );
			file.write( data );
			file.close();
			pixmap.loadFromData(data);
			cover->setIcon( pixmap );
		}
		else
		{
			if (  pixmap.convertFromImage(meta.cover()) )
				cover->setIcon( pixmap );
		}
	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKITEM_H
