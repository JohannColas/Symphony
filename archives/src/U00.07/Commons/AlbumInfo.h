#ifndef ALBUMINFO_H
#define ALBUMINFO_H
/**********************************************/
#include <QApplication>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QPixmap>
#include <QString>
/**********************************************/
#include "Commons/Album.h"
#include "Commons/Network.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumInfo
		: public QObject
{
	Q_OBJECT
public:
	enum Gender
	{
		MALE,
		FEMALE,
		NONE
	};

protected:
	Album* _album = 0;
	QString _path = "";
	QString _title = "";
	QString _description = "";
	int _releaseYear = -1;
	QString _genre = "";
	QString _style = "";
	QString _thumb = "";

public:
	AlbumInfo( Album* album = 0 )
	{
		setAlbum( album );
	}
	void setAlbum( Album* album )
	{
		if ( album )
		{
			if ( album->title() != "" )
			{
				_album = album;
				_path = "./libraries/artists/"+_album->artist()+"/"+_album->title();
				QDir dir;
				if ( !dir.exists( _path ) ) dir.mkpath( _path );
				update();
			}
		}
	}
	const QString& path()
	{
		return _path;
	}
	const QString& description()
	{
		return _description;
	}
	const QString& title()
	{
		return _title;
	}
	int releaseYear()
	{
		return _releaseYear;
	}
	const QString& genre()
	{
		return _genre;
	}
	const QString& style()
	{
		return _style;
	}
	const QString& thumbUrl()
	{
		return _thumb;
	}
	QPixmap thumb()
	{
		return pixmap( "thumb" );
	}
public slots:
	void update()
	{
		if ( _album )
		{
			QFile file( _path+"/album.alb" );
			if ( file.open( QIODevice::ReadOnly) )
			{
				read();
			}
			else
			{
				QString url = "https://www.theaudiodb.com/api/v1/json/1/searchalbum.php?a="+_album->title();
				url = url.replace(" ", "%20");

//				Network *network = new Network;
//				connect( network, &Network::downloaded,
//						 this, &AlbumInfo::readJson );
//				network->download( url );
			}
		}
	}
private:
	void read()
	{
		QSettings setsFile( _path+"/artist.alb", QSettings::IniFormat );
		_title = setsFile.value("title").toString();
		_description = setsFile.value("description").toString();
		_releaseYear = setsFile.value("releaseYear").toInt();
		_genre = setsFile.value("genre").toString();
		_style = setsFile.value("style").toString();
		_thumb = setsFile.value("thumb").toString();
	}
	void readJson( const QByteArray& data )
	{
		QJsonDocument jsonDoc = QJsonDocument::fromJson( data );
		QJsonObject jsonObject = jsonDoc.object();
		QVariantMap jsonMap = jsonObject.toVariantMap();

		for (auto elm : jsonMap.keys() )
		{
			if ( elm == "album" )
			{
				QJsonArray artistArray = jsonMap[elm].toJsonArray();
				QVariantMap artistMap = artistArray[0].toObject().toVariantMap();
				for (auto elm : artistMap.keys() )
				{
					if ( elm == "strAlbum" )
					{
						_title = artistMap[elm].toString();
					}
					else if ( elm == "intYearReleased" )
					{
						_releaseYear = artistMap[elm].toInt();
					}
					else if ( elm == "strAlbumThumb" )
					{
						_thumb = artistMap[elm].toString();
					}
					else if ( elm == "strDescriptionFR" )
					{
						_description = artistMap[elm].toString();
					}
					else if ( elm == "strGenre" )
					{
						_genre = artistMap[elm].toString();
					}
					else if ( elm == "strStyle" )
					{
						_style = artistMap[elm].toString();
					}
				}
			}
		}
		emit readJsonEnded();
		save();
	}
	void save()
	{
		QFile file( _path );
		file.remove();

		QSettings setsFile( _path+"/album.alb", QSettings::IniFormat );
		setsFile.setValue("title", _title );
		setsFile.setValue("description", _description );
		setsFile.setValue("releaseYear", _releaseYear );
		setsFile.setValue("genre", _genre );
		setsFile.setValue("style", _style );
		setsFile.setValue("thumb", _thumb );
	}
	const QPixmap pixmap( const QString& filePath )
	{
		QPixmap pixmap;
		pixmap.load( _path+"/"+filePath+".png" );
		return pixmap;
	}
	void saveThumb( const QByteArray& data )
	{
		if ( data.length() > 0 )
		{
			QFile file( _path+"/thumb.png" );
			if ( !file.exists() )
			{
				file.open( QIODevice::WriteOnly );
				file.write( data );
				file.close();
			}
		}
	}

signals:
	void readJsonEnded();
	void pixmapDownload( const QPixmap& pixmap );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMINFO_H
