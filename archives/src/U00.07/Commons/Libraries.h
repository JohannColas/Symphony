#ifndef LIBRARIES_H
#define LIBRARIES_H
/**********************************************/
#include <QObject>
#include <QFile>
#include <QDomDocument>
#include "Commons/Icons.h"
/**********************************************/
#include "Commons/Library.h"
#include "Commons/Artist.h"
/**********************************************/
#include "Commons/XML.h"
#include "Commons/OnLineMetadata.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Libraries
		: public QObject
{
	Q_OBJECT
protected:
	QString _path = "./libraries/#libraries.slbs";
	QString _libraryPath = "./libraries/#library.slb";
	QString _libraryName = "Library";
	QList<Library*> _libraries;
	QStringList _filesFilters = {"*.mp3", "*.ogg", ".wav", ".flac", ".m4a", ".wma"};
	QList<Artist*> _artists;
	QList<Album*> _albums;
	QList<Track*> _tracks;

public:
	Libraries()
	{
		initLibraries();
		initLibrary();
	}
	QStringList libraryNames()
	{
		QStringList names;
		for ( Library* library : _libraries )
			names.append( library->name() );
		return names;
	}
	int nbLibraries()
	{
		return _libraries.size();
	}
	Library* at( int index )
	{
		if ( -1 < index && index < _libraries.size() )
			return _libraries.at( index );
		return 0;
	}
	void connectTo( Library* library )
	{
		connect( library, &Library::changed,
				 this, &Libraries::changed );
	}
	void disconnectTo( Library* library )
	{
		if ( library )
		{
			disconnect( library, &Library::changed,
						this, &Libraries::changed );
		}
	}

public slots:
	void addNew()
	{
		int idLy = 0;
		for( Library* library : _libraries )
		{
			if ( library->name().contains( _libraryName ) )
			{
				int lyID = library->name().remove( _libraryName + " ").toInt();
				if ( lyID == idLy )
					++idLy;
				else if ( lyID > idLy )
					idLy = lyID + 1;
			}
		}
		QString newname = _libraryName + " " + QString::number(idLy);
		_libraries.append( new Library( newname ) );
		emit changed();
	}
	void initLibraries()
	{
		// Getting the XML data from file
		QDomDocument xmlDoc = XML::read( _path );
		if ( xmlDoc.isNull() )
			return;
		// Getting root element ("libraries")
		QDomElement el_libraries = xmlDoc.documentElement();
		// Getting library elements
		QDomElement el_libray = el_libraries.firstChild().toElement();
		while ( !el_libray.isNull() )
		{
			QString name = el_libray.attribute("name");
			QString path = el_libray.attribute("path");
			bool hide = (el_libray.attribute("hide") == "true") ? true : false;
			Library* library = new Library( name, path, hide );
			connectTo( library );
			_libraries.append( library );
			el_libray = el_libray.nextSiblingElement();
		}
	}
	void saveLibraries()
	{
		// Create the XML data from file
		QDomDocument xmlDoc;
		// Getting root element ("libraries")
		QDomElement el_libraries = xmlDoc.createElement("libraries");
		xmlDoc.appendChild( el_libraries );
		// Getting library elements
		for ( Library* library : _libraries )
		{
			QDomElement el_libray = xmlDoc.createElement("library");
			el_libray.setAttribute( "name", library->name() );
			el_libray.setAttribute( "path", library->path() );
			el_libray.setAttribute( "hide", library->hide() );
			el_libraries.appendChild( el_libray );
		}
		XML::save( _path, xmlDoc );
	}
	void initLibrary()
	{
		_artists.clear();
		_albums.clear();
		_tracks.clear();
		// ~ 20ms for 2500 tracks
		// Getting the XML data from file
		QDomDocument xmlDoc = XML::read( _libraryPath );
		if ( xmlDoc.isNull() )
			return;
		// Getting root element ("library")
		QDomElement el_libray = xmlDoc.documentElement();
		// Getting First Artist element
		QDomElement el_artist = el_libray.firstChild().toElement();
		Album* unknownAlbum = new Album("");
		_albums.append( unknownAlbum );
		while ( !el_artist.isNull() )
		{
			Artist* artist = new Artist( el_artist.attributeNode("name").value() );
			QDomElement el_album = el_artist.firstChild().toElement();
			while ( !el_album.isNull() )
			{
				Album* album = new Album( el_album.attributeNode("title").value(), artist->name() );
				QDomElement el_track = el_album.firstChild().toElement();
				while ( !el_track.isNull() )
				{
					Track* track = new Track( el_track.attributeNode("path").value() );
					track->setTitle( el_track.attributeNode("title").value() );
					album->addTrack( track );
					_tracks.append( track );
					el_track = el_track.nextSiblingElement();
				}
				if ( album->title() == "" )
				{
					unknownAlbum->append( album );
				}
				else
				{
					_albums.append( album );
				}
				artist->addAlbum( album );
				el_album = el_album.nextSiblingElement();
			}
			_artists.append( artist );
			el_artist = el_artist.nextSiblingElement();
		}
		sortArtistByName();
		emit changed();
        OnlineMetadata* meta = new OnlineMetadata( _artists );
        meta->start();
	}
	void updateLibrary()
	{
		//
		QDomDocument xmlDoc;
		// Writing the Settings of XML Files
		QDomElement el_library = xmlDoc.createElement("library");
		xmlDoc.appendChild( el_library );

		for ( Library* library : _libraries )
		{
			if ( !library->hide() )
			{
				// Getting directory path
				QDir dir( library->path() );
				dir.setSorting( QDir::SortFlag::Name |
								QDir::IgnoreCase |
								QDir::LocaleAware );
				dir.setFilter( QDir::Files );
				dir.setNameFilters( _filesFilters );
				// Getting files in directory
				for ( QString trackpath : dir.entryList() )
				{
					// Getting the Metadata of the current file
					Metadata meta( new Track( library->path() + "/" + trackpath) );
					// Getting the Artist
					bool artistExits = false;
					QDomElement el_artist = el_library.firstChildElement();
					// Checking if the Artist exits in the file
					while ( !el_artist.isNull() )
					{
						if ( el_artist.attribute("name") == meta.artist() )
						{
							artistExits = true;
							break;
						}
						el_artist = el_artist.nextSiblingElement();
					}
					// if Artist DONT exits, create it
					if ( !artistExits )
					{
						el_artist = xmlDoc.createElement("artist");
						el_artist.setAttribute( "name", meta.artist() );
						el_library.appendChild( el_artist );
					}
					// Getting the Album
					bool albumExits = false;
					QDomElement el_album = el_artist.firstChildElement();
					// Checking if the Album exits in the file
					while ( !el_album.isNull() )
					{
						if ( el_album.attribute("title") == meta.album() )
						{
							albumExits = true;
							break;
						}
						el_album = el_album.nextSiblingElement();
					}
					// if Album DONT exits, create it
					if ( !albumExits )
					{
						el_album = xmlDoc.createElement("album");
						el_album.setAttribute( "title", meta.album() );
						el_artist.appendChild( el_album );
					}
					// Getting the Track
					bool trackExits = false;
					QDomElement el_track = el_artist.firstChildElement();
					// Checking if the Album exits in the file
					while ( !el_track.isNull() )
					{
						if ( el_track.attribute("path") == meta.path() )
						{
							trackExits = true;
							break;
						}
						el_track = el_track.nextSiblingElement();
					}
					// if Track DONT exits, create it
					if ( !trackExits )
					{
						el_track = xmlDoc.createElement("track");
						el_track.setAttribute( "title", meta.title() );
						el_track.setAttribute( "path", meta.path() );
						el_track.setAttribute( "genre", meta.genre() );
						el_album.appendChild( el_track );
					}
				}

				XML::save( _libraryPath, xmlDoc );
			}
		}
	}
	QList<Artist*> artists()
	{
		return _artists;
	}
	QList<Album*> albums()
	{
		return _albums;
	}
	QList<Track*> tracks()
	{
		return _tracks;
	}
	TrackList* toTracklist()
	{
		TrackList* newTL = new TrackList;
		newTL->add( tracks() );
		return newTL;
	}
	void sortArtistBy(  )
	{

	}
	void sortArtistByName(  )
	{
		std::sort( _artists.begin(), _artists.end(), Artist::compare );
	}

signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARIES_H
