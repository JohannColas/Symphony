#ifndef SETTINGS_H
#define SETTINGS_H
/**********************************************/
#include <QApplication>
#include <QFile>
#include <QIcon>
/**********************************************/
#include <QSettings>
#include <QDir>
#include <QFileInfo>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Settings
        : public QObject
{
	Q_OBJECT
signals:
    void changed();
private:
    QSettings* _settings = 0;
    QSettings* _theme = 0;

public:
    ~Settings()
    {
        if ( _settings )
            delete _settings;
    }
    Settings()
    {
        // Get user directory
        QDir userDir = getUserDir();
        // Copy settings.ini in userDir if not exists
        if ( !userDir.exists( "settings.ini" ) )
            QFile::copy( "./settings.ini", userDir.path() + "/settings.ini" );
        // Open settings
        _settings = new QSettings( userDir.path() + "/settings.ini", QSettings::IniFormat );
        getTheme();
    }
    void save( QString key, const QVariant& value )
    {
        QColor color = value.value<QColor>();
        if ( color.isValid() )
            _settings->setValue( key, color.name(QColor::HexArgb) );
        else
            _settings->setValue( key, value );
        emit valueChanged( key );
    }
    int getInt( QString key )
    {
        return _settings->value(key).toInt();
    }
    QString getString( QString key )
    {
        return _settings->value(key).toString();
    }
    QChar getChar( QString key )
    {
        return _settings->value(key).toChar();
    }
    QColor getColor( QString key )
    {
        return QColor(getString(key));
    }
    bool getBool( QString key )
    {
        return _settings->value(key).toBool();
    }
	QIcon icon( const QString& name )
	{
        return QIcon( "./icons/" + getString("Appearance/iconsPack") + "/" + name +".svg" );
	}
    void getTheme()
    {
        QString theme = getString("Appearance/theme");
        if ( theme != "Default" )
        {
            if ( _theme )
                delete _theme;
            _theme = new QSettings( "/"+theme+".thm", QSettings::IniFormat );
        }
    }
	void applyTheme() {
//        QString theme = getString("Appearance/theme");
//        if ( theme == "" )
//		{
//			qApp->setStyleSheet( "" );
//		}
//		else
//		{
//            QFile file( "themes/" + theme + "/stylesheet.qss" );
//			if ( file.open(QFile::ReadOnly) )
//			{
//				QString style = QLatin1String( file.readAll() );
//				style.remove('\n').remove('\t');
//				qApp->setStyleSheet( style );
//			}
//			file.close();
//		}
	}

public slots:
    void reset()
    {
        QDir userDir = getUserDir();
        QFile::copy( "./settings.ini", userDir.path() + "/settings.ini" );
    }

private:
    QDir getUserDir()
    {
        // Create user dir if not exists
        QString configPath = "/.config";
        #ifdef Q_OS_WIN
            configPath = "/AppData/Roaming";
        #endif
        QDir dir( QDir::homePath() + configPath );
        if ( !dir.exists( "Symphony" ) )
            dir.mkdir( "Symphony" );
        dir.cd( "Symphony" );
        return dir;
    }

signals:
    void valueChanged( QString key );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGS_H
