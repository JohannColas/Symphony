#ifndef NOWPLAYINGFRAME_H
#define NOWPLAYINGFRAME_H
/**********************************************/
#include "BasicWidgets/Frame.h"
/**********************************************/
#include "TracksWidget/TrackWidget.h"
#include "TracksWidget/TrackSelector.h"
/**********************************************/
#include "Commons/Icons.h"
#include "Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class NowPlayingWidget
		: public Frame
{
	Q_OBJECT
private:
	TrackList* _nowPlaylist = 0;
	TrackWidget* wid_trackInfo = new TrackWidget;
	TrackSelector* wid_trackList = new TrackSelector;
	Icons* _icons = 0;

public:
	NowPlayingWidget( QWidget* parent = nullptr );
	void setPlaylists( Playlists* playlists )
	{
		wid_trackList->setPlaylists( playlists );
	}


public slots:
	void setPlayer( QMediaPlayer* player );
	void setPlaylist( TrackList* playlist );
	void setTrackList( TrackList* list );
	void updateList();
	void update();
	void updateIcons( Icons* icons );

signals:
	void indexChanged( int index );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // NOWPLAYINGFRAME_H
