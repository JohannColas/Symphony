#ifndef PLAYERCONTROLS_H
#define PLAYERCONTROLS_H
/**********************************************/
#include <QWidget>
#include "PlayerControl.h"
#include "BasicWidgets/Layouts.h"
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
class PlayerControls
        : public QWidget
{
    Q_OBJECT
private:
    PlayerControl* pb_playpause = new PlayerControl(this);
    PlayerControl* pb_stop = new PlayerControl(this);
    PlayerControl* pb_backward = new PlayerControl(this);
    PlayerControl* pb_forward = new PlayerControl(this);
    Settings* _sets;
    bool _isPlaying = false;

public:
    ~PlayerControls()
    {

    }
    PlayerControls( QWidget* parent = 0 )
        : QWidget( parent )
    {
        HorizontalLayout* lay_main = new HorizontalLayout;
        lay_main->addSpacer();
        lay_main->addWidget( pb_backward );
        lay_main->addWidget( pb_playpause );
        lay_main->addWidget( pb_stop );
        lay_main->addWidget( pb_forward );
        lay_main->addSpacer();
        setLayout( lay_main );
        setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );

        connect( pb_playpause, &QPushButton::clicked,
                 this, &PlayerControls::playpause );
        connect( pb_stop, &QPushButton::clicked,
                 this, &PlayerControls::stop );
        connect( pb_backward, &QPushButton::clicked,
                 this, &PlayerControls::backward );
        connect( pb_forward, &QPushButton::clicked,
                 this, &PlayerControls::forward );
    }
    void setSettings( Settings* sets )
    {
        _sets = sets;
        updateIcons();
    }

public slots:
    void updateIcons()
    {
        pb_backward->setIcon( _sets->icon("skip_previous") );
        pb_playpause->setIcon( _sets->icon("play") );
        pb_stop->setIcon( _sets->icon("stop") );
        pb_forward->setIcon( _sets->icon("skip_next") );
    }

protected:

signals:
    void playpause();
    void stop();
    void backward();
    void forward();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERCONTROLS_H
/**********************************************/
