#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H
/**********************************************/
#include <QElapsedTimer>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QFrame>
#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QProgressBar>
#include <QTimer>
#include <QMouseEvent>
#include "BasicWidgets/Frame.h"
#include "Commons/Track.h"
#include "Commons/TrackList.h"
#include "Commons/Icons.h"
#include "Commons/Lang.h"
#include "Commons/Settings.h"
/**********************************************/
#include "PlayerControls.h"
#include "PlayerSubControls.h"
#include "PlayerTrackBanner.h"
#include "BasicWidgets/PlayerButton.h"
#include "BasicWidgets/PlayerOptionButton.h"
#include "BasicWidgets/AppButton.h"
#include "BasicWidgets/SystemButton.h"
#include "BasicWidgets/CoverImage.h"
#include "BasicWidgets/TitleLabel.h"
#include "BasicWidgets/ArtistLabel.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerWidget
		: public Frame
{
	Q_OBJECT
public:
	enum Repeat {
		NOREPEAT,
		REPEATALL,
		REPEATONE
	};

private:
	bool _movable = false;
	TrackList* _nowPlaylist = 0;
	QMediaPlayer* _player = new QMediaPlayer;
	QTimer* _timer = new QTimer(this);
    bool _isPlaying = false;
    // Player Widgets
    PlayerControls* _playerControls = new PlayerControls(this);
    PlayerSubControls* _playerSubControls = new PlayerSubControls(this);
    PlayerTrackBanner* _playerTrackBanner = new PlayerTrackBanner(this);
    //
    PlayerButton* pb_play = new PlayerButton;
	PlayerButton* pb_stop = new PlayerButton;
	PlayerButton* pb_backward = new PlayerButton;
	PlayerButton* pb_forward = new PlayerButton;
	PlayerOptionButton* pb_repeat = new PlayerOptionButton;
	Repeat _repeat = Repeat::NOREPEAT;
	PlayerOptionButton* pb_shuffle = new PlayerOptionButton;
	bool _isShuffle = false;
	PlayerOptionButton* pb_equalizer = new PlayerOptionButton;
	PlayerOptionButton* pb_volume = new PlayerOptionButton;
	//
	CoverImage* img_cover = new CoverImage;
	TitleLabel* lb_title = new TitleLabel;
	ArtistLabel* lb_artist = new ArtistLabel;
	QSlider* sl_time = new QSlider;
	QLabel* lb_time = new QLabel;
	int _newPos = -1;
	int _duration = -1;
	// Application Button
	AppButton* pb_settings = new AppButton;
	// System Button
	SystemButton* pb_minimize = new SystemButton;
	SystemButton* pb_maximize = new SystemButton;
	SystemButton* pb_close = new SystemButton;
	Icons* _icons = 0;
	QList<int> _listOrder;
	int _index = 0;
    Settings* _sets;

public:
	~PlayerWidget();
	PlayerWidget( QWidget* parent = nullptr );
	void randomize();

public slots:
	void playTrack( Track* track );
	void addTrack( Track* track );
	void playTracklist( TrackList* tracklist );
	void addTracklist( TrackList* tracklist );
	void insertTracklist( TrackList* tracklist );
	void receiveTrackList( TrackList* tracklist );
	void setPlaylistIndex( int index );
	void setTrackList( TrackList* playlist );
	//
	void setTrack();
	void play();
	void pause();
	void playpause();
	void stop();
	void backward();
	void forward();
	void repeat();
	void shuffle();
	void changePosition( qint64 pos );
	void setPlayerPos( int pos );
	void updatePlayerPos();
	void updateIcons( Icons* icons );
	void updatePlayPauseIcon();
	void mousePressEvent( QMouseEvent* event ) override;
	void mouseReleaseEvent( QMouseEvent* event ) override;
	void mouseMoveEvent( QMouseEvent* event ) override;
	void mouseDoubleClickEvent( QMouseEvent* event ) override;
	void connectTo( Frame* frame );
	void onMediaStatusChanged();
	void updateTrackInfo( Track* track );
    void setSettings( Settings* sets )
    {
        _sets = sets;
        connect( _sets, &Settings::valueChanged,
                 this, &PlayerWidget::settingsChanged );
        _playerControls->setSettings( _sets );
        _playerSubControls->setSettings( _sets );
        updateLayout();
    }
    void settingsChanged( QString key )
    {
        if ( key == "Player/position" ||
             key == "Player/size" )
            updateLayout();
    }
    void updateLayout()
    {
        QString pos = _sets->getString("Player/position");
        lay_main->removeWidget(_playerControls);
        lay_main->removeWidget(_playerSubControls);
        if ( pos == 't' ||
             pos == 'b' )
        {
            lay_main->addWidget(_playerControls, 0, 0);
            lay_main->addWidget(_playerTrackBanner, 0, 1);
            lay_main->addWidget(_playerSubControls, 0, 2);
            lay_main->addSpacer(0, 3, false);
            setSizePolicy( QSizePolicy::Expanding,
                           QSizePolicy::Maximum );
        }
        else
        {
            lay_main->addWidget(_playerTrackBanner, 0, 0);
            lay_main->addWidget(_playerControls, 1, 0);
            lay_main->addWidget(_playerSubControls, 2, 0);
            lay_main->addSpacer(3, 0, true);
            setSizePolicy( QSizePolicy::Maximum,
                           QSizePolicy::Expanding );
        }
    }

signals:
	void indexChanged();
	void playlistChanged( TrackList* list );
	void onMinimize();
	void onMaximize();
	void moveWindow( const QPoint& pos );
	void showSettings();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERWIDGET_H
