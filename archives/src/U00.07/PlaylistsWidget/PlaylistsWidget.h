#ifndef PLAYLISTSWIDGET_H
#define PLAYLISTSWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "Commons/Playlists.h"
#include "Commons/TrackListModifier.h"
#include "PlaylistsWidget/PlaylistSelector.h"
#include "PlaylistsWidget/PlaylistBanner.h"
#include "TracksWidget/TrackSelector.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistsWidget
		: public Frame
{
	Q_OBJECT
private:
	Playlists* _playlists = 0;
	TrackListModifier* wid_playlistModifier = new TrackListModifier;
	PlaylistSelector* sel_playlist = new PlaylistSelector;
	PlaylistBanner* ban_playlist = new PlaylistBanner;
	TrackSelector* wid_playlist = new TrackSelector;

public:
	PlaylistsWidget( QWidget* parent = 0 )
		: Frame( parent )
	{
		hide();
		addWidget( wid_playlistModifier, 0, 0 );
		addWidget( sel_playlist, 1, 0 );
		VerticalLayout* lay_right = new VerticalLayout;
		lay_right->addWidget( ban_playlist );
		lay_right->addWidget( wid_playlist );
		addLayout( lay_right, 0, 1, 2, 1 );

		connect( wid_playlistModifier, &TrackListModifier::toPlay,
				 this, &PlaylistsWidget::playPlaylist );
		connect( wid_playlistModifier, &TrackListModifier::toAdd,
				 this, &PlaylistsWidget::addPlaylist );
		connect( wid_playlistModifier, &TrackListModifier::toDelete,
				 this, &PlaylistsWidget::deletePlaylist );

		connect( sel_playlist, &PlaylistSelector::indexToChange,
				 this, &PlaylistsWidget::showPlaylist );
		connectTo( wid_playlist );
		connect( sel_playlist, &PlaylistSelector::toReset,
				 wid_playlist, &TrackSelector::reset );
	}

public slots:
	void updateIcons( Icons* icons )
	{
		wid_playlistModifier->updateIcons( icons );
		sel_playlist->updateIcons( icons );
		wid_playlist->updateIcons( icons );
		wid_playlist->allowRemove();


	}
	void updateLang( Lang* /*lang*/ )
	{
//		lb_title->setText( );
	}
	void setPlaylists( Playlists* playlists = 0 )
	{
		_playlists = playlists;
		sel_playlist->setPlaylists( playlists );
		wid_playlist->setPlaylists( playlists );
		wid_playlist->contextMenu()->setType( PlayMenu::FORPLAYLIST );

		connect( wid_playlistModifier, &TrackListModifier::toNew,
				 _playlists, &Playlists::addNew );
		connect( playlists, &Playlists::currentTrackListChanged,
				 wid_playlist, &TrackSelector::setTrackList );
	}
	void showPlaylist( int index )
	{
		if ( index != -1 )
		{
			wid_playlist->setTrackList( _playlists->tracklist(index) );
			ban_playlist->setPlaylist( _playlists->tracklist(index) );
		}
	}
	void sendTrackToPlay( int index )
	{
		if ( index != -1 )
			emit trackToPlay( sel_playlist->currentTrackList()->track(index) );
	}
	void playPlaylist()
	{
		if ( _playlists->currentTracklist() )
			emit tracklistToPlay( _playlists->currentTracklist() );
	}
	void addPlaylist()
	{
		if ( _playlists->currentTracklist() )
			emit tracklistToAdd( _playlists->currentTracklist() );
	}
	void deletePlaylist()
	{
//		_tracklists->tracklist(0)->updateLibrary();
	}

signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTSWIDGET_H
