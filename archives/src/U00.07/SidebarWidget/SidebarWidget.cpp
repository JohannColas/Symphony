#include "SidebarWidget.h"
/**********************************************/
/**********************************************/
/* */
SidebarWidget::~SidebarWidget()
{
}
/**********************************************/
/**********************************************/
/* */
SidebarWidget::SidebarWidget( QWidget* parent )
	: QWidget( parent )
{
	ac_textVisibility->setCheckable(true);
	connect( ac_textVisibility, &QAction::toggled,
			 this, &SidebarWidget::setTextVisibility );
	ac_nowPlaying->setCheckable(true);
	ac_nowPlaying->setChecked(true);
	ac_artists->setCheckable(true);
	ac_artists->setChecked(true);
	ac_albums->setCheckable(true);
	ac_albums->setChecked(true);
	ac_genres->setCheckable(true);
	ac_genres->setChecked(true);
	ac_tracks->setCheckable(true);
	ac_tracks->setChecked(true);
	ac_playlists->setCheckable(true);
	ac_playlists->setChecked(true);
	ac_libraries->setCheckable(true);
	ac_libraries->setChecked(true);
	ac_radio->setCheckable(true);
	ac_radio->setChecked(true);
	ac_podcats->setCheckable(true);
	ac_podcats->setChecked(true);
	pop_options->addSection( "Options" );
	pop_options->addAction( ac_textVisibility );
	pop_options->addSection( "Sections" );
	pop_options->addAction( ac_nowPlaying );
	pop_options->addAction( ac_artists );
	pop_options->addAction( ac_albums );
	pop_options->addAction( ac_genres );
	pop_options->addAction( ac_tracks );
	pop_options->addAction( ac_playlists );
	pop_options->addAction( ac_libraries );
	pop_options->addAction( ac_radio );
	pop_options->addAction( ac_podcats );
	pop_options->setWindowFlags( Qt::Popup );
	pb_options->setMenu( pop_options );

	addItem( "Now Playing" );
	connect( ac_nowPlaying, &QAction::toggled,
			this, &SidebarWidget::setNowPlayingVisible );
	addItem( "Artists" );
	connect( ac_artists, &QAction::toggled,
			this, &SidebarWidget::setArtistsVisible );
	addItem( "Albums" );
	connect( ac_albums, &QAction::toggled,
			this, &SidebarWidget::setAlbumsVisible );
	addItem( "Genres" );
	connect( ac_tracks, &QAction::toggled,
			this, &SidebarWidget::setTracksVisible );
	addItem( "Tracks" );
	connect( ac_genres, &QAction::toggled,
			this, &SidebarWidget::setGenresVisible );
	addItem( "Playlists" );
	connect( ac_playlists, &QAction::toggled,
			this, &SidebarWidget::setPlaylistsVisible );
	addItem( "Libraries" );
	connect( ac_libraries, &QAction::toggled,
			this, &SidebarWidget::setLibrariesVisible );
	addItem( "Radio" );
	connect( ac_radio, &QAction::toggled,
            this, &SidebarWidget::setRadiosVisible );
	addItem( "Podcasts" );
	connect( ac_podcats, &QAction::toggled,
			this, &SidebarWidget::setPodcastsVisible );
	addItem( "Settings" );
	wid_list->item( 9 )->setHidden( true );
	changeCurrentWidget( 0 );

	wid_list->setFrameShape( QFrame::NoFrame );
	wid_list->setMouseTracking( true );
	wid_list->setMaximumWidth( 200 );
	wid_list->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Preferred );
	wid_list->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
	wid_list->setTextElideMode( Qt::ElideRight );
	connect( wid_list, &QListWidget::currentRowChanged,
			 this, &SidebarWidget::changeCurrentWidget );

	QVBoxLayout* lay_sidebar = new QVBoxLayout;
	lay_sidebar->setSpacing(0);
	lay_sidebar->setMargin(0);
	lay_sidebar->addWidget( wid_list );
	lay_sidebar->addWidget( pb_options );
	_sidebar->setLayout( lay_sidebar );
	lay_main->setSpacing(0);
	lay_main->setMargin(0);
	lay_main->addWidget( _sidebar, 0, 0 );

	setLayout( lay_main );

	setMinimumWidth( 400 );
	ac_textVisibility->setChecked(true);
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::addItem( const QString& name, QWidget* widget )
{
	widget->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
	wid_list->blockSignals( true );
	QListWidgetItem* item = new QListWidgetItem;
	SidebarItem* sitem = new SidebarItem;
	sitem->setText( name );
	wid_list->addItem( item );
	wid_list->setItemWidget( item, sitem );
	wid_list->blockSignals( false );
	lay_main->addWidget( widget, 0, 1 );
	_widgets.append( widget );
	widget->hide();
}

void SidebarWidget::setWidget( int index, QWidget* widget )
{
	if ( index > -1 && index < count() )
	{
		lay_main->replaceWidget( _widgets.at(index), widget );
		_widgets.replace( index, widget );
	}
}
/**********************************************/
/**********************************************/
/* */
int SidebarWidget::count()
{
	return wid_list->count();
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::changeCurrentWidget(int index)
{
	if ( index > -1 && index < count() )
	{
		if ( _selectedTab > -1 && _selectedTab < count() ) {
			_widgets.at( _selectedTab )->hide();
		}
		if ( index > -1 && index < count() ) {
			_selectedTab = index;
			_widgets.at( index )->show();
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::updateIcons(Icons* icons)
{
	pb_options->setIcon( icons->get("more_horiz") );
	// Now Playing
	SidebarItem* item = sidebarItem( 0 );
	item->setIcon( icons->get("now_playing") );
	// Artists
	item = sidebarItem( 1 );
	item->setIcon( icons->get("artist") );
	// Albums
	item = sidebarItem( 2 );
	item->setIcon( icons->get("album") );
	// Genres
	item = sidebarItem( 3 );
	item->setIcon( icons->get("genre") );
	// Tracks
	item = sidebarItem( 4 );
	item->setIcon( icons->get("track") );
	// Playlists
	item = sidebarItem( 5 );
	item->setIcon( icons->get("playlist") );
	// Libraries
	item = sidebarItem( 6);
	item->setIcon( icons->get("libraries") );
	// Radios
	item = sidebarItem( 7 );
	item->setIcon( icons->get("radio") );
	// Podcasts
	item = sidebarItem( 8 );
	item->setIcon( icons->get("podcast") );
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::setTextVisibility( bool check )
{
	int width = 0;
	for ( int it = 0; it < wid_list->count(); ++it )
	{
		SidebarItem* item = sidebarItem( it );
		item->setLabelVisible( check );
		width = qMax( width, item->sizeHint().width() );
	}
	for ( int it = 0; it < wid_list->count(); ++it )
	{
		SidebarItem* item = sidebarItem( it );
		wid_list->item( it )->setSizeHint( {width, item->sizeHint().height()} );
	}
	width += wid_list->contentsMargins().left() + wid_list->contentsMargins().right();
    wid_list->setFixedWidth( width );
    if ( check != ac_textVisibility->isChecked() )
        ac_textVisibility->setChecked( check );
    if ( _sets )
        _sets->save( "Appearance/hideSidebarText", !check );
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::setNowPlayingVisible( bool /*check*/ )
{
//	wid_list->item( 0 )->setHidden( !check );
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::setArtistsVisible( bool check )
{
	wid_list->item( 1 )->setHidden( !check );
    if ( check != ac_artists->isChecked() )
        ac_artists->setChecked( check );
    _sets->save( "Menu/hideArtists", !check );
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::setAlbumsVisible( bool check )
{
	wid_list->item( 2 )->setHidden( !check );
    if ( check != ac_albums->isChecked() )
        ac_albums->setChecked( check );
    _sets->save( "Menu/hideAlbums", !check );
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::setGenresVisible( bool check )
{
	wid_list->item( 3 )->setHidden( !check );
    if ( check != ac_genres->isChecked() )
        ac_genres->setChecked( check );
    _sets->save( "Menu/hideGenres", !check );
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::setTracksVisible( bool check )
{
    wid_list->item( 4 )->setHidden( !check );
    if ( check != ac_tracks->isChecked() )
        ac_tracks->setChecked( check );
    _sets->save( "Menu/hideTracks", !check );
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::setPlaylistsVisible( bool check )
{
	wid_list->item( 5 )->setHidden( !check );
    if ( check != ac_playlists->isChecked() )
        ac_playlists->setChecked( check );
    _sets->save( "Menu/hidePlaylists", !check );
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::setLibrariesVisible( bool check )
{
	wid_list->item( 6 )->setHidden( !check );
    if ( check != ac_libraries->isChecked() )
        ac_libraries->setChecked( check );
    _sets->save( "Menu/hideLibraries", !check );
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::setRadiosVisible( bool check )
{
	wid_list->item( 7 )->setHidden( !check );
    if ( check != ac_radio->isChecked() )
        ac_radio->setChecked( check );
    _sets->save( "Menu/hideRadios", !check );
}
/**********************************************/
/**********************************************/
/* */
void SidebarWidget::setPodcastsVisible( bool check )
{
	wid_list->item( 8 )->setHidden( !check );
    if ( check != ac_podcats->isChecked() )
        ac_podcats->setChecked( check );
    _sets->save( "Menu/hidePodcasts", !check );
}
/**********************************************/
/**********************************************/
