#ifndef WIDGET_H
#define WIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "Commons/Settings.h"
#include "Commons/Theme.h"
#include "Commons/Icons.h"
#include "Commons/Lang.h"
#include "Commons/TrackList.h"
#include "Commons/Playlists.h"
#include "Commons/Libraries.h"
#include <QListWidget>
/**********************************************/
#include "PlayerWidget/PlayerWidget.h"
#include "BasicWidgets/SystemControls.h"
#include "SidebarWidget/SidebarWidget.h"
#include "NowPlayingWidget/NowPlayingWidget.h"
#include "ArtistsWidget/ArtistsWidget.h"
#include "AlbumsWidget/AlbumsWidget.h"
#include "GenresWidget/GenresWidget.h"
#include "TracksWidget/TracksWidget.h"
#include "PlaylistsWidget/PlaylistsWidget.h"
#include "LibrariesWidget/LibrariesWidget.h"
#include "RadiosWidget/RadiosWidget.h"
#include "PodcastsWidget/PodcastsWidget.h"
#include "SettingsWidget/SettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Symphony
        : public Frame
{
    Q_OBJECT
protected:
    // Window Parameters
    int invisibleBorderSize = 2;
    bool isPressedWidget;
    QPoint _lastPos = {-1,0};
    int _oldWidget = 0;
    // Initialise the TrackList
    TrackList* _nowPlaylist = new TrackList( "#NowPlaylist", TrackList::NOWPLAYLIST );
    Playlists* _playlists = new Playlists;
    Libraries* _libraries = new Libraries;
//	Radios* _radios = new Radios;
//	Podcasts* _radios = new Podcasts;
    // Initialise the Widgets
    PlayerWidget* wid_player = new PlayerWidget(this);
    SidebarWidget* wid_sidebar = new SidebarWidget;
    NowPlayingWidget* wid_nowPlaying = new NowPlayingWidget;
    ArtistsWidget* wid_artists = new ArtistsWidget;
    AlbumsWidget* wid_albums = new AlbumsWidget;
    GenresWidget* wid_genres = new GenresWidget;
    TracksWidget* wid_tracks = new TracksWidget;
    PlaylistsWidget* wid_playlists = new PlaylistsWidget;
    LibrariesWidget* wid_libraries = new LibrariesWidget;
    RadiosWidget* wid_radios = new RadiosWidget;
    PodcastsWidget* wid_podcasts = new PodcastsWidget;
    SettingsWidget* wid_settings = new SettingsWidget;
    //
    SystemControls* _systemControls = new SystemControls(this);
    //
    QSpacerItem* _spacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Maximum );
    // Application Settings
    Settings* _settings = new Settings;
    Theme* _theme = new Theme;
    Icons* _icons = new Icons;
    Lang* _lang = new Lang;

public:
    ~Symphony();
    Symphony( QWidget *parent = nullptr );


public slots:
    void showSettings();
    void updateSettings();
    void updateIcons();
    void updateLang();
    void move( const QPoint& pos );
    void closeEvent( QCloseEvent* event );
    void maximise()
    {
        isMaximized() ? showNormal() : showMaximized();
        _settings->save("Geometry/maximized", isMaximized() );
    }
    void updateLayout()
    {
        QString playerPos = _settings->getString("Player/position");
        lay_main->removeWidget(wid_player);
        lay_main->removeWidget(wid_sidebar);
        lay_main->removeWidget(_systemControls);
        lay_main->removeItem(_spacer);
        if ( playerPos == 't' )
        {
            addWidget(wid_player, 0, 0, 1, 1);
            addWidget(wid_sidebar, 1, 0, 1, 2);
            addWidget(_systemControls, 0, 1, 1, 1);
            lay_main->setAlignment(wid_player, Qt::AlignVCenter);
        }
        else if ( playerPos == 'b' )
        {
            addWidget(wid_player, 2, 0, 1, 2);
            addWidget(wid_sidebar, 1, 0, 1, 2);
            addWidget(_systemControls, 0, 1, 1, 1);
            lay_main->addItem(_spacer, 0, 0);
            lay_main->setAlignment(wid_player, Qt::AlignVCenter);
        }
        else if ( playerPos == 'l' )
        {
            addWidget(wid_player, 0, 0, 2, 1);
            addWidget(wid_sidebar, 1, 1, 1, 2);
            addWidget(_systemControls, 0, 2, 1, 1);
            lay_main->addItem(_spacer, 0, 1);
            lay_main->setAlignment(wid_player, Qt::AlignTop);
        }
        else
        {
            addWidget(wid_player, 1, 1, 1, 2);
            addWidget(wid_sidebar, 0, 0, 2, 1);
            addWidget(_systemControls, 0, 2, 1, 1);
            lay_main->addItem(_spacer, 0, 1);
//            lay_main->setAlignment(wid_player, Qt::AlignTop);
        }
        lay_main->setAlignment(_systemControls, Qt::AlignVCenter);
    }
    void settingsChanged( QString key )
    {
        if ( key == "Player/position" ||
             key == "Player/size" )
        {
            updateLayout();
        }
        else if ( key == "Appearance/theme" )
        {

        }
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WIDGET_H
