#ifndef TRACKSELECTOR_H
#define TRACKSELECTOR_H
/**********************************************/
#include <QInputDialog>
#include <QListWidget>
#include <QMenu>
#include <QWidgetAction>
#include "BasicWidgets/ListWidget.h"
#include "Commons/Icons.h"
#include "Commons/TrackList.h"
#include "Commons/Playlists.h"
#include "BasicWidgets/TrackItem.h"
#include "Commons/Libraries.h"
#include "BasicWidgets/PlayMenu.h"
#include "BasicWidgets/LineEdit.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackSelector
		: public ListWidget
{
	Q_OBJECT
private:
	Icons* _icons = 0;
	TrackList* _tracklist = 0;
	Playlists* _playlists = 0;
//	QMenu* menu = new QMenu( this );
	PlayMenu* _contextMenu = new PlayMenu(this);

public:
	TrackSelector( QWidget* parent = nullptr )
		: ListWidget( parent )
	{
		connect( this, &TrackSelector::indexToChange,
				 this, &TrackSelector::indexChange );
		connect( _contextMenu, &PlayMenu::play,
				 this, &TrackSelector::on_play );
		connect( _contextMenu, &PlayMenu::add,
				 this, &TrackSelector::on_add );
		connect( _contextMenu, &PlayMenu::addAndPlay,
				 this, &TrackSelector::on_addAndPlay );
		connect( _contextMenu, &PlayMenu::insert,
				 this, &TrackSelector::on_insert );
		connect( _contextMenu, &PlayMenu::addToNewPlaylist,
				 this, &TrackSelector::on_addToNewPlaylist );
		connect( _contextMenu, &PlayMenu::addToPlaylist,
				 this, &TrackSelector::on_addToPlaylist );
		connect( _contextMenu, &PlayMenu::remove,
				 this, &TrackSelector::on_remove );
		connect( _contextMenu, &PlayMenu::removeAll,
				 this, &TrackSelector::on_removeAll );
	}
	void setPlaylists( Playlists* playlists )
	{
		_playlists = playlists;
		_contextMenu->setPlaylists( playlists );
	}
	PlayMenu* contextMenu()
	{
		return _contextMenu;
	}
	Track* currentTrack()
	{
		if ( _tracklist && index() != -1 )
			return _tracklist->track( index() );
		return 0;
	}

public slots:
	void connectTrackList()
	{
		connect( _tracklist, &TrackList::changed,
				 this, &TrackSelector::update );
		if ( _tracklist->type() == TrackList::PLAYLIST )
		{
			connect( this, &TrackSelector::removeAll,
					 _tracklist, &TrackList::removeAll );
			connect( this, &TrackSelector::tracklistToRemove,
					 _tracklist, &TrackList::remove );
		}
	}
	void disconnectTrackList()
	{
		if ( _tracklist )
		{
			disconnect( _tracklist, &TrackList::changed,
						this, &TrackSelector::update );
		}
	}
	void reset()
	{

	}
	void setTrackList( TrackList* tracklist )
	{
//		disconnectTrackList();
		_tracklist = tracklist;
		connectTrackList();
		update();
	}
	void setLibraries( Libraries* libraries = 0 )
	{
		if ( libraries )
		{
			setTrackList( libraries->toTracklist() );
			_contextMenu->setType( PlayMenu::FORTRACKSELECTOR );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		update();
	}
	void update() override
	{
		if ( _tracklist )
		{
			clear();
			int x = contentsMargins().left();
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();
			int nbItems = 0;
			for ( int it = _begin; it < _tracklist->count(); ++it )
			{
				QString options = "cover:||/title/artist/||:--:duration";
				options = "cover:title/artist:--:duration";
//				options = "cover:||/title/artist/album/duration/||:--";
				QIcon icon = QIcon();
				if ( _icons )
					icon = _icons->get("track");
				TrackItem* trackItem = new TrackItem( _tracklist->track(it), options, icon, this );
				if ( it == _tracklist->currentTrackIndex() )
					trackItem->setChecked( true );
				trackItem->setGeometry( x, totheight,
										w,
										trackItem->sizeHint().height() );
				// Adding the Item to TrackSelector
				addItem( trackItem );
				totheight += trackItem->sizeHint().height();
				if ( totheight > height() )
				{
					break;
				}
				++nbItems;
			}
			updateScrollbar( _tracklist->count() );
		}
	}
	void showContextMenu( const QPoint& pos ) override
	{
		_contextMenu->updateActions();
		_contextMenu->exec( this->mapToGlobal(pos) );
	}
	void indexChange( int index )
	{
		emit trackChanged( _tracklist->track(index) );
	}
	TrackList* selectedTracks()
	{
		TrackList* list = new TrackList;
		int lt = 0;
		for ( Track* track : _tracklist->list() )
		{
			if ( _selection.contains(lt) )
			{
				list->append( track );
				if ( _selection.size() == 1 )
					break;
			}
			++lt;
		}
		if ( list->count() == 0 )
		{
			delete list;
			return 0;
		}
		return list;
	}
	void on_play()
	{
		TrackList* tltmp = selectedTracks();
		if ( tltmp )
		{
			emit tracklistToPlay( tltmp );
		}
	}
	void on_add()
	{
		TrackList* tltmp = selectedTracks();
		if ( tltmp )
			emit tracklistToAdd( tltmp );
	}
	void on_addAndPlay()
	{
		TrackList* tltmp = selectedTracks();
		if ( tltmp )
			emit tracklistToInsert( tltmp );
	}
	void on_insert()
	{
		TrackList* tltmp = selectedTracks();
		if ( tltmp )
			emit tracklistToInsert( tltmp );
	}
	void on_addToPlaylist( const QString& playlist )
	{
//		qDebug() << playlist;
		TrackList* pltmp = _playlists->playlist( playlist );
		TrackList* tltmp = selectedTracks();
		if ( tltmp && pltmp )
			pltmp->add( tltmp );
	}
	void on_addToNewPlaylist( const QString& newplaylist )
	{
		TrackList* pltmp = _playlists->addNewPlaylist( newplaylist );
		TrackList* tltmp = selectedTracks();
		if ( tltmp && pltmp )
			pltmp->add( tltmp );
	}
	void on_remove()
	{
		TrackList* tltmp = selectedTracks();
		if ( tltmp )
			emit tracklistToRemove( tltmp );
	}
	void on_removeAll()
	{
		emit removeAll();
	}

signals:
	void trackChanged( Track* track );
	void currentTracklistChanged( TrackList* tracklist );
	void renamed( const QString& name );
	void deleted();
	void removeAll();
	void tracklistToRemove( TrackList* tracklist );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKSELECTOR_H
