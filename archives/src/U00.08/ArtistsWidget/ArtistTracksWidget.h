#ifndef ARTISTTRACKSWIDGET_H
#define ARTISTTRACKSWIDGET_H
#include <QMenu>
/**********************************************/
#include "BasicWidgets/ListWidget.h"
#include "Commons/Icons.h"
#include "Commons/TrackList.h"
#include "Commons/Playlists.h"
#include "Commons/Sorting.h"
#include "Commons/Artist.h"
/**********************************************/
#include "AlbumsWidget/AlbumItem.h"
#include "BasicWidgets/TrackItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistTracksWidget
		: public ListWidget
{
	Q_OBJECT
private:
	Artist* _artist = 0;
	Playlists* _playlists = 0;
	Icons* _icons = 0;

public:
	ArtistTracksWidget( QWidget *parent = 0 )
		: ListWidget( parent )
	{
		setMinimumWidth(300);
		connect( this, &ArtistTracksWidget::indexToPlay,
				 this, &ArtistTracksWidget::toPlay );
	}
	void setPlaylists( Playlists* playlists )
	{
		_playlists = playlists;
	}

public slots:
	void setArtist( Artist* artist = 0 )
	{
		if ( artist )
		{
			_artist = artist;

			setBegin( 0 );
			update();
		}
	}
	void update() override
	{
		if ( _artist )
		{
			clear();
			int x = contentsMargins().left();
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();
			int nbItems = 0;
			int it = 0;
			for ( Album* album : _artist->albums() )
			{
				if ( it >= _begin )
				{
					QIcon icon = QIcon();
					if ( _icons )
						icon = _icons->get("album");
					AlbumItem* it_Album = new AlbumItem( album, icon, this, "number" );
					//
					addItem( it_Album );
					it_Album->setGeometry( x, totheight,
									   w,
									   it_Album->sizeHint().height() );
					totheight += it_Album->sizeHint().height();
					if ( totheight > height() )
					{
						break;
					}
					++nbItems;
				}
				++it;
				for ( Track* track : album->tracks() )
				{
					if ( it >= _begin )
					{
						QString options = "number:title:--:duration";
						TrackItem* item = new TrackItem(track, options, _icons->get("track"), this);
						addItem( item );
						// Adding the Item to ArtistTracksSelector
						item->setGeometry( x, totheight,
										   w,
										   item->sizeHint().height() );
						totheight += item->sizeHint().height();
						if ( totheight > height() )
						{
							break;
						}
						++nbItems;
					}
					++it;
				}
			}
			updateScrollbar( _artist->nbLines() );
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
	//
	void toPlay( int /*index*/ )
	{
		TrackList* tltmp = selectedTracks();
		if ( tltmp )
			emit tracklistToPlay( tltmp );
	}
	//
	void showContextMenu( const QPoint& pos ) override
	{
		QMenu* menu = new QMenu( this );
		QAction* ac_play = menu->addAction( "Play Now (and remove Current Playlist)" );
		QAction* ac_add = menu->addAction( "Add to Current Playlist" );
		//QAction* ac_add = menu->addAction( "Add and Play to Current Playlist" );
		QAction* ac_insert = menu->addAction( "Insert after the Current Track" );
		menu->addSeparator();
		QMenu* ac_addToPlaylist = menu->addMenu( "Add to a Playlist" );
		QList<QAction*> playlists;
		for ( TrackList* elm : _playlists->list() )
		{
			QAction* playlist = ac_addToPlaylist->addAction( elm->name() );
			playlists.append( playlist );
		}
		QAction* action = menu->exec( this->mapToGlobal(pos) );
		if ( action == ac_play )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToPlay( tltmp );
		}
		else if ( action == ac_add )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToAdd( tltmp );
		}
		else if ( action == ac_insert )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToInsert( tltmp );
		}
		for ( int it = 0; it < playlists.size(); ++it )
		{
			if ( action == playlists.at(it) )
			{
				TrackList* tltmp = selectedTracks();
				TrackList* pltmp = _playlists->tracklist(it);
				if ( tltmp && pltmp )
					pltmp->add(tltmp);
				break;
			}
		}
	}
	TrackList* selectedTracks()
	{
		TrackList* list = new TrackList;
		int lt = 0;
		for ( Album* album : _artist->albums() )
		{
			bool addAll = false;
			if ( _selection.contains(lt) )
				addAll = true;
			++lt;
			for ( Track* track : album->tracks() )
			{
				if ( _selection.contains(lt) || addAll )
				{
					list->append( track );
					if ( _selection.size() == 1 && !addAll )
						break;
				}
				++lt;
			}
		}
		if ( list->count() == 0 )
		{
			delete list;
			return 0;
		}
		return list;
	}

signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTTRACKSWIDGET_H
