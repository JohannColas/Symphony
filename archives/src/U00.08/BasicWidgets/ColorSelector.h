#ifndef COLORSELECTOR_H
#define COLORSELECTOR_H

#include <QScreen>
#include <QWidget>
#include <QLabel>
#include <QMouseEvent>
#include <QPainter>
#include <QSpacerItem>
#include <QPushButton>
#include <QColor>
#include <QColorDialog>
#include <QComboBox>
#include "BasicWidgets/ScrollArea.h"
#include "BasicWidgets/Layouts.h"

#include <QDebug>

/**********************************************/
/**********************************************/
/**********************************************/
/* ColorLabel
 *
 * */
class ColorLabel
		: public QLabel
{
	Q_OBJECT
public:
	ColorLabel( QWidget* parent = nullptr )
		: QLabel( parent )
	{
		setFixedSize( w, h );
	}
	void setColor( const QColor& color ) {
		this->color = color;
		setToolTip( this->color.name() );
	}
	void setSize( int width, int height ) {
		w = width;
		h = height;
		setFixedSize( w, h );
		repaint();
	}
	void setHoverable( bool hoverable ) {
		this->hoverable = hoverable;
		if ( !hoverable ) hover = false;
	}
	void paintEvent ( QPaintEvent * ) {
	   QPainter painter( this );

	   int ohue = (color.hue() + 180);
	   if ( ohue > 360 ) ohue -= 360;
	   QColor oppoCol = QColor();
//	   if ( color.lightness() == 0 )
//		   oppoCol.setHsl( 0, 0, 255 );
//	   if ( color.lightness() == 255 )
//		   oppoCol.setHsl( 0, 0, 255 );
//	   if ( color.lightness() == 0 )
		   oppoCol.setHsl(
					   ohue,
					   color.saturation(),
					   255 - color.lightness() );
		   oppoCol = QColor( "transparent");

	   if ( hover )
		   painter.setPen( QPen( oppoCol, 2 ) );
	   else
		   painter.setPen( QPen(Qt::NoPen) );
	   painter.setBrush( QBrush(color) );
	   if ( hover || !hoverable )
		   painter.drawRect( 0, 0, w, h );
	   else
		   painter.drawRect( 2, 2, w-4, h-4 );
	}
	void mousePressEvent ( QMouseEvent *event ) {
		if ( event->button() == Qt::LeftButton ) {
			emit clicked();
			emit emitColor( color );
		}
	}
	void enterEvent( QEvent* event ) {
		if ( hoverable ) {
			hover = true;
			repaint();
		}
		QLabel::enterEvent( event );
	}
	void leaveEvent( QEvent* event ) {
		if ( hoverable ) {
			hover = false;
			repaint();
		}
		QLabel::leaveEvent( event );
	}


private:
	int w = 16;
	int h = 16;
	QColor color = QColor("black");
	bool hover = false;
	bool hoverable = true;

signals:
	void clicked();
	void emitColor( const QColor& color );
};
/**********************************************/
/**********************************************/
/**********************************************/
/* ColorMenu
 *
 * */
class ColorMenu
		: public QWidget
{
	Q_OBJECT
public:
	ColorMenu( QWidget* parent = nullptr )
		: QWidget( parent )
	{
		setWindowFlags( Qt::Popup );
		HTMLcolors = QColor::colorNames();
		// Sorting color by Hue
		int n;
		int i;
		for (n=0; n < HTMLcolors.count(); n++)
		{
			for (i=n+1; i < HTMLcolors.count(); i++)
			{
				int valorN = QColor( HTMLcolors.at(n) ).hue();
				int valorI = QColor( HTMLcolors.at(i) ).hue();
				if ( valorN > valorI )
				{
					HTMLcolors.move(i, n);
					n=0;
				}
			}
//			qDebug() << HTMLcolors.at(n);
		}
		// -----
		GridLayout* mainLay = new GridLayout;
		mainLay->setMargin( 0 );
		mainLay->setSpacing( 1 );

		int row = 0, col = 0;
		for ( QString color : HTMLcolors ) {
			ColorLabel* collab = new ColorLabel;
//			connect( collab, &ColorLabel::emitColor,
//					 this, &ColorPickerMenu::changeColor );
//			connect( collab, &ColorLabel::emitColor,
//					 this, &ColorPickerMenu::emitColor );
			connect( collab, SIGNAL(emitColor(QColor)),
					 this, SIGNAL(emitColor(QColor)) );
			if ( col == rowCut ) {
				++row;
				col = 0;
			}
			collab->setColor( color );
			mainLay->addWidget( collab, row, col );
			++col;
		}
		QWidget* container = new QWidget;
//		container->setStyleSheet( "background-color: orange;");
		container->setLayout( mainLay );
		ScrollArea* scroll = new ScrollArea;
		scroll->setWidgetResizable( false );
		scroll->setWidget( container );
		scroll->setSizePolicy( QSizePolicy::Preferred,
							   QSizePolicy::Preferred );

		GridLayout* lay2 = new GridLayout;
		lay2->addWidget( scroll, 2, 0, 1, 3 );

		lb_currentColor->setText( "Current Color : " );
		col_currentColor->setColor( currentColor );
		col_currentColor->setSize( 30, 20 );
		col_currentColor->setHoverable( false );
		lay2->addWidget( lb_currentColor, 0, 0 );
		lay2->addWidget( col_currentColor, 0, 1 );
		lay2->addItem( new QSpacerItem(0, 0, QSizePolicy::Expanding), 0, 2 );

		QHBoxLayout* lay3 = new QHBoxLayout;
		lay3->addWidget( co_palette );
		lay3->addSpacerItem( new QSpacerItem(0,0,QSizePolicy::Expanding));
		lay2->addLayout( lay3, 1, 0, 1, 3 );

//		moreBut->uncolored();
		moreBut->setText( "More" );
		moreBut->setSizePolicy( QSizePolicy::Maximum,
							   QSizePolicy::Preferred );
//		connect( moreBut, &ColorButton::changed,
//				 this, &ColorPickerMenu::changeColor );
		connect( moreBut, SIGNAL(changed(QColor)),
				 this, SIGNAL(emitColor(QColor)) );

		lay2->addWidget( moreBut, 3, 0, 1, 3 );

		setLayout( lay2 );
		setSizePolicy( QSizePolicy::Minimum,
					   QSizePolicy::Preferred );

		scroll->setMaximumHeight( 250 );
		setFixedWidth( container->width() + 2*lay2->margin() + scroll->verticalScrollBar()->sizeHint().width() + 10 );

	}
	void setCurrentColor( const QColor& color ) {
		currentColor = color;
		col_currentColor->setColor( currentColor );
//		moreBut->setColor( currentColor );
	}


public slots:
	void changeColor( const QColor& color ) {
		Q_UNUSED(color)
		//qDebug() << color;

	}


private:
	QStringList HTMLcolors;
	QList<QColor> colors;
	int rowCut = 8;
	QColor currentColor = QColor("#5543A6");
	QLabel* lb_currentColor = new QLabel;
	ColorLabel* col_currentColor = new ColorLabel;
	QComboBox* co_palette = new QComboBox;
//	ColorButton* moreBut = new ColorButton;
	QPushButton* moreBut = new QPushButton;

signals:
	void emitColor( const QColor& color );
};
/**********************************************/
/**********************************************/
/**********************************************/
/* ColorSelector
 *
 * */
class ColorSelector
		: public QPushButton
{
	Q_OBJECT
private:
	QColor color = QColor("black");
	ColorMenu* menu = new ColorMenu;

public:
	~ColorSelector()
	{

	}
	ColorSelector( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		menu->hide();
		connect( menu, &ColorMenu::emitColor,
				 this, &ColorSelector::setColor );
		connect( menu, &ColorMenu::emitColor,
				 this, &ColorSelector::changed );

	}
	QColor getColor() const
	{
		return color;
	}
	void mouseReleaseEvent ( QMouseEvent* event ) {
		QPoint newPos = this->mapToGlobal( QPoint(0, this->height()) );
		QRect rec = QGuiApplication::screens().at(0)->geometry();
		int dx = newPos.x(), dy = newPos.y();
		if ( dx + menu->sizeHint().width() > rec.width() )
			dx = rec.width() - menu->sizeHint().width();
		if ( dy + menu->sizeHint().height() > rec.height() )
			dy -= menu->sizeHint().height()+this->height();
		menu->move( dx, dy );
		menu->setWindowFlags( Qt::Popup );
		menu->show();
		QPushButton::mouseReleaseEvent( event );
	}

public slots:
	void setColor( const QColor& value )
	{
		color = value;
		QPixmap pix( 40, 16 );
		pix.fill( color );
		QIcon icon( pix );
		setIcon( icon );
		setIconSize( {40, 16} );
		menu->hide();
		menu->setCurrentColor( color );
//		qDebug() << color;
	}

signals:
	void changed( const QColor& color );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COLORSELECTOR_H
