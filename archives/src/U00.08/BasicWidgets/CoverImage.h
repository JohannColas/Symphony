#ifndef COVERIMAGE_H
#define COVERIMAGE_H
/**********************************************/
#include "BasicWidgets/LockedButton.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CoverImage
        : public LockedButton
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	CoverImage( QWidget* parent = 0 )
        : LockedButton( parent )
	{
		setFlat( true );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setPixmap( const QPixmap& pix )
	{
		setIcon( pix );
	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COVERIMAGE_H
