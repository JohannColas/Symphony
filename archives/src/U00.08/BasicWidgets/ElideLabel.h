//#ifndef ELIDELABEL_H
//#define ELIDELABEL_H

//#include <QFrame>
//#include <QPainter>
//#include <QTextLayout>
//#include <QDebug>
////#include <cmath>



//class ElideLabel : public QFrame
//{
//	Q_OBJECT
////	Q_PROPERTY(QString text READ text WRITE setText)
////	Q_PROPERTY(bool isElided READ isElided)
//protected:
//	QString _text = "";
//	bool _elide = true;
//	bool _multiLine = false;
//	bool _fitWidthToContent = false;
//	bool _fitHeightToContent = true;

//public:
//	ElideLabel( QWidget *parent = 0 )
//		: QFrame( parent )
//	{
//		setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
//	}
//	ElideLabel( const QString &text, QWidget *parent = 0 )
//		: QFrame( parent )
//	{
//		setText( text );
//		setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
//	}
//	const QString& text() const
//	{
//		return _text;
//	}
//	bool isElided() const
//	{
//		return _elide;
//	}
//	void allowElide()
//	{
//		_elide = true;
//		unfitToContent();
//	}
//	void avoidElide()
//	{
//		_elide = false;
//		repaint();
//	}
//	void allowMultiLine()
//	{
//		_multiLine = true;
//	}
//	void avoidMultiLine()
//	{
//		_multiLine = false;
//	}
//	void fitToContent()
//	{
//		_fitWidthToContent = true;
//		_fitHeightToContent = true;
//		_elide = false;
//		repaint();
//	}
//	void fitWidthToContent()
//	{
//		_fitWidthToContent = true;
//		_elide = false;
//		repaint();
//	}
//	void fitHeightToContent()
//	{
//		_fitHeightToContent = true;
//		_elide = false;
//		repaint();
//	}
//	void unfitToContent()
//	{
//		_fitWidthToContent = false;
//		repaint();
//	}

//public slots:
//	void setText( const QString& text )
//	{
//		_text = text;
//		repaint();
//	}

//protected:
//	void paintEvent( QPaintEvent *event ) override
//	{
//		QFrame::paintEvent( event );

//		QPainter painter( this );
//		QFontMetrics metrix = painter.fontMetrics();
//		int cww = contentsRect().width();
//		int _width = 1;
//		int _height = contentsMargins().top() +
//					  contentsMargins().bottom();
//		int x = contentsMargins().left();
//		int y = metrix.ascent() + contentsMargins().top();
//		QStringList lines = _text.split("\n");
//		for ( QString line : lines )
//		{
//			// Elide line
//			if ( _elide && !_fitWidthToContent )
//				while ( cww < metrix.boundingRect( line ).width() )
//					line = line.left( line.length() -4 ) + "...";
//			// Getting the maximum line width
//			if ( _width < metrix.boundingRect( line ).width() )
//				_width = metrix.boundingRect( line ).width();
//			// Drawing Line if the Text Height is < Height
//			if ( _height+metrix.height() <= height() )
//			{
//				painter.drawText( x, y, line );
//			}
//			y += metrix.height();
//			_height += metrix.height();
//			if ( !_multiLine )
//				break;
//		}
//		if ( _fitWidthToContent )
//		{
//			_width += contentsMargins().left() + contentsMargins().right();
//			setFixedWidth( _width );
//		}
//		if ( _fitHeightToContent )
//			setFixedHeight( _height );

//		//		bool didElide = false;
//		//		int lineSpacing = metrix.lineSpacing();
//		//		int y = 0;

//		//		QTextLayout textLayout( content, painter.font() );
//		//		textLayout.beginLayout();
//		//		forever {
//		//			QTextLine line = textLayout.createLine();

//		//			if ( !line.isValid() )
//		//				break;

//		//			line.setLineWidth( width() );
//		//			int nextLineY = y + lineSpacing;

//		//			if ( height() >= nextLineY + lineSpacing )
//		//			{
//		//				line.draw( &painter, QPoint(0, y) );
//		//				y = nextLineY;
//		//				//! [2]
//		//				//! [3]
//		//			}
//		//			else
//		//			{
//		//				QString lastLine = content.mid( line.textStart() );
//		//				QString elidedLastLine =
//		//						metrix.elidedText( lastLine,
//		//												Qt::ElideRight,
//		//												width() -contentsMargins().left() - contentsMargins().right()
//		//												);
//		//				painter.drawText(
//		//							QPoint( contentsMargins().top(),
//		//									y + metrix.ascent() + contentsMargins().left()),
//		//							elidedLastLine );
//		//				line = textLayout.createLine();
//		//				didElide = line.isValid();
//		////				qDebug() << fontMetrics.horizontalAdvance( elidedLastLine );
//		//				break;
//		//			}
//		//		}
//		//		textLayout.endLayout();
//		//		setFixedHeight( textLayout.boundingRect().height() +
//		//						contentsMargins().top() +
//		//						contentsMargins().bottom() );
//		//		if ( didElide != elided )
//		//		{
//		//			elided = didElide;
//		//			emit elisionChanged( didElide );
//		//		}
//	}

//signals:
////	void elisionChanged(bool elided);

//private:
////	bool elided;
////	QString content;
//};
//#endif // ELIDELABEL_H
