#ifndef SECTIONBUTTON_H
#define SECTIONBUTTON_H
/**********************************************/
#include <QPushButton>
#include <QMouseEvent>
#include "BasicWidgets/Layouts.h"
#include "BasicWidgets/LockedButton.h"
#include "BasicWidgets/Label.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SectionButton
        : public QPushButton
{
    Q_OBJECT
public:
    enum ButtonStyle {
        IconOnly,
        TextOnly,
        TextBesideIcon,
        TextUnderIcon
    };
private:
    LockedButton* _icon = new LockedButton(this);
    Label* _text = new Label(this);
    GridLayout* lay_main = new GridLayout(this);
    ButtonStyle _style = SectionButton::TextUnderIcon;
public:
    SectionButton( QWidget* parent = nullptr )
        : QPushButton( parent )
    {
        setFlat( true );
        setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
        setContentsMargins(10, 0, 10, 0 );
        setLayout( lay_main );
//        _text->fitWidthToContent();
    }
    SectionButton( const QString& text, const QIcon& icon, QWidget* parent = nullptr )
        : QPushButton( parent )
    {
        setFlat( true );
        lay_main->setSizeConstraint( QLayout::SetMinimumSize );
        setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
        setContentsMargins(10, 0, 10, 0 );
        setLayout( lay_main );
//        _icon->setFlat( true );
        setIcon( icon );
        setIconSize( 36 );
//        _icon->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
//        lay_main->setAlignment( _icon, Qt::AlignHCenter | Qt::AlignVCenter );
//        _text->setFlat( true );
        _text->fitWidthToContent();
        setText( text );
//        _text->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
//        lay_main->setAlignment( _text, Qt::AlignHCenter | Qt::AlignVCenter );
        updateLayout();
    }

public slots:
    void setText( const QString& text )
    {
        _text->setText( text );
//        _text->resize( _text->sizeHint() );
        updateLayout();
    }
    void setIcon( const QIcon& icon )
    {
        _icon->setIcon( icon );
        updateLayout();
    }
    void setIconSize( const QSize& size )
    {
        _icon->setIconSize( size );
        updateLayout();
    }
    void setIconSize( int size )
    {
        setIconSize( QSize(size, size) );
    }
    void setStyle( const SectionButton::ButtonStyle style )
    {
        _style = style;
        updateLayout();
    }
    void updateLayout()
    {
        lay_main->removeWidget( _icon );
        lay_main->removeWidget( _text );
//        _text->setLayoutDirection( Qt::LeftToRight );
        if ( _style == SectionButton::IconOnly )
        {
            lay_main->addWidget( _icon, 0, 0 );
            _icon->show();
            _text->hide();
//            lay_main->setAlignment( _icon, Qt::AlignHCenter | Qt::AlignVCenter );
            lay_main->setAlignment( _icon, Qt::AlignHCenter | Qt::AlignVCenter );
        }
        else if ( _style == SectionButton::TextOnly )
        {
            lay_main->addWidget( _text, 0, 0 );
            _icon->hide();
            _text->show();
//            lay_main->setAlignment( _text, Qt::AlignHCenter | Qt::AlignVCenter );
            lay_main->setAlignment( _text, Qt::AlignHCenter | Qt::AlignVCenter );
        }
        else if ( _style == SectionButton::TextUnderIcon )
        {
            lay_main->addWidget( _icon, 0, 0 );
            lay_main->addWidget( _text, 1, 0 );
            _icon->show();
            _text->show();
//            lay_main->setAlignment( _icon, Qt::AlignHCenter );
//            lay_main->setAlignment( _text, Qt::AlignHCenter );
            lay_main->setAlignment( _text, Qt::AlignHCenter | Qt::AlignVCenter );
            lay_main->setAlignment( _icon, Qt::AlignHCenter | Qt::AlignVCenter );
        }
        else if ( _style == SectionButton::TextBesideIcon )
        {
            lay_main->addWidget( _icon, 0, 0 );
            lay_main->addWidget( _text, 0, 1 );
            _icon->show();
            _text->show();
//            _text->setStyleSheet("text-align:left;");
            lay_main->setAlignment( _icon, Qt::AlignVCenter | Qt::AlignLeft );
            lay_main->setAlignment( _text, Qt::AlignVCenter | Qt::AlignLeft );
        }
        _text->fitWidthToContent();
//        lay_main->setAlignment( _icon, Qt::AlignHCenter | Qt::AlignVCenter );
//        lay_main->setAlignment( _text, Qt::AlignHCenter | Qt::AlignVCenter );
        _icon->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
        _text->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SECTIONBUTTON_H
/**********************************************/
