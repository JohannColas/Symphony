#ifndef SYSTEMCONTROLS_H
#define SYSTEMCONTROLS_H

/**********************************************/
#include <QWidget>
#include <QApplication>
#include "AppButton.h"
#include "SystemButton.h"
#include "Layouts.h"
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
class SystemControls
        : public QWidget
{
    Q_OBJECT
private:
    // Application Button
    AppButton* pb_settings = new AppButton(this);
    // System Button
    SystemButton* pb_minimize = new SystemButton(this);
    SystemButton* pb_maximize = new SystemButton(this);
    SystemButton* pb_close = new SystemButton(this);
    Settings* _sets;

public:
    ~SystemControls()
    {

    }
    SystemControls( QWidget* parent = 0 )
        : QWidget( parent )
    {
        HorizontalLayout* lay_main = new HorizontalLayout;
//        lay_main->addSpacer();
        lay_main->addWidget( pb_settings );
        lay_main->addWidget( pb_minimize );
        lay_main->addWidget( pb_maximize );
        lay_main->addWidget( pb_close );
        setLayout( lay_main );
        setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );

        connect( pb_settings, &QPushButton::clicked,
                 this, &SystemControls::onSettings );
        connect( pb_minimize, &QPushButton::clicked,
                 this, &SystemControls::onMinimize );
        connect( pb_maximize, &QPushButton::clicked,
                 this, &SystemControls::onMaximize );
        connect( pb_close, &QPushButton::clicked,
                 qApp, &QApplication::closeAllWindows );
    }
    void setSettings( Settings* sets )
    {
        _sets = sets;
        updateIcons();
    }

public slots:
    void updateIcons()
    {
        pb_settings->setIcon( _sets->icon("settings") );
        pb_minimize->setIcon( _sets->icon("minimise") );
        pb_maximize->setIcon( _sets->icon("maximise") );
        pb_close->setIcon( _sets->icon("close") );
    }

protected:

signals:
    void onSettings();
    void onMinimize();
    void onMaximize();
//    void onClose();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SYSTEMCONTROLS_H
/**********************************************/
