#ifndef FILE_H
#define FILE_H

#include <QFile>
#include <QPixmap>

class Filing
{
public:
    static inline void save( const QString& path, const QByteArray& data )
    {
        QFile file( path );
        if ( !file.exists() )
        {
            file.open( QIODevice::WriteOnly );
            file.write( data );
            file.close();
        }
    }
    static inline QByteArray read( const QString& path )
    {
        QFile file( path );
        if ( !file.exists() )
        {
            file.open( QIODevice::ReadOnly );
            file.close();
        }
        return QByteArray();
    }
    static inline QPixmap readImage( const QString& path )
    {
        QPixmap pixmap;
        pixmap.load( path );
        return pixmap;
    }
    static inline void remove( const QString& path )
    {
        QFile file( path );
        file.remove();
    }
};

#endif // FILE_H
