#ifndef ONLINEMETADATA_H
#define ONLINEMETADATA_H

#include <QApplication>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSettings>
#include <QThread>
#include "Commons/Artist.h"
#include "Commons/File.h"

class OnlineMetadata
        : public QThread
{
    Q_OBJECT
    QList<Artist*> _artists;
    QNetworkAccessManager nam;
    void run() override
    {
        this->setPriority( Priority::LowPriority );
        for ( Artist* artist : _artists )
        {
            getArtistInfo( artist );
            for ( Album* album : artist->albums() )
            {
                getAlbumInfo( album );
                for ( Track* track : album->tracks() )
                {
                    getTrackInfo( track );
                }
            }
        }
    }

public:
    ~OnlineMetadata()
    {
        nam.deleteLater();
        this->quit();
        this->wait();
    }
    OnlineMetadata( const QList<Artist*>& artists )
    {
        nam.moveToThread( this );
        _artists = artists;
        connect( this, &OnlineMetadata::finished,
                 this, &QObject::deleteLater );
        connect( this, &OnlineMetadata::finished,
                 this, &OnlineMetadata::quit );
    }
    void getArtistInfo( Artist* artist )
    {
//        qDebug().noquote() << "Get info of:"<<artist->name();
        if ( artist->name() != "" )
        {
            QString path = /*QDir::currentPath() +*/ "./libraries/artists/" + artist->name() + "/";
    //        QFile file( path + "artist.ats" );
    //        file.remove();

            QFileInfo infoF( path + "artist.ats" );
            if ( !infoF.exists() )
            {
                QSettings setsFile( path + "artist.ats", QSettings::IniFormat );

                QString url = QString("https://www.theaudiodb.com/api/v1/json/1/search.php?s="+artist->name()).replace(" ", "%20");
                QByteArray data = network( url );
                QJsonDocument jsonDoc = QJsonDocument::fromJson( data );
                QVariantMap jsonMap = jsonDoc.object().toVariantMap();

                for (auto elm : jsonMap.keys() )
                {
                    if ( elm == "artists" )
                    {
                        QJsonArray artistArray = jsonMap[elm].toJsonArray();
                        QVariantMap artistMap = artistArray[0].toObject().toVariantMap();
                        for (auto elm : artistMap.keys() )
                        {
                            if ( elm == "artist" )
                            {
                                setsFile.setValue("name", artistMap[elm] );
                            }
                            else if ( elm == "intBornYear" )
                            {
                                setsFile.setValue("bornedYear", artistMap[elm] );
                            }
                            else if ( elm == "intMembers" )
                            {
                                setsFile.setValue("nbMembers", artistMap[elm] );
                            }
                            else if ( elm == "strArtistFanart" )
                            {
                                QString fanartUrl = artistMap[elm].toString();
                                QByteArray array = network( fanartUrl );
                                if ( !array.isEmpty() )
                                    Filing::save( path + "fanart.png", array );
                            }
                            else if ( elm == "strArtistThumb" )
                            {
                                QString thumbUrl = artistMap[elm].toString();
                                QByteArray array = network( thumbUrl );
                                if ( !array.isEmpty() )
                                    Filing::save( path + "thumb.png", array );
                            }
                            else if ( elm == "strBiographyFR" )
                            {
                                setsFile.setValue("biography", artistMap[elm] );
                            }
                            else if ( elm == "strCountryCode" )
                            {
                                setsFile.setValue("country", artistMap[elm] );
                            }
                            else if ( elm == "strGender" )
                            {
                                setsFile.setValue("gender", artistMap[elm] );
                            }
                            else if ( elm == "strWebsite" )
                            {
                                setsFile.setValue("website", artistMap[elm] );
                            }
                        }
                    }
                }
            }
        }
    }
    void getAlbumInfo( Album* album )
    {
    album->title();
    }
    void getTrackInfo( Track* track )
    {
track->title();
    }
    QByteArray network( const QString& url )
    {
        QNetworkReply* reply = nam.get( QNetworkRequest( url ) );
        while ( !reply->isFinished() )
        {
            qApp->processEvents();
        }
        reply->deleteLater();
        return reply->readAll();
    }
};



#endif // ONLINEMETADATA_H
