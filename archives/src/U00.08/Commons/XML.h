#ifndef XML_H
#define XML_H

#include <QDomDocument>
#include <QFile>
#include <QTextStream>


class XML
{

public:
	static inline void save( const QString& path, QDomDocument& xmlDoc )
	{
		// Getting root element
		QDomElement el_root = xmlDoc.documentElement();
		// Writing the Settings of XML Files
		QDomProcessingInstruction settings =
				xmlDoc.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"utf-8\"" );
		xmlDoc.insertBefore( settings, el_root );
		// Create the XML if DONT exists
		QFile xmlFile( path );
		if( !xmlFile.open( QFile::WriteOnly ) )
			return;
		// Create QTextStream
		QTextStream stream( &xmlFile );
		// Writing the
		xmlDoc.save( stream, 4, QDomDocument::EncodingFromTextStream );
		//Fermeture du fichier
		xmlFile.close();
	}
	static inline QDomDocument read( const QString& path )
	{
		// Getting root element
		QDomDocument xmlDoc;
		QFile file( path );
		// Opening and Checking if the file exists
		if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
			return xmlDoc;
		// Settings and Checking if the file containts XML data
		xmlDoc.setContent( &file );
		file.close();
		return xmlDoc;
	}
};

#endif // XML_H
