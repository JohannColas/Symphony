#ifndef LIBRARYITEM_H
#define LIBRARYITEM_H
/**********************************************/
#include "BasicWidgets/ListItem.h"
#include "Commons/Icons.h"
#include "Commons/TrackList.h"
/**********************************************/
#include <QLabel>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LibraryItemIcon
		: public QLabel
{

public:
	LibraryItemIcon( QWidget* parent = 0 )
		: QLabel( parent )
	{
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	}
};
class LibraryItemName
		: public QLabel
{
};

class LibraryItem
		: public ListItem
{
	Q_OBJECT
private:

public:
	LibraryItem( const QString& name, QIcon icon, QWidget* parent = 0 )
		: ListItem( parent )
	{
		// Adding the widgets
		setText( name );
		setIcon( icon );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARYITEM_H
