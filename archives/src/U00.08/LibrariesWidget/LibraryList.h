#ifndef LIBRARYLIST_H
#define LIBRARYLIST_H
/**********************************************/
#include <QObject>
#include <QSettings>
#include "Commons/TrackList.h"
#include "Commons/Library.h"
#include "Commons/Artist.h"
#include <QTextStream>
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LibraryList
		: public QObject
{
	Q_OBJECT
private:
	QList<TrackList*> _list;
	QList<Library*> _libraries;

public:
	LibraryList()
	{
		update();
	}
	QList<TrackList*> list()
	{
		return _list;
	}
	int count() const
	{
		return _libraries.count();
	}
	bool isIndexValid( int index ) const
	{
		return ( index > -1 && index < count() );
	}

public slots:
	void update()
	{
		QFile file( "./libraries/#libraries.slbs" );
		if ( file.open(QIODevice::ReadOnly) )
		{
			QTextStream in(&file);
			while ( !in.atEnd() )
			{
				QString name = in.readLine();
				addLibrary( name );

			}
		}
		else
			qDebug() << "error Reading file";
	}
	void addLibrary( const QString& name )
	{
		_list.append( new TrackList( name, TrackList::LIBRARY) );
		_libraries.append( new Library(name) );
	}
	void moveLibrary( int from, int to )
	{
		_list.move( from, to );
		_libraries.move( from, to );
	}
	void removeLibrary( int index )
	{
		_list.removeAt( index );
		_libraries.removeAt( index );
	}
	void clear()
	{
		_list.clear();
		_libraries.clear();
		emit cleared();
	}
	void setCurrentIndex( int index )
	{
		if ( isIndexValid( index ) )
		{
//			_currentTracklistIndex = index;
			emit currentLibraryChanged( _list.at(index) );
		}
	}
    QList<Artist*> artists()
    {
        QList<Artist*> artists;
        //	for ( Library* library : _libraries )
        {
            //		if ( !library->hide() )
            //			artists.append( library->artists() );
        }
        return artists;
    }
    Artist* artist( const QString& name )
    {
        return 0;
    }
    QList<Album*> albums()
    {
        QList<Album*> albums;
        //	for ( Library* library : _libraries )
        {
            //		if ( !library->hide() )
            //			albums.append( library->albums() );
        }
        return albums;
    }
    Album* album( const QString& albumTitle )
    {
        return 0;
    }
    Album* album( const QString& artistName, const QString& albumTitle )
    {
        return 0;
    }
    QList<Track*> tracks()
    {
        QList<Track*> tracks;
        //	for ( Library* library : _libraries )
        {
            //		if ( !library->hide() )
            //			tracks.append( library->tracks() );
        }
        return tracks;
    }

signals:
	void cleared();
	void currentLibraryChanged( TrackList* library );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARYLIST_H
