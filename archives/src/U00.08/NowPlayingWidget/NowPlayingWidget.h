#ifndef NOWPLAYINGFRAME_H
#define NOWPLAYINGFRAME_H
/**********************************************/
#include "BasicWidgets/Frame.h"
/**********************************************/
#include "TracksWidget/TrackWidget.h"
#include "TracksWidget/TrackSelector.h"
/**********************************************/
#include "Commons/Icons.h"
#include "Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class NowPlayingWidget
		: public Frame
{
	Q_OBJECT
private:
	TrackList* _nowPlaylist = 0;
	TrackWidget* wid_trackInfo = new TrackWidget;
	TrackSelector* wid_trackList = new TrackSelector;
	Icons* _icons = 0;

public:
    NowPlayingWidget( QWidget* parent = nullptr )
        : Frame( parent )
    {
        addWidget( wid_trackInfo, 0, 0 );
        addWidget( wid_trackList, 0, 1 );
        wid_trackList->allowRemove();

        wid_trackList->setMaximumWidth( 400 );
        wid_trackList->avoidMultipleSelection();
        wid_trackList->avoidRightClick();
        connect( wid_trackList, &TrackSelector::indexToPlay,
                 this, &NowPlayingWidget::indexChanged );
    }
	void setPlaylists( Playlists* playlists )
	{
		wid_trackList->setPlaylists( playlists );
	}


public slots:
    void setPlayer( QMediaPlayer* player )
    {
    }
    void setPlaylist( TrackList* playlist )
    {
        _nowPlaylist = playlist;
        wid_trackList->setTrackList( _nowPlaylist );
        wid_trackInfo->setTrack( _nowPlaylist->track(0) );
        connect( wid_trackList, &TrackSelector::trackChanged,
                 wid_trackInfo, &TrackWidget::setTrack );
        connect( _nowPlaylist, &TrackList::changed,
                 this, &NowPlayingWidget::update );
        connect( _nowPlaylist, &TrackList::trackAdded,
                 this, &NowPlayingWidget::updateList );
    }
    void setTrackList( TrackList* playlist )
    {
        _nowPlaylist = playlist;
        wid_trackList->setTrackList( _nowPlaylist );
        wid_trackInfo->setTrack( _nowPlaylist->track(0) );
        connect( wid_trackList, &TrackSelector::trackChanged,
                 wid_trackInfo, &TrackWidget::setTrack );
        connect( _nowPlaylist, &TrackList::changed,
                 this, &NowPlayingWidget::update );
        connect( _nowPlaylist, &TrackList::trackAdded,
                 this, &NowPlayingWidget::updateList );
    }
    void updateList()
    {
        wid_trackList->update();
        wid_trackInfo->setTrack( _nowPlaylist->currentTrack() );
    }
    void update()
    {
        wid_trackList->setBegin( _nowPlaylist->currentTrackIndex() );
    }
    void updateIcons( Icons* icons )
    {
        _icons = icons;
        wid_trackInfo->updateIcons( _icons );
        wid_trackList->updateIcons( _icons );
    }

signals:
	void indexChanged( int index );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // NOWPLAYINGFRAME_H
