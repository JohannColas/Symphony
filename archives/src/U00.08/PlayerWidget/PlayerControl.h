#ifndef PLAYERCONTROL_H
#define PLAYERCONTROL_H
/**********************************************/
#include <QPushButton>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerControl
        : public QPushButton
{
    Q_OBJECT

public:
    PlayerControl( QWidget* parent = 0 )
        : QPushButton( parent )
    {
        setFlat( true );
        setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERCONTROL_H
/**********************************************/
