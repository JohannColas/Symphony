#ifndef PLAYERSUBCONTROLS_H
#define PLAYERSUBCONTROLS_H
/**********************************************/
#include <QWidget>
#include "PlayerSubControl.h"
#include "BasicWidgets/Layouts.h"
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
class PlayerSubControls
        : public QWidget
{
    Q_OBJECT
private:
    PlayerSubControl* pb_repeat = new PlayerSubControl(this);
    PlayerSubControl* pb_shuffle = new PlayerSubControl(this);
    PlayerSubControl* pb_equalizer = new PlayerSubControl(this);
    PlayerSubControl* pb_volume = new PlayerSubControl(this);
    Settings* _sets;

public:
    ~PlayerSubControls()
    {

    }
    PlayerSubControls( QWidget* parent = 0 )
        : QWidget( parent )
    {
        HorizontalLayout* lay_main = new HorizontalLayout;
        lay_main->addSpacer();
        lay_main->addWidget( pb_repeat );
        lay_main->addWidget( pb_shuffle );
        lay_main->addWidget( pb_equalizer );
        lay_main->addWidget( pb_volume );
        lay_main->addSpacer();
        setLayout( lay_main );
        setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );

        connect( pb_repeat, &PlayerSubControl::released,
                 this, &PlayerSubControls::onRepeat );
        connect( pb_shuffle, &PlayerSubControl::released,
                 this, &PlayerSubControls::onShuffle );
        connect( pb_equalizer, &PlayerSubControl::released,
                 this, &PlayerSubControls::onEqualizer );
        connect( pb_volume, &PlayerSubControl::released,
                 this, &PlayerSubControls::onVolume );
    }
    void setSettings( Settings* sets )
    {
        _sets = sets;
        updateIcons();
    }

public slots:
    void updateIcons()
    {
        pb_repeat->setIcon( _sets->icon("no_repeat") );
        pb_shuffle->setIcon( _sets->icon("no_shuffle") );
        pb_equalizer->setIcon( _sets->icon("equalizer") );
        pb_volume->setIcon( _sets->icon("volume") );
    }
    void noRepeatIcon()
    {
        if ( _sets )
            pb_repeat->setIcon( _sets->icon("no_repeat") );
    }
    void repeatOneIcon()
    {
        if ( _sets )
            pb_repeat->setIcon( _sets->icon("repeat_one") );
    }
    void repeatAllIcon()
    {
        if ( _sets )
            pb_repeat->setIcon( _sets->icon("repeat_all") );
    }
    void noShuffleIcon()
    {
        if ( _sets )
            pb_shuffle->setIcon( _sets->icon("no_shuffle") );
    }
    void shuffleIcon()
    {
        if ( _sets )
            pb_shuffle->setIcon( _sets->icon("shuffle") );
    }

signals:
    void onShuffle();
    void onRepeat();
    void onEqualizer();
    void onVolume();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERSUBCONTROLS_H
/**********************************************/
