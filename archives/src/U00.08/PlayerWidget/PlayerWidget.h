#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include <QFrame>
#include <QPushButton>
#include <QElapsedTimer>
#include <QSlider>
#include <QProgressBar>
#include <QTimer>
#include <QMouseEvent>
#include "Commons/Track.h"
#include "Commons/TrackList.h"
#include "Commons/Icons.h"
#include "Commons/Lang.h"
#include "Commons/Settings.h"
/**********************************************/
#include "PlayerControls.h"
#include "PlayerSubControls.h"
#include "PlayerTrackBanner.h"
/**********************************************/
#include <QMediaPlayer>
#include <QMediaPlaylist>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerWidget
		: public Frame
{
	Q_OBJECT
public:
	enum Repeat {
		NOREPEAT,
		REPEATALL,
		REPEATONE
	};

private:
	bool _movable = false;
	TrackList* _nowPlaylist = 0;
	QMediaPlayer* _player = new QMediaPlayer;
	QTimer* _timer = new QTimer(this);
    bool _isPlaying = false;
    // Player Widgets
    PlayerControls* _playerControls = new PlayerControls(this);
    PlayerSubControls* _playerSubControls = new PlayerSubControls(this);
    PlayerTrackBanner* _playerTrackBanner = new PlayerTrackBanner(this);
    //
    Repeat _repeat = Repeat::NOREPEAT;
    bool _isShuffle = false;
	//
	CoverImage* img_cover = new CoverImage;
	TitleLabel* lb_title = new TitleLabel;
	ArtistLabel* lb_artist = new ArtistLabel;
	QSlider* sl_time = new QSlider;
	QLabel* lb_time = new QLabel;
	int _newPos = -1;
    int _duration = -1;
	QList<int> _listOrder;
	int _index = 0;
    Settings* _sets;

public:
    ~PlayerWidget()
    {

    }
    PlayerWidget( QWidget* parent = nullptr )
        : Frame( parent )
    {
        show();

        connect( _player, &QMediaPlayer::positionChanged,
                 this, &PlayerWidget::changePosition );
        connect( _player, &QMediaPlayer::mediaStatusChanged,
                 this, &PlayerWidget::onMediaStatusChanged );

        connect( _playerControls, &PlayerControls::playpause,
                 this, &PlayerWidget::playpause );
        connect( _playerControls, &PlayerControls::stop,
                 this, &PlayerWidget::stop );
        connect( _playerControls, &PlayerControls::backward,
                 this, &PlayerWidget::backward );
        connect( _playerControls, &PlayerControls::forward,
                 this, &PlayerWidget::forward );

        connect( _playerSubControls, &PlayerSubControls::onShuffle,
                 this, &PlayerWidget::shuffle );
        connect( _playerSubControls, &PlayerSubControls::onRepeat,
                 this, &PlayerWidget::repeat );
        //    connect( _playerSubControls, &PlayerSubControls::onEqualizer,
        //             this, &PlayerWidget::equalizer );
        //    connect( _playerSubControls, &PlayerSubControls::onVolume,
        //             this, &PlayerWidget::volume );

        connect( sl_time, &QSlider::valueChanged,
                 this, &PlayerWidget::setPlayerPos );
        _timer->setSingleShot( true );
        connect( _timer, &QTimer::timeout,
                 this, &PlayerWidget::updatePlayerPos );

        setMouseTracking( true );
    }
    void randomize()
    {
        _listOrder.clear();
        for ( int it = 0; it < _nowPlaylist->count(); ++it )
        {
            _listOrder.append( it );
        }
        std::srand(std::time(nullptr));
        std::random_shuffle( _listOrder.begin(), _listOrder.end() );
        if ( _index != -1 && _listOrder.contains(_index) )
        {
            while ( _listOrder.first() != _index )
            {
                _listOrder.append( _listOrder.takeFirst() );
            }
        }
    }

public slots:
    void playTrack( Track* track )
    {
        _nowPlaylist->clear();
        _nowPlaylist->add( track );
        _index = 0;
        setTrack();
        play();
    }
    void addTrack( Track* track )
    {
        _nowPlaylist->add( track );
    }
    void playTracklist( TrackList* tracklist )
    {
        _nowPlaylist->clear();
        _nowPlaylist->add( tracklist );
        _index = 0;
        setTrack();
        play();
    }
    void addTracklist( TrackList* tracklist )
    {
        _nowPlaylist->add( tracklist );
    }
    void insertTracklist( TrackList* tracklist )
    {
        if ( _isShuffle )
            addTracklist( tracklist );
        else
            _nowPlaylist->insert( _index, tracklist );
    }
    void receiveTrackList( TrackList* tracklist )
    {
        _nowPlaylist->clear();
        _nowPlaylist->add( tracklist );
        _index = 0;
        setTrack();
        play();
    }
    void setPlaylistIndex( int index )
    {
        _index = index;
        if ( _isShuffle )
            _index = _listOrder.indexOf( index );
        setTrack();
    }
    void setTrackList( TrackList* playlist )
    {
        _nowPlaylist = playlist;
        _index = 0;
        setTrack();
        connect( _nowPlaylist, &TrackList::changed,
                 this, &PlayerWidget::randomize );
    }
	//
    void setTrack()
    {
        int index = _index;
        if ( _isShuffle )
            index = _listOrder.at( index );
        Track* track = _nowPlaylist->track(index);
        _player->setMedia( QUrl::fromLocalFile( track->path() ) );
        _playerTrackBanner->setTrack( track );
        updateTrackInfo( track );
        sl_time->setValue(0);
        sl_time->setMaximum( _duration );
        if ( _isPlaying )
            play();
        updatePlayPauseIcon();
    }
    void play()
    {
        _player->play();
        _isPlaying = true;
    }
    void pause()
    {
        _player->pause();
        _isPlaying = false;
    }
    void playpause()
    {
        if ( _player->state() == QMediaPlayer::PlayingState )
            pause();
        else
            play();
        // Update Play/Pause Icon
        updatePlayPauseIcon();
    }
    void stop()
    {
        _player->stop();
        _isPlaying = false;
        QString playerPos = _sets->getString("Player/position");
        if ( playerPos == "t" )
            playerPos = "b";
        else if ( playerPos == "b" )
            playerPos = "l";
        else if ( playerPos == "l" )
            playerPos = "r";
        else
            playerPos = "t";
        _sets->save("Player/position", playerPos);
    }
    void backward()
    {
        if ( sl_time->value() < 40 )
        {
            --_index;
            if ( _index == -1 )
                _index = _nowPlaylist->count() - 1;
            setTrack();
        }
        else
            _player->setPosition( 0 );
    }
    void forward()
    {
        ++_index;
        if ( _index == _nowPlaylist->count() )
        {
            _index = 0;
            if ( _repeat == Repeat::NOREPEAT )
            {
                stop();
            }
        }
        setTrack();
    }
    void repeat()
    {
        if ( _repeat == Repeat::NOREPEAT )
        {
            _repeat = Repeat::REPEATALL;
            _playerSubControls->repeatAllIcon();
            qDebug() << "Repeat All";
        }
        else if ( _repeat == Repeat::REPEATALL )
        {
            _repeat = Repeat::REPEATONE;
            _playerSubControls->repeatOneIcon();
            qDebug() << "Repeat One";
        }
        else if ( _repeat == Repeat::REPEATONE )
        {
            _repeat = Repeat::NOREPEAT;
            _playerSubControls->noRepeatIcon();
            qDebug() << "No Repeat";
        }
    }
    void shuffle()
    {
        _isShuffle = !_isShuffle;
        if ( _isShuffle )
        {
            randomize();
            _playerSubControls->shuffleIcon();
        }
        else
        {
            _index = _listOrder.at(_index);
            _listOrder.clear();
            _playerSubControls->noShuffleIcon();
        }
    }
    void changePosition( qint64 pos )
    {
        //	if ( _player->duration() )
        {

            //		int perc = _duration ? (0.001*pos)/_duration : 0;
            //		sl_time->setValue( perc );
            QString curT = QTime( 0, 0, 0 ).addMSecs( pos ).toString();
            if( curT.left(2) == "00" )
                curT = curT.mid( 3 );
            QString durT = QTime( 0, 0, 0).addSecs( _duration ).toString();
            if( durT.left(2) == "00" )
                durT = durT.mid( 3 );
            lb_time->setText( curT + " / " + durT );
            sl_time->blockSignals( true );
            sl_time->setValue(0.001*pos);
            sl_time->blockSignals( false );
        }
    }
    void setPlayerPos( int pos )
    {
        _newPos = pos;
        _timer->start(100);
    }
    void updatePlayerPos()
    {
        if ( _newPos != -1 )
        {
            _player->setPosition( 1000*_newPos );
        }
    }
//	void updateIcons( Icons* icons );
    void updatePlayPauseIcon()
    {
        if ( _player->state() == QMediaPlayer::PlayingState )
            _playerControls->setPauseIcon();
        else
            _playerControls->setPlayIcon();
    }
    void mousePressEvent( QMouseEvent* event ) override
    {
        if ( event->button() == Qt::LeftButton )
        {
            _movable = true;
            emit moveWindow( event->globalPos() );
        }
        event->ignore();
    }
    void mouseReleaseEvent( QMouseEvent* event ) override
    {
        if ( event->button() == Qt::LeftButton )
        {
            _movable = false;
            emit moveWindow( {-1,0} );
        }
        event->ignore();
    }
    void mouseMoveEvent( QMouseEvent* event ) override
    {
        if ( _movable )
            emit moveWindow( event->globalPos() );
        event->ignore();
    }
    void mouseDoubleClickEvent( QMouseEvent* event ) override
    {
        if ( event->button() == Qt::LeftButton )
            onMaximize();
        event->ignore();
    }
    void connectTo( Frame* frame )
    {
        connect( frame, &Frame::trackToPlay,
                 this, &PlayerWidget::playTrack );
        connect( frame, &Frame::trackToAdd,
                 this, &PlayerWidget::addTrack );
        connect( frame, &Frame::tracklistToPlay,
                 this, &PlayerWidget::playTracklist );
        connect( frame, &Frame::tracklistToAdd,
                 this, &PlayerWidget::addTracklist );
        connect( frame, &Frame::tracklistToInsert,
                 this, &PlayerWidget::insertTracklist );
    }
    void onMediaStatusChanged()
    {
        updatePlayPauseIcon();
        if ( _player->mediaStatus() == QMediaPlayer::EndOfMedia )
        {
            if ( _repeat == Repeat::REPEATONE )
            {
                _player->setPosition(0);
                play();
            }
            else
                forward();
        }
    }
    void updateTrackInfo( Track* track )
    {
        if ( track )
        {
            Metadata meta( track );
            QPixmap pix = QPixmap();
            if ( pix.convertFromImage( meta.cover() ) )
                img_cover->setPixmap( pix );
            else
            {
                if ( _sets )
                    img_cover->setIcon( _sets->icon("track") );
            }
            lb_title->setText( meta.title() );
            lb_artist->setText( meta.artist() );
            _duration = meta.duration();
            changePosition(0);
        }
    }
    void setSettings( Settings* sets )
    {
        _sets = sets;
        connect( _sets, &Settings::valueChanged,
                 this, &PlayerWidget::settingsChanged );
        _playerControls->setSettings( _sets );
        _playerSubControls->setSettings( _sets );
        updateLayout();
    }
    void settingsChanged( QString key )
    {
        if ( key == "Player/position" ||
             key == "Player/size" )
            updateLayout();
    }
    void updateLayout()
    {
        QString pos = _sets->getString("Player/position");
        lay_main->removeWidget(_playerControls);
        lay_main->removeWidget(_playerSubControls);
        if ( pos == 't' ||
             pos == 'b' )
        {
            lay_main->addWidget(_playerControls, 0, 0);
            lay_main->addWidget(_playerTrackBanner, 0, 1);
            lay_main->addWidget(_playerSubControls, 0, 2);
            lay_main->addSpacer(0, 3, false);
            setSizePolicy( QSizePolicy::Expanding,
                           QSizePolicy::Maximum );
        }
        else
        {
            lay_main->addWidget(_playerTrackBanner, 0, 0);
            lay_main->addWidget(_playerControls, 1, 0);
            lay_main->addWidget(_playerSubControls, 2, 0);
            lay_main->addSpacer(3, 0, true);
            setSizePolicy( QSizePolicy::Maximum,
                           QSizePolicy::Expanding );
        }
    }

signals:
	void indexChanged();
	void playlistChanged( TrackList* list );
	void onMinimize();
	void onMaximize();
	void moveWindow( const QPoint& pos );
	void showSettings();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERWIDGET_H
