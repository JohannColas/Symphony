#ifndef PLAYLISTBANNER_H
#define PLAYLISTBANNER_H
/**********************************************/
/**********************************************/
#include "BasicWidgets/Frame.h"
/**********************************************/
#include "Commons/TrackList.h"
/**********************************************/
#include "BasicWidgets/ThumbImage.h"
#include "BasicWidgets/PlaylistEdit.h"
#include "BasicWidgets/ArtistLabel.h"
/**********************************************/
#include "Commons/Icons.h"
#include "Commons/Lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistBanner
		: public Frame
{
	Q_OBJECT
protected:
	TrackList* _tracklist = 0;
	ThumbImage* lb_image = new ThumbImage;
	PlaylistEdit* lb_name = new PlaylistEdit;
	ArtistLabel* lb_artist = new ArtistLabel;
	Label* lb_info = new Label;
	Icons* _icons = 0;

public:
	PlaylistBanner( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( lb_image, 0, 0, 2 );
		addWidget( lb_name, 0, 1 );
		addWidget( lb_info, 1, 1 );
		lb_name->setText("----");
		lb_info->setText("----");

		lb_image->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
		setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
	}

public slots:
	void setPlaylist( TrackList* tracklist )
	{
		_tracklist = tracklist;
		update();
	}
	void update()
	{
		if ( _tracklist )
		{
			QString name = _tracklist->name();
			lb_name->setText( name );
			QPixmap pix = QPixmap();
			if ( pix.convertFromImage( QImage() ) )
				lb_image->setIcon( pix );
			else
			{
				if ( _icons )
				{
					lb_image->setIcon( _icons->get("playlist") );
				}
			}
			//
			resize( width(), sizeHint().height() );
			repaint();
		}
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTBANNER_H
