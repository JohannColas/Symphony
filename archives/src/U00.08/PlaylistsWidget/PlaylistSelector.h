#ifndef PLAYLISTSELECTOR_H
#define PLAYLISTSELECTOR_H
/**********************************************/
#include <QInputDialog>
#include <QListWidget>
#include <QMenu>
#include "BasicWidgets/ListWidget.h"
#include "PlaylistsWidget/PlaylistItem.h"
#include "Commons/Icons.h"
#include "Commons/Playlists.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistSelector
		: public ListWidget
{
	Q_OBJECT
private:
	Icons* _icons = 0;
	Playlists* _playlists = 0;

public:
	PlaylistSelector( QWidget* parent = nullptr )
		: ListWidget( parent )
	{
	}
	TrackList* currentTrackList()
	{
		if ( _playlists && index() != -1 )
			return _playlists->tracklist( index() );
		return 0;
	}

public slots:
	void setPlaylists( Playlists* playlists )
	{
		_playlists = playlists;
		connect( _playlists, &Playlists::changed,
				 this, &PlaylistSelector::update );
		update();
	}
	void updateIcons( Icons* icons )
	{
		_icons = icons;
		update();
	}
	void update() override
	{
		if ( _playlists )
		{
			clear();
			int x = contentsMargins().left();
			int w = width() - contentsMargins().left() - contentsMargins().right();
			int totheight = contentsMargins().top();
			QIcon icon = _icons ? _icons->get("playlist") : QIcon();
			int nbItems = 0;
			for ( int it = _begin; it < _playlists->count(); ++it )
			{
				PlaylistItem* item = new PlaylistItem( _playlists->tracklist(it)->name(), icon, this );
				item->setGeometry( x, totheight,
										w,
										item->sizeHint().height() );
				// Adding the Item to PlaylistSelector
				addItem( item );
				totheight += item->sizeHint().height();
				++_nbItems;
				if ( totheight > height() )
				{
					break;
				}
				++nbItems;
			}
			updateScrollbar( _playlists->count() );
		}
	}
	void showContextMenu( const QPoint& pos ) override
	{
		QMenu* menu = new QMenu( this );
		QAction* ac_rename = menu->addAction( "Rename" );
		QAction* ac_delete = menu->addAction( "Delete" );
		QAction* action = menu->exec( this->mapToGlobal(pos) );
		if ( action == ac_rename )
		{
			QString text =  QInputDialog::getText( this, "New name", "New name:", QLineEdit::Normal, _playlists->tracklist(index())->name() );

			if ( text != "" )
			{
				_playlists->tracklist(index())->setName( text );
				emit renamed( text );
			}
		}
		else if ( action == ac_delete )
		{
			_playlists->removeThis( currentTrackList() );
			emit deleted();
		}
	}

signals:
	void currentTracklistChanged( TrackList* tracklist );
	void renamed( const QString& name );
	void deleted();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTSELECTOR_H
