#ifndef SECTIONSSELECTOR_H
#define SECTIONSSELECTOR_H
/**********************************************/
#include <QWidget>
#include <QToolButton>
#include <QMouseEvent>
#include <QIcon>
#include <QMenu>
#include "BasicWidgets/SectionButton.h"
#include "BasicWidgets/Layouts.h"
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SectionsSelector
        : public QWidget
{
    Q_OBJECT
protected:
//    GridLayout* lay_main = new GridLayout;
    BoxLayout* lay_main = new BoxLayout(this);
    QList<SectionButton*> _buttons;
    QList<QWidget*> _widgets;
    QWidget* _currentWidget = 0;
    SectionButton* _moreButton = new SectionButton("", QIcon(), this);
    Settings* _sets = 0;
    bool _textHide = false;
    //
    QMenu* pop_options = new QMenu;
    QAction*  ac_textVisibility = new QAction("Text visible");
    QAction*  ac_nowPlaying = new QAction("Now Playing");
    QAction*  ac_artists = new QAction("Artists");
    QAction*  ac_albums = new QAction("Albums");
    QAction*  ac_tracks = new QAction("Tracks");
    QAction*  ac_genres = new QAction("Genres");
    QAction*  ac_playlists = new QAction("Playlists");
    QAction*  ac_libraries = new QAction("Libraries");
    QAction*  ac_radio = new QAction("Radio");
    QAction*  ac_podcats = new QAction("Podcasts");

public:
    SectionsSelector( QWidget* parent = nullptr )
        : QWidget( parent )
    {
//        SectionButton* _sect1 = new SectionButton( "ttddtd",  QIcon(), this );
        for ( int it = 0; it < 9; ++it )
        {
            SectionButton* newBut = new SectionButton("", QIcon(), this);
            _buttons.append( newBut );
        }
        connect( _buttons.at(0), &SectionButton::released,
                 this, &SectionsSelector::showNowPlaying );
        connect( _buttons.at(1), &SectionButton::released,
                 this, &SectionsSelector::showArtists );
        connect( _buttons.at(2), &SectionButton::released,
                 this, &SectionsSelector::showAlbums );
        connect( _buttons.at(3), &SectionButton::released,
                 this, &SectionsSelector::showGenres );
        connect( _buttons.at(4), &SectionButton::released,
                 this, &SectionsSelector::showTracks );
        connect( _buttons.at(5), &SectionButton::released,
                 this, &SectionsSelector::showPlaylists );
        connect( _buttons.at(6), &SectionButton::released,
                 this, &SectionsSelector::showLibraries );
        connect( _buttons.at(7), &SectionButton::released,
                 this, &SectionsSelector::showRadios );
        connect( _buttons.at(8), &SectionButton::released,
                 this, &SectionsSelector::showPodcasts );

        ac_textVisibility->setCheckable(true);
        connect( ac_textVisibility, &QAction::toggled,
                 this, &SectionsSelector::setTextVisibility );
        ac_nowPlaying->setCheckable(true);
        ac_nowPlaying->setChecked(true);
        ac_artists->setCheckable(true);
        ac_artists->setChecked(true);
        ac_albums->setCheckable(true);
        ac_albums->setChecked(true);
        ac_genres->setCheckable(true);
        ac_genres->setChecked(true);
        ac_tracks->setCheckable(true);
        ac_tracks->setChecked(true);
        ac_playlists->setCheckable(true);
        ac_playlists->setChecked(true);
        ac_libraries->setCheckable(true);
        ac_libraries->setChecked(true);
        ac_radio->setCheckable(true);
        ac_radio->setChecked(true);
        ac_podcats->setCheckable(true);
        ac_podcats->setChecked(true);
        pop_options->addSection( "Options" );
        pop_options->addAction( ac_textVisibility );
        pop_options->addSection( "Sections" );
        pop_options->addAction( ac_nowPlaying );
        pop_options->addAction( ac_artists );
        pop_options->addAction( ac_albums );
        pop_options->addAction( ac_genres );
        pop_options->addAction( ac_tracks );
        pop_options->addAction( ac_playlists );
        pop_options->addAction( ac_libraries );
        pop_options->addAction( ac_radio );
        pop_options->addAction( ac_podcats );
        pop_options->setWindowFlags( Qt::Popup );
        _moreButton->setMenu( pop_options );

        connect( ac_nowPlaying, &QAction::toggled,
                this, &SectionsSelector::setNowPlayingVisible );
        connect( ac_artists, &QAction::toggled,
                this, &SectionsSelector::setArtistsVisible );
        connect( ac_albums, &QAction::toggled,
                this, &SectionsSelector::setAlbumsVisible );
        connect( ac_tracks, &QAction::toggled,
                this, &SectionsSelector::setTracksVisible );
        connect( ac_genres, &QAction::toggled,
                this, &SectionsSelector::setGenresVisible );
        connect( ac_playlists, &QAction::toggled,
                this, &SectionsSelector::setPlaylistsVisible );
        connect( ac_libraries, &QAction::toggled,
                this, &SectionsSelector::setLibrariesVisible );
        connect( ac_radio, &QAction::toggled,
                this, &SectionsSelector::setRadiosVisible );
        connect( ac_podcats, &QAction::toggled,
                this, &SectionsSelector::setPodcastsVisible );
        lay_main->setSizeConstraint( QLayout::SetMinimumSize );
        setLayout( lay_main );
        setMouseTracking( true );
        setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
        _moreButton->setIconSize(20);
        updateLang();
        updateLayout();
//        topToBottom();
//        leftToRight();
    }
    void setWidgets( QList<QWidget*> list )
    {
        _widgets = list;
    }
    void setSettings( Settings* sets = 0 )
    {
        if ( sets )
        {
            _sets = sets;
            setTextVisibility( !_sets->getBool("SectionsSelector/hideText") );
            setArtistsVisible( !_sets->getBool("SectionsSelector/hideArtists") );
            setAlbumsVisible( !_sets->getBool("SectionsSelector/hideAlbums") );
            setGenresVisible( !_sets->getBool("SectionsSelector/hideGenres") );
            setTracksVisible( !_sets->getBool("SectionsSelector/hideTracks") );
            setPlaylistsVisible( !_sets->getBool("SectionsSelector/hidePlatlists") );
            setLibrariesVisible( !_sets->getBool("SectionsSelector/hideLibraries") );
            setRadiosVisible( !_sets->getBool("SectionsSelector/hideRadios") );
            setPodcastsVisible( !_sets->getBool("SectionsSelector/hidePodcasts") );
            updateIcons();
//            updateLayout();
        }
    }
    QWidget* widget( int index )
    {
        if ( index >= 0 && index < _widgets.size() )
            return _widgets.at(index);
        return 0;
    }
    QWidget* currentWidget()
    {
        return _currentWidget;
    }
    void showWidget( int index )
    {
        _currentWidget = widget(index);
        if ( _currentWidget )
            _currentWidget->show();
    }

public slots:
    void setHorizontal()
    {
        lay_main->setDirection( QBoxLayout::LeftToRight );
        setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
        for ( int it = 0; it < 9; ++it )
        {
            if ( !_textHide )
                _buttons.at(it)->setStyle( SectionButton::TextUnderIcon );
            _buttons.at(it)->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Expanding );
        }
        if ( !_textHide )
            _moreButton->setStyle( SectionButton::TextUnderIcon );
        _moreButton->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Expanding );
        //        updateLayout();
    }
    void setVertical()
    {
        lay_main->setDirection( QBoxLayout::TopToBottom );
        setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Expanding );
        for ( int it = 0; it < 9; ++it )
        {
            if ( !_textHide )
                _buttons.at(it)->setStyle( SectionButton::TextBesideIcon );
            _buttons.at(it)->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
        }
        if ( !_textHide )
            _moreButton->setStyle( SectionButton::TextBesideIcon );
        _moreButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
//        updateLayout();
    }
    void updateLang()
    {
        _buttons.at(0)->setText( "Now Playing" );
        _buttons.at(1)->setText( "Artists" );
        _buttons.at(2)->setText( "Albums" );
        _buttons.at(3)->setText( "Genres" );
        _buttons.at(4)->setText( "Tracks" );
        _buttons.at(5)->setText( "Playlists" );
        _buttons.at(6)->setText( "Libraries" );
        _buttons.at(7)->setText( "Radios" );
        _buttons.at(8)->setText( "Podcasts" );
        _moreButton->setText( "More" );
    }
    void updateIcons()
    {
        if ( _sets )
        {
            _buttons.at(0)->setIcon( _sets->icon("now_playing") );
            _buttons.at(1)->setIcon( _sets->icon("artist") );
            _buttons.at(2)->setIcon( _sets->icon("album") );
            _buttons.at(3)->setIcon( _sets->icon("genre") );
            _buttons.at(4)->setIcon( _sets->icon("track") );
            _buttons.at(5)->setIcon( _sets->icon("playlist") );
            _buttons.at(6)->setIcon( _sets->icon("libraries") );
            _buttons.at(7)->setIcon( _sets->icon("radio") );
            _buttons.at(8)->setIcon( _sets->icon("podcast") );
            _moreButton->setIcon( _sets->icon("more_horiz") );
        }
    }
    void updateLayout()
    {
        for ( int it = 0; it < 9; ++it )
        {
            lay_main->removeWidget( _buttons.at(it) );
        }
        lay_main->addWidget( _buttons.at(0) );
        lay_main->addWidget( _buttons.at(1) );
        lay_main->addWidget( _buttons.at(2) );
        lay_main->addWidget( _buttons.at(3) );
        lay_main->addWidget( _buttons.at(4) );
        lay_main->addWidget( _buttons.at(5) );
        lay_main->addWidget( _buttons.at(6) );
        lay_main->addWidget( _buttons.at(7) );
        lay_main->addWidget( _buttons.at(8) );
        lay_main->addSpacer();
        lay_main->addWidget( _moreButton );
    }
    void hideAllWidgets()
    {
//        for ( QWidget* widget : _widgets )
        if ( _currentWidget )
            _currentWidget->hide();
        if ( !_widgets.isEmpty() )
            _widgets.last()->hide();
    }
    void showNowPlaying()
    {
        hideAllWidgets();
        showWidget(0);
    }
    void showArtists()
    {
        hideAllWidgets();
        showWidget(1);
    }
    void showAlbums()
    {
        hideAllWidgets();
        showWidget(2);
    }
    void showGenres()
    {
        hideAllWidgets();
        showWidget(3);
    }
    void showTracks()
    {
        hideAllWidgets();
        showWidget(4);
    }
    void showPlaylists()
    {
        hideAllWidgets();
        showWidget(5);
    }
    void showLibraries()
    {
        hideAllWidgets();
        showWidget(6);
    }
    void showRadios()
    {
        hideAllWidgets();
        showWidget(7);
    }
    void showPodcasts()
    {
        hideAllWidgets();
        showWidget(8);
    }
    void setTextVisibility( bool check )
    {
        _textHide = !check;
        SectionButton::ButtonStyle style = SectionButton::IconOnly;
        if ( !_textHide )
        {
            if ( lay_main->direction() == QBoxLayout::LeftToRight )
                style = SectionButton::TextUnderIcon;
            if ( lay_main->direction() == QBoxLayout::TopToBottom )
                style = SectionButton::TextBesideIcon;
        }
        for ( SectionButton* but : _buttons )
        {
            but->setStyle( style );
        }
        _moreButton->setStyle( style );
        if ( check != ac_textVisibility->isChecked() )
            ac_textVisibility->setChecked( check );
        if ( _sets )
            _sets->save( "Appearance/hideSidebarText", !check );
    }
    /**********************************************/
    /**********************************************/
    /* */
    void setNowPlayingVisible( bool /*check*/ )
    {
    //	wid_list->item( 0 )->setHidden( !check );
    }
    /**********************************************/
    /**********************************************/
    /* */
    void setArtistsVisible( bool check )
    {
        _buttons.at(1)->setHidden( !check );
        if ( check != ac_artists->isChecked() )
            ac_artists->setChecked( check );
        _sets->save( "SectionsSelector/hideArtists", !check );
    }
    /**********************************************/
    /**********************************************/
    /* */
    void setAlbumsVisible( bool check )
    {
        _buttons.at(2)->setHidden( !check );
        if ( check != ac_albums->isChecked() )
            ac_albums->setChecked( check );
        _sets->save( "SectionsSelector/hideAlbums", !check );
    }
    /**********************************************/
    /**********************************************/
    /* */
    void setGenresVisible( bool check )
    {
        _buttons.at(3)->setHidden( !check );
        if ( check != ac_genres->isChecked() )
            ac_genres->setChecked( check );
        _sets->save( "SectionsSelector/hideGenres", !check );
    }
    /**********************************************/
    /**********************************************/
    /* */
    void setTracksVisible( bool check )
    {
        _buttons.at(4)->setHidden( !check );
        if ( check != ac_tracks->isChecked() )
            ac_tracks->setChecked( check );
        _sets->save( "SectionsSelector/hideTracks", !check );
    }
    /**********************************************/
    /**********************************************/
    /* */
    void setPlaylistsVisible( bool check )
    {
        _buttons.at(5)->setHidden( !check );
        if ( check != ac_playlists->isChecked() )
            ac_playlists->setChecked( check );
        _sets->save( "SectionsSelector/hidePlaylists", !check );
    }
    /**********************************************/
    /**********************************************/
    /* */
    void setLibrariesVisible( bool check )
    {
        _buttons.at(6)->setHidden( !check );
        if ( check != ac_libraries->isChecked() )
            ac_libraries->setChecked( check );
        _sets->save( "SectionsSelector/hideLibraries", !check );
    }
    /**********************************************/
    /**********************************************/
    /* */
    void setRadiosVisible( bool check )
    {
        _buttons.at(7)->setHidden( !check );
        if ( check != ac_radio->isChecked() )
            ac_radio->setChecked( check );
        _sets->save( "SectionsSelector/hideRadios", !check );
    }
    void setPodcastsVisible( bool check )
    {
        _buttons.at(8)->setHidden( !check );
        if ( check != ac_podcats->isChecked() )
            ac_podcats->setChecked( check );
        _sets->save( "SectionsSelector/hidePodcasts", !check );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SECTIONSSELECTOR_H
/**********************************************/
