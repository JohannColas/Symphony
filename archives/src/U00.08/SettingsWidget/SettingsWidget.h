#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
/**********************************************/
#include "BasicWidgets/TabWidget.h"

#include <QListWidget>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SettingsWidget
		: public QWidget
{
	Q_OBJECT
private:
	QListWidget* wid_list = new QListWidget;
	QGridLayout* lay_main = new QGridLayout;
    QList<QWidget*> _widgets;
	int _selectedTab = -1;

public:
	SettingsWidget( QWidget* parent = 0 )
		: QWidget( parent )
	{
		hide();

		addItem( "itm_themeIcons" );
		addItem( "itm_APIs" );
		changeCurrentWidget( 0 );

		wid_list->setFrameShape( QFrame::NoFrame );
		wid_list->setMouseTracking( true );
		wid_list->setMaximumWidth( 200 );
		wid_list->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Preferred );
		wid_list->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
		wid_list->setTextElideMode( Qt::ElideRight );

		QVBoxLayout* lay_sidebar = new QVBoxLayout;
		lay_sidebar->setSpacing(0);
		lay_sidebar->setMargin(0);
		lay_sidebar->addWidget( wid_list );
//		lay_sidebar->addWidget( pb_options );
//		_sidebar->setLayout( lay_sidebar );
		lay_main->setSpacing(0);
		lay_main->setMargin(0);
//		lay_main->addWidget( _sidebar, 0, 0 );

		setLayout( lay_main );

		setMinimumWidth( 400 );
	}
	void addItem( const QString& name, QWidget* widget = new QWidget )
	{
		widget->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
		wid_list->blockSignals( true );
		QListWidgetItem* item = new QListWidgetItem;
//		SidebarItem* sitem = new SidebarItem;
//		sitem->setText( name );
//		wid_list->addItem( item );
//		wid_list->setItemWidget( item, sitem );
		wid_list->blockSignals( false );
		lay_main->addWidget( widget, 0, 1/*, 2, 1*/ );
		_widgets.append( widget );
		widget->hide();
	}
	int count()
	{
		return wid_list->count();
	}
	void changeCurrentWidget(int index)
	{
		if ( index > -1 && index < count() )
		{
			if ( _selectedTab > -1 && _selectedTab < count() ) {
				_widgets.at( _selectedTab )->hide();
			}
			if ( index > -1 && index < count() ) {
				_selectedTab = index;
				_widgets.at( index )->show();
			}
		}
	}

public slots:
	void paintEvent( QPaintEvent* event ) override {
		QWidget::paintEvent( event );
	}
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */

/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGSWIDGET_H
