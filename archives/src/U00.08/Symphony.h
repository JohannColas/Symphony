#ifndef WIDGET_H
#define WIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "Commons/Settings.h"
#include "Commons/Theme.h"
#include "Commons/Icons.h"
#include "Commons/Lang.h"
#include "Commons/TrackList.h"
#include "Commons/Playlists.h"
#include "Commons/Libraries.h"
#include <QListWidget>
/**********************************************/
#include "PlayerWidget/PlayerWidget.h"
#include "BasicWidgets/SystemControls.h"
#include "SectionsSelector/SectionsSelector.h"
#include "NowPlayingWidget/NowPlayingWidget.h"
#include "ArtistsWidget/ArtistsWidget.h"
#include "AlbumsWidget/AlbumsWidget.h"
#include "GenresWidget/GenresWidget.h"
#include "TracksWidget/TracksWidget.h"
#include "PlaylistsWidget/PlaylistsWidget.h"
#include "LibrariesWidget/LibrariesWidget.h"
#include "RadiosWidget/RadiosWidget.h"
#include "PodcastsWidget/PodcastsWidget.h"
#include "SettingsWidget/SettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Symphony
        : public Frame
{
    Q_OBJECT
protected:
    // Window Parameters
    int invisibleBorderSize = 2;
    bool isPressedWidget;
    QPoint _lastPos = {-1,0};
    int _oldWidget = 0;
    QWidget* _oldOne = 0;
    // Initialise the TrackList
    TrackList* _nowPlaylist = new TrackList( "#NowPlaylist", TrackList::NOWPLAYLIST );
    Playlists* _playlists = new Playlists;
    Libraries* _libraries = new Libraries;
//	Radios* _radios = new Radios;
//	Podcasts* _radios = new Podcasts;
    // Initialise the Widgets
//    GridLayout* lay_sections = new GridLayout;
    BoxLayout* lay_sections = new BoxLayout;
    PlayerWidget* wid_player = new PlayerWidget(this);
    SectionsSelector* _sectionsSelector = new SectionsSelector(this);
    NowPlayingWidget* wid_nowPlaying = new NowPlayingWidget(this);
    ArtistsWidget* wid_artists = new ArtistsWidget(this);
    AlbumsWidget* wid_albums = new AlbumsWidget(this);
    GenresWidget* wid_genres = new GenresWidget(this);
    TracksWidget* wid_tracks = new TracksWidget(this);
    PlaylistsWidget* wid_playlists = new PlaylistsWidget(this);
    LibrariesWidget* wid_libraries = new LibrariesWidget(this);
    RadiosWidget* wid_radios = new RadiosWidget(this);
    PodcastsWidget* wid_podcasts = new PodcastsWidget(this);
    SettingsWidget* wid_settings = new SettingsWidget(this);
    //
    SystemControls* _systemControls = new SystemControls(this);
    //
    QSpacerItem* _spacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Maximum );
    // Application Settings
    Settings* _settings = new Settings;
    Theme* _theme = new Theme;
    Icons* _icons = new Icons;
    Lang* _lang = new Lang;

public:
    ~Symphony()
    {
        delete _theme;
        delete _icons;
        delete _lang;
    }
    Symphony( QWidget *parent = nullptr )
        : Frame( parent )
    {
        // Window Geometry
        setMinimumSize( 800, 500 );
        wid_player->setMinimumHeight( 50 );
        //    addWidget( wid_player, 0, 0, 1, 2 );
        //    addWidget( wid_sidebar, 1, 0, 1, 2 );
        // Adding TrackLists to Widgets
        wid_player->setTrackList( _nowPlaylist );
        wid_nowPlaying->setTrackList( _nowPlaylist );
        wid_artists->setLibraries( _libraries );
        wid_albums->setLibraries( _libraries );
        wid_tracks->setLibraries( _libraries );
        wid_genres->setLibraries( _libraries );
        wid_libraries->setLibraries( _libraries );

        wid_playlists->setPlaylists( _playlists );
        wid_nowPlaying->setPlaylists( _playlists );
        wid_artists->setPlaylists( _playlists );
        wid_albums->setPlaylists( _playlists );
        //	wid_genres->setPlaylists( _playlists );
        wid_tracks->setPlaylists( _playlists );
        wid_libraries->setPlaylists( _playlists );
        //	wid_radios->setTracklistList( _radios );
        //	wid_podcasts->setTracklistList( _podcasts );

        //    _sectionsSelector->topToBottom();
        _sectionsSelector->setWidgets( {wid_nowPlaying,
                                        wid_artists,
                                        wid_albums,
                                        wid_genres,
                                        wid_tracks,
                                        wid_playlists,
                                        wid_libraries,
                                        wid_radios,
                                        wid_podcasts,
                                        wid_settings
                                       });
        _sectionsSelector->showNowPlaying();
        //    lay_sections->addWidget( _sectionsSelector, 0, 0 );
        //    lay_sections->addWidget( wid_nowPlaying, 0, 1 );
        //    lay_sections->addWidget( wid_artists, 0, 1 );
        //    lay_sections->addWidget( wid_albums, 0, 1 );
        //    lay_sections->addWidget( wid_genres, 0, 1 );
        //    lay_sections->addWidget( wid_tracks, 0, 1 );
        //    lay_sections->addWidget( wid_playlists, 0, 1 );
        //    lay_sections->addWidget( wid_libraries, 0, 1 );
        //    lay_sections->addWidget( wid_radios, 0, 1 );
        //    lay_sections->addWidget( wid_podcasts, 0, 1 );
        //    lay_sections->addWidget( wid_settings, 0, 1 );
        lay_sections->addWidget( _sectionsSelector );
        lay_sections->addWidget( wid_nowPlaying );
        lay_sections->addWidget( wid_artists );
        lay_sections->addWidget( wid_albums );
        lay_sections->addWidget( wid_genres );
        lay_sections->addWidget( wid_tracks );
        lay_sections->addWidget( wid_playlists );
        lay_sections->addWidget( wid_libraries );
        lay_sections->addWidget( wid_radios );
        lay_sections->addWidget( wid_podcasts );
        lay_sections->addWidget( wid_settings );


        // Adding Widgets to SidebarWidget
        //	wid_sidebar->setWidget( 0, wid_nowPlaying );
        //	wid_sidebar->setWidget( 1, wid_artists );
        //	wid_sidebar->setWidget( 2, wid_albums );
        //	wid_sidebar->setWidget( 3, wid_genres );
        //	wid_sidebar->setWidget( 4, wid_tracks );
        //	wid_sidebar->setWidget( 5, wid_playlists );
        //	wid_sidebar->setWidget( 6, wid_libraries );
        //	wid_sidebar->setWidget( 7, wid_radios );
        //	wid_sidebar->setWidget( 8, wid_podcasts );
        //	wid_sidebar->setWidget( 9, wid_settings );
        ////	wid_sidebar->setWidget( 10, wid_lyrics );
        //	wid_sidebar->changeCurrentWidget( 0 );

        // Connections between widgets
        connect( wid_nowPlaying, &NowPlayingWidget::indexChanged,
                 wid_player, &PlayerWidget::setPlaylistIndex );
        connect( wid_player, &PlayerWidget::playlistChanged,
                 wid_nowPlaying, &NowPlayingWidget::setPlaylist );
        connect( _systemControls, &SystemControls::onMinimize ,
                 this, &Symphony::showMinimized );
        connect( _systemControls, &SystemControls::onMaximize,
                 this, &Symphony::maximise );
        connect( wid_player, &PlayerWidget::moveWindow,
                 this, &Symphony::move );
        connect( _systemControls, &SystemControls::onSettings,
                 this, &Symphony::showSettings );
        wid_player->connectTo( wid_artists );
        wid_player->connectTo( wid_albums );
        wid_player->connectTo( wid_tracks );
        wid_player->connectTo( wid_playlists );
        wid_player->connectTo( wid_libraries );


        // -----------------------
        // -----------------------
        // Themes
        _theme->apply();
        updateIcons();
        updateLang();
        // -----------------------
        // -----------------------
        setMouseTracking( true );

        connect( _settings, &Settings::valueChanged,
                 this, &Symphony::settingsChanged );
        // Update Sidebar
        wid_player->setSettings( _settings );
        _systemControls->setSettings( _settings );
        _sectionsSelector->setSettings( _settings );
        updateLayout();
        //    this->show();
        if ( _settings->getBool("Geometry/fullscreen") )
            setWindowState(windowState() | Qt::WindowFullScreen);
        else if ( _settings->getBool("Geometry/maximized") )
            setWindowState(windowState() | Qt::WindowMaximized);
    }


public slots:
    void showSettings()
    {
        //    _sectionsSelector->hideAllWidgets();
        if ( wid_settings->isHidden() )
        {
            wid_settings->show();
            _sectionsSelector->currentWidget()->hide();
        }
        else
        {
            wid_settings->hide();
            _sectionsSelector->currentWidget()->show();
        }
        //	if ( wid_sidebar->selectedTab() != 9 )
        //	{
        //		_oldWidget = wid_sidebar->selectedTab();
        //		wid_sidebar->changeCurrentWidget( 9 );
        //	}
        //	else
        //	{
        //		wid_sidebar->changeCurrentWidget( _oldWidget );
        //	}
    }
    void updateSettings()
    {
        //	_settings->update();
        //	wid_player->updateIcons( _settings );
        //	wid_sidebar->updateIcons( _settings );
        //	wid_nowPlaying->updateIcons( _settings );
        //	wid_libraries->updateIcons( _settings );
    }
    void updateIcons()
    {
        _icons->update();
        setWindowIcon( _icons->get( "Symphony" ) );
        //	wid_player->updateIcons( _icons );
        wid_nowPlaying->updateIcons( _icons );
        wid_artists->updateIcons( _icons );
        wid_albums->updateIcons( _icons );
        wid_tracks->updateIcons( _icons );
        wid_playlists->updateIcons( _icons );
        wid_libraries->updateIcons( _icons );
    }
    void updateLang()
    {
        _lang->update();

    }
    void move( const QPoint& pos )
    {
        if ( _lastPos.x() != -1 && pos.x() != -1 && !isMaximized() )
        {
            Frame::move( this->pos() - _lastPos + pos );
        }
        _lastPos = pos;
    }
    void closeEvent( QCloseEvent* event )
    {
        QFrame::closeEvent( event );
        //	qDebug().noquote() << "";
        //	qDebug().noquote() << "--------------";
        //    qDebug().noquote() << "Saving...";
        //	QElapsedTimer timer;
        //    timer.start();
        _nowPlaylist->save();
        _playlists->save();
        for ( TrackList* playlist : _playlists->list() )
            playlist->save();
        _libraries->saveLibraries();
        //	QString time = QString::number( timer.elapsed() );
        //	time.insert( time.length()-3, "s " );
        //	time += "ms";
        //	qDebug().noquote() << "Time to save Libraries & Playlists : " + time;
        //	qDebug().noquote() << "--------------";
    }
    void maximise()
    {
        isMaximized() ? showNormal() : showMaximized();
        _settings->save("Geometry/maximized", isMaximized() );
    }
    void updateLayout()
    {
        QString playerPos = _settings->getString("Player/position");
        lay_main->removeWidget( wid_player );
        lay_main->removeWidget( _systemControls );
        lay_main->removeItem( lay_sections );
        lay_main->removeItem( _spacer );
        if ( playerPos == 't' )
        {
            addWidget(wid_player, 0, 0, 1, 1);
//            addWidget(wid_sidebar, 1, 0, 1, 2);
            addLayout( lay_sections, 1, 0, 1, 2 );
            addWidget(_systemControls, 0, 1, 1, 1);
            lay_main->setAlignment(wid_player, Qt::AlignVCenter);
        }
        else if ( playerPos == 'b' )
        {
            addWidget(wid_player, 2, 0, 1, 2);
//            addWidget(wid_sidebar, 1, 0, 1, 2);
            addLayout( lay_sections, 1, 0, 1, 2 );
            addWidget(_systemControls, 0, 1, 1, 1);
            lay_main->addItem(_spacer, 0, 0);
            lay_main->setAlignment(wid_player, Qt::AlignVCenter);
        }
        else if ( playerPos == 'l' )
        {
            addWidget(wid_player, 0, 0, 2, 1);
//            addWidget(wid_sidebar, 1, 1, 1, 2);
            addLayout( lay_sections, 1, 1, 1, 2 );
            addWidget(_systemControls, 0, 2, 1, 1);
            lay_main->addItem(_spacer, 0, 1);
            lay_main->setAlignment(wid_player, Qt::AlignTop);
        }
        else
        {
            addWidget(wid_player, 1, 1, 1, 2);
//            addWidget(wid_sidebar, 0, 0, 2, 1);
            addLayout( lay_sections, 0, 0, 2, 1 );
            addWidget(_systemControls, 0, 2, 1, 1);
            lay_main->addItem(_spacer, 0, 1);
//            lay_main->setAlignment(wid_player, Qt::AlignTop);
        }
//        lay_main->addWidget( _sectionsSelector, 4, 0, 6, 6 );
        lay_main->setAlignment(_systemControls, Qt::AlignVCenter);
        QString sectionsPos = _settings->getString("SectionsSelector/position");
        if ( sectionsPos == 't' )
        {
            lay_sections->setDirection( QBoxLayout::TopToBottom );
            _sectionsSelector->setHorizontal();
        }
        else if ( sectionsPos == 'b' )
        {
            lay_sections->setDirection( QBoxLayout::BottomToTop );
            _sectionsSelector->setHorizontal();
        }
        else if ( sectionsPos == 'l' )
        {
            lay_sections->setDirection( QBoxLayout::LeftToRight );
            _sectionsSelector->setVertical();
        }
        else
        {
            lay_sections->setDirection( QBoxLayout::RightToLeft );
            _sectionsSelector->setVertical();
        }
    }
    void settingsChanged( QString key )
    {
        if ( key == "Player/position" ||
             key == "Player/size" )
        {
            updateLayout();
        }
        else if ( key == "Appearance/theme" )
        {

        }
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WIDGET_H
