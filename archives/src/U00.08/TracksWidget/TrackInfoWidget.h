#ifndef TRACKINFOWIDGET_H
#define TRACKINFOWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
/**********************************************/
#include "Commons/Track.h"
#include "Commons/Metadata.h"
#include "Commons/TrackInfo.h"
/**********************************************/
#include "BasicWidgets/TextWidget.h"
#include "BasicWidgets/AlbumLabel.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackInfoWidget
		: public Frame
{
	Q_OBJECT
protected:
	TextWidget* wid_description = new TextWidget;
//	FormLayout* lay_form = new FormLayout;
	Label* lb_album = new Label("Album :");
	AlbumLabel* _album = new AlbumLabel;

public:
	TrackInfoWidget( QWidget* parent = 0 )
		: Frame( parent )
	{
//		lay_form->addRow( lb_album, _album );
//		addLayout( lay_form, 0, 0 );
		addWidget( lb_album, 0, 0 );
		lb_album->fitWidthToContent();
		addWidget( _album, 0, 1 );
		addWidget( wid_description, 1, 0, 1, 2 );
//		wid_description->setTextInteractionFlags( wid_description->textInteractionFlags() | Qt::LinksAccessibleByMouse );
	}

public slots:
	void showDownload( const QByteArray& data )
	{
		wid_description->setHtml( QString( data ) );
	}
	void setTrack( Track* track = 0 )
	{
		if ( track )
		{
			TrackInfo trackInfo( track );
			wid_description->setText( trackInfo.description() );
			_album->setText( trackInfo.album() );
		}
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKINFOWIDGET_H
