#ifndef TRACKSWIDGET_H
#define TRACKSWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "Commons/Playlists.h"
#include "TracksWidget/TrackSelector.h"
#include "TracksWidget/TrackWidget.h"
#include "Commons/Lang.h"
#include "Commons/Libraries.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TracksWidget
		: public Frame
{
	Q_OBJECT
private:
	Libraries* _libraries = 0;
	TrackSelector* sel_track = new TrackSelector;
	TrackWidget* wid_track = new TrackWidget;

public:
	TracksWidget( QWidget* parent = 0 )
		: Frame( parent )
	{
		addWidget( sel_track, 0, 0 );
		addWidget( wid_track, 0, 1 );

		connect( sel_track, &TrackSelector::trackChanged,
				 wid_track, &TrackWidget::setTrack );
		connectTo( sel_track );
	}
	void setPlaylists( Playlists* playlists )
	{
		sel_track->setPlaylists( playlists );
	}

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
		sel_track->setLibraries( _libraries );
		wid_track->setLibraries( _libraries );
	}
	void updateIcons( Icons* icons )
	{
		sel_track->updateIcons( icons );
		wid_track->updateIcons( icons );
	}
	void updateLang( Lang* /*lang*/ )
	{
	}
	//
	void sendTrackToPlay( Track* track )
	{
		emit trackToPlay( track );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKSWIDGET_H
