#ifndef ALBUMSWIDGET_H
#define ALBUMSWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "Commons/Playlists.h"
#include "AlbumsWidget/AlbumSelector.h"
#include "AlbumsWidget/AlbumWidget.h"
#include "Commons/Libraries.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumsWidget
		: public Frame
{
	Q_OBJECT
private:
	Libraries* _libraries = 0;
	AlbumSelector* sel_album = new AlbumSelector;
	AlbumWidget* wid_album = new AlbumWidget;

public:
	AlbumsWidget( QWidget* parent = 0 )
		: Frame( parent )
	{
		hide();

		addWidget( sel_album, 0, 0 );
		addWidget( wid_album, 0, 1, 1, 1 );

		connectTo( wid_album );
	}
	void setPlaylists( Playlists* playlists )
	{
		wid_album->setPlaylists( playlists );
	}

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		_libraries = libraries;
		sel_album->setLibraries( _libraries );
		wid_album->setLibraries( _libraries );
		connect( sel_album, &AlbumSelector::indexToChange,
				 wid_album, &AlbumWidget::setArtistByIndex );
	}
    void updateIcons()
    {
	}
    void updateLang()
	{
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMSWIDGET_H
