#ifndef ALBUMLABEL_H
#define ALBUMLABEL_H
/**********************************************/
#include "BasicWidgets/Label.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AlbumLabel
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	AlbumLabel( QWidget* parent = 0 )
		: Label( parent )
	{
	}
	AlbumLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUMLABEL_H
