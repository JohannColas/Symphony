#ifndef COUNTLABEL_H
#define COUNTLABEL_H
/**********************************************/
#include "BasicWidgets/Label.h"
#include "BasicWidgets/ElideLabel.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CountLabel
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	CountLabel( QWidget* parent = 0 )
		: Label( parent )
	{
//		fitWidthToContent();
	}
	CountLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
//		fitWidthToContent();
	}
	CountLabel( int count, QWidget* parent = 0 )
		: Label( parent )
	{
		setText( QString::number( count ) );
//		fitWidthToContent();
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COUNTLABEL_H
