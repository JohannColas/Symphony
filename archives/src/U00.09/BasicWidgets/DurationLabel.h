#ifndef DURATIONLABEL_H
#define DURATIONLABEL_H
/**********************************************/
#include "BasicWidgets/Label.h"
/**********************************************/
#include <QTime>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class DurationLabel
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	DurationLabel( QWidget* parent = 0 )
		: Label( parent )
	{
	}
	DurationLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
	}
	DurationLabel( int duration, QWidget* parent = 0 )
		: Label( parent )
	{
		QString durT = QTime( 0, 0, 0 ).addSecs(duration).toString();
		if( durT.left(2) == "00" )
			durT = durT.mid( 3 );
		setText( durT );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DURATIONLABEL_H
