#ifndef FANARTIMAGE_H
#define FANARTIMAGE_H
/**********************************************/
#include "BasicWidgets/LockedButton.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class FanartImage
		: public LockedButton
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	FanartImage( QWidget* parent = 0 )
		: LockedButton( parent )
	{
		setFlat( true );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setPixmap( const QPixmap& pix )
	{
		setIcon( pix );
	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FANARTIMAGE_H
