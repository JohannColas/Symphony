#ifndef LINEEDIT_H
#define LINEEDIT_H
/**********************************************/
#include <QLineEdit>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LineEdit
		: public QLineEdit
{
	Q_OBJECT
public:
	LineEdit( const QString& text, QWidget* parent = 0 )
		: QLineEdit( text, parent )
	{
		init();
	}
	LineEdit( QWidget* parent = 0 )
		: QLineEdit( parent )
	{
		init();
	}
	void init()
	{
		connect( this, &LineEdit::editingFinished,
				 this, &LineEdit::onEditingFinished );
	}
public slots:
	void onEditingFinished()
	{
		emit editingEnded( this->text() );
	}
signals:
	void editingEnded( const QString& text );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LINEEDIT_H
