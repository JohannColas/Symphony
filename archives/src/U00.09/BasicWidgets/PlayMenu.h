#ifndef PLAYMENU_H
#define PLAYMENU_H
/**********************************************/
#include <QMenu>
#include <QWidgetAction>
#include <QDebug>
/**********************************************/
#include "Commons/Playlists.h"
/**********************************************/
#include "BasicWidgets/LineEdit.h"
/**********************************************/
/**********************************************/
/**********************************************/
/* PlayMenu
 *
 * */
class PlayMenu
		: public QMenu
{
	Q_OBJECT
private:
	QList<QAction*> acs_playlists;
	Playlists* _playlists = 0;
public:
//	enum Action
//	{
//		PLAY,
//		ADD,
//		ADDANDPLAY,
//		INSERT,
//		REMOVE,
//		REMOVEALL,
//	};
	enum Type
	{
		FORPLAYLIST,
		FORTRACKSELECTOR
	};

public:
	PlayMenu( QWidget* parent = 0 )
		: QMenu( parent )
	{
		// -----
		update();
		// -----
		connect( le_newPlaylist, &LineEdit::editingFinished,
				 this, &PlayMenu::on_addToNewPlaylist );
	}
	void setPlaylists( Playlists* playlists )
	{
		_playlists = playlists;
	}
	void setType( PlayMenu::Type type )
	{
		_type = type;
		update();
	}


public slots:
	void update()
	{
		for ( QAction* ac : this->actions() )
			this->removeAction(ac);

		addAction( ac_play );
		addAction( ac_add );
		addAction( ac_addAndPlay );
		addAction( ac_insert );
		sep1 = addSeparator();
		addMenu( ac_addToPlaylist );
		wid_ac_newPlaylist->setDefaultWidget( le_newPlaylist );
		ac_addToPlaylist->addAction( wid_ac_newPlaylist );
		if ( _type != PlayMenu::FORTRACKSELECTOR )
		{
			sep2 = addSeparator();
			addAction( ac_remove );
			addAction( ac_removeAll);
		}
		else if ( _type == PlayMenu::FORPLAYLIST )
		{

		}
	}
	void on_addToNewPlaylist()
	{
		emit addToNewPlaylist( le_newPlaylist->text() );
		le_newPlaylist->blockSignals(true);
		this->close();
	}
	void updateActions()
	{
		if ( _playlists )
		{
			for ( QAction* ac : acs_playlists )
			{
				ac_addToPlaylist->removeAction( ac );
				delete ac;
			}
			acs_playlists.clear();
			for ( TrackList* elm : _playlists->list() )
			{
				QAction* playlist = ac_addToPlaylist->addAction( elm->name() );
				acs_playlists.append( playlist );
			}
		}
	}
	void exec( const QPoint& pos )
	{
		le_newPlaylist->setText( "New Playlist" );
		le_newPlaylist->blockSignals(false);
		QAction* action = QMenu::exec( pos );
		if ( action == ac_play )
			emit play();
		else if ( action == ac_add )
			emit add();
		else if ( action == ac_addAndPlay )
			emit addAndPlay();
		else if ( action == ac_insert )
			emit insert();
		else if ( action == ac_remove )
			emit remove();
		else if ( action == ac_removeAll )
			emit removeAll();
		for ( int it = 0; it < acs_playlists.size(); ++it )
		{
			if ( action == acs_playlists.at(it) )
			{
				emit addToPlaylist( action->text() );
				return;
			}
		}
	}

private:
	Type _type = PlayMenu::FORTRACKSELECTOR;
	QAction* ac_play = new QAction( "Play Now (and remove Current Playlist)" );
	QAction* ac_add = new QAction( "Add to Current Playlist" );
	QAction* ac_addAndPlay = new QAction( "Add and Play to Current Playlist" );
	QAction* ac_insert = new QAction( "Insert after the Current Track" );
	QAction* sep1 = 0;
	QMenu* ac_addToPlaylist = new QMenu( "Add to a Playlist" );
	LineEdit* le_newPlaylist = new LineEdit( "New Playlist" );
	QWidgetAction* wid_ac_newPlaylist = new QWidgetAction( ac_addToPlaylist );
	QAction* sep2 = 0;
	QAction* ac_remove = new QAction( "Remove" );
	QAction* ac_removeAll = new QAction( "Remove All" );

signals:
	void play();
	void add();
	void addAndPlay();
	void insert();
	void remove();
	void removeAll();
	void addToPlaylist( const QString& playlistname );
	void addToNewPlaylist( const QString& playlistname );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYMENU_H
