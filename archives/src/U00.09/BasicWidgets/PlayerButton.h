#ifndef PLAYERBUTTON_H
#define PLAYERBUTTON_H
/**********************************************/
#include <QPushButton>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerButton
		: public QPushButton
{
	Q_OBJECT

public:
	PlayerButton( QWidget* parent = 0 )
		: QPushButton( parent )
	{
		setFlat( true );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERBUTTON_H
