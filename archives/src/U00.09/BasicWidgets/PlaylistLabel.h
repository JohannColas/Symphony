#ifndef PLAYLISTLABEL_H
#define PLAYLISTLABEL_H
/**********************************************/
#include "BasicWidgets/Label.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistLabel
		: public Label
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	PlaylistLabel( QWidget* parent = 0 )
		: Label( parent )
	{
	}
	PlaylistLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTLABEL_H
