#ifndef SWIDGET_H
#define SWIDGET_H
#include <QDebug>
/**********************************************/
#include <QWidget>
#include "BasicWidgets/Layouts.h"
#include "Commons/Settings.h"
#include "Commons/TrackList.h"
#include "BasicWidgets/Frame.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SWidget
        : public QWidget
{
    Q_OBJECT
protected:
    Settings* _settings = 0;
//    QString _name;
    /* ------------------------ */
    /* ------------------------ */
public:
    SWidget( QWidget* parent = 0 )
        : QWidget( parent )
    {
        setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
//        setMinimumSize( 100, 100 );
//        show();
    }
    SWidget( SWidget* parent = 0 )
        : QWidget( parent )
    {
        setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
//        setMinimumSize( 100, 100 );
//        show();
        setSettings( parent->settings() );
        initiate();
    }
    Settings* settings()
    {
        return _settings;
    }
    virtual void setSettings( Settings* sets = 0 )
    {
        makeDisconnect();
        _settings = sets;
        makeConnect();
        extraSettings();
    }
    virtual void extraSettings()
    {

    }
    virtual void initiate()
    {

    }
    void saveSettings( const QVariant& value, const QString& key, const QString& subkey = "" )
    {
        if ( _settings )
            _settings->save( value, key, subkey );
    }
//    void setWidgetName( const QString& name )
//    {
//        _name = name;
//    }
    void connectTo( Frame* frame, bool isFrame = true )
    {
        connect( frame, &Frame::trackToPlay,
                 this, &SWidget::trackToPlay );
        connect( frame, &Frame::trackToAdd,
                 this, &SWidget::trackToAdd );
        if ( isFrame )
        {
            connect( frame, &Frame::tracklistToPlay,
                     this, &SWidget::tracklistToPlay );
            connect( frame, &Frame::tracklistToAdd,
                     this, &SWidget::tracklistToAdd );
            connect( frame, &Frame::tracklistToInsert,
                     this, &SWidget::tracklistToInsert );
        }
    }
    void connectTo( SWidget* swidget, bool isFrame = true )
    {
        connect( swidget, &SWidget::trackToPlay,
                 this, &SWidget::trackToPlay );
        connect( swidget, &SWidget::trackToAdd,
                 this, &SWidget::trackToAdd );
        if ( isFrame )
        {
            connect( swidget, &SWidget::tracklistToPlay,
                     this, &SWidget::tracklistToPlay );
            connect( swidget, &SWidget::tracklistToAdd,
                     this, &SWidget::tracklistToAdd );
            connect( swidget, &SWidget::tracklistToInsert,
                     this, &SWidget::tracklistToInsert );
        }
    }
    virtual void updateLayout()
    {

    }
    virtual void updateIcons()
    {

    }
    virtual void updateLang()
    {

    }
    /* ------------------------ */
    /* ------------------------ */
public slots:
    virtual void settingsChanged( const QString& key )
    {
        Q_UNUSED(key);
    }
//    void updateStyle()
//    {
//        if ( _settings )
//        {
//            QSettings* theme = _settings->theme();
//            QString col = theme->value(_name+"/backgroundColor").toString();
//            QString bdCol = theme->value(_name+"/bordersColor").toString();
//            int bdSize = theme->value(_name+"/bordersWidth").toInt();
//            bool hideBd = theme->value(_name+"/hideBorders").toBool();

//            QString style = "background:" + col + ";";
//            if ( hideBd )
//                style += "border-style:none;";
//            else
//                style += "border-style:solid;";
//            style +=  "border-color:" + bdCol + ";" +
//                    "border-width:" + QString::number(bdSize) + ";";
////            qDebug() << style;
//            setStyleSheet( style );
//        }
//    }
protected:
    void makeConnect()
    {
        if ( _settings )
        {
            connect( _settings, &Settings::valueChanged,
                     this, &SWidget::settingsChanged );
        }
    }
    void makeDisconnect()
    {
        if ( _settings )
        {
            disconnect( _settings, &Settings::valueChanged,
                        this, &SWidget::settingsChanged );
        }
    }
signals:
    void trackToPlay( Track* track );
    void trackToAdd( Track* track );
    void tracklistToPlay( TrackList* tracklist );
    void tracklistToAdd( TrackList* tracklist );
    void tracklistToInsert( TrackList* tracklist );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SWIDGET_H
/**********************************************/
