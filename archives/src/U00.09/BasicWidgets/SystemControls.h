#ifndef SYSTEMCONTROLS_H
#define SYSTEMCONTROLS_H

/**********************************************/
#include "BasicWidgets/SWidget.h"
#include <QApplication>
#include "AppButton.h"
#include "SystemButton.h"
/**********************************************/
/**********************************************/
/**********************************************/
class SystemControls
        : public SWidget
{
    Q_OBJECT
private:
    // Application Button
    AppButton* pb_settings = new AppButton(this);
    // System Button
    SystemButton* pb_minimize = new SystemButton(this);
    SystemButton* pb_maximize = new SystemButton(this);
    SystemButton* pb_close = new SystemButton(this);

public:
    ~SystemControls()
    {

    }
    SystemControls( SWidget* parent = 0 )
        : SWidget( parent )
    {
        HorizontalLayout* lay_main = new HorizontalLayout;
//        lay_main->addSpacer();
        lay_main->addWidget( pb_settings );
        lay_main->addWidget( pb_minimize );
        lay_main->addWidget( pb_maximize );
        lay_main->addWidget( pb_close );
        setLayout( lay_main );
        setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );

        connect( pb_settings, &QPushButton::clicked,
                 this, &SystemControls::onSettings );
        connect( pb_minimize, &QPushButton::clicked,
                 this, &SystemControls::onMinimize );
        connect( pb_maximize, &QPushButton::clicked,
                 this, &SystemControls::onMaximize );
        connect( pb_close, &QPushButton::clicked,
                 qApp, &QApplication::closeAllWindows );
    }

public slots:
    void updateIcons() override
    {
        pb_settings->setIcon( _settings->icon("settings") );
        pb_minimize->setIcon( _settings->icon("minimise") );
        pb_maximize->setIcon( _settings->icon("maximise") );
        pb_close->setIcon( _settings->icon("close") );
    }

protected:

signals:
    void onSettings();
    void onMinimize();
    void onMaximize();
//    void onClose();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SYSTEMCONTROLS_H
/**********************************************/
