#ifndef TEXT_H
#define TEXT_H
/**********************************************/
//#include "SWidget.h"
#include <QLabel>
#include <QTimer>
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Text
        : public QLabel
{
    Q_OBJECT
private:
    QString _text = "";
    bool _elide = true;
    bool _multiLine = true;
    QTimer* _timer = new QTimer(this);
    int _txtBegin = 0;
    int _scaleFactor = 5;
    bool _isElide = false;

public:
    Text( const QString& text, QWidget* parent = 0 )
        : QLabel( parent )
    {
        setText( text );
        init();
    }
    Text( QWidget* parent = 0 )
        : QLabel( parent )
    {
        init();
    }
    void init()
    {
        setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
        _timer->setSingleShot( true );
        connect( _timer, &QTimer::timeout,
                 this, &Text::onTimerOut );
    }
    const QString& text() const
    {
        return _text;
    }
    bool isElided() const
    {
        return _elide;
    }
    int scaleFactor()
    {
        return _scaleFactor;
    }
    void setScaleFactor( int factor )
    {
        _scaleFactor = factor;
        elide();
    }

public slots:
    void fixWidth( int width )
    {
        setMaximumWidth( width );
        elide();
    }
    void fixHeight( int height )
    {
        setFixedHeight( height );
    }
    void setText( const QString& text )
    {
        _text = text;
        elide();
    }
    void allowElide()
    {
        _elide = true;
        repaint();
    }
    void avoidElide()
    {
        _elide = false;
    }

private:
//    bool eventFilter(QObject* object, QEvent* event) override
//    {
//        if (event->type() == QEvent::Paint) { return true; }
//        return false;
//    }
//    void paintEvent( QPaintEvent* event ) override
//    {
//        QPainter painter( this );
//        QFontMetrics metrix = painter.fontMetrics();
//        int mtxH = metrix.height();
//        int mtxW = scaleFactor()*mtxH;
//        int maxH = 0;
//        int maxW = mtxW;
//        int x = contentsMargins().left();
//        int y = mtxH + contentsMargins().top();
//        QStringList lines = _text.split("\n");
//        for ( QString line : lines )
//        {
//            // Elide line
//            QString newline = line;
//            if ( _elide )
//            {
//                newline = metrix.elidedText( line.remove(0, _txtBegin ), Qt::ElideRight, maxW );
//                if ( newline != line )
//                    _isElide = true;
//                if ( newline.length() < 5 )
//                    _txtBegin = 0;
//            }
//            else
//            {
//                _isElide = false;
//                int wid = metrix.boundingRect( newline ).width();
//                if ( wid > maxW )
//                    maxW = wid;
//            }
//            painter.drawText( x, y, newline );
//            qDebug() << newline
//                        ;
//            y += mtxH;
//            maxH += mtxH;
//            if ( !_multiLine )
//                break;
//        }
//        maxW += contentsMargins().left() + contentsMargins().right();
//        maxH += contentsMargins().top() + contentsMargins().bottom();
//        setFixedSize( maxW, maxH );
//        setMinimumSize( maxW, maxH );

//        setStyleSheet("background:green;");
//        QWidget::paintEvent( event );
//    }
    void elide()
    {
        QFontMetrics metrix = fontMetrics();
        int mtxH = metrix.height();
        int mtxW = scaleFactor()*mtxH;
        int maxH = 0;
        int maxW = mtxW;
        QStringList lines = _text.split("\n");
        QString newlines;
        for ( int it = 0; it < lines.size(); ++it )
        {
            // Elide line
            QString newline = lines.at(it);
            if ( _elide )
            {
                newline = metrix.elidedText( newline.remove(0, _txtBegin ), Qt::ElideRight, maxW );
                if ( newline != lines.at(it) )
                    _isElide = true;
                if ( newline.length() < 5 )
                    _txtBegin = 0;
            }
            else
            {
                _isElide = false;
                int wid = metrix.boundingRect( newline ).width();
                if ( wid > maxW )
                    maxW = wid;
            }
            newlines += newline;
            maxH += mtxH;
            if ( !_multiLine )
                break;
            if ( it != lines.size()-1 )
                newlines += "\n";
        }
        QLabel::setText( newlines );
        maxW += contentsMargins().left() + contentsMargins().right();
        maxH += contentsMargins().top() + contentsMargins().bottom();
        setFixedSize( maxW, maxH );
        setMinimumSize( maxW, maxH );
        resize( maxW, maxH );

        setStyleSheet("background:green;");
    }
    void onTimerOut()
    {
        ++_txtBegin;
        elide();
        _timer->start( 400 );
    }
    void enterEvent( QEvent* event ) override
    {
        if ( _isElide )
            _timer->start( 500 );
        QLabel::enterEvent( event );
    }
    void leaveEvent( QEvent* event ) override
    {
        _timer->stop();
        _txtBegin = 0;
        elide();
        QLabel::leaveEvent( event );
    }
    void resizeEvent( QResizeEvent* event ) override
    {
        elide();
        QLabel::resizeEvent( event );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TEXT_H
/**********************************************/
