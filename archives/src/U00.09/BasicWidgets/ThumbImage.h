#ifndef THUMBIMAGE_H
#define THUMBIMAGE_H
/**********************************************/
#include "BasicWidgets/LockedButton.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ThumbImage
		: public LockedButton
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	ThumbImage( QWidget* parent = 0 )
		: LockedButton( parent )
	{
		setFlat( true );
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setPixmap( const QPixmap& pix )
	{
		setIcon( pix );
	}
	void setIcon( const QIcon& icon )
	{
		LockedButton::setIcon( icon );
	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // THUMBIMAGE_H
