#ifndef WEBSITELABEL_H
#define WEBSITELABEL_H
/**********************************************/
#include "BasicWidgets/Label.h"

#include <QDesktopServices>
#include <QMouseEvent>
#include <QUrl>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class WebsiteLabel
		: public Label
{
	Q_OBJECT
protected:
//	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	WebsiteLabel( QWidget* parent = 0 )
		: Label( parent )
	{
//		fitWidthToContent();
	}
	WebsiteLabel( const QString& text, QWidget* parent = 0 )
		: Label( text, parent )
	{
//		fitWidthToContent();
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	void setHtml( const QString& html )
	{
		if ( html == "" )
			setText( "www.google.fr");
		else
			setText( html );
	}
	void openLink( const QString & link )
	{
		QDesktopServices::openUrl(QUrl(link));
	}
	void mouseReleaseEvent( QMouseEvent* event )
	{
		if ( event->button() == Qt::LeftButton )
		{
			openLink( text() );
		}
	}
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WEBSITELABEL_H
