#ifndef ARTIST_H
#define ARTIST_H
/**********************************************/
#include <QObject>
#include "Commons/Album.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Artist
		: public QObject
{
	Q_OBJECT
protected:
	QString _name = "";
	QList<Album*> _albums;

public:
	static inline bool compare( Artist* art1, Artist* art2 )
	{
		return ( clean(art1->name()) < clean(art2->name()) );
	}
	static inline QString clean( const QString& str )
	{
		QString temp = str;
		if ( temp.left(4) == "The " )
			temp = temp.mid( 4 );
		temp.replace("É", "E").replace("é", "e");
		return temp.toLower();
	}
	Artist( const QString& name )
		: _name( name )
	{

	}
	QString name() const
	{
		return _name;
	}
	int nbAlbums()
	{
		return _albums.size();
	}
	int nbLines()
	{
		int nbLines = _albums.size();
		for ( Album* album : _albums )
			nbLines += album->nbTracks();
		return nbLines;
	}
	QList<Album*> albums()
	{
		return _albums;
	}
	void addAlbum( Album* album )
	{
		_albums.append( album );
	}
	void addAlbum( const QString& title )
	{
		_albums.append( new Album(title) );
	}
	bool removeAlbum( const QString& title )
	{
		for ( Album* album : _albums )
		{
			if ( album->title() == title )
			{
				_albums.removeOne( album );
				return true;
			}
		}
		return false;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTIST_H
