#ifndef PLAYLISTS_H
#define PLAYLISTS_H
/**********************************************/
#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QDirIterator>
#include <QSettings>
#include <QDomDocument>
#include "Commons/TrackList.h"
#include "Commons/Metadata.h"
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Playlists
        : public QObject
{
    Q_OBJECT
protected:
    QList<TrackList*> _list;
    QString _playlistName = "Playlist";
    int _currentIndex = -1;
    Settings* _settings = 0;

public:
    Playlists()
    {
    }
    QList<TrackList*> list()
    {
        return _list;
    }
    TrackList* tracklist( int index ) const
    {
        if ( isIndexValid( index ) )
            return _list.at( index );
        return 0;
    }
    int currentIndex() const
    {
        return _currentIndex;
    }
    TrackList* currentTracklist() const
    {
        if ( isIndexValid( _currentIndex ) )
            return tracklist( currentIndex() );
        return 0;
    }
    TrackList* currentTrackIndex() const
    {
        if ( isIndexValid( _currentIndex ) )
            return _list.at( _currentIndex );
        return 0;
    }
    int count() const
    {
        return _list.size();
    }
    bool isIndexValid( int index ) const
    {
        return ( index > -1 && index < count() );
    }
    TrackList* playlist( const QString& name )
    {
        for ( TrackList* tl : _list )
            if ( tl->name() == name )
                return tl;
        return 0;
    }
    void setSettings( Settings* sets )
    {
        _settings = sets;
        update();
    }

public slots:
    void setCurrentIndex( int index )
    {
        if ( isIndexValid( index ) )
        {
            _currentIndex = index;
            emit currentIndexChanged( currentIndex() );
            emit currentTrackListChanged( currentTracklist() );
        }
    }
    void addNew()
    {
        QString path = "";
        int idLy = 0;
        for( TrackList* playlist : _list )
        {
            if ( playlist->name().contains( _playlistName ) )
            {
                int lyID = playlist->name().remove( _playlistName + " ").toInt();
                if ( lyID == idLy )
                    ++idLy;
                else if ( lyID > idLy )
                    idLy = lyID + 1;
            }
        }
        path = _playlistName + " " + QString::number(idLy);

        TrackList* tracklist = new TrackList( path,
                                              TrackList::PLAYLIST );
        _list.append( tracklist );
        emit tracklistAdded();
        emit changed();
    }
    void rename( const QString& name )
    {
        if ( isIndexValid( currentIndex() ) )
        {
            currentTracklist()->setName( name );
            emit changed();
        }
    }
    void addTracklist( const QString& path )
    {
        TrackList* tracklist = new TrackList( path,
                                              TrackList::PLAYLIST );
        _list.append( tracklist );
        connect( tracklist, &TrackList::changed,
                 this, &Playlists::changed );
        emit tracklistAdded();
        emit changed();
    }
    void add( TrackList* tracklist )
    {
        _list.append( tracklist );
        emit tracklistAdded();
    }
    TrackList* addNewPlaylist( const QString& path )
    {
        TrackList* tmptl = 0;
        for ( TrackList* tl : _list )
        {
            if ( tl->name() == path )
            {
                tmptl = tl;
                break;
            }
        }
        if ( !tmptl )
        {
            tmptl = new TrackList( path, TrackList::PLAYLIST );
            _list.append( tmptl );
        }
        emit changed();
        return tmptl;
    }
    void move( int from, int to )
    {
        if ( isIndexValid( from ) &&
             isIndexValid( to ) )
        {
            _list.move( from, to );
            emit tracklistMoved();
        }
    }
    void remove()
    {
        if ( isIndexValid( currentIndex() ) )
        {
            _list.removeAt( currentIndex() );
            emit tracklistRemoved();
            emit changed();
        }
    }
    void removeThis( TrackList* tl )
    {
        _list.removeOne( tl );
        delete tl;
        save();
        emit changed();
    }
    void clear()
    {
        _list.clear();
        emit cleared();
    }
    void update()
    {
        QString libraryPath = Files::playlistsPath( Files::PlaylistsName );
        QString contents;
        if ( Files::read( libraryPath, contents ) )
        {
            QStringList strs = contents.split("\n");
            for ( QString str : strs )
                if ( !str.isEmpty() )
                    addTracklist( str );
        }
        else
        {
            qDebug() << "Playlists - Update - error Reading file :"
                     << libraryPath;
        }
    }
    void save()
    {
        QString path = Files::playlistsPath( Files::PlaylistsName );
        if ( Files::exists( path ) )
        {
            QString out;
            for ( TrackList* tl : _list )
                out += tl->name() + "\n";
            Files::save( path, out );
        }
        else
        {
            qDebug() << "Playlists - Save - error Writing file :"
                     << path;
        }
    }

signals:
    void changed();
    void tracklistAdded();
    void tracklistMoved();
    void tracklistRemoved();
    void cleared();
    void currentIndexChanged( int index );
    void currentTrackListChanged( TrackList* tracklist );
    void currentTrackChanged( Track* track );

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTS_H
