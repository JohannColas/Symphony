#ifndef SETTINGS_H
#define SETTINGS_H
/**********************************************/
#include <QApplication>
#include <QFile>
#include <QIcon>
/**********************************************/
#include <QSettings>
#include <QDir>
#include <QFileInfo>
#include <QDebug>
#include "Files.h"
/**********************************************/
namespace Keys {
    // General Keys
    static QString Fullscreen = "fullscreen";
    static QString Maximized = "maximized";
    static QString X = "x";
    static QString Y = "y";
    static QString Height = "height";
    static QString Width = "width";
    // Appearance Keys
    static QString Appearance = "Appearance";
    static QString IconPack = "iconPack";
    static QString Theme = "theme";
    // Behaviour Keys
    static QString Behaviour = "Behaviour";
    static QString OnDoubleClick = "onDoubleClick";
    // Player Keys
    static QString Player = "Player";
    static QString Position = "position";
    static QString Repeat = "repeat";
    static QString Shuffle = "shuffle";
    static QString Size = "size";
    // SectionsSelector Keys
    static QString SectionsSelector = "SectionsSelector";
    static QString HideText= "hideText";
    static QString HideArtists = "hideArtists";
    static QString HideAlbums = "hideAlbums";
    static QString HideGenres = "hideGenres";
    static QString HideTracks = "hideTracks";
    static QString HidePlaylists = "hidePlaylists";
    static QString HideLibraries = "hideLibraries";
    static QString HideRadios = "hideRadios";
    static QString HidePodcasts = "hidePodcasts";
    // Shortcuts Keys
    static QString Shortcuts = "Shortcuts";
    static QString PlayPause = "playpause";
    static QString Stop = "stop";
    static QString Forward = "forward";
    static QString Backward = "backward";
//    static QString Repeat = "stop";
//    static QString Shuffle = "stop";
}
enum Position
{
    TOP = 0,
    RIGHT = 1,
    BOTTOM = 2,
    LEFT = 3,
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Settings
        : public QObject
{
	Q_OBJECT
signals:
    void changed();
private:
    QSettings* _settings = 0;
    QSettings* _theme = 0;

public:
    ~Settings()
    {
        if ( _settings )
            delete _settings;
    }
    Settings()
    {
        // Get user directory
        QDir userDir = getUserDir();
        // Copy settings.ini in userDir if not exists
        if ( !userDir.exists( "settings.ini" ) )
            QFile::copy( "./settings.ini", userDir.path() + "/settings.ini" );
        // Open settings
        _settings = new QSettings( userDir.path() + "/settings.ini", QSettings::IniFormat );
        applyTheme();
        getTheme();
        connect( this, &Settings::valueChanged,
                 this, &Settings::onChanged );
    }
    QSettings* settings()
    {
        return _settings;
    }
    void save( const QVariant& value, const QString& key, const QString& subkey = "" )
    {
        QString flkey = Settings::fullkey(key, subkey);
        QColor color = value.value<QColor>();
        if ( color.isValid() )
            _settings->setValue( flkey, color.name(QColor::HexArgb) );
        else
            _settings->setValue( flkey, value );
//        emit valueChanged( flkey );
    }
    QVariant value( const QString& key, const QString& subkey = "" )
    {
        if ( _settings )
        {
            QVariant variant = _settings->value( fullkey( key, subkey ) );
//            if ( variant.isValid() )
                return variant;
        }
        return QVariant();
    }
    int getInt( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toInt();
        return 0;
    }
    QString getString( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toString();
        return QString();
    }
    QChar getChar( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toChar();
        return QChar();
    }
    QColor getColor( const QString& key, const QString& subkey = "" )
    {
        return QColor(getString(key, subkey));
    }
    bool getBool( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toBool();
        return false;
    }
	QIcon icon( const QString& name )
	{
        QString path = getString( Keys::Appearance, Keys::IconPack );
        if ( !path.isEmpty() )
        {
            path = "./icons/" + path + "/" + name +".svg";
            return QIcon( path );
        }
        return QIcon();
	}
    void getTheme()
    {
        QString theme = getString( Keys::Appearance, Keys::Theme );
        if ( theme != "Default" )
        {
            if ( _theme )
                delete _theme;
            _theme = new QSettings( "./themes/"+theme+".thm", QSettings::IniFormat );
        }
    }
    QSettings* theme()
    {
        return _theme;
    }
    void beginGroup( const QString& group )
    {
        _settings->beginGroup( group );
    }
    void endGroup()
    {
        _settings->endGroup();
    }
    static QString fullkey( const QString& key, const QString& subkey = "" )
    {
        QString flkey = key;
        if ( !subkey.isEmpty() )
            flkey += "/" + subkey;
        return flkey;
    }
    QString localPath()
    {
        // Create user dir if not exists
        QString configPath = "/.local/share";
        #ifdef Q_OS_WIN
            configPath = "/AppData/Roaming";
        #endif
        QDir dir( QDir::homePath() + configPath );
        dir.mkdir( "Symphony" );
        if ( dir.cd( "Symphony" ) )
            return dir.path();
        return QDir::homePath();
    }
    QString librariesPath()
    {
        QDir dir( localPath() );
        dir.mkdir( "Libraries" );
        if ( dir.cd( "Libraries" ) )
            return dir.path();
        return QDir::homePath();
    }
    QString playlistsPath()
    {
        QDir dir( localPath() );
        dir.mkdir( "Playlists" );
        if ( dir.cd( "Playlists" ) )
            return dir.path();
        return QDir::homePath();
    }

public slots:
    void reset()
    {
        QDir userDir = localPath();
        QFile::copy( "./settings.ini", userDir.path() + "/settings.ini" );
    }
    void applyTheme()
    {
        QString theme = getString("Appearance/theme");
        if ( theme == "System" )
        {
            qApp->setStyleSheet( "" );
        }
        else
        {
            QFile file( "./themes/" + theme + "/style.qss" );
            if ( file.open(QFile::ReadOnly) )
            {
                QString style = QLatin1String( file.readAll() );
                style.remove('\n').remove('\t');
                qApp->setStyleSheet( style );
            }
            file.close();
        }
    }

protected slots:
    void onChanged( QString key )
    {
        if ( key == fullkey( Keys::Appearance, Keys::Theme ) )
            applyTheme();
    }

private:
    QDir getUserDir()
    {
        // Create user dir if not exists
        QString configPath = "/.config";
        #ifdef Q_OS_WIN
            configPath = "/AppData/Roaming";
        #endif
        QDir dir( QDir::homePath() + configPath );
        if ( !dir.exists( "Symphony" ) )
            dir.mkdir( "Symphony" );
        dir.cd( "Symphony" );
        return dir;
    }

signals:
    void valueChanged( QString key );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGS_H
