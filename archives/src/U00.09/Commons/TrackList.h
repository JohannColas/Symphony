#ifndef TRACKLIST_H
#define TRACKLIST_H
/**********************************************/
#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QDirIterator>
#include <QSettings>
#include <QDomDocument>
#include <QMediaPlaylist>
#include "Commons/Track.h"
#include "Commons/Metadata.h"
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackList
		: public QObject
{
	Q_OBJECT
public:
	enum Type
	{
        QUEUE,
		TRACKLIST,
        PLAYLIST,
		LIBRARY
	};
private:
	QList<Track*> _list;
    QString _name = "";
    TrackList::Type _type = Type::TRACKLIST;
	bool _hide = false;
	int _currentTrack = 0;

public:
	~TrackList()
	{
//        if ( type() == Type::QUEUE ||
//			 type() == Type::PLAYLIST )
//		{
//			QString path = "./playlists/" + name() + ".spl";
//			QFile file( path );
//			file.remove();
//		}
	}
	TrackList()
	{

	}
    TrackList( const QString& name, TrackList::Type type = TrackList::TRACKLIST )
    {
        setName( name );
        setType( type );
		update();
	}
	Track* track( int index ) const
	{
		if ( isIndexValid( index ) )
			return _list.at( index );
		return new Track("");
	}
	QList<Track*> list() const
	{ return _list; }
	QString name() const
	{ return _name; }
	TrackList::Type type() const
	{ return _type; }
	QString path() const
    {
        if ( type() == Type::QUEUE )
            return Files::localPath( name() );
        else if ( type() == Type::PLAYLIST )
            return Files::playlistsPath( name() + ".spl" );
        return QString();
    }
	bool hide() const
	{ return _hide;	}
	int currentTrackIndex() const
	{ return _currentTrack;	}
	Track* currentTrack() const
	{
		if ( isIndexValid( _currentTrack ) )
			return _list.at( _currentTrack );
		return new Track("");
	}
	int count() const
	{
		return _list.size();
	}
	bool isIndexValid( int index ) const
	{
		return (index > -1 && index < count());
	}
	QMediaPlaylist* mediaPlaylist()
	{
		QMediaPlaylist* playlist = new QMediaPlaylist;
		for ( Track* track : _list )
		{
			playlist->addMedia( QUrl::fromLocalFile( track->path() ) );
		}
		return playlist;
	}
	TrackList* getTracklistByIndexes( QList<int> indexes )
	{
		TrackList* newTL = new TrackList;
		for ( int index : indexes )
		{
			if ( isIndexValid(index) )
				newTL->add( track(index) );
		}
		return newTL;
	}

public slots:
	void setName( const QString& name )
    {
        Files::remove( path() );
        _name = name;
        save();
        emit changed();
    }
	void setType( const TrackList::Type& type )
    { _type = type; }
	void setHide( bool hide )
	{ _hide = hide; emit changed(); }
	void setCurrentTrack( int index )
	{
		if ( isIndexValid( index ) )
			_currentTrack = index;
		emit currentTrackIndexChanged( _currentTrack );
		emit currentTrackChanged( _list.at( _currentTrack ) );
	}
	void addTrack( const QString& path )
	{
		_list.append( new Track(path) );
		emit trackAdded();
	}
	void append( Track* track )
	{
		_list.append( track );
	}
	void add( Track* track )
	{
		*this += track;
		emit trackAdded();
	}
	void add( QList<Track*> tracks )
	{
//		_list.append( tracks );
		for ( Track* track : tracks )
            _list.append( track );
//		emit trackAdded();
		emit changed();
	}
	void add( TrackList* tracklist )
	{
//		QElapsedTimer timer;
//		timer.start();

		*this += tracklist;

//		QString time = QString::number( timer.elapsed() );
//		time.insert( time.length()-3, "s " );
//		time += "ms";
//		qDebug().noquote() << "Time to create MediaPlaylist : " << time;
		emit trackAdded();
	}
	void insert( int index, TrackList* tracklist )
	{
		if ( isIndexValid(index) )
		{
			int it = 1;
			for ( Track* track : tracklist->list() )
			{
				_list.insert( index+it, track );
				++it;
			}
		}
		emit trackAdded();
		emit changed();
	}
	void move( int from, int to )
	{
		if ( isIndexValid( from ) &&
			 isIndexValid( to ) )
		{
			_list.move( from, to );
			emit trackMoved();
		}
	}
//	void remove( int index )
//	{
//		if ( isIndexValid( index ) )
//		{
//			_list.removeAt( index );
//			emit trackRemoved();
//		}
//	}
	void remove( TrackList* tracklist )
	{
		for ( Track* track : tracklist->list() )
			_list.removeOne( track );
		emit trackRemoved();
		emit changed();
	}
	void removeAll()
	{
		_list.clear();
		emit changed();
	}
	void clear()
	{
		_list.clear();
		emit cleared();
	}
	void update()
	{
        if ( type() == Type::QUEUE ||
			 type() == Type::PLAYLIST )
        {
            QString contents;
            if ( Files::read( path(), contents ) )
            {
                QStringList strs = contents.split("\n");
                for ( QString str : strs )
                    if ( !str.isEmpty() )
                        addTrack( str );
            }
			else
                qDebug() << "Tracklist - Update - error Reading file :"
                         << path();
		}
	}
	void save()
	{
        if ( type() == Type::QUEUE ||
			 type() == Type::PLAYLIST )
        {
            if ( Files::exists( path() ) )
            {
                QString out;
                for ( Track* track : _list )
                    out += track->path() + "\n";
                Files::save( path(), out );
            }
			else
                qDebug() << "TrackList - Save - error Writing file :"
                         << path();
		}
	}
//	void updateLibrary()
//	{
//		//Emplacement et nom du fichier
//        QString sName = Files::librariesPath( name() + ".slb" );
//		//Création du QDomDoc qui servira de tampon pr les données contenues dans le fichier Xml
//		QDomDocument domDoc;
//		//Noeud Racine Phonebook
//		QDomElement el_library = domDoc.createElement("library");
//		el_library.setAttribute( "path", path() );
//		el_library.setAttribute( "hide", hide() ? "true" : "false" );
//        domDoc.appendChild( el_library );
//		// Parcours de la liste de track
//		for( int it = 0 ; it < _list.size() ; ++it )
//		{
//			Metadata meta( _list.at(it) );
////			bool artistExits = false;
//            QDomElement el_artist = el_library.firstChildElement();
////			while ( !el_artist.isNull() )
////			{
////				if ( el_artist.attribute("name") == meta.artist() )
////				{
////					artistExits = true;
////					break;
////				}
////				el_artist = el_artist.nextSiblingElement();
////			}
//            if ( !Files::findElement( el_artist, "name", meta.artist() ) )
//			{
//				el_artist = domDoc.createElement("artist");
//				el_artist.setAttribute( "name", meta.artist() );
//				el_library.appendChild( el_artist );
//			}
////			bool albumExits = false;
//			QDomElement el_album = el_artist.firstChildElement();
////			while ( !el_album.isNull() )
////			{
////				if ( el_album.attribute("title") == meta.album() )
////				{
////					albumExits = true;
////					break;
////				}
////				el_album = el_album.nextSiblingElement();
////			}
//            if ( !Files::findElement( el_album, "title", meta.album() ) )
//			{
//				el_album = domDoc.createElement("album");
//				el_album.setAttribute( "title", meta.album() );
//				el_artist.appendChild( el_album );
//			}
//            QDomElement el_track = el_album.firstChildElement();
//            if ( !Files::findElement( el_track, "path", meta.path() ) )
//            {
//                QDomElement el_track = domDoc.createElement("track");
//                el_track.setAttribute( "title", meta.title() );
//                el_track.setAttribute( "path", meta.path() );
//                el_album.appendChild( el_track );
//            }
//		}
//        Files::saveXML( sName, domDoc );
//	}

signals:
	void changed();
	void trackAdded();
	void trackMoved();
	void trackRemoved();
	void cleared();
	void currentTrackIndexChanged( int index );
	void currentTrackChanged( Track* track );

public:
	// Operators
	TrackList& operator+= ( const TrackList& tracklist )
	{
		for ( int it = 0; it < tracklist.count(); ++it )
			 this->append( tracklist.track(it) );
		return (*this);
	}
	TrackList& operator+= ( TrackList* tracklist )
	{
		for ( int it = 0; it < tracklist->count(); ++it )
			 this->append( tracklist->track(it) );
		return (*this);
	}
	TrackList& operator+= ( Track* track )
	{
		this->append( track );
		return (*this);
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKLIST_H
