#ifndef LIBRARYFILESWIDGET_H
#define LIBRARYFILESWIDGET_H
/**********************************************/
#include <QInputDialog>
#include <QListWidget>
#include <QMenu>
#include "BasicWidgets/ListWidget.h"
#include "Commons/Playlists.h"
#include "Commons/Libraries.h"
#include "BasicWidgets/TrackItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class FileSelector
		: public ListWidget
{
	Q_OBJECT
private:
	Library* _library = 0;
	Playlists* _playlists = 0;
	QStringList _filesFilters = {"*.mp3", "*.ogg", ".wav", ".flac", ".m4a", ".wma"};
	QStringList _files;

public:
	FileSelector( QWidget* parent = nullptr )
		: ListWidget( parent )
	{
	}
	Track* currentTrack()
	{
		return 0;
	}
	void setPlaylists( Playlists* playlists )
	{
		_playlists = playlists;
	}

public slots:
	void reset()
	{
		_begin = 0;
		getFiles();
		update();
	}
	void setLibrary( Library* library = 0 )
	{
		_library = library;
		getFiles();
	}
	void getFiles()
	{
		_files.clear();
		QDir dir( _library->path() );
		dir.setSorting( QDir::SortFlag::Name |
						QDir::IgnoreCase |
						QDir::LocaleAware );
		dir.setFilter( QDir::Files );
		dir.setNameFilters( _filesFilters );
		_files = dir.entryList();
		update();
	}
    void updateIcons()
    {
		update();
	}
	void update() override
	{
		clear();
		int x = contentsMargins().left();
		int w = width() - contentsMargins().left() - contentsMargins().right();
		int totheight = contentsMargins().top();
		int nbItems = 0;
		for ( int it = _begin; it < _files.size(); ++it )
		{
			ListItem *item = new ListItem( this );
			item->setGeometry( x, totheight,
									w,
									item->sizeHint().height() );
			item->setText( _files.at(it) );
			// Adding the Item to LibraryFilesWidget
			addItem( item );

			totheight += item->sizeHint().height();
			if ( totheight > height() )
			{
				break;
			}
			++nbItems;
		}
		updateScrollbar( _files.size() );
	}
	void showContextMenu( const QPoint& pos ) override
	{
		QMenu* menu = new QMenu( this );
		// Adding Play/Add/Insert Options
		QAction* ac_play = menu->addAction( "Play Now (and remove Current Playlist)" );
		QAction* ac_add = menu->addAction( "Add to Current Playlist" );
		//QAction* ac_add = menu->addAction( "Add and Play to Current Playlist" );
		QAction* ac_insert = menu->addAction( "Insert after the Current Track" );
		menu->addSeparator();
		// Adding Playlists Menu
		QMenu* ac_addToPlaylist = menu->addMenu( "Add to a Playlist" );
		QList<QAction*> playlists;
		for ( TrackList* elm : _playlists->list() )
		{
			QAction* playlist = ac_addToPlaylist->addAction( elm->name() );
			playlists.append( playlist );
		}
		// Adding Remove Options
		QAction* ac_remove;
		QAction* ac_removeall;
		if ( _allowRemove )
		{
			menu->addSeparator();
			ac_remove = menu->addAction( "Remove" );
			ac_removeall = menu->addAction( "Remove All" );
		}
		QAction* action = menu->exec( this->mapToGlobal(pos) );
		if ( action == ac_play )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToPlay( tltmp );
		}
		else if ( action == ac_add )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToAdd( tltmp );
		}
		else if ( action == ac_insert )
		{
			TrackList* tltmp = selectedTracks();
			if ( tltmp )
				emit tracklistToInsert( tltmp );
		}
		else if ( action == ac_remove )
		{
//			TrackList* tltmp = selectedTracks();
//			if ( tltmp )
//				emit tracklistToRemove( tltmp );
		}
		else if ( action == ac_removeall )
		{
//			emit removeAll();
		}
		for ( int it = 0; it < playlists.size(); ++it )
		{
			if ( action == playlists.at(it) )
			{
//				TrackList* tltmp = selectedTracks();
//				TrackList* pltmp = _playlists->tracklist(it);
//				if ( tltmp && pltmp )
//					pltmp->add(tltmp);
				break;
			}
		}
	}
	TrackList* selectedTracks()
	{
		TrackList* list = new TrackList;
		int lt = 0;
		for ( QString file : _files )
		{
			if ( _selection.contains(lt) )
			{
//				list->add( file );
				if ( _selection.size() == 1 )
					break;
			}
			++lt;
		}
		if ( list->count() == 0 )
		{
			delete list;
			return 0;
		}
		return list;
	}

signals:
	void currentTracklistChanged( TrackList* tracklist );
	void renamed( const QString& name );
	void deleted();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARYFILESWIDGET_H
