#ifndef PLAYERCONTROLS_H
#define PLAYERCONTROLS_H
/**********************************************/
#include "BasicWidgets/SWidget.h"
#include "PlayerControl.h"
#include "BasicWidgets/Layouts.h"
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
class PlayerControls
        : public SWidget
{
    Q_OBJECT
private:
    PlayerControl* pb_playpause = new PlayerControl(this);
    PlayerControl* pb_stop = new PlayerControl(this);
    PlayerControl* pb_backward = new PlayerControl(this);
    PlayerControl* pb_forward = new PlayerControl(this);
//    Settings* _settings = 0;

public:
    ~PlayerControls()
    {

    }
    PlayerControls( SWidget* parent = 0 )
        : SWidget( parent )
    {
        HorizontalLayout* lay_main = new HorizontalLayout;
//        lay_main->addSpacer();
        lay_main->addWidget( pb_backward );
        lay_main->addWidget( pb_playpause );
        lay_main->addWidget( pb_stop );
        lay_main->addWidget( pb_forward );
//        lay_main->addSpacer();
        setLayout( lay_main );
        setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
        lay_main->setSizeConstraint( QLayout::SetMinimumSize );

        connect( pb_playpause, &QPushButton::clicked,
                 this, &PlayerControls::playpause );
        connect( pb_stop, &QPushButton::clicked,
                 this, &PlayerControls::stop );
        connect( pb_backward, &QPushButton::clicked,
                 this, &PlayerControls::backward );
        connect( pb_forward, &QPushButton::clicked,
                 this, &PlayerControls::forward );
        updateIcons();
    }
//    void setSettings( Settings* sets )
//    {
//        _settings = sets;
//        updateIcons();
//    }

public slots:
    void updateIcons() override
    {
        if ( _settings )
        {
            pb_backward->setIcon( _settings->icon("skip_previous") );
            pb_playpause->setIcon( _settings->icon("play") );
            pb_stop->setIcon( _settings->icon("stop") );
            pb_forward->setIcon( _settings->icon("skip_next") );
        }
    }
    void setPlayIcon()
    {
        if ( _settings )
            pb_playpause->setIcon( _settings->icon("play") );
    }
    void setPauseIcon()
    {
        if ( _settings )
            pb_playpause->setIcon( _settings->icon("pause") );
    }

protected slots:

signals:
    void playpause();
    void stop();
    void backward();
    void forward();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERCONTROLS_H
/**********************************************/
