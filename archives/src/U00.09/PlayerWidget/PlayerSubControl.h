#ifndef PLAYERSUBCONTROL_H
#define PLAYERSUBCONTROL_H
/**********************************************/
#include <QPushButton>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerSubControl
        : public QPushButton
{
    Q_OBJECT

public:
    PlayerSubControl( QWidget* parent = 0 )
        : QPushButton( parent )
    {
        setFlat( true );
        setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERSUBCONTROL_H
/**********************************************/
