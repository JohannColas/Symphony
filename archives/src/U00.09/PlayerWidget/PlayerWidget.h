#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "BasicWidgets/SWidget.h"
#include <QFrame>
#include <QPushButton>
#include <QElapsedTimer>
#include <QSlider>
#include <QProgressBar>
#include <QTimer>
#include <QMouseEvent>
#include "Commons/Track.h"
#include "Commons/TrackList.h"
#include "Commons/Settings.h"
/**********************************************/
#include "PlayerControls.h"
#include "PlayerSubControls.h"
#include "PlayerTrackBanner.h"
/**********************************************/
#include <QMediaPlayer>
#include <QMediaPlaylist>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerWidget
        : public SWidget
{
	Q_OBJECT
public:
	enum Repeat {
        NOREPEAT = 0,
        REPEATALL = 1,
        REPEATONE = 2
	};

private:
	bool _movable = false;
    TrackList* _queue = 0;
	QMediaPlayer* _player = new QMediaPlayer;
	QTimer* _timer = new QTimer(this);
    bool _isPlaying = false;
    BoxLayout* lay_main = new BoxLayout;
    // Player Widgets
    PlayerControls* _playerControls;// = new PlayerControls(this);
    PlayerSubControls* _playerSubControls;// = new PlayerSubControls(this);
    PlayerTrackBanner* _playerTrackBanner;// = new PlayerTrackBanner(this);
    QSpacerItem* _spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding );
    //
    Repeat _repeat = Repeat::NOREPEAT;
    bool _isShuffle = false;
	//
	CoverImage* img_cover = new CoverImage;
	TitleLabel* lb_title = new TitleLabel;
	ArtistLabel* lb_artist = new ArtistLabel;
	QSlider* sl_time = new QSlider;
	QLabel* lb_time = new QLabel;
	int _newPos = -1;
    int _duration = -1;
	QList<int> _listOrder;
	int _index = 0;
//    Settings* _sets = 0;

public:
    ~PlayerWidget()
    {

    }
    PlayerWidget( SWidget* parent = 0 )
        : SWidget( parent )
    {

        setLayout( lay_main );
        lay_main->setSizeConstraint( QLayout::SetMinimumSize );
        show();

        _playerControls = new PlayerControls(this);
        _playerSubControls = new PlayerSubControls(this);
        _playerTrackBanner = new PlayerTrackBanner(this);

        connect( _player, &QMediaPlayer::positionChanged,
                 this, &PlayerWidget::changePosition );
        connect( _player, &QMediaPlayer::mediaStatusChanged,
                 this, &PlayerWidget::onMediaStatusChanged );

        connect( _playerControls, &PlayerControls::playpause,
                 this, &PlayerWidget::playpause );
        connect( _playerControls, &PlayerControls::stop,
                 this, &PlayerWidget::stop );
        connect( _playerControls, &PlayerControls::backward,
                 this, &PlayerWidget::backward );
        connect( _playerControls, &PlayerControls::forward,
                 this, &PlayerWidget::forward );

        connect( _playerSubControls, &PlayerSubControls::onShuffle,
                 this, &PlayerWidget::shuffle );
        connect( _playerSubControls, &PlayerSubControls::onRepeat,
                 this, &PlayerWidget::repeat );
        //    connect( _playerSubControls, &PlayerSubControls::onEqualizer,
        //             this, &PlayerWidget::equalizer );
        //    connect( _playerSubControls, &PlayerSubControls::onVolume,
        //             this, &PlayerWidget::volume );

        connect( sl_time, &QSlider::valueChanged,
                 this, &PlayerWidget::setPlayerPos );
        _timer->setSingleShot( true );
        connect( _timer, &QTimer::timeout,
                 this, &PlayerWidget::updatePlayerPos );

        setMouseTracking( true );
        //
        initiate();
    }
    void initiate() override
    {
        if ( _settings )
        {
            _isShuffle = _settings->getBool( Keys::Player, Keys::Shuffle );
            updateShuffleIcon();
            if ( _isShuffle )
                randomize();
            _repeat = (Repeat)_settings->getInt( Keys::Player, Keys::Repeat );
            updateRepeatIcon();
            updateLayout();
        }
    }
    void randomize()
    {
        if ( _queue )
        {
            _listOrder.clear();
            for ( int it = 0; it < _queue->count(); ++it )
            {
                _listOrder.append( it );
            }
            std::srand(std::time(nullptr));
            std::random_shuffle( _listOrder.begin(), _listOrder.end() );
            if ( _index != -1 && _listOrder.contains(_index) )
            {
                while ( _listOrder.first() != _index )
                {
                    _listOrder.append( _listOrder.takeFirst() );
                }
            }
        }
    }

public slots:
    void playTrack( Track* track )
    {
        _queue->clear();
        _queue->add( track );
        _index = 0;
        setTrack();
        play();
    }
    void addTrack( Track* track )
    {
        _queue->add( track );
    }
    void playTracklist( TrackList* tracklist )
    {
        _queue->clear();
        _queue->add( tracklist );
        _index = 0;
        setTrack();
        play();
    }
    void addTracklist( TrackList* tracklist )
    {
        _queue->add( tracklist );
    }
    void insertTracklist( TrackList* tracklist )
    {
        if ( _isShuffle )
            addTracklist( tracklist );
        else
            _queue->insert( _index, tracklist );
    }
    void receiveTrackList( TrackList* tracklist )
    {
        _queue->clear();
        _queue->add( tracklist );
        _index = 0;
        setTrack();
        play();
    }
    void setPlaylistIndex( int index )
    {
        _index = index;
        if ( _isShuffle )
            _index = _listOrder.indexOf( index );
        setTrack();
    }
    void setTrackList( TrackList* playlist )
    {
        _queue = playlist;
        _index = 0;
        setTrack();
        connect( _queue, &TrackList::changed,
                 this, &PlayerWidget::randomize );
    }
	//
    void setTrack()
    {
        int index = _index;
        if ( _isShuffle && _listOrder.size() > 0)
            index = _listOrder.at( index );
        Track* track = _queue->track(index);
        _player->setMedia( QUrl::fromLocalFile( track->path() ) );
        _playerTrackBanner->setTrack( track );
        updateTrackInfo( track );
        sl_time->setValue(0);
        sl_time->setMaximum( _duration );
        if ( _isPlaying )
            play();
        updatePlayPauseIcon();
    }
    void play()
    {
        _player->play();
        _isPlaying = true;
    }
    void pause()
    {
        _player->pause();
        _isPlaying = false;
    }
    void playpause()
    {
        if ( _player->state() == QMediaPlayer::PlayingState )
            pause();
        else
            play();
        // Update Play/Pause Icon
        updatePlayPauseIcon();
    }
    void stop()
    {
        _player->stop();
        _isPlaying = false;
    }
    void backward()
    {
        if ( sl_time->value() < 40 )
        {
            --_index;
            if ( _index == -1 )
                _index = _queue->count() - 1;
            setTrack();
        }
        else
            _player->setPosition( 0 );
    }
    void forward()
    {
        ++_index;
        if ( _index == _queue->count() )
        {
            _index = 0;
            if ( _repeat == Repeat::NOREPEAT )
            {
                stop();
            }
        }
        setTrack();
    }
    void repeat()
    {
        if ( _repeat == Repeat::NOREPEAT )
            _repeat = Repeat::REPEATALL;
        else if ( _repeat == Repeat::REPEATALL )
            _repeat = Repeat::REPEATONE;
        else if ( _repeat == Repeat::REPEATONE )
            _repeat = Repeat::NOREPEAT;
        updateRepeatIcon();
        saveSettings( _repeat, Keys::Player, Keys::Repeat );
    }
    void updateRepeatIcon()
    {
        if ( _repeat == Repeat::NOREPEAT )
            _playerSubControls->noRepeatIcon();
        else if ( _repeat == Repeat::REPEATALL )
            _playerSubControls->repeatAllIcon();
        else if ( _repeat == Repeat::REPEATONE )
            _playerSubControls->repeatOneIcon();
    }
    void shuffle()
    {
        _isShuffle = !_isShuffle;
        if ( _isShuffle )
        {
            randomize();
        }
        else
        {
            if ( _index >= 0 && _index < _listOrder.size() )
                _index = _listOrder.at(_index);
            _listOrder.clear();
        }
        updateShuffleIcon();
        saveSettings( _isShuffle, Keys::Player, Keys::Shuffle );
    }
    void updateShuffleIcon()
    {
        if ( _isShuffle )
            _playerSubControls->shuffleIcon();
        else
            _playerSubControls->noShuffleIcon();
    }
    void changePosition( qint64 pos )
    {
        //	if ( _player->duration() )
        {

            //		int perc = _duration ? (0.001*pos)/_duration : 0;
            //		sl_time->setValue( perc );
            QString curT = QTime( 0, 0, 0 ).addMSecs( pos ).toString();
            if( curT.left(2) == "00" )
                curT = curT.mid( 3 );
            QString durT = QTime( 0, 0, 0).addSecs( _duration ).toString();
            if( durT.left(2) == "00" )
                durT = durT.mid( 3 );
            lb_time->setText( curT + " / " + durT );
            sl_time->blockSignals( true );
            sl_time->setValue(0.001*pos);
            sl_time->blockSignals( false );
        }
    }
    void setPlayerPos( int pos )
    {
        _newPos = pos;
        _timer->start(100);
    }
    void updatePlayerPos()
    {
        if ( _newPos != -1 )
        {
            _player->setPosition( 1000*_newPos );
        }
    }
    void updatePlayPauseIcon()
    {
        if ( _player->state() == QMediaPlayer::PlayingState )
            _playerControls->setPauseIcon();
        else
            _playerControls->setPlayIcon();
    }
    void mousePressEvent( QMouseEvent* event ) override
    {
        if ( event->button() == Qt::LeftButton )
        {
            _movable = true;
            emit moveWindow( event->globalPos() );
        }
        event->ignore();
    }
    void mouseReleaseEvent( QMouseEvent* event ) override
    {
        if ( event->button() == Qt::LeftButton )
        {
            _movable = false;
            emit moveWindow( {-1,0} );
        }
        event->ignore();
    }
    void mouseMoveEvent( QMouseEvent* event ) override
    {
        if ( _movable )
            emit moveWindow( event->globalPos() );
        event->ignore();
    }
    void mouseDoubleClickEvent( QMouseEvent* event ) override
    {
        if ( event->button() == Qt::LeftButton )
            onMaximize();
        event->ignore();
    }
    void connectTo( Frame* frame )
    {
        connect( frame, &Frame::trackToPlay,
                 this, &PlayerWidget::playTrack );
        connect( frame, &Frame::trackToAdd,
                 this, &PlayerWidget::addTrack );
        connect( frame, &Frame::tracklistToPlay,
                 this, &PlayerWidget::playTracklist );
        connect( frame, &Frame::tracklistToAdd,
                 this, &PlayerWidget::addTracklist );
        connect( frame, &Frame::tracklistToInsert,
                 this, &PlayerWidget::insertTracklist );
    }
    void connectTo( SWidget* swidget )
    {
        connect( swidget, &SWidget::trackToPlay,
                 this, &PlayerWidget::playTrack );
        connect( swidget, &SWidget::trackToAdd,
                 this, &PlayerWidget::addTrack );
        connect( swidget, &SWidget::tracklistToPlay,
                 this, &PlayerWidget::playTracklist );
        connect( swidget, &SWidget::tracklistToAdd,
                 this, &PlayerWidget::addTracklist );
        connect( swidget, &SWidget::tracklistToInsert,
                 this, &PlayerWidget::insertTracklist );
    }
    void onMediaStatusChanged()
    {
        updatePlayPauseIcon();
        if ( _player->mediaStatus() == QMediaPlayer::EndOfMedia )
        {
            if ( _repeat == Repeat::REPEATONE )
            {
                _player->setPosition(0);
                play();
            }
            else
                forward();
        }
    }
    void updateTrackInfo( Track* track )
    {
        if ( track )
        {
            Metadata meta( track );
            QPixmap pix = QPixmap();
            if ( pix.convertFromImage( meta.cover() ) )
                img_cover->setPixmap( pix );
            else
            {
                if ( _settings )
                    img_cover->setIcon( _settings->icon("track") );
            }
            lb_title->setText( meta.title() );
            lb_artist->setText( meta.artist() );
            _duration = meta.duration();
            changePosition(0);
        }
    }
    void settingsChanged( const QString& key ) override
    {
        if ( key == Settings::fullkey( Keys::Player, Keys::Position ) ||
             key == Settings::fullkey( Keys::Player, Keys::Size ) )
            updateLayout();
    }
    void updateLayout() override
    {
        if ( _settings )
        {
            QString pos = _settings->getString( Keys::Player, Keys::Position );
            lay_main->removeWidget( _playerControls );
            lay_main->removeWidget( _playerSubControls );
            lay_main->removeWidget( _playerTrackBanner );
            lay_main->removeItem( _spacer );
            if ( pos == 't' ||
                 pos == 'b' )
            {
                lay_main->setDirection( QBoxLayout::LeftToRight );
                lay_main->addWidget( _playerControls );
                lay_main->addWidget( _playerTrackBanner );
                lay_main->addWidget( _playerSubControls );
                lay_main->addItem( _spacer );
                setSizePolicy( QSizePolicy::Expanding,
                               QSizePolicy::Maximum );
            }
            else
            {
                lay_main->setDirection( QBoxLayout::TopToBottom );
                lay_main->addWidget( _playerTrackBanner );
                lay_main->addWidget( _playerControls );
                lay_main->addWidget( _playerSubControls );
                lay_main->addItem( _spacer );
                setSizePolicy( QSizePolicy::Maximum,
                               QSizePolicy::Expanding );
            }
            lay_main->setAlignment( _playerControls, Qt::AlignCenter );
            lay_main->setAlignment( _playerSubControls, Qt::AlignCenter );
        }
    }
    void updateIcons() override
    {
        _playerControls->updateIcons();
        _playerSubControls->updateIcons();
    }

signals:
	void indexChanged();
	void playlistChanged( TrackList* list );
	void onMinimize();
	void onMaximize();
	void moveWindow( const QPoint& pos );
	void showSettings();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERWIDGET_H
