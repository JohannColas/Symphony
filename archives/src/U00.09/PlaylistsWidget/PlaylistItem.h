#ifndef PLAYLISTITEM_H
#define PLAYLISTITEM_H
/**********************************************/
#include "BasicWidgets/ListItem.h"
#include "Commons/TrackList.h"
/**********************************************/
#include <QLabel>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlaylistItemIcon
		: public QLabel
{

public:
	PlaylistItemIcon( QWidget* parent = 0 )
		: QLabel( parent )
	{
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	}
};
class PlaylistItemName
		: public QLabel
{
};

class PlaylistItem
		: public ListItem
{
	Q_OBJECT
private:

public:
	PlaylistItem( const QString& name, QIcon icon, QWidget* parent = 0 )
		: ListItem( parent )
	{
		setText( name );
		setIcon( icon );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTITEM_H
