//#ifndef PLAYLISTLIST_H
//#define PLAYLISTLIST_H
///**********************************************/
//#include <QObject>
//#include <QSettings>
//#include "Commons/TrackList.h"
//#include <QTextStream>
//#include <QDebug>
//#include "Commons/Settings.h"
///**********************************************/
///**********************************************/
///**********************************************/
///*
// *
// * */
//class PlaylistList
//		: public QObject
//{
//	Q_OBJECT
//private:
//	QList<TrackList*> _tracklists;
//	int _currentTracklistIndex = -1;
//    Settings* _settings = 0;

//public:
//	PlaylistList()
//	{
//		update();
//	}
//	int currentTracklistIndex() const
//	{
//		return _currentTracklistIndex;
//	}
//	TrackList* currentTracklist() const
//	{
//		if ( isIndexValid( _currentTracklistIndex ) )
//			return _tracklists.at(_currentTracklistIndex);
//		return new TrackList();
//	}
//	int nbTracks() const
//	{
//		return _tracklists.count();
//	}
//	bool isIndexValid( int index ) const
//	{
//		return (index > -1 && index < nbTracks());
//	}
//    void setSettings( Settings* sets )
//    {
//        _settings = sets;
//    }

//public slots:
//	void addTracklist( const QString& path )
//	{
//		_tracklists.append( new TrackList( path, TrackList::PLAYLIST ) );
//		emit tracklistAdded();
//	}
//	void add( TrackList* tracklist )
//	{
//		_tracklists.append( tracklist);
//		emit tracklistAdded();
//	}
//	void move( int from, int to )
//	{
//		if ( isIndexValid( from ) &&
//			 isIndexValid( to ) )
//		{
//			_tracklists.move( from, to );
//			emit tracklistMoved();
//		}
//	}
//	void remove( int index )
//	{
//		if ( isIndexValid( index ) )
//		{
//			_tracklists.removeAt( index );
//			emit tracklistRemoved();
//		}
//	}
//	void clear()
//	{
//		_tracklists.clear();
//		emit cleared();
//	}
//	QList<TrackList*> list()
//	{
//		return _tracklists;
//	}
//	void update()
//	{
//        QString path;
//        if ( _settings )
//            path += _settings->playlistsPath() + "/";
//        path += "#playlists.spls";
//        QFile file( path );
//		if ( file.open(QIODevice::ReadOnly) )
//		{
//			QTextStream in(&file);
//			while ( !in.atEnd() )
//			{
//				QString line = in.readLine();
//				addTracklist( line );
//			}
//		}
//		else
//			qDebug() << "error Reading file";

//	}
//	void setCurrentIndex( int index )
//	{
//		if ( isIndexValid( index ) )
//		{
//			_currentTracklistIndex = index;
//			emit currentTracklistChanged( currentTracklist() );
//		}
//	}

//signals:
//	void tracklistAdded();
//	void tracklistMoved();
//	void tracklistRemoved();
//	void cleared();
//	void currentTracklistChanged( TrackList* tracklist );
//};
///**********************************************/
///**********************************************/
///**********************************************/
//#endif // PLAYLISTLIST_H
