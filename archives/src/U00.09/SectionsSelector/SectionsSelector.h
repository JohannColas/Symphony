#ifndef SECTIONSSELECTOR_H
#define SECTIONSSELECTOR_H
/**********************************************/
#include <QWidget>
#include "BasicWidgets/SWidget.h"
#include <QToolButton>
#include <QMouseEvent>
#include <QIcon>
#include <QMenu>
#include "BasicWidgets/SectionButton.h"
#include "BasicWidgets/Layouts.h"
#include "Commons/Settings.h"
/**********************************************/
class Action
        : public QAction
{
    Q_OBJECT
    int _id = -1;
    QString _key = QString();

public:
    Action( const QString& text, QObject* obj = 0 )
        : QAction( text, obj )
    {
        setCheckable( true );
        connect( this, &Action::toggled,
                 this, &Action::onToggled );
    }
    int id() { return _id; }
    void setID( int id ) { _id = id; }
    QString key() { return _key; }
    void setKey( const QString& key ) { _key = key; }
protected slots:
    void onToggled( bool toggled )
    {
        emit sendID( _id, toggled );
    }

signals:
    void sendID( int id, bool toggled );
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SectionsSelector
        : public SWidget
{
    Q_OBJECT
protected:
    //    GridLayout* lay_main = new GridLayout;
    BoxLayout* lay_main = new BoxLayout(this);
    QList<SectionButton*> _buttons;
    QList<QWidget*> _widgets;
    QWidget* _currentWidget = 0;
    SectionButton* _moreButton = new SectionButton("", QIcon(), this);
    QSpacerItem* _spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding );
    //    Settings* _settings = 0;
    bool _textHide = false;
    //
    QMenu* pop_options = new QMenu(this);
    QAction* ac_textVisibility = new QAction("Text visible", this);
    QList<Action*> ac_sections;

public:
    SectionsSelector( SWidget* parent = 0 )
        : SWidget( parent )
    {
        ac_textVisibility->setCheckable(true);
        connect( ac_textVisibility, &QAction::toggled,
                 this, &SectionsSelector::setTextVisibility );
        pop_options->addSection( "Options" );
        pop_options->addAction( ac_textVisibility );
        pop_options->addSection( "Sections" );
        pop_options->setWindowFlags( Qt::Popup );
        _moreButton->setMenu( pop_options );

        lay_main->setSizeConstraint( QLayout::SetMinimumSize );
        setLayout( lay_main );
        setMouseTracking( true );
        setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
        _moreButton->setIconSize(20);
    }
    void extraSettings() override
    {
        updateLayout();
        updateLang();
        updateIcons();
        updateMenu();
    }
    void updateMenu()
    {
        if ( _settings )
        {
            setTextVisibility( !_settings->getBool( Keys::SectionsSelector, Keys::HideText ) );
            for ( Action* ac : ac_sections )
                setSectionVisible( ac->id(), !_settings->getBool( Keys::SectionsSelector, _buttons.at(ac->id())->key() ) );
        }
    }
    QWidget* widget( int index )
    {
        if ( index >= 0 && index < _widgets.size() )
            return _widgets.at(index);
        return 0;
    }
    QWidget* currentWidget()
    {
        return _currentWidget;
    }
    void showWidget( int index )
    {
        _currentWidget = widget(index);
        if ( _currentWidget )
            _currentWidget->show();
    }
    void addSection( QWidget* wid, const QString& key = QString() )
    {
        _widgets.append( wid );
        SectionButton* but = new SectionButton(this);
        but->setID( _widgets.size()-1 );
        but->setKey( key );
        connect( but, &SectionButton::sendID,
                 this, &SectionsSelector::showWidgetByID );
        _buttons.append( but );
        if ( key.isEmpty() )
            return;
        Action* ac = new Action( "", this );
        ac->setID( _buttons.size()-1 );
        ac->setKey( key );
        connect( ac, &Action::sendID,
                 this, &SectionsSelector::setSectionVisible );
        ac_sections.append( ac );
        pop_options->addAction( ac );
    }
    void addWidget( QWidget* wid )
    {
        _widgets.append( wid );
    }

public slots:
    void setHorizontal()
    {
        lay_main->setDirection( QBoxLayout::LeftToRight );
        setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
        for ( int it = 0; it < 9; ++it )
        {
            if ( !_textHide )
                _buttons.at(it)->setStyle( SectionButton::TextUnderIcon );
            _buttons.at(it)->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Expanding );
        }
        if ( !_textHide )
            _moreButton->setStyle( SectionButton::TextUnderIcon );
        _moreButton->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Expanding );
    }
    void setVertical()
    {
        lay_main->setDirection( QBoxLayout::TopToBottom );
        setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Expanding );
        for ( SectionButton* but : _buttons )
        {
            if ( !_textHide )
                but->setStyle( SectionButton::TextBesideIcon );
            but->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
        }
        if ( !_textHide )
            _moreButton->setStyle( SectionButton::TextBesideIcon );
        _moreButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
    }
    void updateLang() override
    {
        _buttons.at(0)->setText( "Queue" );
        _buttons.at(1)->setText( "Artists" );
        _buttons.at(2)->setText( "Albums" );
        _buttons.at(3)->setText( "Genres" );
        _buttons.at(4)->setText( "Tracks" );
        _buttons.at(5)->setText( "Playlists" );
        _buttons.at(6)->setText( "Libraries" );
        _buttons.at(7)->setText( "Radios" );
        _buttons.at(8)->setText( "Podcasts" );
        ac_sections.at(0)->setText( "Artists" );
        ac_sections.at(1)->setText( "Albums" );
        ac_sections.at(2)->setText( "Genres" );
        ac_sections.at(3)->setText( "Tracks" );
        ac_sections.at(4)->setText( "Playlists" );
        ac_sections.at(5)->setText( "Libraries" );
        ac_sections.at(6)->setText( "Radios" );
        ac_sections.at(7)->setText( "Podcasts" );
        _moreButton->setText( "More" );
    }
    void updateIcons() override
    {
        if ( _settings )
        {
            _buttons.at(0)->setIcon( _settings->icon("now_playing") );
            _buttons.at(1)->setIcon( _settings->icon("artist") );
            _buttons.at(2)->setIcon( _settings->icon("album") );
            _buttons.at(3)->setIcon( _settings->icon("genre") );
            _buttons.at(4)->setIcon( _settings->icon("track") );
            _buttons.at(5)->setIcon( _settings->icon("playlist") );
            _buttons.at(6)->setIcon( _settings->icon("libraries") );
            _buttons.at(7)->setIcon( _settings->icon("radio") );
            _buttons.at(8)->setIcon( _settings->icon("podcast") );
            _moreButton->setIcon( _settings->icon("more_horiz") );
        }
    }
    void updateLayout() override
    {
        for ( SectionButton* but : _buttons )
            lay_main->removeWidget( but );
        lay_main->removeItem( _spacer );
        lay_main->removeWidget( _moreButton );
        for ( SectionButton* but : _buttons )
            lay_main->addWidget( but );
        lay_main->addItem( _spacer );
        lay_main->addWidget( _moreButton );
    }
    void hideAllWidgets()
    {
        for ( QWidget* wid : _widgets )
            wid->hide();
//        if ( _currentWidget )
//            _currentWidget->hide();
//        if ( !_widgets.isEmpty() )
//            _widgets.last()->hide();
    }
    void showWidgetByID( int id )
    {
        if ( id >= 0 && id < _widgets.size() )
        {
            hideAllWidgets();
            showWidget(id);
        }
    }
    void setTextVisibility( bool check )
    {
        _textHide = !check;
        SectionButton::ButtonStyle style = SectionButton::IconOnly;
        if ( !_textHide )
        {
            if ( lay_main->direction() == QBoxLayout::LeftToRight )
                style = SectionButton::TextUnderIcon;
            if ( lay_main->direction() == QBoxLayout::TopToBottom )
                style = SectionButton::TextBesideIcon;
        }
        for ( SectionButton* but : _buttons )
        {
            but->setStyle( style );
        }
        _moreButton->setStyle( style );
        if ( check != ac_textVisibility->isChecked() )
            ac_textVisibility->setChecked( check );
        if ( _settings )
            _settings->save( !check, Keys::SectionsSelector, Keys::HideText );
    }
    /**********************************************/
    /**********************************************/
    /* */
    void setNowPlayingVisible( bool /*check*/ )
    {
        //	wid_list->item( 0 )->setHidden( !check );
    }
    void setSectionVisible( int index, bool check )
    {
        if ( index >= 0 && index < _buttons.size() )
        {
            _buttons.at(index)->setHidden( !check );
            Action* ac = ac_sections.at(index-1);
            if ( check != ac->isChecked() )
                ac->setChecked( check );
            _settings->save( !check, Keys::SectionsSelector, ac->key() );
        }
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SECTIONSSELECTOR_H
/**********************************************/
