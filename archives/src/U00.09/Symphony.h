#ifndef WIDGET_H
#define WIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "Commons/Settings.h"
#include "Commons/TrackList.h"
#include "Commons/Playlists.h"
#include "Commons/Libraries.h"
#include <QListWidget>
/**********************************************/
#include "PlayerWidget/PlayerWidget.h"
#include "BasicWidgets/SystemControls.h"
#include "SectionsSelector/SectionsSelector.h"
#include "QueueWidget/QueueWidget.h"
#include "ArtistsWidget/ArtistsWidget.h"
#include "AlbumsWidget/AlbumsWidget.h"
#include "GenresWidget/GenresWidget.h"
#include "TracksWidget/TracksWidget.h"
#include "PlaylistsWidget/PlaylistsWidget.h"
#include "LibrariesWidget/LibrariesWidget.h"
#include "RadiosWidget/RadiosWidget.h"
#include "PodcastsWidget/PodcastsWidget.h"
#include "SettingsWidget/SettingsWidget.h"
#include "BasicWidgets/SWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Symphony
        : public SWidget
{
    Q_OBJECT
protected:
    // Window Parameters
    int invisibleBorderSize = 2;
    bool isPressedWidget;
    QPoint _lastPos = {-1,0};
    int _oldWidget = 0;
    QWidget* _oldOne = 0;
    // Initialise TrackLists
    TrackList* _queue = 0;
    Playlists* _playlists = 0;
    Libraries* _libraries = 0;
//	Radios* _radios = new Radios;
//	Podcasts* _radios = new Podcasts;
    // Initialise Widgets & Layouts
    GridLayout* lay_main = new GridLayout;
    BoxLayout* lay_sections = new BoxLayout;
    PlayerWidget* wid_player;
    SectionsSelector* _sectionsSelector;
    QueueWidget* wid_queue;
    ArtistsWidget* wid_artists;
    AlbumsWidget* wid_albums;
    GenresWidget* wid_genres;
    TracksWidget* wid_tracks;
    PlaylistsWidget* wid_playlists;
    LibrariesWidget* wid_libraries;
    RadiosWidget* wid_radios;
    PodcastsWidget* wid_podcasts;
    SettingsWidget* wid_settings;
    SystemControls* _systemControls;
    //
    QSpacerItem* _spacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Maximum );

public:
    ~Symphony()
    {
    }
    Symphony( QWidget *parent = 0 )
        : SWidget( parent )
    {
        setSettings( new Settings );
        wid_player = new PlayerWidget(this);
        _sectionsSelector = new SectionsSelector(this);
        wid_queue = new QueueWidget(this);
        wid_artists = new ArtistsWidget(this);
        wid_albums = new AlbumsWidget(this);
        wid_genres = new GenresWidget(this);
        wid_tracks = new TracksWidget(this);
        wid_playlists = new PlaylistsWidget(this);
        wid_libraries = new LibrariesWidget(this);
        wid_radios = new RadiosWidget(this);
        wid_podcasts = new PodcastsWidget(this);
        wid_settings = new SettingsWidget(this);
        _systemControls = new SystemControls(this);
        setLayout( lay_main );

        // Window Geometry
        setMinimumSize( 800, 500 );
        wid_player->setMinimumHeight( 50 );

        // Adding TrackLists to Widgets
        _queue = new TrackList( Files::QueueName, TrackList::QUEUE );
        _playlists = new Playlists;
        _libraries = new Libraries;
        wid_player->setTrackList( _queue );
        wid_queue->setTrackList( _queue );
        wid_artists->setLibraries( _libraries );
        wid_albums->setLibraries( _libraries );
        wid_tracks->setLibraries( _libraries );
        wid_genres->setLibraries( _libraries );
        wid_libraries->setLibraries( _libraries );

        wid_playlists->setPlaylists( _playlists );
        wid_queue->setPlaylists( _playlists );
        wid_artists->setPlaylists( _playlists );
        wid_albums->setPlaylists( _playlists );
        //	wid_genres->setPlaylists( _playlists );
        wid_tracks->setPlaylists( _playlists );
        wid_libraries->setPlaylists( _playlists );
        //	wid_radios->setTracklistList( _radios );
        //	wid_podcasts->setTracklistList( _podcasts );

        // Initialize SectionsSelector
        lay_sections->addWidget( _sectionsSelector );
        addSection( wid_queue );
        addSection( wid_artists, Keys::HideArtists );
        addSection( wid_albums, Keys::HideAlbums );
        addSection( wid_genres, Keys::HideGenres );
        addSection( wid_tracks, Keys::HideTracks );
        addSection( wid_playlists, Keys::HidePlaylists );
        addSection( wid_libraries, Keys::HideLibraries );
        addSection( wid_radios, Keys::HideRadios );
        addSection( wid_podcasts, Keys::HidePodcasts );
        _sectionsSelector->extraSettings();
        // Show Queue Widget at the beginning
        _sectionsSelector->showWidget( 0 );
        // adding wid_settings to SectionsSelector
        _sectionsSelector->addWidget( wid_settings );
        lay_sections->addWidget( wid_settings );
        // Connections between widgets
        connect( wid_queue, &QueueWidget::indexChanged,
                 wid_player, &PlayerWidget::setPlaylistIndex );
        connect( wid_player, &PlayerWidget::playlistChanged,
                 wid_queue, &QueueWidget::setPlaylist );
        connect( _systemControls, &SystemControls::onMinimize ,
                 this, &Symphony::showMinimized );
        connect( _systemControls, &SystemControls::onMaximize,
                 this, &Symphony::maximise );
        connect( wid_player, &PlayerWidget::moveWindow,
                 this, &Symphony::move );
        connect( _systemControls, &SystemControls::onSettings,
                 this, &Symphony::showSettings );
        wid_player->connectTo( wid_artists );
        wid_player->connectTo( wid_albums );
        wid_player->connectTo( wid_tracks );
        wid_player->connectTo( wid_playlists );
        wid_player->connectTo( wid_libraries );


        // -----------------------
        // -----------------------
        setMouseTracking( true );

//        connect( _settings, &Settings::valueChanged,
//                 this, &Symphony::settingsChanged );
        // Update Sidebar
//        setWindowIcon( _settings->icon( "Symphony" ) );
        wid_queue->setSettings( _settings );
        _libraries->setSettings( _settings );
        _playlists->setSettings( _settings );
//        _systemControls->setSettings( _settings );
//        _sectionsSelector->setSettings( _settings );
        _settings->applyTheme();
        updateLayout();
        updateIcons();
        updateLang();
        //
        if ( _settings->getBool( Keys::Fullscreen ) )
            setWindowState(windowState() | Qt::WindowFullScreen);
        else if ( _settings->getBool( Keys::Maximized ) )
            setWindowState(windowState() | Qt::WindowMaximized);
    }
    void addSection( QWidget* wid, const QString& key = QString() )
    {
        _sectionsSelector->addSection( wid, key );
        lay_sections->addWidget( wid );
    }

public slots:
    void showSettings()
    {
        if ( wid_settings->isHidden() )
        {
            wid_settings->show();
            _sectionsSelector->currentWidget()->hide();
        }
        else
        {
            wid_settings->hide();
            _sectionsSelector->currentWidget()->show();
        }
    }
    void updateSettings()
    {
        //	_settings->update();
        //	wid_player->updateIcons( _settings );
        //	wid_sidebar->updateIcons( _settings );
        //	wid_queue->updateIcons( _settings );
        //	wid_libraries->updateIcons( _settings );
    }
    void updateIcons() override
    {
        setWindowIcon( _settings->icon( "Symphony" ) );
        _sectionsSelector->updateIcons();
        wid_player->updateIcons();
        _systemControls->updateIcons();
//        wid_queue->updateIcons( _icons );
//        wid_artists->updateIcons( _icons );
//        wid_albums->updateIcons( _icons );
//        wid_tracks->updateIcons( _icons );
//        wid_playlists->updateIcons( _icons );
//        wid_libraries->updateIcons( _icons );
    }
    void updateLang() override
    {
//        _lang->update();

    }
    void move( const QPoint& pos )
    {
        if ( _lastPos.x() != -1 && pos.x() != -1 && !isMaximized() )
        {
            SWidget::move( this->pos() - _lastPos + pos );
        }
        _lastPos = pos;
    }
    void closeEvent( QCloseEvent* event ) override
    {
        SWidget::closeEvent( event );
        //	qDebug().noquote() << "";
        //	qDebug().noquote() << "--------------";
        //    qDebug().noquote() << "Saving...";
        //	QElapsedTimer timer;
        //    timer.start();
        _queue->save();
        _playlists->save();
        for ( TrackList* playlist : _playlists->list() )
            playlist->save();
        _libraries->saveLibraries();
        //	QString time = QString::number( timer.elapsed() );
        //	time.insert( time.length()-3, "s " );
        //	time += "ms";
        //	qDebug().noquote() << "Time to save Libraries & Playlists : " + time;
        //	qDebug().noquote() << "--------------";
    }
    void maximise()
    {
        isMaximized() ? showNormal() : showMaximized();
        _settings->save( isMaximized(), Keys::Maximized );
    }
    void updateLayout() override
    {
        QString playerPos = _settings->getString( Keys::Player, Keys::Position );
        lay_main->removeWidget( wid_player );
        lay_main->removeWidget( _systemControls );
        lay_main->removeItem( lay_sections );
        lay_main->removeItem( _spacer );
        if ( playerPos == 't' )
        {
            lay_main->addWidget( wid_player, 0, 0, 1, 1 );
            lay_main->addLayout( lay_sections, 1, 0, 1, 2 );
            lay_main->addWidget( _systemControls, 0, 1, 1, 1 );
//            lay_main->setAlignment( wid_player, Qt::AlignCenter );
        }
        else if ( playerPos == 'b' )
        {
            lay_main->addWidget( wid_player, 2, 0, 1, 2 );
            lay_main->addLayout( lay_sections, 1, 0, 1, 2  );
            lay_main->addWidget( _systemControls, 0, 1, 1, 1 );
            lay_main->addItem( _spacer, 0, 0);
//            lay_main->setAlignment( wid_player, Qt::AlignVCenter );
        }
        else if ( playerPos == 'l' )
        {
            lay_main->addWidget( wid_player, 0, 0, 2, 1 );
            lay_main->addLayout( lay_sections, 1, 1, 1, 2 );
            lay_main->addWidget( _systemControls, 0, 2, 1, 1 );
            lay_main->addItem( _spacer, 0, 1);
            lay_main->setAlignment( wid_player, Qt::AlignTop );
        }
        else
        {
            lay_main->addWidget( wid_player, 1, 1, 1, 2 );
            lay_main->addLayout( lay_sections, 0, 0, 2, 1 );
            lay_main->addWidget( _systemControls, 0, 2, 1, 1 );
            lay_main->addItem( _spacer, 0, 1 );
            lay_main->setAlignment( wid_player, Qt::AlignTop );
        }
        lay_main->setAlignment( _systemControls, Qt::AlignVCenter );
        QString sectionsPos = _settings->getString( Keys::SectionsSelector, Keys::Position );
        if ( sectionsPos == 't' )
        {
            lay_sections->setDirection( QBoxLayout::TopToBottom );
            _sectionsSelector->setHorizontal();
        }
        else if ( sectionsPos == 'b' )
        {
            lay_sections->setDirection( QBoxLayout::BottomToTop );
            _sectionsSelector->setHorizontal();
        }
        else if ( sectionsPos == 'l' )
        {
            lay_sections->setDirection( QBoxLayout::LeftToRight );
            _sectionsSelector->setVertical();
        }
        else
        {
            lay_sections->setDirection( QBoxLayout::RightToLeft );
            _sectionsSelector->setVertical();
        }
    }
    void settingsChanged( const QString& key ) override
    {
        if ( key == Settings::fullkey( Keys::Player, Keys::Position ) ||
             key == Settings::fullkey( Keys::Player, Keys::Size ) ||
             key == Settings::fullkey( Keys::SectionsSelector, Keys::Position ) )
        {
            updateLayout();
        }
        else if ( key == Settings::fullkey( Keys::Appearance, Keys::Theme ) )
        {

        }
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WIDGET_H
