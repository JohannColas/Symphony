QT       += core gui multimedia xml concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp

HEADERS += \
    AlbumsWidget/AlbumBanner.h \
    AlbumsWidget/AlbumInfoWidget.h \
    AlbumsWidget/AlbumItem.h \
    AlbumsWidget/AlbumTracksWidget.h \
    ArtistsWidget/ArtistBanner.h \
    ArtistsWidget/ArtistInfoWidget.h \
    ArtistsWidget/ArtistItem.h \
    ArtistsWidget/ArtistTracksWidget.h \
    BasicWidgets/AlbumLabel.h \
    BasicWidgets/AppButton.h \
    BasicWidgets/ArtistLabel.h \
    AlbumsWidget/AlbumSelector.h \
    AlbumsWidget/AlbumWidget.h \
    ArtistsWidget/ArtistWidget.h \
    BasicWidgets/ColorSelector.h \
    BasicWidgets/CountLabel.h \
    BasicWidgets/CoverImage.h \
    BasicWidgets/DescriptionText.h \
    BasicWidgets/DurationLabel.h \
    BasicWidgets/ElideLabel.h \
    BasicWidgets/FanartImage.h \
    BasicWidgets/GenreLabel.h \
    BasicWidgets/LibraryLabel.h \
    BasicWidgets/LineEdit.h \
    BasicWidgets/LockedButton.h \
    BasicWidgets/LyricsText.h \
    BasicWidgets/NumberLabel.h \
    BasicWidgets/PathButton.h \
    BasicWidgets/PlayMenu.h \
    BasicWidgets/PlayerButton.h \
    BasicWidgets/PlayerOptionButton.h \
    BasicWidgets/PlaylistEdit.h \
    BasicWidgets/PlaylistLabel.h \
    BasicWidgets/SWidget.h \
    BasicWidgets/ScrollArea.h \
    BasicWidgets/SectionButton.h \
    BasicWidgets/SystemButton.h \
    BasicWidgets/SystemControls.h \
    BasicWidgets/TabButton.h \
    BasicWidgets/TabWidget.h \
    BasicWidgets/Text.h \
    BasicWidgets/TextWidget.h \
    BasicWidgets/ThumbImage.h \
    BasicWidgets/TitleLabel.h \
    BasicWidgets/TrackItem.h \
    BasicWidgets/WebsiteLabel.h \
    Commons/Album.h \
    Commons/AlbumInfo.h \
    Commons/Artist.h \
    BasicWidgets/Frame.h \
    BasicWidgets/Label.h \
    BasicWidgets/Layouts.h \
    BasicWidgets/ListItem.h \
    BasicWidgets/ListWidget.h \
    Commons/ArtistInfo.h \
    Commons/Files.h \
    Commons/Libraries.h \
    Commons/Library.h \
    Commons/Network.h \
    Commons/OnLineMetadata.h \
    Commons/Playlists.h \
    Commons/Settings.h \
    Commons/Shorcuts.h \
    Commons/Sorting.h \
    Commons/TrackInfo.h \
    GenresWidget/GenreSelector.h \
    GenresWidget/GenreWidget.h \
    LibrariesWidget/LibraryFilesWidget.h \
    LibrariesWidget/LibraryItem.h \
    LibrariesWidget/LibraryListItem.h \
    LibrariesWidget/LibraryWidget.h \
    LyricsWidget/LyricsWidget.h \
    QueueWidget/QueueWidget.h \
    PlayerWidget/PlayerControl.h \
    PlayerWidget/PlayerControls.h \
    PlayerWidget/PlayerSubControl.h \
    PlayerWidget/PlayerSubControls.h \
    PlayerWidget/PlayerTrackBanner.h \
    PlaylistsWidget/PlaylistBanner.h \
    PlaylistsWidget/PlaylistItem.h \
    PodcastsWidget/PodcastSelector.h \
    PodcastsWidget/PodcastsWidget.h \
    RadiosWidget/RadioSelector.h \
    RadiosWidget/RadiosWidget.h \
    AlbumsWidget/AlbumsWidget.h \
    ArtistsWidget/ArtistsWidget.h \
    ArtistsWidget/ArtistSelector.h \
    Commons/Metadata.h \
    Commons/Track.h \
    Commons/TrackList.h \
    Commons/TrackListModifier.h \
    GenresWidget/GenresWidget.h \
    LibrariesWidget/LibrariesWidget.h \
    LibrariesWidget/LibraryList.h \
    LibrariesWidget/LibrarySelector.h \
    PlayerWidget/PlayerWidget.h \
    PlaylistsWidget/PlaylistList.h \
    PlaylistsWidget/PlaylistSelector.h \
    PlaylistsWidget/PlaylistsWidget.h \
    SectionsSelector/SectionsSelector.h \
    SettingsWidget/SettingsWidget.h \
    FrameLess.h \
    Symphony.h \
    TracksWidget/TrackBanner.h \
    TracksWidget/TrackInfoWidget.h \
    TracksWidget/TrackSelector.h \
    TracksWidget/TrackWidget.h \
    TracksWidget/TracksWidget.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += \
    /usr/local/include/taglib
DEPENDPATH += /usr/local/include/taglib

LIBS += \
    -L/usr/local/lib \
    -ltag

INCLUDEPATH += \
    /usr/local/include
DEPENDPATH += /usr/local/include
LIBS += \
    -L/usr/local/lib \
    -lz

