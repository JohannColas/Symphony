#ifndef TRACKWIDGET_H
#define TRACKWIDGET_H
/**********************************************/
#include "BasicWidgets/SWidget.h"
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QMediaPlayer>
#include "Commons/Metadata.h"
#include "Commons/Track.h"
#include "Commons/Libraries.h"
/**********************************************/
#include "TracksWidget/TrackBanner.h"
#include "TracksWidget/TrackInfoWidget.h"
#include "BasicWidgets/CoverImage.h"
#include "BasicWidgets/TitleLabel.h"
#include "BasicWidgets/ArtistLabel.h"
#include "BasicWidgets/TabWidget.h"
#include "BasicWidgets/LyricsText.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TrackWidget
        : public SWidget
{
	Q_OBJECT
protected:
	TrackList* _tracklist = 0;
	QMediaPlayer* _player = 0;
    TrackBanner* ban_track;
    TabWidget* tab_menu;
    TrackInfoWidget* inf_track;
    LyricsText* txt_lyrics;
    GridLayout* lay_main = new GridLayout;


public:
	~TrackWidget()
	{
	}
    TrackWidget( SWidget* parent = 0 )
        : SWidget( parent )
	{
		show();
        setLayout( lay_main );
        ban_track = new TrackBanner(this);
        tab_menu = new TabWidget(this);
        inf_track = new TrackInfoWidget(this);
        txt_lyrics = new LyricsText(this);
        lay_main->addWidget( ban_track, 0, 0 );
        lay_main->addWidget( tab_menu, 1, 0 );
		tab_menu->addTab( "Info", inf_track );
		tab_menu->addTab( "Lyrics", txt_lyrics );
		tab_menu->changeTab(0);

		Network* network = new Network;
		connect( network, &Network::downloaded,
				 inf_track, &TrackInfoWidget::showDownload );
        network->download( QString("https://en.wikipedia.org/wiki/ID3#ID3v2") );

    }

public slots:
	void setLibraries( Libraries* libraries = 0 )
	{
		if ( libraries )
		{
			_tracklist = libraries->toTracklist();
		}
	}
	void setPlayer( QMediaPlayer* player )
	{
		_player = player;
		connect( player, SIGNAL(metaDataChanged()),
				 this, SLOT(update()) );
	}
	void setTrack( Track* track )
	{
		ban_track->setTrack( track );
		inf_track->setTrack( track );
		txt_lyrics->setTrack( track );
	}
	void update()
	{
		if ( _player )
		{
		}
	}
    void updateIcons()
    {
	}
//    void settingsChanged() override
//    {
//        ban_track->setSettings( _sets );
//    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACKWIDGET_H
