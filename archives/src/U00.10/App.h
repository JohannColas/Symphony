#ifndef APP_H
#define APP_H

#include "Commons/Settings.h"
#include "Commons/Theme.h"
#include "Commons/TrackList.h"
#include "Commons/Playlists.h"
#include "Commons/Libraries.h"

#include "Commons/Player.h"

#include <QElapsedTimer>

class App
{
	static inline QString _settingsName = "settings.ini";
	static inline Settings* _settings = 0;
	static inline Theme* _theme = 0;
	static inline Icons* _icons = 0;
	static inline Lang* _lang = 0;
	//
	static inline Player* _player = 0;
	// Initialise TrackLists
	static inline TrackList* _queue = 0;
	static inline Playlists* _playlists = 0;
	static inline Libraries* _libraries = 0;
//	static inline Radios* _radios = 0;
//	static inline Podcasts* _podcasts = 0;

public:
    static inline void init()
	{
		qDebug().noquote() << "";
		qDebug().noquote() << "--------------";
		qDebug().noquote() << "App Class Initialisation";
		// Settings initialisation
		if ( _settings )
			delete _settings;
		_settings = new Settings;
		// Theme initialisation
		if ( _theme )
			delete _theme;
		_theme = new Theme;
		_theme->setPath( Theme::availableThemes().first().absoluteFilePath(), true );
		qDebug() << "Theme path :" << Theme::availableThemes().first().absoluteFilePath();
		// Icon initialisation
		Icons::setPath( Icons::availableIconPacks().first().absoluteFilePath() );
		qDebug() << "Icon path :" << Icons::availableIconPacks().first().absoluteFilePath();
		//
		_player = new Player;
		// Libraries initialisation
		if ( _libraries )
			delete _libraries;
		_libraries = new Libraries;
		// Queue initialisation
		if ( _queue )
			delete _queue;
		_queue = new TrackList( Files::QueueName, TrackList::QUEUE );
		// Playlists initialisation
		if ( _playlists )
			delete _playlists;
		_playlists = new Playlists;
		// Radios initialisation
		// Podcasts initialisation
		qDebug().noquote() << "--------------";
		qDebug().noquote() << "";
	}
    static inline void save()
	{
		qDebug().noquote() << "";
		qDebug().noquote() << "--------------";
		qDebug().noquote() << "App Class";
		qDebug().noquote() << "Saving...";
		QElapsedTimer timer;
		timer.start();
		_queue->save();
		_playlists->save();
		_libraries->saveLibraries();
//		_libraries->updateLibrary();
		QString time = QString::number( timer.elapsed() );
		time.insert( time.length()-3, "s " );
		time += "ms";
		qDebug().noquote() << "Time to save Libraries & Playlists : " + time;
		qDebug().noquote() << "--------------";
		qDebug().noquote() << "";
	}
	static inline Settings* settings()
	{
		return _settings;
	}
	static inline QVariant settings( const QString& key )
	{
		return _settings->value( key );
	}
	static inline Theme* theme()
	{
		return _theme;
	}
	static inline Player* player()
	{
		return _player;
	}
	static inline TrackList* queue()
	{
		return _queue;
	}
    static inline Playlists* playlists()
	{
		return _playlists;
	}
    static inline Libraries* librairies()
	{
		return _libraries;
	}
};

#endif // APP_H
