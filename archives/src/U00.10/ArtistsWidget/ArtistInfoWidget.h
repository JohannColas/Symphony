#ifndef ARTISTINFOWIDGET_H
#define ARTISTINFOWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "BasicWidgets/TextWidget.h"
#include "BasicWidgets/WebsiteLabel.h"
/**********************************************/
#include "Commons/ArtistInfo.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistInfoWidget
		: public Frame
{
	Q_OBJECT
private:
	TextWidget* txt_biography = new TextWidget;
	WebsiteLabel* lb_website = new WebsiteLabel;


public:
	ArtistInfoWidget( QWidget* parent = 0 )
		: Frame( parent )
	{

		addWidget( txt_biography, 0, 0 );
		addWidget( lb_website, 1, 0 );
		txt_biography->setAlignment( Qt::AlignJustify );
//		lb_website->setTextFormat( Qt::RichText );
//		lb_website->setTextInteractionFlags( Qt::TextBrowserInteraction );
//		lb_website->setOpenExternalLinks(true);
	}

public slots:
	void setArtistInfo( ArtistInfo* info = 0 )
	{
		if ( info )
		{
			txt_biography->setHtml( info->biography() );
			lb_website->setText( info->website() );
		}
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTINFOWIDGET_H
