#ifndef ARTISTWIDGET_H
#define ARTISTWIDGET_H
/**********************************************/
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "BasicWidgets/TabWidget.h"
/**********************************************/
#include "ArtistsWidget/ArtistBanner.h"
#include "ArtistsWidget/ArtistInfoWidget.h"
/**********************************************/
#include "Commons/Libraries.h"
#include "Commons/ArtistInfo.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistWidget
		: public Frame
{
	Q_OBJECT
protected:
	Libraries* _libraries = 0;
	Artist* _artist = 0;
	ArtistInfo* _artistInfo = new ArtistInfo;
	ArtistBanner* inf_artist = new ArtistBanner;
	TabWidget* tab_menu = new TabWidget;
	ArtistInfoWidget* info_artist = new ArtistInfoWidget;

public:
	ArtistWidget( QWidget* parent = nullptr )
		: Frame( parent )
	{
		show();
		addWidget( inf_artist, 0, 0 );
		addWidget( tab_menu, 1, 0 );
//		tab_menu->addTab( "Tracks", wid_tracks );
		tab_menu->addTab( "Info", info_artist );
		tab_menu->changeTab( 0 );

//		connectTo( wid_tracks );
	}
//	void setPlaylists( Playlists* playlists )
//	{
//		wid_tracks->setPlaylists( playlists );
//	}

public slots:
//	void setLibraries( Libraries* libraries = 0 )
//	{
//		_libraries = libraries;
//	}
//	void setArtist( const QString& /*artist*/ )
//	{
//		update();
//	}
//	void setArtist( Artist* artist )
//	{
//		_artist = artist;
//		inf_artist->setArtist( artist );
////		wid_tracks->setArtist( artist );
//		_artistInfo->setArtist( artist );
//		_artistInfo->thumb();
//		info_artist->setArtistInfo( _artistInfo );
//		update();
//	}
//	void setArtistByIndex( int index )
//	{
//		if ( -1 < index && index < _libraries->artists().size() )
//		{
//			setArtist( _libraries->artists().at(index) );
//		}
//	}
//	void update()
//	{
//	}
//    void updateIcons()
//    {
////		inf_artist->updateIcons( icons );
////		wid_tracks->updateIcons( icons );
//	}

//signals:
//	void trackToPlay( Track* track );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTWIDGET_H
