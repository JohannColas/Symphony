#ifndef ARTISTSWIDGET_H
#define ARTISTSWIDGET_H
/**********************************************/
#include "BasicWidgets/SWidget.h"
#include "Commons/Playlists.h"
#include "BasicWidgets/ListView.h"
#include "ArtistsWidget/ArtistWidget.h"
#include "Commons/Libraries.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistsWidget
        : public SWidget
{
	Q_OBJECT
private:
	Libraries* _libraries = 0;
//    ArtistSelector* sel_artist;
//	ArtistWidget* wid_artist = new ArtistWidget;
	GridLayout* lay_main = new GridLayout;
	ListView* ls_vie = new ListView;
	ListView* ls_vie2 = new ListView;

public:
    ArtistsWidget( SWidget* parent = 0 )
        : SWidget( parent )
	{
		hide();
        setLayout( lay_main );

//		sel_artist = new ArtistSelector;

//        lay_main->addWidget( sel_artist, 0, 0 );
//		lay_main->addWidget( wid_artist, 0, 1 );
		lay_main->addWidget( ls_vie, 0, 0 );
		lay_main->addWidget( ls_vie2, 0, 1 );
		ls_vie2->setType( Type::TrackList );
//		connectTo( wid_artist );
	}
	void setPlaylists( Playlists* /*playlists*/ )
	{
//		wid_artist->setPlaylists( playlists );
	}

public slots:
	void onTypeChanged( int ind )
	{
		if ( ind == 1 )
			ls_vie->setType( Type::ArtistList );
		else if ( ind == 2 )
			ls_vie->setType( Type::AlbumList );
		else if ( ind == 4 )
			ls_vie->setType( Type::TrackList );
	}
//	void setLibraries( Libraries* libraries = 0 )
//	{
//		_libraries = libraries;
////		ls_vie->setArtists( {} );
////		ls_vie->setAlbums( {} );
//		ls_vie->setType( Type::TrackList );
////		sel_artist->setLibraries( _libraries );
////		wid_artist->setLibraries( _libraries );
////		ls_vie->setArtists( _libraries->artists() );
////		connect( sel_artist, &ArtistSelector::indexToChange,
////				 wid_artist, &ArtistWidget::setArtistByIndex );
//	}
//    void updateIcons()
//	{
////		sel_artist->updateIcons( icons );
////		wid_artist->updateIcons( icons );
//	}
//    void updateLang()
//	{
//	}
	//
//	void sendTrackToPlay( Track* track )
//	{
//		emit trackToPlay( track );
//	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTSWIDGET_H
