#ifndef ARTISTLABEL_H
#define ARTISTLABEL_H
/**********************************************/
#include "BasicWidgets/Text.h"
#include "BasicWidgets/LockedButton.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ArtistLabel
		: public Text
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	ArtistLabel( QWidget* parent = 0 )
		: Text( parent )
	{
		allowElide();
	}
	ArtistLabel( const QString& text, QWidget* parent = 0 )
		: Text( text, parent )
	{
		allowElide();
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTISTLABEL_H
