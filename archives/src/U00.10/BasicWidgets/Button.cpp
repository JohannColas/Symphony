#include "Button.h"
/**********************************************/
#include <QMouseEvent>
#include <QPainter>
#include "App.h"
/**********************************************/
/**********************************************/
Button::Button( QWidget* parent )
	: Widget( parent )
{
	setType( Type::SimpleButton );
}
/**********************************************/
/**********************************************/
Button::Button( const QString& type, QWidget* parent )
	: Widget( parent )
{
	setType( type );
}
/**********************************************/
/**********************************************/
Button::Button( const QString& text, const QString& type, QWidget* parent )
	: Widget( parent )
{
	setType( type );
	setText( text );
}
/**********************************************/
/**********************************************/
Button::Button( const QString& text, const QString& icon, const QString& type, QWidget* parent )
	: Widget( parent )
{
	setType( type );
	setText( text );
	setIcon( icon );
}
/**********************************************/
/**********************************************/
void Button::setText( const QString& text )
{
	_text = text;
}
/**********************************************/
/**********************************************/
void Button::setIcon( const QString& icon )
{
	_icon = icon;
}
/**********************************************/
/**********************************************/
void Button::setCheckable( bool checkable )
{
	_checkable = checkable;
	setChecked( false );
}
/**********************************************/
/**********************************************/
bool Button::isCheckable() const
{
	return _checkable;
}
/**********************************************/
/**********************************************/
void Button::setChecked( bool checked )
{
	_checked = checked;
	updateState();
}
/**********************************************/
/**********************************************/
bool Button::isChecked() const
{
	return _checked;
}
/**********************************************/
/**********************************************/
void Button::updateState()
{
	if ( isChecked() )
		_state = State::Checked;
	else if ( isHovered() )
		_state = State::Hovered;
	else
		_state = State::Normal;
	repaint();
}
/**********************************************/
/**********************************************/
void Button::setAction( const Button::Action& action )
{
	_ac = action;
}
/**********************************************/
/**********************************************/
void Button::mouseReleaseEvent( QMouseEvent* event )
{
	if ( event->button() == Qt::LeftButton )
	{
		if ( _ac == Action::NoAction )
		{
			if ( isCheckable() )
			{
				setChecked( !isChecked() );
//				emit checked( _action, isChecked() );
				emit sendAction( Action::Checked, isChecked() );
			}
			else
			{
				emit sendAction( Action::Released, QVariant() );
			}
		}
		else
		{
			if ( _ac == Action::AddToPlaylist )
				emit sendAction( _ac, _text );
			else
				emit sendAction( _ac, QVariant() );
		}
	}
}
/**********************************************/
/**********************************************/
void Button::paintEvent( QPaintEvent* event )
{
	Widget::paintEvent( event );
	QPainter paint( this );
	paint.setRenderHint( QPainter::Antialiasing, true );
	Font ft_text = App::theme()->font( _type, ThemeKeys::Text+"."+ThemeKeys::Font+"."+_state );
	// Icon
	if ( _style != Button::TextOnly )
	{
		QPixmap icon;
		if ( _icon.isEmpty() )
		{

		}
		else
		{

		}
		if ( !icon.isNull() )
			paint.drawPixmap( 0, 0, 48, 48, icon );
	}
	// Text
	if ( _style != Button::IconOnly )
	{
		QString elidedText = _text;
		paint.setFont( ft_text );
		paint.setPen( ft_text.pen() );
		if ( !elidedText.isEmpty() )
			paint.drawText( 0, 20, elidedText );
	}
}
/**********************************************/
/**********************************************/
