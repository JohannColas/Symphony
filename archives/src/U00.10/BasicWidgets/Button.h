#ifndef BUTTON_H
#define BUTTON_H
/**********************************************/
#include "Widget.h"
/**********************************************/
namespace Type {
	static inline QString SimpleButton = "SimpleButton";
	static inline QString PlayerButton = "PlayerButton";
	static inline QString SectionButton = "SectionButton";
	static inline QString AppButton = "AppButton";
	static inline QString SelectorButton = "SelectorButton";
	static inline QString ComboButton = "ComboButton";
	static inline QString CheckBox = "CheckBox";
}
//namespace Action {
//	static inline QString Artist = "Artist";
//	static inline QString Album = "Album";
//	static inline QString Track = "Track";
//	static inline QString Gender = "Gender";
//	static inline QString Playlist = "Playlist";
//	static inline QString Library = "Library";
//	static inline QString Radio = "Radio";
//	static inline QString Podcast = "Podcast";
//}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Button
		: public Widget
{
	Q_OBJECT
public:
	enum Style {
		TextBesideIcon = 2,
		TextUnderIcon = 3,
		TextOnly = 5,
		IconOnly = 7,
	};
	enum Mode {
		AdjustToContent = 2,
		DontAdjust = 3,
	};
	enum Action {
		NoAction,
		PlayNow,
		AddToQueue,
		PlayAndAddToQueue,
		InsertToQueue,
		AddToPlaylist,
		Remove,
		Checked,
		Released,
		ShowMenu,
	};
private:
//	QString _type = Type::SimpleButton;
	Button::Style _style = Button::TextBesideIcon;
	Button::Mode _mode = Button::DontAdjust;
	Button::Action _ac = Action::NoAction;
	/**********************************************/
	QString _text = QString();
	QString _icon = QString();
	/**********************************************/
	bool _checkable = false;
	bool _checked = false;
	/**********************************************/
public:
	Button( QWidget* parent = 0 );
	Button( const QString& type, QWidget* parent = 0 );
	Button( const QString& text, const QString& type, QWidget* parent = 0 );
	Button( const QString& text, const QString& icon, const QString& type, QWidget* parent = 0 );
	/**********************************************/
	void setAction( const Button::Action& action );
	void setText( const QString& text );
	void setIcon( const QString& icon );
	/**********************************************/
	void setCheckable( bool checkable );
	bool isCheckable() const;
	void setChecked( bool checked );
	bool isChecked() const;
	/**********************************************/
	virtual void updateState() override;
	/**********************************************/
protected slots:
	virtual void mouseReleaseEvent( QMouseEvent* event ) override;
	/**********************************************/
	virtual void paintEvent( QPaintEvent* event ) override;
	/**********************************************/
signals:
//	void released( const QString& action );
//	void checked( const QString& action, bool checked );
	void sendAction( const Button::Action& action, const QVariant& value );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // BUTTON_H
