#include "ListItem.h"
#include "App.h"
/**********************************************/
/**********************************************/
ListItem::~ListItem()
{

}
/**********************************************/
/**********************************************/
ListItem::ListItem( QWidget* parent )
	: Widget( parent )
{
	_type = Type::ListItem;
	show();
	//	setLayout( lay_main );

	setSizePolicy( QSizePolicy::Preferred,
				   QSizePolicy::Maximum );
}
/**********************************************/
/**********************************************/
void ListItem::setArtist( Artist* artist )
{
	_type = Type::ArtistItem;
	_artist = artist;
	_album = 0;
	_track = 0;
	repaint();
}
/**********************************************/
/**********************************************/
void ListItem::setAlbum( Album* album )
{
	_type = Type::AlbumItem;
	_artist = 0;
	_album = album;
	_track = 0;
}
/**********************************************/
/**********************************************/
void ListItem::setTrack( Track* track )
{
	_type = Type::TrackItem;
	_artist = 0;
	_album = 0;
	_track = track;
}
/**********************************************/
/**********************************************/
void ListItem::setIndex( int index )
{
	_index = index;
}
/**********************************************/
/**********************************************/
int ListItem::index()
{
	return _index;
}
/**********************************************/
/**********************************************/
void ListItem::setChecked( bool checked )
{
	_checked = checked;
	updateState();
}
/**********************************************/
/**********************************************/
bool ListItem::isChecked()
{
	return _checked;
}
/**********************************************/
/**********************************************/
void ListItem::setLayout( const QString& layout )
{
	_layout = layout;
}
/**********************************************/
/**********************************************/
void ListItem::updateState()
{
	if ( isHovered() )
		_state = State::Hovered;
	else if ( isChecked() )
		_state = State::Checked;
	else
		_state = State::Normal;
	repaint();
}
/**********************************************/
/**********************************************/
void ListItem::addWidget( QWidget* widget, int row, int col, int rowSpan, int colSpan )
{
	lay_main->addWidget( widget, row, col, rowSpan, colSpan );
}
/**********************************************/
/**********************************************/
void ListItem::mousePressEvent( QMouseEvent* event )
{
	if ( event->button() == Qt::LeftButton )
	{
		setChecked( !isChecked() );
		//    repaint();
		//    if ( isChecked() )
		emit selected( _index, isChecked() );
	}
	else if ( event->button() == Qt::RightButton )
	{
		QPoint pos = this->mapToGlobal( event->pos() );
		emit showMenu( {pos.x(), pos.y(), 0, 0} );
	}
	//	else
	//		emit unselected( _index );

	//		int nb = 1000;
	//		QElapsedTimer timer;
	//		timer.start();
	//		for ( int it = nb; it > 0; --it )
	//		{
	//			repaint();
	//		}
	//		qDebug() << "ListItem - "+QString("%1").arg(nb)+" PaintEvents realise in :" << timer.elapsed();
	event->ignore();
}
/**********************************************/
/**********************************************/
void ListItem::mouseDoubleClickEvent( QMouseEvent* /*event*/ )
{
	emit play( _index );
}
/**********************************************/
/**********************************************/
void ListItem::paintEvent( QPaintEvent* event )
{
	// 2021.06.26 - PaintEvent in ~0.2ms
	// 2021.08.07 - PaintEvent in ~1ms
	// 2021. - PaintEvent in ~ms
	//		QElapsedTimer timer;
	//		timer.start();
	Widget::paintEvent( event );

	//Icon
	int iconsize = 48;
	Q_UNUSED( iconsize );
	// Title
	Font ft_title = /*App::theme()->title();//*/App::theme()->font( _type, "title." + ThemeKeys::Font + "." + _state );
	//	App::theme()->font("");
	// Left Info
	Font ft_infol = /*App::theme()->infoL();//*/App::theme()->font( _type, "infoL." + ThemeKeys::Font + "." + _state );
	// Right Info
	Font ft_infor;
	// Prefix
	Font ft_prefix;
	// Suffix
	Font ft_suffix;
	// Initialize painter
	QPainter paint(this);
	paint.setRenderHint( QPainter::Antialiasing, true );
	if ( _asize.width() > 0 && _asize.height() > 0 )
		paint.setClipRect( 0, 0, _asize.width(), _asize.height(), Qt::IntersectClip );
	// --------------------------------
	// Build Title
	QString title;
	QString infoL;
	QIcon icon;
	if ( _artist )
	{
		title = _artist->name();
		infoL = QString::number( _artist->nbAlbums() ) + " albums";
		icon = Icons::get( IconsKeys::Artist );

	}
	if ( _album )
	{
		title = _album->title();
		infoL = QString::number( _album->nbTracks() ) + " tracks";
		icon = Icons::get( IconsKeys::Album );
	}
	if ( _track )
	{
		title = _track->title();
		infoL = _track->album()->artist2()->name();
		icon = Icons::get( IconsKeys::Track );
	}
	if ( title.isEmpty() )
		title = "[UNKNOWN]";
	if ( infoL.isEmpty() )
		infoL = "[UNKNOWN]";

	QFontMetrics metrix = QFontMetrics( ft_title);
	Spacing spg = /*App::theme()->spacing();//*/App::theme()->spacing( _type, ThemeKeys::Spacing );
	int icSize = iconsize;// availableContentRect().height();
//	if ( _layout == Layout::InfoUnderIcon )
//		icSize -= metrix.height() + spg.vertical();

	int cww = availableContentRect().width();
	if ( _layout == Layout::InfoBesideIcon )
		cww -= icSize + spg.horizontal();
	while ( cww < metrix.horizontalAdvance( title ) )
		title = title.left( title.length()-4 ) + "...";

	QRect iconrect = {availableContentRect().x(), availableContentRect().y(), icSize, icSize};
	QPoint titlepos = {iconrect.right()+spg.horizontal(), 25};
	QPoint infoLpos = {iconrect.right()+spg.horizontal(), 45};
	if ( _layout == Layout::InfoUnderIcon )
	{
		iconrect = {int(0.5*(width()-icSize)), availableContentRect().y(), icSize, icSize};
		titlepos = {availableContentRect().x(), iconrect.bottom()+spg.vertical()+metrix.ascent()};
	}
	else if ( _layout == Layout::InfoOverIcon )
	{
		iconrect = {int(0.5*(width()-icSize)), availableContentRect().y(), icSize, icSize};
		titlepos = {availableContentRect().x(), iconrect.bottom()-metrix.descent()};
	}
	// Draw Icon
	paint.drawPixmap( iconrect, icon.pixmap(iconrect.size()) );
	// Draw Title
	paint.setPen( ft_title.pen() );
	paint.setFont( ft_title/*.font()*/ );
	paint.drawText( titlepos, title );
	// Draw Left Info
	if ( _layout == Layout::InfoBesideIcon )
	{
		paint.setPen( ft_infol.pen() );
		paint.setFont( ft_infol/*.font()*/ );
		paint.drawText( infoLpos, infoL );
	}


//	QMargins margins = App::theme()->margins( type(), ThemeKeys::Margins );
//	if ( _layout == Layout::InfoBesideIcon )
//	{
//		setFixedHeight( margins.top()+margins.bottom()+iconsize	);
//	}
//	else if ( _layout == Layout::InfoBesideIcon )
//	{
//		int size = margins.top()+margins.bottom()+iconsize+spg.vertical()+metrix.height();
//		setFixedSize( size, size );
//	}
//	else if ( _layout == Layout::InfoOverIcon )
//	{
//		int size = margins.top()+margins.bottom()+iconsize;
//		setFixedSize( size, size );
//	}
	//	qDebug() << title << _asize;
	// --------------------------------
	// Build Icon
	//		  paint.drawPixmap();
	//		  paint.drawImage();
	// --------------------------------
	// Build InfoL
	// --------------------------------
	// Build InfoR
	// --------------------------------
	// Build Prefix
	// --------------------------------
	// Build Suffix
	// --------------------------------
	//		qDebug() << "ListItem - PaintEvent realise in :" << timer.elapsed();
}
/**********************************************/
/**********************************************/
