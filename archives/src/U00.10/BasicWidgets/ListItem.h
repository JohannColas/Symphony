#ifndef LISTITEM_H
#define LISTITEM_H
/**********************************************/
#include "BasicWidgets/Widget.h"
#include <QWidget>
#include <QGridLayout>
#include <QMouseEvent>
#include <QDebug>
#include <QPainter>
#include <QPainterPath>
#include <QElapsedTimer>
#include "Commons/Artist.h"
/**********************************************/
namespace Type {
	static inline QString ListItem = "ListItem";
	static inline QString FileItem = "FileItem";
	static inline QString ArtistItem = "ArtistItem";
	static inline QString AlbumItem = "AlbumItem";
	static inline QString TrackItem = "TrackItem";
	static inline QString GenderItem = "GenderItem";
	static inline QString PlaylistItem = "PlaylistItem";
	static inline QString LibraryItem = "LibraryItem";
	static inline QString RadioItem = "RadioItem";
	static inline QString PodcastItem = "PodcastItem";
}
namespace Layout {
	static inline QString InfoBesideIcon = "InfoBesideIcon";
	static inline QString InfoUnderIcon = "InfoUnderIcon";
	static inline QString InfoOverIcon = "InfoOverIcon";
}
namespace DetailMode {
	static inline QString FullDetailed = "FullDetailed";
	static inline QString Detailed = "Detailed";
	static inline QString Minimalist = "Minimalist";
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ListItem
		: public Widget
{
	Q_OBJECT
protected:
	QGridLayout* lay_main = new QGridLayout;
	QString _layout = Layout::InfoBesideIcon;
	bool _checked = false;
    int _index = -1;
    // Stored data
    Artist* _artist = 0;
    Album* _album = 0;
	Track* _track = 0;
    // Data to show
//    QString _prefix;
//    QString _title;
//    QString _infoL;
//    QString _infoR;
//    QString _suffix;
//    QPixmap _icon;
    /**********************************************/
public:
    ~ListItem();
    /**********************************************/
    ListItem( QWidget* parent = 0 );
    /**********************************************/
    void setArtist( Artist* artist );
    /**********************************************/
    void setAlbum( Album* album );
    /**********************************************/
	void setTrack( Track* track );
	/**********************************************/
	void setIndex( int index );
	/**********************************************/
	int index();
	/**********************************************/
	void setChecked( bool checked );
	/**********************************************/
	bool isChecked();
	/**********************************************/
	void setLayout( const QString& layout );
	/**********************************************/
	void updateState() override;
    /**********************************************/
    void addWidget( QWidget* widget,
                    int row, int col,
                    int rowSpan = 1, int colSpan = 1 );
	/**********************************************/
	void mousePressEvent( QMouseEvent* event ) override;
	void mouseDoubleClickEvent( QMouseEvent* event ) override;
    /**********************************************/
    void paintEvent( QPaintEvent* event ) override;
	/**********************************************/
signals:
	void selected( int index, bool selected );
	void showMenu( const QRect& rect );
	void play( int index );
//	void unselected( int index );
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LISTITEM_H
