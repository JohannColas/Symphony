#include "ListView.h"
#include "App.h"
/**********************************************/
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
ListView::ListView( QWidget* parent )
	: Widget( parent )
{
	_state = State::Normal;
    for ( int it = 0; it < 240; ++it )
		_artists.append( new Artist( QString("Artist %1").arg(it) ) );
	newMenu = new Menu;
	newMenu->addAction( "Play now", Button::PlayNow );
	newMenu->addAction( "Add to queue", Button::AddToQueue );
	newMenu->addAction( "Add to playlist" );
	connect( newMenu, &Menu::sendAction,
			 this, &ListView::onMenuAction );
}
/**********************************************/
/**********************************************/
void ListView::setMode( ListView::Mode mode )
{
    if ( _mode == mode )
        return;
    _mode = mode;
    updateLayout();
}
/**********************************************/
/**********************************************/
void ListView::setType( const QString& type )
{
	_type = type;
	updateData();
}
/**********************************************/
/**********************************************/
ListView::Mode ListView::mode() const
{
    return _mode;
}
/**********************************************/
/**********************************************/
void ListView::setLevel(int level)
{
    _level = level;
}
/**********************************************/
/**********************************************/
void ListView::setItemInfo( ListItem* item, int index )
{
	if ( (index < _artists.size() && _type == Type::ArtistList) ||
		 (index < _albums.size() && _type == Type::AlbumList) ||
		 (index < _tracks.size() && (_type == Type::TrackList || _type == Type::Queue)) )
    {
		if ( _type == Type::ArtistList )
            item->setArtist( _artists.at(index) );
		else if ( _type == Type::AlbumList )
			item->setAlbum( _albums.at(index) );
		else if ( _type == Type::TrackList || _type == Type::Queue )
			item->setTrack( _tracks.at(index) );
        if ( !item->isVisible() )
        {
            item->setVisible( true );
        }
    }
    else
        item->setVisible( false );
}
/**********************************************/
/**********************************************/
void ListView::updateSelection()
{
	for ( ListItem* item : _items )
	{
		if ( _selection.contains(item->index()) )
			item->setChecked( true );
		else
			item->setChecked( false );
	}
}
/**********************************************/
/**********************************************/
void ListView::updateData()
{
	_artists.clear();
	_albums.clear();
	_tracks.clear();
	disconnect( App::librairies(), &Libraries::changed,
				this, &ListView::updateData );
	disconnect( App::queue(), &TrackList::changed,
				this, &ListView::updateData );
	if ( _type == Type::ArtistList )
	{
		_artists = App::librairies()->artists();
		connect( App::librairies(), &Libraries::changed,
						this, &ListView::updateData );
	}
	else if ( _type == Type::AlbumList )
	{
		_albums = App::librairies()->albums();
		connect( App::librairies(), &Libraries::changed,
						this, &ListView::updateData );
	}
	else if ( _type == Type::TrackList )
	{
		_tracks = App::librairies()->tracks();
		connect( App::librairies(), &Libraries::changed,
						this, &ListView::updateData );
	}
	else if ( _type == Type::Queue )
	{
		_tracks = App::queue()->list();
		connect( App::queue(), &TrackList::changed,
					this, &ListView::updateData );
	}
	_selection.clear();
	_begin = 0;
	updateLayout();
}
/**********************************************/
/**********************************************/
void ListView::updateLayout()
{
    QElapsedTimer timer;
    timer.start();
    int itemW = 150;
    int itemH = 150;
//    int groupitemW = this->width() - 2*_radius;
//    int groupitemH = 50;
	int spacing = 5; // minimal spacing
	_mode = (Mode)App::settings()->getInt( _type, "mode" );
	QString itemmode = Layout::InfoBesideIcon;
	if ( _mode == ListView::Iconized )
		itemmode = Layout::InfoUnderIcon;
	else if ( _mode == ListView::Iconized2 )
		itemmode = Layout::InfoOverIcon;
	if ( _mode != ListView::Iconized && _mode != ListView::Iconized2 )
    {
        itemW = this->width() - 2*_radius - 2*spacing;
		itemH = 50;
        if ( _mode == ListView::DetailedList )
			itemH = 50;
        _nbColumns = 1;
    }
    else
        _nbColumns = (this->width() - 2*_radius)/(itemW + spacing);
    if ( _nbColumns > 1 ) // Adjusting spacing
    {
        bool _adjustSpacing = false;
        if ( _adjustSpacing )
            spacing = (this->width() - 2*_radius - _nbColumns*itemW)/(_nbColumns + 1);
		else if ( _mode == ListView::Iconized || _mode == ListView::Iconized2 )
            itemW = (this->width() - 2*_radius - (_nbColumns+1)*spacing)/_nbColumns;
    }
    _nbRows = 1+(this->height() - 2*_radius - spacing)/(itemH + spacing);
    int availableH = this->height() - 2*_radius - spacing;
    int it = 0;
    int jt = 0;
    int x = _radius + spacing;
    int y = _radius + spacing;
    if ( _level == 0 && view1 )
    {
        view1->resize( this->width() - 2*_radius, 300 );
        y += view1->height() + spacing;
        availableH -= view1->height() + spacing;
    }
    int it_inf = 0;
	for ( ListItem* item : qAsConst(_items) )
	{
//		int newh = itemH;
		if ( it == _nbRows-1 )
			item->setAvailableSize( {itemW, availableH} );
		else
			item->setAvailableSize( {-1, -1} );
		item->setGeometry( x, y, itemW, itemH );
		item->setIndex( _begin+it_inf );
		item->setLayout( itemmode );
        setItemInfo( item, _begin+it_inf );
        ++it_inf;
        ++jt;
        x += itemW + spacing;
        if ( jt == _nbColumns )
        {
            jt = 0;
            x = _radius + spacing;
            ++it;
            y += itemH + spacing;
			availableH -= itemH+spacing;
        }
    }
    while ( _items.size() < _nbColumns*_nbRows )
	{
        if ( _level == 0 )
        {
//            if ( !view1 )
//                view1 = new ListView(this);
//            view1->setMode( ListView::DetailedList );
//            view1->setLevel( 1 );
//            view1->move( _radius, _radius );
//            view1->resize( this->width() - 2*_radius, 300 );
//            view1->updateLayout();
        }
		ListItem* item = new ListItem( this );
		connect( item, &ListItem::selected,
				 this, &ListView::addSelected );
		connect( item, &ListItem::showMenu,
				 newMenu, &Menu::exec );
		connect( item, &ListItem::play,
				 this, &ListView::playTracks );
//		int newh = itemH;
		if ( it == _nbRows-1 )
			item->setAvailableSize( {itemW, availableH} );
		else
			item->setAvailableSize( {-1, -1} );
		item->setGeometry( x, y, itemW, itemH );
		item->setIndex( _begin+it_inf );
		item->setLayout( itemmode );
		setItemInfo( item, _begin+it_inf );
        ++it_inf;
        _items.append( item );
        ++jt;
        x += itemW + spacing;
        if ( jt == _nbColumns )
        {
            jt = 0;
            x = _radius + spacing;
            ++it;
            y += itemH + spacing;
			availableH -= itemH + spacing;
        }
    }
	while ( _items.size() > _nbColumns*_nbRows )
        delete _items.takeLast();
	updateSelection();
	qDebug() << "Time to build" << _items.size() << "items in" << _type << ":" << timer.elapsed();
}
/**********************************************/
/**********************************************/
void ListView::playTracks( int index )
{
	QList<Track*> tracks;
	if ( _type == Type::ArtistList )
		tracks.append( _artists.at(index)->tracks() );
	else if ( _type == Type::AlbumList )
		tracks.append( _albums.at(index)->tracks() );
	else if ( _type == Type::TrackList || _type == Type::Queue )
		tracks.append( _tracks.at(index) );
	App::queue()->clear();
	App::queue()->add( tracks );
	App::player()->reset();
	App::player()->play();
//	emit play(tracks);
}
/**********************************************/
/**********************************************/
void ListView::addSelected( int index, bool selected )
{
	if ( _selectionMode == ListView::OnlyOne )
		_selection.clear();
	if ( selected && !_selection.contains(index) )
		_selection.append( index );
	else if ( !selected )
		_selection.removeAll( index );
	updateSelection();
}
/**********************************************/
/**********************************************/
void ListView::onMenuAction( const Button::Action& action, const QVariant& value )
{
	QList<Track*> tracks;
	if ( _type == Type::ArtistList )
	{
		for ( int it : _selection )
		{
			if ( 0 <= it && it < _artists.size() )
				tracks.append( _artists.at(it)->tracks() );
			else
				continue;
		}
	}
	else if ( _type == Type::AlbumList )
	{
		for ( int it : _selection )
		{
			if ( 0 <= it && it < _albums.size() )
				tracks.append( _albums.at(it)->tracks() );
			else
				continue;
		}
	}
	else if ( _type == Type::TrackList || _type == Type::Queue )
	{
		for ( int it : _selection )
		{
			if ( 0 <= it && it < _tracks.size() )
				tracks.append( _tracks.at(it) );
			else
				continue;
		}
	}
	if ( action == Button::PlayNow )
	{
//		App::queue()->add( tracks );
//		App::player()->play();
		App::queue()->clear();
		App::queue()->add( tracks );
		App::player()->reset();
		App::player()->play();
	}
	else if ( action == Button::AddToQueue )
	{
//		App::queue()->add( tracks );
//		App::queue()->clear();
		App::queue()->add( tracks );
		App::player()->reset();
//		App::player()->play();
	}
	else if ( action == Button::InsertToQueue )
	{
//		App::queue()->insert( 0, tracks );
	}
	else if ( action == Button::AddToPlaylist )
	{
		App::playlists()->addToPlaylist( value.toString(), tracks );
	}
}
/**********************************************/
/**********************************************/
void ListView::wheelEvent( QWheelEvent* event )
{
    if ( event->angleDelta().y() > 0 )
        _begin += _nbColumns;
    else
        _begin -= _nbColumns;
    if ( _begin < 0 )
		_begin = 0;
	else if ( _type == Type::ArtistList && _begin > _artists.size()-1 )
		_begin = _artists.size()-1;
	else if ( _type == Type::AlbumList && _begin > _albums.size()-1 )
		_begin = _albums.size()-1;
	else if ( (_type == Type::TrackList || _type == Type::Queue) && _begin > _tracks.size()-1 )
		_begin = _tracks.size()-1;
    if ( _oldbegin != _begin )
        updateLayout();
    else
        return;
    _oldbegin = _begin;
    event->accept();
}
/**********************************************/
/**********************************************/
void ListView::paintEvent( QPaintEvent* event )
{
//    // Background Style
//    QBrush bs_background;
//    // Borders Style
//    int bdwidth = 3;
//    QPen pn_borders;
////    if ( hasFocused() )
////    {
////        bs_background = QBrush( QColor("#FF4400") );
////        pn_borders = QPen( QColor("#FFFF00"), bdwidth );
////    }
////    else
//    {
//        bs_background = QBrush( QColor(50, 50, 50) );
//        pn_borders = QPen( QColor( 228, 181, 56 ), bdwidth );
//    }
//    // Initialize painter
//    QPainter paint(this);
//    // --------------------------------
//    // Build Borders and Background
//    paint.setPen( pn_borders );
//    paint.setBrush( bs_background );
//    paint.setRenderHint( QPainter::Antialiasing, true );
//    paint.drawRoundedRect( 0, 0, this->width(), this->height(), _radius, _radius );
	Widget::paintEvent( event );
}
/**********************************************/
/**********************************************/
void ListView::resizeEvent( QResizeEvent* /*event*/ )
{
    updateLayout();
}
/**********************************************/
/**********************************************/
