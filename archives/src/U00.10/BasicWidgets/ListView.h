#ifndef LISTVIEW_H
#define LISTVIEW_H
/**********************************************/
#include "BasicWidgets/Widget.h"
/**********************************************/
#include "ListItem.h"
#include "GroupItem.h"
#include "Menu.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ListView
		: public Widget
{
    Q_OBJECT
public:
	enum Mode {
		NormalList = 0,
		DetailedList = 1,
		Iconized = 2,
		Iconized2 = 7,
		NormalTree = 3,
		DetailedTree = 5,
	};
	enum SelectionMode {
		OnlyOne = 0,
		Multiple = 1,
	};
    /**********************************************/
    enum GroupBy {
        AscendingOrder = 1,
        DescendingOrder = -1,
        NonGrouped = 0,
        Name = 2,
        Title = 3,
        ArtistName = 5,
        Gender = 7,
        Date = 11,
    };
    /**********************************************/
private:
	Mode _mode = Mode::Iconized;
    GroupBy _grouped = GroupBy::NonGrouped;
    QList<ListItem*> _items;
//    QList<GroupItem*> _groupitems;
	QList<Artist*> _artists;
    QList<Album*> _albums;
    QList<Track*> _tracks;
    int _nbItems = 0;
    int _nbColumns = 0;
    int _nbRows = 0;
    int _elementSize = 0;
    QList<QWidget*> _sections;
    int _lastBegin = -1;
    int _begin = 0;
    int _oldbegin = 0;
    int _index = -1;
	ListView::SelectionMode _selectionMode = ListView::OnlyOne;
    QList<int> _selection;
    bool _avoidRightClick = false;
    bool _multipleSelectable = true;
    bool _allowRemove = false;
    int _radius = 10;
    ListView* view1 = 0;
    int _level = 0;
	Menu* newMenu = 0;
    /**********************************************/
public:
    ListView( QWidget *parent = 0 );
    /**********************************************/
    void setMode( ListView::Mode mode );
	/**********************************************/
	void setType( const QString& type ) override;
    /**********************************************/
    ListView::Mode mode() const;
    /**********************************************/
    void setLevel( int level );
    /**********************************************/
protected:
    void setItemInfo( ListItem* item, int index = -1 );
    /**********************************************/
	void updateSelection();
	/**********************************************/
protected slots:
	void updateData();
	void updateLayout();
	/**********************************************/
	void playTracks( int index );
	void addSelected( int index, bool selected );
//	void removeSelected( int index );
	/**********************************************/
	void onMenuAction( const Button::Action& action, const QVariant& value );
	/**********************************************/
    void wheelEvent( QWheelEvent *event ) override;
    /**********************************************/
    void paintEvent( QPaintEvent* event ) override;
    /**********************************************/
    void resizeEvent( QResizeEvent* event ) override;
    /**********************************************/
signals:
	void play( QList<Track*> tracks );
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LISTVIEW_H
