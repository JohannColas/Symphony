#ifndef LockedButton_H
#define LockedButton_H
/**********************************************/
#include <QPushButton>
#include <QMouseEvent>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LockedButton
		: public QPushButton
{
	Q_OBJECT

public:
	LockedButton( QWidget* parent = 0 )
		: QPushButton( parent )
	{
		setFlat( true );
		setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	}
public slots:
	void mousePressEvent( QMouseEvent* event ) override
	{
		event->ignore();
	}
	void mouseDoubleClickEvent( QMouseEvent* event ) override
	{
		event->ignore();
	}
	void mouseReleaseEvent( QMouseEvent* event ) override
	{
		event->ignore();
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LockedButton_H
