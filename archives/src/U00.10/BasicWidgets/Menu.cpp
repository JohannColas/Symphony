#include "Menu.h"
/**********************************************/
/**********************************************/
Menu::Menu( QWidget* parent )
	: Widget( parent )
{
	setWindowFlag( Qt::Popup );
	setType( Type::Menu );
}
/**********************************************/
/**********************************************/
void Menu::addAction( const QString& text, const Button::Action& action )
{
	Button* newbt = new Button(this);
	newbt->resize( 100, 20 );
	newbt->setType( Type::Button );
	newbt->setText( text );
	newbt->setIcon( "" );
	newbt->setAction( action );
	connect( newbt, &Button::sendAction,
			 this, &Menu::sendAction );
	_actions.append( newbt );
}
/**********************************************/
/**********************************************/
void Menu::updateLayout()
{

}
/**********************************************/
/**********************************************/
void Menu::exec( const QRect& rect = QRect() )
{
	int y = 5;
	for ( Button* bt : _actions )
	{
		bt->move( 0, y);
		y += bt->height()+5;
	}
	this->move( rect.x(), rect.y() );
	this->show();
}

void Menu::leaveEvent(QEvent* event)
{
	Widget::leaveEvent( event );
//	this->hide();
}
/**********************************************/
/**********************************************/
