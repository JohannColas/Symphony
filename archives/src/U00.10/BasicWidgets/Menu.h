#ifndef MENU_H
#define MENU_H
/**********************************************/
#include "Widget.h"
#include "Button.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Menu
		: public Widget
{
	Q_OBJECT
protected:
	QList<Button*> _actions;
public:
	Menu( QWidget* parent = 0 );
	/**********************************************/
	void addAction( const QString& actionName, const Button::Action& action = Button::NoAction );
	/**********************************************/
	void updateLayout();
	/**********************************************/
public slots:
	void exec( const QRect& rect );
	/**********************************************/
	void leaveEvent( QEvent* event ) override;
	/**********************************************/
signals:
	void released( const QString& action );
	void checked( const QString& action, bool checked );
	void sendAction( const Button::Action& action, const QVariant& value );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // MENU_H
