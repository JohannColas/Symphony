#ifndef SECTIONBUTTON_H
#define SECTIONBUTTON_H
/**********************************************/
#include <QPushButton>
#include <QMouseEvent>
#include "BasicWidgets/Layouts.h"
#include "BasicWidgets/LockedButton.h"
#include "BasicWidgets/Label.h"
#include "BasicWidgets/Text.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SectionButton
        : public QPushButton
{
    Q_OBJECT
public:
    enum ButtonStyle {
        IconOnly,
        TextOnly,
        TextBesideIcon,
        TextUnderIcon
    };
protected:
    int _id = -1;
    QString _key;
    LockedButton* _icon = new LockedButton(this);
    Text* _text = new Text(this);
    GridLayout* lay_main = new GridLayout(this);
    ButtonStyle _style = SectionButton::TextUnderIcon;
    QSpacerItem* _spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding );
    void init()
    {
        setFlat( true );
        lay_main->setSizeConstraint( QLayout::SetMinimumSize );
        setContentsMargins( 10, 0, 10, 0 );
        setLayout( lay_main );
        setIconSize( 36 );
        connect( this, &SectionButton::released,
                 this, &SectionButton::onReleased );
        _text->setScaleFactor( 3 );
    }
public:
    SectionButton( QWidget* parent = nullptr )
        : QPushButton( parent )
    {
        init();
    }
    SectionButton( const QString& text, const QIcon& icon, QWidget* parent = nullptr )
        : QPushButton( parent )
    {
        init();
        setIcon( icon );
        setText( text );
        setFlat( true );
        updateLayout();
    }
    int id() const
    {
        return _id;
    }
    void setID( int id )
    {
        _id = id;
    }
    QString key() const { return _key; }
    void setKey( const QString& key ) { _key = key; }
    QString text() const
    {
        return _text->text();
    }

public slots:
    void setText( const QString& text )
    {
        _text->setText( text );
        updateLayout();
    }
    void setIcon( const QIcon& icon )
    {
        _icon->setIcon( icon );
        updateLayout();
    }
    void setIconSize( const QSize& size )
    {
        _icon->setIconSize( size );
        updateLayout();
    }
    void setIconSize( int size )
    {
        setIconSize( QSize(size, size) );
    }
    void setStyle( const SectionButton::ButtonStyle style )
    {
        _style = style;
        updateLayout();
    }
    void updateLayout()
    {
        lay_main->removeWidget( _icon );
        lay_main->removeWidget( _text );
        lay_main->removeItem( _spacer );
        if ( _style == SectionButton::IconOnly )
        {
            lay_main->addWidget( _icon, 0, 0 );
            _icon->show();
            _text->hide();
            lay_main->setAlignment( _icon, Qt::AlignHCenter | Qt::AlignVCenter );
        }
        else if ( _style == SectionButton::TextOnly )
        {
            lay_main->addWidget( _text, 0, 0 );
            _icon->hide();
            _text->show();
            lay_main->setAlignment( _text, Qt::AlignHCenter | Qt::AlignVCenter );
        }
        else if ( _style == SectionButton::TextUnderIcon )
        {
            lay_main->addWidget( _icon, 0, 0 );
            lay_main->addWidget( _text, 1, 0 );
            _icon->show();
            _text->show();
            lay_main->setAlignment( _text, Qt::AlignHCenter | Qt::AlignVCenter );
            lay_main->setAlignment( _icon, Qt::AlignHCenter | Qt::AlignVCenter );
        }
        else if ( _style == SectionButton::TextBesideIcon )
        {
            lay_main->addWidget( _icon, 0, 0 );
            lay_main->addWidget( _text, 0, 1 );
            lay_main->addItem( _spacer, 0, 2 );
            _icon->show();
            _text->show();
            lay_main->setAlignment( _icon, Qt::AlignVCenter | Qt::AlignLeft );
            lay_main->setAlignment( _text, Qt::AlignVCenter | Qt::AlignLeft );
        }
//        _icon->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
        _text->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
    }
    void onReleased()
    {
        emit sendID( _id );
    }

signals:
    void sendID( int id );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SECTIONBUTTON_H
/**********************************************/
