#ifndef SYSTEMCONTROLS_H
#define SYSTEMCONTROLS_H

/**********************************************/
#include "BasicWidgets/SWidget.h"
#include <QApplication>
#include "AppButton.h"
#include "SystemButton.h"
#include "App.h"
#include "Commons/Icons.h"
/**********************************************/
/**********************************************/
/**********************************************/
class SystemControls
        : public SWidget
{
    Q_OBJECT
private:
    // Application Button
    AppButton* pb_settings = new AppButton(this);
    // System Button
    SystemButton* pb_minimize = new SystemButton(this);
    SystemButton* pb_maximize = new SystemButton(this);
    SystemButton* pb_close = new SystemButton(this);
	//
	bool _maximized = false;

public:
    ~SystemControls()
    {

    }
    SystemControls( SWidget* parent = 0 )
        : SWidget( parent )
    {
        HorizontalLayout* lay_main = new HorizontalLayout;
//        lay_main->addSpacer();
        lay_main->addWidget( pb_settings );
        lay_main->addWidget( pb_minimize );
        lay_main->addWidget( pb_maximize );
        lay_main->addWidget( pb_close );
        setLayout( lay_main );
        setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );

        connect( pb_settings, &QPushButton::clicked,
                 this, &SystemControls::onSettings );
        connect( pb_minimize, &QPushButton::clicked,
				 this, &SystemControls::onMinimize );
		connect( pb_maximize, &QPushButton::clicked,
				 this, &SystemControls::onMaximize );
		connect( pb_maximize, &QPushButton::clicked,
				 this, &SystemControls::maximize );
        connect( pb_close, &QPushButton::clicked,
                 qApp, &QApplication::closeAllWindows );
    }

public slots:
    void updateIcons() override
    {
		pb_settings->setIcon( Icons::get( IconsKeys::Settings ) );
		pb_minimize->setIcon( Icons::get( IconsKeys::Minimize ) );
		pb_maximize->setIcon( Icons::get( IconsKeys::Maximize ) );
		pb_close->setIcon( Icons::get( IconsKeys::Close ) );
    }
	void maximize()
	{
		_maximized = !_maximized;
		QString key = IconsKeys::Maximize;
		if ( _maximized )
			key = IconsKeys::Unmaximize;
		pb_maximize->setIcon( Icons::get( key ) );
	}

protected:

signals:
    void onSettings();
    void onMinimize();
    void onMaximize();
//    void onClose();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SYSTEMCONTROLS_H
/**********************************************/
