#ifndef TITLEWIDGET_H
#define TITLEWIDGET_H
/**********************************************/
#include "BasicWidgets/Label.h"
#include "BasicWidgets/Text.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TitleLabel
		: public Text
{
	Q_OBJECT
protected:
	QString _options = "";
	/* ------------------------ */
	/* ------------------------ */
public:
	TitleLabel( QWidget* parent = 0 )
		: Text( parent )
	{
	}
	TitleLabel( const QString& text, QWidget* parent = 0 )
		: Text( text, parent )
	{
	}
	/* ------------------------ */
	/* ------------------------ */
public slots:
	/* ------------------------ */
	/* ------------------------ */
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TITLEWIDGET_H
