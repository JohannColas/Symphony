#include "Widget.h"

#include <QEvent>
#include <QPainter>
/**********************************************/
#include "App.h"
/**********************************************/
/**********************************************/
Widget::Widget( QWidget *parent )
	: QWidget( parent )
{

}
/**********************************************/
/**********************************************/
QString Widget::type() const
{
	return _type;
}
/**********************************************/
/**********************************************/
QString Widget::name() const
{
	return _name;
}
/**********************************************/
/**********************************************/
QString Widget::state() const
{
	return _state;
}
/**********************************************/
/**********************************************/
bool Widget::isHovered() const
{
	return _hovered;
}
/**********************************************/
/**********************************************/
QRect Widget::availableContentRect() const
{
	return _crect;
}
/**********************************************/
/**********************************************/
void Widget::setType( const QString& type )
{
	_type = type;
}
/**********************************************/
/**********************************************/
void Widget::setName( const QString& name )
{
	_name = name;
}
/**********************************************/
/**********************************************/
void Widget::setState( const QString& state )
{
	_state = state;
}
/**********************************************/
/**********************************************/
void Widget::updateState()
{
	if ( isHovered() )
		_state = State::Hovered;
	else
		_state = State::Normal;
	repaint();
}
/**********************************************/
/**********************************************/
void Widget::setHovered( bool hovered )
{
	_hovered = hovered;
	updateState();
}
/**********************************************/
/**********************************************/
void Widget::setAvailableSize( const QSize& asize )
{
	_asize = asize;
}
/**********************************************/
/**********************************************/
void Widget::setAvailableContentRect( const QRect& crect )
{
	_crect = crect;
}
/**********************************************/
/**********************************************/
void Widget::enterEvent( QEvent* event )
{
	setHovered( true );
	event->ignore();
}
/**********************************************/
/**********************************************/
void Widget::leaveEvent( QEvent* event )
{
	setHovered( false );
	event->ignore();
}

QRect fonction( Widget* wid, QSize& _asize )
{

	Brush background = App::theme()->brush( wid->type(), "background."+wid->state() );
	int radius = background.radius();
	Pen borders = App::theme()->pen( wid->type(), "borders."+wid->state() );
	QString bdtype = borders.type();
	int bdwidth = borders.width();
	if ( bdtype == Border::None )
		bdwidth = 0;
	QMargins margins = App::theme()->margins( wid->type(), ThemeKeys::Margins );
	QPainter paint(wid);
	paint.setRenderHint( QPainter::Antialiasing, true );
	if ( _asize.width() > 0 && _asize.height() > 0 )
		paint.setClipRect( 0, 0, _asize.width(), _asize.height(), Qt::IntersectClip );
	// --------------------------------
	// Build Borders and Background
	QRect arect = {margins.left(),
				   margins.top(),
				   wid->width()-margins.left()-margins.right(),
				   wid->height()-margins.top()-margins.bottom() };
	QRect rect1 = arect;
	int offset = 0.5*bdwidth + bdwidth % 2;
	if ( bdtype == Border::All )
	{
		rect1 -= {offset, offset, offset, offset};
		arect -= { radius, radius, radius, radius };
	}
	else if ( bdtype == Border::Left )
	{
		rect1 -= {offset, 0, 0, 0};
		arect -= { bdwidth, radius, radius, radius };
	}
	else if ( bdtype == Border::Bottom )
	{
		rect1 -= {0, 0, 0, offset};
		arect -= { radius, radius, radius, bdwidth };
	}
	else if ( bdtype == Border::Right )
	{
		rect1 -= {0, 0, offset, 0};
		arect -= { radius, radius, bdwidth, radius };
	}
	else if ( bdtype == Border::Top )
	{
		rect1 -= {0, offset, 0, 0};
		arect -= { radius, bdwidth, radius, radius };
	}
	QPainterPath main;
	main.addRoundedRect( rect1, radius, radius );
	QPainterPath mask;
	if ( bdtype == Border::Left )
		mask.addRect( rect1.x(), rect1.y(), 0.5*rect1.width(), rect1.height() );
	else if ( bdtype == Border::Bottom )
		mask.addRect( rect1.x(), 0.5*rect1.height(), rect1.width(), 0.5*rect1.height() );
	else if ( bdtype == Border::Right )
		mask.addRect( 0.5*rect1.width(), rect1.y(), 0.5*rect1.width(), rect1.height() );
	else if ( bdtype == Border::Top )
		mask.addRect( rect1.x(), rect1.y(), rect1.width(), 0.5*rect1.height() );
	main = main.united( mask );
	paint.fillPath( main, background );
	if ( bdtype == Border::All )
		paint.strokePath( main, borders );
	else if ( bdtype != Border::None )
	{
		borders.setCapStyle( Qt::FlatCap );
		paint.setPen( borders );
		if ( bdtype == Border::Left )
			paint.drawLine( rect1.x(), rect1.y(), rect1.x(), rect1.y()+rect1.height() );
		else if ( bdtype == Border::Bottom )
			paint.drawLine( rect1.x(), rect1.height(), rect1.x()+rect1.width(), rect1.height() );
		else if ( bdtype == Border::Right )
			paint.drawLine( rect1.width(), rect1.y(), rect1.width(), rect1.y()+rect1.height() );
		else if ( bdtype == Border::Top )
			paint.drawLine( rect1.x(), rect1.y(), rect1.x()+rect1.width(), rect1.y() );
	}
	QMargins padding = App::theme()->margins( wid->type(), ThemeKeys::Padding );
	arect -= padding;
	return arect;
}
/**********************************************/
/**********************************************/
void Widget::paintEvent( QPaintEvent* /*event*/ )
{
//	Brush background = App::theme()->brush( _type, "background."+_state );
//	int radius = background.radius();
//	Pen borders = App::theme()->pen( _type, "borders."+_state );
//	QString bdtype = borders.type();
//	int bdwidth = borders.width();
//	if ( bdtype == Border::None )
//		bdwidth = 0;
//	QMargins margins = App::theme()->margins( _type, ThemeKeys::Margins );
//	// Initialize painter
//	QPainter paint(this);
//	paint.setRenderHint( QPainter::Antialiasing, true );
//	if ( _asize.width() > 0 && _asize.height() > 0 )
//		paint.setClipRect( 0, 0, _asize.width(), _asize.height(), Qt::IntersectClip );
//	// --------------------------------
//	// Build Borders and Background
	QRect arect = fonction( this,  _asize);
//	QRect rect1 = arect;
//	int offset = 0.5*bdwidth + bdwidth % 2;
//	if ( bdtype == Border::All )
//	{
//		rect1 -= {offset, offset, offset, offset};
//		arect -= { radius, radius, radius, radius };
//	}
//	else if ( bdtype == Border::Left )
//	{
//		rect1 -= {offset, 0, 0, 0};
//		arect -= { bdwidth, radius, radius, radius };
//	}
//	else if ( bdtype == Border::Bottom )
//	{
//		rect1 -= {0, 0, 0, offset};
//		arect -= { radius, radius, radius, bdwidth };
//	}
//	else if ( bdtype == Border::Right )
//	{
//		rect1 -= {0, 0, offset, 0};
//		arect -= { radius, radius, bdwidth, radius };
//	}
//	else if ( bdtype == Border::Top )
//	{
//		rect1 -= {0, offset, 0, 0};
//		arect -= { radius, bdwidth, radius, radius };
//	}
//	QPainterPath main;
//	main.addRoundedRect( rect1, radius, radius );
//	QPainterPath mask;
//	if ( bdtype == Border::Left )
//		mask.addRect( rect1.x(), rect1.y(), 0.5*rect1.width(), rect1.height() );
//	else if ( bdtype == Border::Bottom )
//		mask.addRect( rect1.x(), 0.5*rect1.height(), rect1.width(), 0.5*rect1.height() );
//	else if ( bdtype == Border::Right )
//		mask.addRect( 0.5*rect1.width(), rect1.y(), 0.5*rect1.width(), rect1.height() );
//	else if ( bdtype == Border::Top )
//		mask.addRect( rect1.x(), rect1.y(), rect1.width(), 0.5*rect1.height() );
//	main = main.united( mask );
//	paint.fillPath( main, background );
//	if ( bdtype == Border::All )
//		paint.strokePath( main, borders );
//	else if ( bdtype != Border::None )
//	{
//		borders.setCapStyle( Qt::FlatCap );
//		paint.setPen( borders );
//		if ( bdtype == Border::Left )
//			paint.drawLine( rect1.x(), rect1.y(), rect1.x(), rect1.y()+rect1.height() );
//		else if ( bdtype == Border::Bottom )
//			paint.drawLine( rect1.x(), rect1.height(), rect1.x()+rect1.width(), rect1.height() );
//		else if ( bdtype == Border::Right )
//			paint.drawLine( rect1.width(), rect1.y(), rect1.width(), rect1.y()+rect1.height() );
//		else if ( bdtype == Border::Top )
//			paint.drawLine( rect1.x(), rect1.y(), rect1.x()+rect1.width(), rect1.y() );
//	}
//	QMargins padding = App::theme()->margins( _type, ThemeKeys::Padding );
//	arect -= padding;
	setAvailableContentRect( arect );
	//	paint.fillRect( arect, QColor("#FFFFFF") );
}
/**********************************************/
/**********************************************/
