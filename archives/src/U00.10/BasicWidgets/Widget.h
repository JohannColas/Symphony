#ifndef WWIDGET_H
#define WWIDGET_H
/**********************************************/
#include <QWidget>
/**********************************************/
namespace Type {
	static inline QString Widget = "Widget";
	static inline QString Button = "Button";
	static inline QString ListView = "ListView";
	static inline QString Queue = "Queue";
	static inline QString ArtistList = "ArtistList";
	static inline QString AlbumList = "AlbumList";
	static inline QString TrackList = "TrackList";
	static inline QString GenderList = "GenderList";
	static inline QString Menu = "GenderList";
}
namespace State {
	static inline QString Normal = "normal";
	static inline QString Hovered = "hovered";
	static inline QString Checked = "checked";
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Widget
		: public QWidget
{
	Q_OBJECT
protected:
	QString _type = Type::Widget;
	QString _name = Type::Widget;
	QString _state = State::Normal;
	bool _hovered = false;
	QSize _asize = {-1, -1};
	QRect _crect = {0, 0, 0, 0};
	/**********************************************/
public:
	explicit Widget( QWidget* parent = 0 );
	/**********************************************/
	QString type() const;
	/**********************************************/
	QString name() const;
	/**********************************************/
	QString state() const;
	/**********************************************/
	bool isHovered() const;
	/**********************************************/
	QRect availableContentRect() const;
	/**********************************************/
public slots:
	virtual void setType( const QString& type );
	/**********************************************/
	void setName( const QString& name );
	/**********************************************/
	void setState( const QString& state );
	virtual void updateState();
	/**********************************************/
	void setHovered( bool hovered );
	/**********************************************/
	void setAvailableSize( const QSize& asize );
	/**********************************************/
	void setAvailableContentRect( const QRect& crect );
	/**********************************************/
	virtual void enterEvent( QEvent* event ) override;
	/**********************************************/
	virtual void leaveEvent( QEvent* event ) override;
	/**********************************************/
	virtual void paintEvent( QPaintEvent* event ) override;
	/**********************************************/
signals:
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WWIDGET_H
