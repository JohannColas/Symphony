#ifndef ALBUM_H
#define ALBUM_H
/**********************************************/
#include <QObject>
#include "Commons/TrackList.h"
/**********************************************/
class Artist;
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Album
		: public QObject
{
	Q_OBJECT
protected:
	QString _title = "";
	QString _artist = "";
	QList<Track*> _tracks;
	Artist* _artist2 = 0;
	QString _genre;
	QString _date;
	QString _description;

public:
	Album( const QString& title, const QString& artist = "" )
		: _title( title ), _artist( artist )
	{

	}
	QString title() const
	{
		return _title;
	}
	QString artist() const
	{
		return _artist;
	}
	int nbTracks()
	{
		return _tracks.size();
	}
	QList<Track*> tracks()
	{
		return _tracks;
	}
	TrackList* toTracklist()
	{
		TrackList* tracklist = new TrackList;
		tracklist->add( tracks() );
		return tracklist;
	}
	void append( Album* album = 0 )
	{
		if ( album )
			for ( Track* track : album->tracks() )
				addTrack( track );
	}
	void addTrack( Track* track )
	{
		_tracks.append( track );
	}
	void addTrack( const QString& title )
	{
		_tracks.append( new Track(title) );
	}
	bool removeTrack( const QString& title )
	{
		for ( Track* track : _tracks )
		{
			if ( track->title() == title )
			{
				_tracks.removeOne( track );
				return true;
			}
		}
		return false;
	}
	void setArtist( Artist* artist )
	{
		_artist2 = artist;
	}
	Artist* artist2() const
	{
		return _artist2;
	}
	QString description() const
	{
		return _description;
	}
	QString date() const
	{
		return _date;
	}
	QString genre() const
	{
		return _genre;
	}
	void setDescription( const QString& description )
	{
		_description = description;
	}
	void setGenre( const QString& genre )
	{
		_genre = genre;
	}
	void setDate( const QString& date )
	{
		_date = date;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ALBUM_H
