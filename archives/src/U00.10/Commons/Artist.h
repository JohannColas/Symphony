#ifndef ARTIST_H
#define ARTIST_H
/**********************************************/
#include <QObject>
#include "Commons/Album.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Artist
		: public QObject
{
	Q_OBJECT
protected:
	QString _name = "";
	QList<Album*> _albums;
	QString _gender;
	QString _nbMembers;
	QString _country;
	QString _date;
	QString _biography;
	QString _genre;
	QString _website;

public:
	static inline bool compare( Artist* art1, Artist* art2 )
	{
		return ( clean(art1->name()) < clean(art2->name()) );
	}
	static inline QString clean( const QString& str )
	{
		QString temp = str;
		if ( temp.left(4) == "The " )
			temp = temp.mid( 4 );
		temp.replace("É", "E").replace("é", "e");
		return temp.toLower();
	}
	Artist( const QString& name )
		: _name( name )
	{

	}
	QString name() const
	{
		return _name;
	}
	int nbAlbums()
	{
		return _albums.size();
	}
	int nbLines()
	{
		int nbLines = _albums.size();
		for ( Album* album : _albums )
			nbLines += album->nbTracks();
		return nbLines;
	}
	QList<Album*> albums()
	{
		return _albums;
	}
	QList<Track*> tracks()
	{
		QList<Track*> tracks;
		for ( Album* album : albums() )
			tracks.append( album->tracks() );
		return tracks;
	}
	void addAlbum( Album* album )
	{
		_albums.append( album );
	}
	void addAlbum( const QString& title )
	{
		_albums.append( new Album(title) );
	}
	bool removeAlbum( const QString& title )
	{
		for ( Album* album : _albums )
		{
			if ( album->title() == title )
			{
				_albums.removeOne( album );
				return true;
			}
		}
		return false;
	}
	QString gender() const
	{
		return _gender;
	}
	QString nbMembers() const
	{
		return _nbMembers;
	}
	QString country() const
	{
		return _country;
	}
	QString date() const
	{
		return _date;
	}
	QString biography() const
	{
		return _biography;
	}
	QString genre() const
	{
		return _genre;
	}
	QString website() const
	{
		return _website;
	}
	void setGender( const QString& gender )
	{
		_gender = gender;
	}
	void setNbMembers( const QString& nbMembers )
	{
		_nbMembers = nbMembers;
	}
	void setCountry( const QString& country )
	{
		_country = country;
	}
	void setDate( const QString& date )
	{
		_date = date;
	}
	void setBiography( const QString& biography )
	{
		_biography = biography;
	}
	void setGenre( const QString& genre )
	{
		_genre = genre;
	}
	void setWebsite( const QString& website )
	{
		_website = website;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ARTIST_H
