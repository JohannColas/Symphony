#ifndef FILE_H
#define FILE_H

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>
#include <QDomDocument>
#include <QPixmap>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QSettings>
#include <QIcon>

class Files
{
public:
    static inline QString PlaylistsName = "#playlists.spls";
    static inline QString LibrariesName = "#libraries.slbs";
    static inline QString LibraryName = "#library.slb";
    static inline QString QueueName = "#Queue.spl";
	static inline QStringList AudioFileFilter = {"*.mp3", "*.ogg", "*.wav", "*.flac", "*.m4a", "*.wma"};
    static inline QString makePath( const QString& dirPath, const QString& filePath = QString() )
    {
        if ( !filePath.isEmpty() )
            return dirPath + "/" + filePath;
        return dirPath;
    }
    static inline QString localPath( const QString& file = QString() )
    {
        // Create user dir if not exists
        QString configPath = "/.local/share";
        #ifdef Q_OS_WIN
            configPath = "/AppData/Roaming";
        #endif
        QDir dir( QDir::homePath() + configPath );
        dir.mkdir( "Symphony" );
        if ( dir.cd( "Symphony" ) )
            return makePath( dir.path(), file );
        return QDir::homePath();
    }
	static inline QString librariesPath( const QString& path = QString() )
    {
        QDir dir( localPath() );
        dir.mkdir( "Libraries" );
        if ( dir.cd( "Libraries" ) )
			return makePath( dir.path(), path );
        return QDir::homePath();
    }
    static inline QString playlistsPath( const QString& file = QString() )
    {
        QDir dir( localPath() );
        dir.mkdir( "Playlists" );
        if ( dir.cd( "Playlists" ) )
            return makePath( dir.path(), file );
        return QDir::homePath();
    }
    static inline QDir localDir()
    {
        // Create user dir if not exists
        QString configPath = "/.local/share";
        #ifdef Q_OS_WIN
            configPath = "/AppData/Roaming";
        #endif
        QDir dir( QDir::homePath() + configPath );
        if ( !dir.exists( "Symphony" ) )
            dir.mkdir( "Symphony" );
        dir.cd( "Symphony" );
        return dir;
	}
	static inline QStringList DirList( const QString& path )
	{
		QDir dir( path );
		dir.setFilter( QDir::Dirs );
		dir.setSorting( QDir::SortFlag::Name |
						QDir::IgnoreCase |
						QDir::LocaleAware );
		return dir.entryList();
	}
	static inline QStringList AudioFileList( const QString& path )
	{
		QDir dir( path );
		dir.setNameFilters( AudioFileFilter );
		dir.setFilter( QDir::Files );
		dir.setSorting( QDir::SortFlag::Name |
						QDir::IgnoreCase |
						QDir::LocaleAware );
		return dir.entryList();
	}
	static inline bool deleteFolder( const QString& path )
	{
		QDir dir( path );
		return dir.removeRecursively();
	}
    static inline bool exists( const QString& path )
    {
        QFileInfo file( path );
        return file.exists();
    }
    static inline bool read( const QString& path, QString& contents, const char* codec = "UTF-8" )
    {
        QFile file( path );
        if ( !file.open( QIODevice::ReadOnly ) )
            return false;
        QTextStream in( &file );
        in.setCodec( codec );
        contents = in.readAll();
        file.close();
        return true;
    }
    static inline bool save( const QString& path, const QString& data, const char* codec = "UTF-8" )
    {
		QFileInfo fileinfo( path );
		fileinfo.absoluteFilePath();
		QStringList pqthSeq = fileinfo.absoluteFilePath().split('/');
        QFile file( path );
        if ( !file.open( QIODevice::WriteOnly ) )
            return false;
        QTextStream out( &file );
        out.setCodec( codec );
        out << data;
        file.close();
        return true;
    }
    static inline QPixmap readImage( const QString& path )
    {
        QPixmap pixmap;
        pixmap.load( path );
        return pixmap;
    }
    static inline bool remove( const QString& path )
    {
//        qDebug() << "File::remove" << path;
        if ( path == "" )
            return false;
        return QFile( path ).remove();
    }
    static inline bool saveXML( const QString& path, QDomDocument& xmlDoc )
    {
        // Getting root element
        QDomElement el_root = xmlDoc.documentElement();
        // Writing the Settings of XML Files
        QDomProcessingInstruction settings =
                xmlDoc.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"utf-8\"" );
        xmlDoc.insertBefore( settings, el_root );
        // Create the XML if DONT exists
        QFile xmlFile( path );
        if( !xmlFile.open( QFile::WriteOnly ) )
            return false;
        // Create QTextStream
        QTextStream stream( &xmlFile );
        // Writing the
        xmlDoc.save( stream, 4, QDomDocument::EncodingFromTextStream );
        //Fermeture du fichier
        xmlFile.close();
        return true;
    }
    static inline QDomDocument readXML( const QString& path )
    {
        // Getting root element
        QDomDocument xmlDoc;
        QFile file( path );
        // Opening and Checking if the file exists
        if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
            return xmlDoc;
        // Settings and Checking if the file containts XML data
        xmlDoc.setContent( &file );
        file.close();
        return xmlDoc;
    }
    static inline bool readXML( const QString& path, QDomDocument& contents )
    {
        // Getting root element
        QFile file( path );
        // Opening and Checking if the file exists
        if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
            return false;
        // Settings and Checking if the file containts XML data
        contents.setContent( &file );
        file.close();
        if ( contents.isNull() )
            return false;
        return true;
    }
    static inline bool findElement( QDomElement& element, const QString& attribute, const QString& comp )
    {
        while ( !element.isNull() )
        {
            if ( element.attribute( attribute ) == comp )
                return true;
            element = element.nextSiblingElement();
        }
        return false;
    }
};

#endif // FILE_H
