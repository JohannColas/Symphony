#ifndef ICONS_H
#define ICONS_H

// ------------------------------------ //
#include <QDir>
#include <QIcon>
#include "Files.h"


// ------------------------------------ //
// ------------ Icons KEYS ------------ //
// ------------------------------------ //
namespace IconsKeys
{
	static inline QString Symphony = "Symphony";
	static inline QString Add = "add";
	static inline QString AddPlaylist = "playlist_add";
	static inline QString AddToPlayer = "addToPlayer";
	static inline QString Album = "album";
	static inline QString Artist = "artist";
	static inline QString Backward = "skip_previous";
	static inline QString Change = "change";
	static inline QString Close = "close";
	static inline QString Delete = "delete";
	static inline QString Equalizer = "equalizer";
	static inline QString FastForward = "fast_forward";
	static inline QString FastRewind = "fast_rewind";
	static inline QString Forward = "skip_next";
	static inline QString FullscreenExit = "fullscreen_exit";
	static inline QString Fullscreen = "fullscreen";
	static inline QString Genre = "genre";
	static inline QString Info = "info";
	static inline QString Libraries = "libraries";
	static inline QString Library = "library";
	static inline QString Maximize = "maximise";
	static inline QString Minimize = "minimise";
	static inline QString More = "more_horiz";
	static inline QString Music = "music";
	static inline QString Mute = "mute";
	static inline QString New = "new";
	static inline QString NoRepeat = "no_repeat";
	static inline QString NoShuffle = "no_shuffle";
	static inline QString Open = "open";
	static inline QString Pause = "pause";
	static inline QString Play = "play";
	static inline QString PlayPlaylist = "playlist_play";
	static inline QString Playlist = "playlist";
	static inline QString Podcast = "podcast";
	static inline QString Queue = "queue";
	static inline QString Radio = "radio";
	static inline QString RepeatAll = "repeat_all";
	static inline QString RepeatOne = "repeat_one";
	static inline QString Repeat = "repeat";
	static inline QString Replay = "replay";
	static inline QString Save = "save";
	static inline QString Search = "search";
	static inline QString Settings = "settings";
	static inline QString Shuffle = "shuffle";
	static inline QString Stop = "stop";
	static inline QString Track = "track";
	static inline QString Unmaximize = "unmaximise";
	static inline QString Update = "update";
	static inline QString Volume = "volume";
	static inline QString VolumeDown = "volume_down";
}

class IconsObject
		: public QObject
{
	Q_OBJECT
public:
	IconsObject( QObject* obj = 0 )
		: QObject( obj )
	{

	}

signals:
	void changed();
};

// ------------------------------------ //
// ------------ Icons CLASS ----------- //
// ------------------------------------ //
class Icons
{
	static inline IconsObject obj_icons = new IconsObject;
	static inline QString _path = "./icons/DarkTheme";
	static inline QFileInfoList _availableIconPacks;

public:
	static inline void setPath( const QString& path )
	{
		_path = path;
		emit obj_icons.changed();
	}
	static inline void setPath( const QFileInfo& fileinfo )
	{
		setPath( fileinfo.absoluteFilePath() );
	}
	// -------------- Get Available Themes -------------- //
	static inline QFileInfoList availableIconPacks()
	{
		_availableIconPacks.clear();
		_availableIconPacks.append( QDir("./icons").entryInfoList( QDir::NoDotAndDotDot | QDir::AllDirs, QDir::Name | QDir::IgnoreCase) );
		_availableIconPacks.append( QDir( Files::localPath() + "/icons").entryInfoList( QDir::NoDotAndDotDot | QDir::AllDirs, QDir::Name | QDir::IgnoreCase) );
//		qDebug() << _availableIconPacks.first();
		return _availableIconPacks;
//		QFileInfoList iconPacks;
//		iconPacks.append( QDir("./icons").entryInfoList( QDir::NoDotAndDotDot | QDir::AllDirs, QDir::Name | QDir::IgnoreCase) );
//		iconPacks.append( QDir( Files::localPath() + "/icons").entryInfoList( QDir::NoDotAndDotDot | QDir::AllDirs, QDir::Name | QDir::IgnoreCase) );
//		return iconPacks;
	}
	// -------------- Get Font -------------- //
	static inline QIcon get( const QString& key )
	{
		QIcon icon( _path + "/" + key + ".svg" );

		return icon;
	}
};


#endif // ICONS_H
