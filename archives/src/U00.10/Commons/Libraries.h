#ifndef LIBRARIES_H
#define LIBRARIES_H
/**********************************************/
#include <QObject>
#include <QFile>
#include <QDomDocument>
/**********************************************/
#include "Commons/Library.h"
#include "Commons/Artist.h"
/**********************************************/
#include "Commons/OnLineMetadata.h"
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Libraries
		: public QObject
{
	Q_OBJECT
protected:
	//    QString _path;
	//    QString _libraryPath;
	QString _libraryName = "Library";
	QList<Library*> _libraries;
	QStringList _filesFilters = {"*.mp3", "*.ogg", ".wav", ".flac", ".m4a", ".wma"};
	QList<Artist*> _artists;
	QList<Album*> _albums;
	QList<Track*> _tracks;
	Settings* _settings = 0;

public:
	Libraries()
	{
		initLibraries();
		initLibrary();
	}
	QStringList libraryNames()
	{
		QStringList names;
		for ( Library* library : _libraries )
			names.append( library->name() );
		return names;
	}
	int nbLibraries()
	{
		return _libraries.size();
	}
	Library* at( int index )
	{
		if ( isIndexValid( index ) )
			return _libraries.at( index );
		return 0;
	}
	bool isIndexValid( int index )
	{
		if ( -1 < index && index < _libraries.size() )
			return true;
		return false;
	}
	void connectTo( Library* library )
	{
		connect( library, &Library::changed,
				 this, &Libraries::changed );
	}
	void disconnectTo( Library* library )
	{
		if ( library )
		{
			disconnect( library, &Library::changed,
						this, &Libraries::changed );
		}
	}
	void setSettings( Settings* sets )
	{
		_settings = sets;
		//        _path = Files::librariesPath( Files::LibrariesName );
		//        _libraryPath = Files::librariesPath( Files::LibraryName );
		initLibraries();
		initLibrary();
		//        updateLibrary();
	}
	QString getNewName( const QString& name )
	{
		QString newname = name;
		bool stop = true;
		for ( int it = 0; it < 1000; ++it )
		{
			if ( it != 0 )
				newname = name + " " + QString::number(it);
			stop = true;
			for( Library* lib : _libraries )
			{
				if ( lib->name() == newname )
				{
					stop = false;
					break;
				}
			}
			if ( stop )
				break;
		}
		return newname;
	}

public slots:
	void addNew( const QString& name = QString() )
	{
		if ( name.isEmpty() )
			return;
		QString newname = name;
		int idLy = 0;
		for( Library* library : _libraries )
		{
			if ( library->name().contains( newname ) )
			{
				int lyID = library->name().remove( newname + " ").toInt();
				if ( lyID == idLy )
					++idLy;
				else if ( lyID > idLy )
					idLy = lyID + 1;
			}
		}
		newname += " " + QString::number(idLy);
		_libraries.append( new Library( newname ) );
		emit changed();
	}
	void initLibraries()
	{
		while ( !_libraries.isEmpty() )
		{
			delete _libraries.takeFirst();
		}
		// Getting the XML data from file
		QDomDocument xmlDoc;
		if ( !Files::readXML( Files::librariesPath( Files::LibrariesName ), xmlDoc ) )
			return;
		// Getting root element ("libraries")
		QDomElement el_libraries = xmlDoc.documentElement();
		// Getting library elements
		QDomElement el_libray = el_libraries.firstChild().toElement();
		while ( !el_libray.isNull() )
		{
			QString name = el_libray.attribute("name");
			QString path = el_libray.attribute("path");
			bool hide = (el_libray.attribute("hide") == "true") ? true : false;
			Library* library = new Library( name, path, hide );
			connectTo( library );
			_libraries.append( library );
			el_libray = el_libray.nextSiblingElement();
		}
	}
	void saveLibraries()
	{
		// Create the XML data from file
		QDomDocument xmlDoc;
		// Getting root element ("libraries")
		QDomElement el_libraries = xmlDoc.createElement("libraries");
		xmlDoc.appendChild( el_libraries );
		// Getting library elements
		for ( Library* library : _libraries )
		{
			QDomElement el_libray = xmlDoc.createElement("library");
			el_libray.setAttribute( "name", library->name() );
			el_libray.setAttribute( "path", library->path() );
			el_libray.setAttribute( "hide", library->hide() );
			el_libraries.appendChild( el_libray );
		}
		Files::saveXML( Files::librariesPath( Files::LibrariesName ), xmlDoc );
	}
	void initLibrary()
	{
		qDebug() << "Init all Libraries";
		QElapsedTimer timer;
		timer.start();
		// Complete in ~ 20ms for 2500 tracks
		_artists.clear();
		_albums.clear();
		_tracks.clear();
		// Getting the XML data from file
		QDomDocument xmlDoc;
		if ( !Files::readXML( Files::librariesPath( Files::LibraryName ), xmlDoc ) )
			return;
		// Getting root element ("library")
		QDomElement el_libray = xmlDoc.documentElement();
		// Getting First Artist element
		QDomElement el_artist = el_libray.firstChild().toElement();
//		Artist* unknownArtist = new Artist("");
//		Album* unknownAlbum = new Album("");
//		_albums.append( unknownAlbum );
		while ( !el_artist.isNull() )
		{
			Artist* artist = new Artist( el_artist.attributeNode("name").value() );
			artist->setBiography( el_artist.attributeNode("biography").value() );
			artist->setDate( el_artist.attributeNode("date").value() );
			artist->setGender( el_artist.attributeNode("gender").value() );
			artist->setGenre( el_artist.attributeNode("genre").value() );
			artist->setNbMembers( el_artist.attributeNode("nbmembers").value() );
			artist->setCountry( el_artist.attributeNode("country").value() );
			artist->setWebsite( el_artist.attributeNode("website").value() );
			QDomElement el_album = el_artist.firstChild().toElement();
			while ( !el_album.isNull() )
			{
				Album* album = new Album( el_album.attributeNode("title").value(), artist->name() );
				album->setDate( el_album.attributeNode("date").value() );
				album->setGenre( el_album.attributeNode("genre").value() );
				album->setDescription( el_album.attributeNode("description").value() );
				album->setArtist( artist );
				QDomElement el_track = el_album.firstChild().toElement();
				while ( !el_track.isNull() )
				{
					Track* track = new Track( el_track.attributeNode("path").value() );
					track->setTitle( el_track.attributeNode("title").value() );
					track->setGenre( el_track.attributeNode("genre").value() );
					track->setNumber( el_track.attributeNode("number").value() );
					track->setDescription( el_track.attributeNode("description").value() );
					track->setComment( el_track.attributeNode("comment").value() );
					track->setAlbum( album );
					album->addTrack( track );
//					_tracks.append( track );
					el_track = el_track.nextSiblingElement();
				}
//				if ( album->title() == "" )
//				{
//					unknownAlbum->append( album );
//				}
//				else
//				{
//					_albums.append( album );
//				}
				artist->addAlbum( album );
				el_album = el_album.nextSiblingElement();
			}
			_artists.append( artist );
			el_artist = el_artist.nextSiblingElement();
		}
		sortArtistByName();
		qDebug() << "  Method 1 :" << timer.elapsed() << "ms";
		timer.start();
		// Complete in ~ 20ms for 2500 tracks
//		_artists.clear();
//		_albums.clear();
//		_tracks.clear();

//		for ( Library* lib : _libraries )
//		{
//			if ( !lib->hide() )
//			{
//				QString libPath = Files::librariesPath();
//				QDir dir( libPath );
//				dir.mkdir( lib->name() );
//				dir.cd( lib->name() );
//				dir.mkdir( "artists" );
//				QDir artistDir(dir);
//				artistDir.cd( "artists" );
//				dir.mkdir( "tracks" );
//				QDir trackDir(dir);
//				trackDir.cd( "tracks" );
//				libPath = Files::librariesPath( lib->name() );
//				QFileInfoList lst = trackDir.entryInfoList( QDir::Files,  QDir::SortFlag::Name |
//													  QDir::IgnoreCase |
//													  QDir::LocaleAware  );
//				for ( QFileInfo tcktle : lst )
//				{
//					QSettings set( tcktle.absoluteFilePath(), QSettings::IniFormat );
//					set.setIniCodec( "UTF-8" );
//					Track* tck = new Track( set.value("path").toString() );
//					tck->setTitle( set.value("title").toString() );
//					QString artist = set.value("artist").toString();
//					bool notfound = true;
//					Artist* artt;
//					for ( Artist* art : _artists )
//					{
//						if ( art->name() == artist )
//						{
//							notfound = false;
//							artt = art;
//							break;
//						}
//					}
//					if ( notfound )
//						artt = new Artist(artist);
//					_artists.append( artt);
//					QString album = set.value("album").toString();
//					notfound = true;
//					Album* albb;
//					for ( Album* alb : artt->albums() )
//					{
//						if ( alb->title() == album )
//						{
//							notfound = false;
//							albb = alb;
//							break;
//						}
//					}
//					if ( notfound )
//						albb = new Album(album);
////					_albums.append( albb);
//					artt->addAlbum( albb );
//					albb->addTrack( tck );
//					tck->setAlbum( albb );
//					albb->setArtist( artt );
//				}
//			}
//		}

//		sortArtistByName();
//		qDebug() << "  Method 2 :" << timer.elapsed() << "ms";
		emit changed();
		OnlineMetadata* meta = new OnlineMetadata( _artists );
		meta->start();
	}
	void updateLibrary()
	{
		qDebug() << "Update all Libraries";
		QElapsedTimer timer;
		timer.start();
		//
		QDomDocument xmlDoc;
		// Writing the Settings of XML Files
		QDomElement el_library = xmlDoc.createElement("library");
		xmlDoc.appendChild( el_library );

		for ( Library* library : _libraries )
		{
			if ( !library->hide() )
			{
				QDir dir( Files::librariesPath() );
				dir.mkdir( library->name() );
				// Getting files in directory
				for ( QString trackpath : Files::AudioFileList( library->path() ) )
				{
					// Getting the Metadata of the current file
					Metadata meta( new Track( library->path() + "/" + trackpath) );
					// Getting the Artist
					QDomElement el_artist = el_library.firstChildElement();
					// Checking if the Artist exits in the file
					if ( !Files::findElement( el_artist, "name", meta.artist() ) )
					{
						el_artist = xmlDoc.createElement("artist");
						el_artist.setAttribute( "name", meta.artist() );
						el_artist.setAttribute( "biography", "biography" );
						el_artist.setAttribute( "date", "date" );
						el_artist.setAttribute( "gender", "gender" );
						el_artist.setAttribute( "genre", "genre" );
						el_artist.setAttribute( "nbMembers", "nbmembers" );
						el_artist.setAttribute( "country", "country" );
						el_artist.setAttribute( "website", "website" );
						el_library.appendChild( el_artist );
					}
					// Getting the Album
					QDomElement el_album = el_artist.firstChildElement();
					// Checking if the Album exits in the file
					if ( !Files::findElement( el_album, "title", meta.album() ) )
					{
						el_album = xmlDoc.createElement("album");
						el_album.setAttribute( "title", meta.album() );
						el_album.setAttribute( "genre", "genre" );
						el_album.setAttribute( "date", "date" );
						el_album.setAttribute( "description", "description" );
						el_artist.appendChild( el_album );
					}
					// Getting the Track
					QDomElement el_track = el_artist.firstChildElement();
					// if Track DONT exits, create it
					if ( !Files::findElement( el_track, "path", meta.path() ) )
					{
						el_track = xmlDoc.createElement("track");
						el_track.setAttribute( "title", meta.title() );
						el_track.setAttribute( "path", meta.path() );
						el_track.setAttribute( "genre", meta.genre() );
						el_track.setAttribute( "number", meta.number() );
						el_track.setAttribute( "description", "description" );
						el_track.setAttribute( "comment", "comment" );
						el_album.appendChild( el_track );
					}
				}
			}

		}
		Files::saveXML( Files::librariesPath( Files::LibraryName ), xmlDoc );
		qDebug() << "  Method 1 :" << timer.elapsed() << "ms";
//		timer.start();
//		// Second method
//		for ( Library* lib : _libraries )
//		{
//			if ( !lib->hide() )
//			{
//				QString libPath = Files::librariesPath();
//				Files::deleteFolder( Files::makePath( libPath, lib->name() ) );
//				QDir dir( libPath );
//				dir.mkdir( lib->name() );
//				dir.cd( lib->name() );
//				dir.mkdir( "artists" );
//				QDir artistDir(dir);
//				artistDir.cd( "artists" );
//				dir.mkdir( "tracks" );
//				QDir trackDir(dir);
//				trackDir.cd( "tracks" );
//				libPath = Files::librariesPath( lib->name() );
//				// Getting files in directory
//				for ( QString trackpath : Files::AudioFileList( lib->path() ) )
//				{
//					Metadata meta( new Track( lib->path() + "/" + trackpath) );
//					meta.saveTrackInfo( trackDir.absolutePath() );
//					QString artist = meta.artist();
//					if ( artist.isEmpty() )
//						artist = "[UNKNOWN]";
//					artistDir.mkdir( artist );
//					//				dir2.mkpath( "artist.info" );
//					QString album = meta.album();
//					if ( album.isEmpty() )
//						album = "[UNKNOWN]";
//					artistDir.mkdir( artist + "/" + album );
//					//				dir2.mkpath( "album.info" );
//					//				dir2.mkpath( meta.title() + ".info" );
//				}
//			}
//		}
//		qDebug() << "  Method 2 :" << timer.elapsed() << "ms";
//		initLibrary();
		emit changed();
	}
	QList<Artist*> artists()
	{
		return _artists;
	}
	QList<Album*> albums()
	{
		QList<Album*> albs;
		for ( Artist* art : _artists )
			albs.append( art->albums() );
		return albs;
	}
	QList<Track*> tracks()
	{
		QList<Track*> tcks;
		for ( Artist* art : _artists )
			for ( Album* alb : art->albums() )
				tcks.append( alb->tracks() );
		return tcks;
	}
	TrackList* toTracklist()
	{
		TrackList* newTL = new TrackList;
		newTL->add( tracks() );
		return newTL;
	}
	void sortArtistBy(  )
	{

	}
	void sortArtistByName(  )
	{
		std::sort( _artists.begin(), _artists.end(), Artist::compare );
	}
	void remove( int index )
	{
		if ( isIndexValid(index) )
		{
			_libraries.removeAt( index );
			//			else
			//			{
			//				Files::deleteFolder( Files::makePath( Files::librariesPath(), library->name() ) );
			//			}
			emit changed();
		}
	}

signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARIES_H
