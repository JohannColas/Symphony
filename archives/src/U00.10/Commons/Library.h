#ifndef LIBRARY_H
#define LIBRARY_H
/**********************************************/
#include <QObject>
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Library
		: public QObject
{
	Q_OBJECT
private:
	QString _name = "";
	QString _path = "";
    bool _hide = false;
    /**********************************************/
public:
    Library( const QString& name, const QString& path = "", bool hide = false )
		: _name( name ), _path( path ), _hide( hide )
	{
	}
	QString name() const
	{ return _name; }
	QString path() const
	{ return _path; }
	bool hide() const
	{ return _hide;	}

public slots:
	void setName( const QString& name )
	{ _name = name; emit changed(); }
	void setPath( const QString& path )
	{ _path = path; emit changed(); }
	void setHide( bool hide )
	{ _hide = hide; emit changed(); }

signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LIBRARY_H
