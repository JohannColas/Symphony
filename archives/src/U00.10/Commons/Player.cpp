#include "Player.h"
#include "App.h"
/**********************************************/
/**********************************************/
Player::Player()
	: QMediaPlayer()
{
}
/**********************************************/
/**********************************************/
void Player::randomize()
{
	if ( App::queue() )
	{
		_listOrder.clear();
		for ( int it = 0; it < App::queue()->count(); ++it )
		{
			_listOrder.append( it );
		}
		std::srand(std::time(nullptr));
		std::random_shuffle( _listOrder.begin(), _listOrder.end() );
		if ( _index != -1 && _listOrder.contains(_index) )
		{
			while ( _listOrder.first() != _index )
			{
				_listOrder.append( _listOrder.takeFirst() );
			}
		}
	}
}
/**********************************************/
/**********************************************/
void Player::reset()
{
	_index = 0;
	if ( _isShuffle )
		randomize();
	else
		_listOrder.clear();
	setTrack();
}
/**********************************************/
/**********************************************/
Track* Player::currentTrack()
{
	return App::queue()->track(_index);
}
/**********************************************/
/**********************************************/
void Player::play()
{
	//	App::queue()->mediaPlaylist();
	setMedia( QUrl::fromLocalFile( App::queue()->track(_index)->path() ) );
	QMediaPlayer::play();
	_isPlaying = true;
}
/**********************************************/
/**********************************************/
void Player::forward()
{
	++_index;
	if ( _index == App::queue()->count() )
	{
		_index = 0;
		if ( _repeat == Repeat::NOREPEAT )
		{
			stop();
		}
	}
	setTrack();
}
/**********************************************/
/**********************************************/
void Player::backward()
{
	if ( position() < 1000 )
	{
		--_index;
		if ( _index == -1 )
			_index = App::queue()->count() - 1;
		setTrack();
	}
	else
		setPosition( 0 );
}
/**********************************************/
/**********************************************/
void Player::repeat()
{
	if ( _repeat == Repeat::NOREPEAT )
		_repeat = Repeat::REPEATALL;
	else if ( _repeat == Repeat::REPEATALL )
		_repeat = Repeat::REPEATONE;
	else if ( _repeat == Repeat::REPEATONE )
		_repeat = Repeat::NOREPEAT;
	//	        updateRepeatIcon();
	//        saveSettings( _repeat, Keys::Player, Keys::Repeat );
	App::settings()->save( Keys::Player, Keys::Repeat, _repeat );
}
/**********************************************/
/**********************************************/
void Player::shuffle()
{
	_isShuffle = !_isShuffle;
	if ( _isShuffle )
	{
		randomize();
	}
	else
	{
		if ( _index >= 0 && _index < _listOrder.size() )
			_index = _listOrder.at(_index);
		_listOrder.clear();
	}
	//	updateShuffleIcon();
	App::settings()->save( Keys::Player, Keys::Shuffle, _isShuffle );
}
/**********************************************/
/**********************************************/
void Player::setTrack()
{
	int index = _index;
	if ( _isShuffle && _listOrder.size() > 0)
		index = _listOrder.at( index );
//	Track* track = App::queue()->track(index);
//	qDebug() << track->title();
//	setMedia( QUrl::fromLocalFile( track->path() ) );
	//	_playerTrackBanner->setTrack( track );
	//	updateTrackInfo( track );
	if ( _isPlaying )
		play();
	//	updatePlayPauseIcon();
	emit trackChanged();
	emit currentTrackChanged( App::queue()->track(_index) );
}
/**********************************************/
/**********************************************/
