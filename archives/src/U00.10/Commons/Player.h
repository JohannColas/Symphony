#ifndef PLAYER_H
#define PLAYER_H
/**********************************************/
#include <QMediaPlayer>
class Track;
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Player
		: public QMediaPlayer
{
	Q_OBJECT
public:
	enum Repeat {
		NOREPEAT = 0,
		REPEATALL = 1,
		REPEATONE = 2
	};
//	static inline QMediaPlayer* _player = new QMediaPlayer;
	//
	bool _isPlaying = false;
	Repeat _repeat = Repeat::NOREPEAT;
	bool _isShuffle = false;
	//
	int _index = 0;
	QList<int> _listOrder;

public:
	Player();
	void randomize();
	void reset();
	Player::Repeat getRepeat() const {return _repeat;}
	bool getShuffle() const {return _isShuffle;}
	Track* currentTrack();
public slots:
	void play();
	void pause()
	{
		QMediaPlayer::pause();
		_isPlaying = false;
	}
	void playpause()
	{
		if ( state() == QMediaPlayer::PlayingState )
			pause();
		else
			play();
		// Update Play/Pause Icon
//        updatePlayPauseIcon();
	}
	void stop()
	{
		QMediaPlayer::stop();
		_isPlaying = false;
	}
	void forward();
	void backward();
	void repeat();
	void shuffle();
	void setTrack();
signals:
	void trackChanged();
	void currentTrackChanged( Track* track );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYER_H
