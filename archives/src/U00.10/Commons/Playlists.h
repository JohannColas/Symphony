#ifndef PLAYLISTS_H
#define PLAYLISTS_H
/**********************************************/
#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QDirIterator>
#include <QSettings>
#include <QDomDocument>
#include "Commons/TrackList.h"
#include "Commons/Metadata.h"
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Playlists
        : public QObject
{
    Q_OBJECT
protected:
	static inline QList<TrackList*> _list2;
	static inline QStringList _savedPlaylists;
	QList<TrackList*> _list;
    QString _playlistName = "Playlist";
    int _currentIndex = -1;
    Settings* _settings = 0;

public:
	static inline QStringList savedPlaylists()
	{
		_savedPlaylists.clear();
		_savedPlaylists.append( QDir( Files::localPath() + "/playlists").entryList({"*.spl"}, QDir::Files, QDir::Name | QDir::IgnoreCase) );
		return _savedPlaylists;
	}
    Playlists()
    {
    }
    QList<TrackList*> list()
    {
        return _list;
    }
    TrackList* tracklist( int index ) const
    {
        if ( isIndexValid( index ) )
            return _list.at( index );
        return 0;
    }
    int currentIndex() const
    {
        return _currentIndex;
    }
    TrackList* currentTracklist() const
    {
        if ( isIndexValid( _currentIndex ) )
            return tracklist( currentIndex() );
        return 0;
    }
    TrackList* currentTrackIndex() const
    {
        if ( isIndexValid( _currentIndex ) )
            return _list.at( _currentIndex );
        return 0;
    }
    int count() const
    {
        return _list.size();
    }
    bool isIndexValid( int index ) const
    {
        return ( index > -1 && index < count() );
	}
	TrackList* playlist( const QString& name )
	{
		for ( TrackList* tl : _list )
			if ( tl->name() == name )
				return tl;
		return 0;
	}
	void addToPlaylist( const QString& name, const QList<Track*> tracks )
	{
		playlist( name )->add( tracks );
//		for ( TrackList* tl : _list )
//		{
//			if ( tl->name() == name )
//			{
//				tl->add( tracks );
//				return;
//			}
//		}
	}
    void setSettings( Settings* sets )
    {
        _settings = sets;
        update();
    }
    QString getNewName( const QString& name )
    {
        QString newname = name;
        bool stop = true;
        for ( int it = 0; it < 1000; ++it )
        {
            if ( it != 0 )
                newname = name + " " + QString::number(it);
            for( TrackList* playlist : _list )
            {
                stop = true;
                if ( playlist->name() == newname )
                {
                    stop = false;
                    break;
                }
            }
            if ( stop )
                break;
        }
        return newname;
    }

public slots:
    void setCurrentIndex( int index )
    {
        if ( isIndexValid( index ) )
        {
            _currentIndex = index;
            emit currentIndexChanged( currentIndex() );
            emit currentTrackListChanged( currentTracklist() );
        }
    }
    void addNew( const QString& name = QString() )
    {
        if ( name.isEmpty() )
            return;
        QString newname = getNewName( name );

        TrackList* tracklist = new TrackList( newname,
                                              TrackList::PLAYLIST );
        _list.append( tracklist );
        emit tracklistAdded();
        emit changed();
    }
    void rename( const QString& name )
    {
        if ( isIndexValid( currentIndex() ) )
        {
            currentTracklist()->setName( name );
            emit changed();
        }
    }
    void addTracklist( const QString& path )
    {
        TrackList* tracklist = new TrackList( path,
                                              TrackList::PLAYLIST );
        _list.append( tracklist );
        connect( tracklist, &TrackList::changed,
                 this, &Playlists::changed );
        emit tracklistAdded();
        emit changed();
    }
    void add( TrackList* tracklist )
    {
        _list.append( tracklist );
        emit tracklistAdded();
    }
    TrackList* addNewPlaylist( const QString& path )
    {
        TrackList* tmptl = 0;
        for ( TrackList* tl : _list )
        {
            if ( tl->name() == path )
            {
                tmptl = tl;
                break;
            }
        }
        if ( !tmptl )
        {
            tmptl = new TrackList( path, TrackList::PLAYLIST );
            _list.append( tmptl );
        }
        emit changed();
        return tmptl;
    }
    void move( int from, int to )
    {
        if ( isIndexValid( from ) &&
             isIndexValid( to ) )
        {
            _list.move( from, to );
            emit tracklistMoved();
        }
    }
    void remove( int index)
    {
        if ( isIndexValid( index ) )
        {
            _list.removeAt( index );
//            emit tracklistRemoved();
            emit changed();
        }
    }
    void removeThis( TrackList* tl )
    {
        _list.removeOne( tl );
        delete tl;
        save();
        emit changed();
    }
    void clear()
    {
        _list.clear();
        emit cleared();
    }
    void update()
    {
        QString libraryPath = Files::playlistsPath( Files::PlaylistsName );
//        QDir dir( Files::playlistsPath() );
//        dir.entryList( {"*.spl"} );
        QString contents;
        if ( Files::read( libraryPath, contents ) )
        {
            QStringList strs = contents.split("\n");
            for ( QString str : strs )
                if ( !str.isEmpty() )
                    addTracklist( str );
        }
        else
        {
            qDebug() << "Playlists - Update - error Reading file :"
                     << libraryPath;
        }
    }
    void save()
    {
        QString path = Files::playlistsPath( Files::PlaylistsName );
//        if ( Files::exists( path ) )
        {
            QString out;
            for ( TrackList* tl : _list )
			{
                out += tl->name() + "\n";
				qDebug() << "Playlists - Saving "
						 << tl->name();
			}
			Files::save( path, out );
        }
        for ( TrackList* playlist : _list )
            playlist->save();
//        else
//        {
//            qDebug() << "Playlists - Save - error Writing file :"
//                     << path;
//        }
    }

signals:
    void changed();
    void tracklistAdded();
    void tracklistMoved();
    void tracklistRemoved();
    void cleared();
    void currentIndexChanged( int index );
    void currentTrackListChanged( TrackList* tracklist );
    void currentTrackChanged( Track* track );

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYLISTS_H
