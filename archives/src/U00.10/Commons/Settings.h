#ifndef SETTINGS_H
#define SETTINGS_H
// ------------------------------------ //
#include "SettingsFile.h"
#include "Theme.h"
#include "Icons.h"
#include "Lang.h"
// ------------------------------------ //
#include <QIcon>
// ------------------------------------ //
// ---------- Settings Keys ----------- //
// ------------------------------------ //
namespace Keys {
	// General Keys
	static QString Geometry = "Geometry";
	static QString Fullscreen = "fullscreen";
    static QString Maximized = "maximized";
    static QString X = "x";
    static QString Y = "y";
	static QString Width = "width";
	static QString Height = "height";
    // Appearance Keys
    static QString Appearance = "Appearance";
    static QString IconPack = "iconPack";
    static QString Theme = "theme";
    // Behaviour Keys
    static QString Behaviour = "Behaviour";
    static QString OnDoubleClick = "onDoubleClick";
    // Player Keys
    static QString Player = "Player";
    static QString Position = "position";
    static QString Repeat = "repeat";
    static QString Shuffle = "shuffle";
    static QString Size = "size";
    // SectionsSelector Keys
    static QString SectionsSelector = "SectionsSelector";
    static QString HideText= "hideText";
    static QString HideArtists = "hideArtists";
    static QString HideAlbums = "hideAlbums";
    static QString HideGenres = "hideGenres";
    static QString HideTracks = "hideTracks";
    static QString HidePlaylists = "hidePlaylists";
    static QString HideLibraries = "hideLibraries";
    static QString HideRadios = "hideRadios";
    static QString HidePodcasts = "hidePodcasts";
    // Shortcuts Keys
    static QString Shortcuts = "Shortcuts";
    static QString PlayPause = "playpause";
    static QString Stop = "stop";
    static QString Forward = "forward";
    static QString Backward = "backward";
//    static QString Repeat = "stop";
//    static QString Shuffle = "stop";
}
enum Position
{
    TOP = 0,
    RIGHT = 1,
    BOTTOM = 2,
    LEFT = 3,
};


// ------------------------------------ //
// ---------- Settings CLASS ---------- //
// ------------------------------------ //
class Settings
		: public SettingsFile
{
	Q_OBJECT
protected:
	static inline QString _settingsName = "settings.ini";
//	static inline SettingsFile* _settings = new SettingsFile;
	static inline SettingsFile* _workingSettings = new SettingsFile;

public:
	Settings()
		: SettingsFile()
	{
		qDebug().noquote() << "";
		qDebug().noquote() << "--------------";
		qDebug().noquote() << "Settings Class Initialisation";
		_workingSettings = new SettingsFile;
		_workingSettings->setPath( _settingsName );
		// Get local directory
		_path = Files::localPath( _settingsName );
		// Copy settings.ini in userDir if not exists
		if ( !QFileInfo(_path).exists() )
			QFile::copy( "./" + _settingsName, _path );
		QString _path = Files::localPath( _settingsName );
		// Copy settings.ini in userDir if not exists
		if ( !QFileInfo(_path).exists() )
			QFile::copy( "./" + _settingsName, _path );
		this->setPath( _path, true );
		qDebug() << "Settings path :" << _path;
		qDebug().noquote() << "--------------";
		qDebug().noquote() << "";
//		init();
	}
//	static inline void init()
//	{
//		qDebug().noquote() << "";
//		qDebug().noquote() << "--------------";
//		qDebug().noquote() << "Settings Class Initialisation";
//		// Get local directory
//		QString _path = Files::localPath( _settingsName );
//		// Copy settings.ini in userDir if not exists
//		if ( !QFileInfo(_path).exists() )
//			QFile::copy( "./" + _settingsName, _path );
//		_settings->setPath( _path, true );
//		qDebug() << "Settings path :" << _path;
////		_settings->init();
//		//
//		Theme::setPath( Theme::availableThemes().first().absoluteFilePath() );
//		qDebug() << "Theme path :" << Theme::availableThemes().first().absoluteFilePath();
//		//
//		Icons::setPath( Icons::availableIconPacks().first().absoluteFilePath() );
//		qDebug() << "Icon path :" << Icons::availableIconPacks().first().absoluteFilePath();
//		//
//		qDebug().noquote() << "--------------";
//		qDebug().noquote() << "";
//	}
	static inline void reset()
	{
		QString _path = Files::localPath( _settingsName );
		QFile::copy( "./" + _settingsName, Files::localPath( "backup.ini" ) );
		QFile::copy( "./" + _settingsName, _path );
//		_settings->setPath( _path, true );
	}
//	static inline void save( const QString& key, const QVariant& value )
//	{
//		_settings->save( key, value );
//	}
//	static inline void save( const QString& key, const QString& subkey, const QVariant& value )
//	{
//		_settings->save( key, subkey, value );
//	}
//	static inline bool getBool( const QString& key, const QString& subkey = QString() )
//	{
//		return _settings->toBool( key, subkey );
//	}
//	static inline int getInt( const QString& key, const QString& subkey = QString() )
//	{
//		return _settings->toInt( key, subkey );
//	}
//	static inline QString getString( const QString& key, const QString& subkey = QString() )
//	{
//		return _settings->toString( key, subkey );
//	}
//	static inline QVariant value( const QString& key, const QString& subkey = QString() )
//	{
//		return _settings->value( key, subkey );
//	}
    // set Settings as Default
//	void setAsDefault()
//	{
//		_path = Files::localPath( _settingsName );
//		QFile::copy( "./" + _settingsName, _path );
//        init();
//    }

};


#endif // SETTINGS_H
