#ifndef SETTINGS2_H
#define SETTINGS2_H

#include "Files.h"

// ------------------------------------ //
// ---------- Settings CLASS ---------- //
// ------------------------------------ //
class Settings2
		: public QObject
{
	Q_OBJECT
protected:
	QSettings* _settings = 0;
	QString _path = "settings.ini";

public:
	Settings2( const QString& path = QString() )
		: QObject()
	{
		init(path);
	}
	// Initialise Settings "object"
	void init( const QString& path = QString() )
	{
		if ( path.isEmpty() ||
			 !QFileInfo(path).exists() )
			return;
		_path = path;
		if ( _settings )
			delete _settings;
		// Open settings
		_settings = new QSettings( _path, QSettings::IniFormat );
	}
	//
	QString path()
	{
		return _path;
	}
	// Get Settings "object"
	QSettings* settings()
	{
		return _settings;
	}
	// Save Setting with key
	void save( const QVariant& value, const QString& key, const QString& subkey = "" )
	{
		if ( _settings )
		{
			QString flkey = fullkey(key, subkey);
			QColor color = value.value<QColor>();
			if ( color.isValid() )
				_settings->setValue( flkey, color.name(QColor::HexArgb) );
			else
				_settings->setValue( flkey, value );
		}
	}
	// Get QVariant Setting
	QVariant value( const QString& key, const QString& subkey = "" )
	{
		if ( _settings )
			return _settings->value( fullkey( key, subkey ) );
		return QVariant();
	}
	// Get integer Setting
	int toInt( const QString& key, const QString& subkey = "" )
	{
		QVariant var = value( key, subkey );
		if ( var.isValid() )
			return var.toInt();
		return 0;
	}
	// Get double Setting
	int toDouble( const QString& key, const QString& subkey = "" )
	{
		QVariant var = value( key, subkey );
		if ( var.isValid() )
			return var.toDouble();
		return 0;
	}
	// Get QString Setting
	QString toString( const QString& key, const QString& subkey = "" )
	{
		QVariant var = value( key, subkey );
		if ( var.isValid() )
			return var.toString();
		return QString();
	}
	// Get QChar Setting
	QChar toChar( const QString& key, const QString& subkey = "" )
	{
		QVariant var = value( key, subkey );
		if ( var.isValid() )
			return var.toChar();
		return QChar();
	}
	// Get QColor Setting
	QColor toColor( const QString& key, const QString& subkey = "" )
	{
		return QColor(toString(key, subkey));
	}
	// Get boolean Setting
	bool toBool( const QString& key, const QString& subkey = "" )
	{
		QVariant var = value( key, subkey );
		if ( var.isValid() )
			return var.toBool();
		return false;
	}
//	void beginGroup( const QString& group )
//	{
//		_settings->beginGroup( group );
//	}
//	void endGroup()
//	{
//		_settings->endGroup();
//	}
	// Get complete key
	QString fullkey( const QString& key, const QString& subkey = "" )
	{
		QString flkey = key;
		if ( !subkey.isEmpty() )
			flkey += "/" + subkey;
		return flkey;
	}

signals:
	void valueChanged( QString key );
};
#endif // SETTINGS2_H
