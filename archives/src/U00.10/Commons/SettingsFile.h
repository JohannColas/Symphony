#ifndef SETTINGSFILE_H
#define SETTINGSFILE_H

#include "Files.h"

// ------------------------------------ //
// ---------- SSettings CLASS ---------- //
// ------------------------------------ //
class SettingsFile
		: public QObject
{
	Q_OBJECT
protected:
	QSettings* _file = 0;
	QString _path;

public:
	SettingsFile( const QString& path = QString() )
		: QObject()
	{
		setPath( path );
	}
	// Initialise Settings "object"
	virtual bool init()
	{
		if ( _path.isEmpty() ||
			 !QFileInfo(_path).exists() )
			return false;
		if ( _file )
			delete _file;
		// Open settings
		_file = new QSettings( _path, QSettings::IniFormat );
		_file->setIniCodec( "UTF-8" );
		return true;
	}
	//
	void setPath( const QString& path = QString(), bool initialise = false )
	{
		_path = path;
		if ( initialise )
			init();
		emit pathChanged();
	}
	//
	QString path()
	{
		return _path;
	}
	// Get Settings "object"
	QSettings* settings()
	{
		return _file;
	}
	// Save Setting with key
	void save( const QString& key, const QVariant& value )
	{
		if ( _file )
		{
			QColor color = value.value<QColor>();
			if ( color.isValid() )
				_file->setValue( key, color.name(QColor::HexArgb) );
			else
				_file->setValue( key, value );
			emit changed( key );
		}
	}
	void save( const QString& key, const QString& subkey, const QVariant& value )
	{
		save( fullkey(key, subkey), value );
	}
	// Get QVariant Setting
	QVariant value( const QString& key, const QString& subkey = "" )
	{
		if ( _file )
			return _file->value( fullkey( key, subkey ) );
		return QVariant();
	}
	// Get integer Setting
	int getInt( const QString& key, const QString& subkey = "" )
	{
		QVariant var = value( key, subkey );
		if ( var.isValid() )
			return var.toInt();
		return 0;
	}
	// Get double Setting
	int getDouble( const QString& key, const QString& subkey = "" )
	{
		QVariant var = value( key, subkey );
		if ( var.isValid() )
			return var.toDouble();
		return 0;
	}
	// Get QString Setting
	QString getString( const QString& key, const QString& subkey = "" )
	{
		QVariant var = value( key, subkey );
		if ( var.isValid() )
			return var.toString();
		return QString();
	}
	// Get QChar Setting
	QChar getChar( const QString& key, const QString& subkey = "" )
	{
		QVariant var = value( key, subkey );
		if ( var.isValid() )
			return var.toChar();
		return QChar();
	}
	// Get QColor Setting
	QColor getColor( const QString& key, const QString& subkey = "" )
	{
		return QColor(getString(key, subkey));
	}
	// Get boolean Setting
	bool getBool( const QString& key, const QString& subkey = "" )
	{
		QVariant var = value( key, subkey );
		if ( var.isValid() )
			return var.toBool();
		return false;
	}
//	void beginGroup( const QString& group )
//	{
//		_file->beginGroup( group );
//	}
//	void endGroup()
//	{
//		_file->endGroup();
//	}
	// Get complete key
	static inline QString fullkey( const QString& key, const QString& subkey = "" )
	{
		QString flkey = key;
		if ( !subkey.isEmpty() )
			flkey += "/" + subkey;
		return flkey;
	}

signals:
	void pathChanged();
	void changed( QString key );
};


#endif // SETTINGSFILE_H
