#ifndef THEME_H
#define THEME_H
// ------------------------------------ //
#include "SettingsFile.h"
// ------------------------------------ //
#include <QBrush>
#include <QFont>
#include <QIcon>
#include <QPen>
namespace ThemeKeys
{
	static inline QString Background = "background";
	static inline QString Borders = "borders";
	static inline QString Margins = "margins";
	static inline QString Padding = "padding";
	static inline QString Spacing = "spacing";
	static inline QString Font = "font";
	static inline QString Text = "text";
	static inline QString Title = "title";
	static inline QString InfoL = "infoL";
	static inline QString InfoR = "infoR";
	static inline QString Prefix = "prefix";
	static inline QString Suffix = "suffix";
}
namespace Border {
	static inline QString None = "none";
	static inline QString Left = "left";
	static inline QString Top = "top";
	static inline QString Right = "right";
	static inline QString Bottom = "bottom";
	static inline QString All = "all";
}
class Brush
		: public QBrush
{
	int _radius = 0;
public:
	Brush() : QBrush()
	{
		setStyle( Qt::SolidPattern );
	}
	int radius() const
	{
		return _radius;
	}
	void setRadius( int radius )
	{
		_radius = radius;
	}
	QBrush brush() const
	{
		QBrush bs;
		bs.setStyle( this->style() );
		bs.setColor( this->color() );
		return bs;
	}
};
class Pen
		: public QPen
{
	QString _type = Border::None;
public:
	Pen( const QBrush& brush = QColor("#00000000"), qreal width = 0, const Qt::PenStyle& style = Qt::SolidLine, const Qt::PenCapStyle& cap = Qt::FlatCap, const Qt::PenJoinStyle& join = Qt::MiterJoin )
		: QPen( brush, width, style, cap, join )
	{

	}
	QString type() const
	{
		return _type;
	}
	void setType( const QString& type )
	{
		_type = type;
	}
	QPen pen() const
	{
		QPen pn( QColor("#00000000"), 0, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin );
		pn.setColor( this->color() );
		pn.setWidth( this->width() );
		pn.setStyle( this->style() );
		return pn;
	}
};
class Font : public QFont
{
	QColor _color = QColor("#000000");
public:
	Font() : QFont() {}
	void setColor( const QString& color ) { _color = color; }
	QPen pen() { QPen pn; pn.setColor( _color ); return pn; }
	QFont font()
	{
		QFont ft;
		ft.setFamily( family() );
		ft.setBold( bold() );
		ft.setWeight( weight() );
		ft.setPointSize( pointSize() );
		ft.setUnderline( underline() );
		ft.setCapitalization( capitalization() );
		return ft;
	}
};
class Spacing
{
	int _horizontal = 0;
	int _vertical = 0;
public:
	Spacing(){}
	Spacing( int horizontal, int vertical )
	{
		setHorizontal( horizontal );
		setVertical( vertical );
	}
	Spacing( int spacing )
	{
		setSpacing( spacing );
	}
	int horizontal() const { return _horizontal; }
	int vertical() const { return _vertical; }
	void setHorizontal( int horizontal ) { _horizontal = horizontal; }
	void setVertical( int vertical ) { _vertical = vertical; }
	void setSpacing( int spacing ) { setHorizontal(spacing); setVertical(spacing); }
};
class Icon
{
	int _size = 20;
public:
	Icon(){}
	Icon( int size )
	{
		setSize( size );
	}
	int size() const { return _size; }
	void setSize( int size ) { _size = size; }
};

// ------------------------------------ //
// ------------ Theme CLASS ----------- //
// ------------------------------------ //
class Theme
		: public SettingsFile
{
	Q_OBJECT
private:
	//	static inline SettingsFile* _theme = new SettingsFile;
	static inline QFileInfoList _availableThemes;

	static inline Font ft_title;// = App::theme()->font( _type, "title." + ThemeKeys::Font + "." + _state );
	//	App::theme()->font("");
	// Left Info
	static inline Font ft_infol;// = App::theme()->font( _type, "infoL." + ThemeKeys::Font + "." + _state );
	// Right Info
	static inline Spacing spg;// = 0;//App::theme()->spacing( _type, ThemeKeys::Spacing );
//	Brush background = App::theme()->brush( _type, "background."+_state );
//	int radius = background.radius();
//	Pen borders = App::theme()->pen( _type, "borders."+_state );
//	QString bdtype = borders.type();
//	int bdwidth = borders.width();
//	if ( bdtype == Border::None )
//		bdwidth = 0;
//	QMargins margins = App::theme()->margins( _type, ThemeKeys::Margins );

//	QMargins padding = App::theme()->margins( _type, ThemeKeys::Padding );

public:
	Theme()
		: SettingsFile()
	{
	}
	Theme( const QString& path, bool initialise = false )
		: SettingsFile()
	{

	}
	bool init() override
	{
		SettingsFile::init();
		spg = spacing( "ArtistItem", ThemeKeys::Spacing );
		ft_title = font( "ArtistItem", "title."+ThemeKeys::Font+".normal");
		ft_infol = font( "ArtistItem", "infoL."+ThemeKeys::Font+".normal" );
		return true;
	}
	static inline Font title()
	{
		return ft_title;
	}
	static inline Font infoL()
	{
		return ft_infol;
	}
	static inline Spacing spacing()
	{
		return spg;
	}
	//	static inline void setPath( const QString& theme )
	//	{
	//		_theme->setPath( theme, true );
	//	}
	//	static inline void setPath( const QFileInfo& fileinfo )
	//	{
	//		setPath( fileinfo.absoluteFilePath() );
	//	}
	// -------------- Get Available Themes -------------- //
	static inline QFileInfoList availableThemes()
	{
		_availableThemes.clear();
		_availableThemes.append( QDir("./themes").entryInfoList({"*.thm"}, QDir::Files, QDir::Name | QDir::IgnoreCase) );
		_availableThemes.append( QDir( Files::localPath() + "/themes").entryInfoList({"*.thm"}, QDir::Files, QDir::Name | QDir::IgnoreCase) );
		return _availableThemes;
		QFileInfoList themes;
		themes.append( QDir("./themes").entryInfoList({"*.thm"}, QDir::Files, QDir::Name | QDir::IgnoreCase) );
		themes.append( QDir( Files::localPath() + "/themes").entryInfoList({"*.thm"}, QDir::Files, QDir::Name | QDir::IgnoreCase) );
		return themes;
	}
	// -------------- Get Font -------------- //
	Font font( const QString& key, const QString& subkey = "" )
	{
		Font ft;
		QStringList params = getString( key, subkey ).remove(" ").split( ";" );
		for ( QString param : params )
		{
			QStringList map = param.split(":");
			if ( map.size() < 2 ) continue;
			if ( map.at(0) == "family" )
				ft.setFamily( map.at(1) );
			else if ( map.at(0) == "size" )
				ft.setPointSize( map.at(1).toInt() );
			else if ( map.at(0) == "color" )
				ft.setColor( map.at(1) );
			else if ( map.at(0) == "weight" )
				ft.setWeight( map.at(1).toInt() );
			else if ( map.at(0) == "cap" )
				ft.setCapitalization( (QFont::Capitalization)map.at(1).toInt() );
		}
		return ft;
	}
	// -------------- Get Brush -------------- //
	Brush brush( const QString& key, const QString& subkey = "" )
	{
		Brush bs;
		bs.setStyle( Qt::SolidPattern );
		QStringList params = getString( key, subkey ).remove(" ").split( ";" );
		for ( QString param : params )
		{
			QStringList map = param.split(":");
			if ( map.size() < 2 ) continue;
			if ( map.at(0) == "color" )
				bs.setColor( QColor(map.at(1)) );
			else if ( map.at(0) == "radius" )
				bs.setRadius( map.at(1).toInt() );
		}
		return bs;
	}
	// -------------- Get Pen -------------- //
	Pen pen( const QString& key, const QString& subkey = "" )
	{
		Pen pn = Pen();
		QStringList params = getString( key, subkey ).remove(" ").split( ";" );
		for ( QString param : params )
		{
			QStringList map = param.split(":");
			if ( map.size() < 2 ) continue;
			if ( map.at(0) == "color" )
				pn.setColor( QColor(map.at(1)) );
			else if ( map.at(0) == "width" )
				pn.setWidth( map.at(1).toInt() );
			else if ( map.at(0) == "style" )
				pn.setStyle( (Qt::PenStyle)map.at(1).toInt() );
			else if ( map.at(0) == "type" )
				pn.setType( map.at(1) );
		}
		return pn;
	}
	// -------------- Get Margins -------------- //
	QMargins margins( const QString& key, const QString& subkey = "" )
	{
		QMargins mgs = {0, 0, 0, 0};
		QStringList params = getString( key, subkey ).remove(" ").split( ";" );
		if ( params.size() == 1 )
		{
			mgs.setLeft( params.at(0).toInt() );
			mgs.setBottom( params.at(0).toInt() );
			mgs.setRight( params.at(0).toInt() );
			mgs.setTop( params.at(0).toInt() );
		}
		else if ( params.size() == 2 )
		{
			mgs.setLeft( params.at(0).toInt() );
			mgs.setBottom( params.at(1).toInt() );
			mgs.setRight( params.at(0).toInt() );
			mgs.setTop( params.at(1).toInt() );
		}
		else if ( params.size() == 4 )
		{
			mgs.setLeft( params.at(0).toInt() );
			mgs.setBottom( params.at(1).toInt() );
			mgs.setRight( params.at(2).toInt() );
			mgs.setTop( params.at(3).toInt() );
		}
		return mgs;
	}
	Spacing spacing( const QString& key, const QString& subkey = "" )
	{
		Spacing spg;
		QStringList params = getString( key, subkey ).remove(" ").split( ";" );
		if ( params.size() == 1 )
		{
			spg.setSpacing( params.at(0).toInt() );
		}
		else if ( params.size() == 2 )
		{
			spg.setHorizontal( params.at(0).toInt() );
			spg.setVertical( params.at(1).toInt() );
		}
		return spg;
	}
	Icon icon( const QString& key, const QString& subkey = "" )
	{
		Icon ic;
		QStringList params = getString( key, subkey ).remove(" ").split( ";" );
		for ( QString param : params )
		{
			QStringList map = param.split(":");
			if ( map.size() < 2 ) continue;
			if ( map.at(0) == "size" )
				ic.setSize( map.at(1).toInt() );
		}
		return ic;
	}
};
// ------------------------------------ //
// ------------------------------------ //
// ------------------------------------ //
#endif // THEME_H
