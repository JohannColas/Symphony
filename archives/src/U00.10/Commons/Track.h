#ifndef TRACK_H
#define TRACK_H
/**********************************************/
#include <QObject>
#include <QString>
#include <QFile>
#include <QUrl>
#include <QIcon>
/**********************************************/
class Album;
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Track
		: public QObject
{
	Q_OBJECT
	public:
	enum Format
	{
		UNKNOWN,
		MP3,
		OGG,
		WAV,
		FLAC,
		M4A,
		WMA
	};
private:
	QString _path = "";
	QString _title = "";
	Track::Format _format = Format::UNKNOWN;
	Album* _album = 0;
	QString _genre;
	QString _number;
	QString _description;
	QString _comment;

public:
	virtual ~Track()
	{

    }
    Track()
    {
    }
    Track( const QString& path )
    {
        _path = path;
        setType();
    }
	QString path() const
	{ return _path; }
	QString title() const
	{ return _title; }
	Track::Format format() const
	{ return _format; }
	QUrl toUrl() const
	{
		return QUrl::fromLocalFile( path() );
	}
	void setAlbum( Album* album )
	{
		_album = album;
	}
	Album* album() const
	{
		return _album;
	}

public slots:
	void setType()
	{
		int pPos =  path().lastIndexOf('.');
		QString ext = path().right( path().length()-pPos-1 );
		if ( ext.toLower() == "mp3" )
			_format = Format::MP3;
		else if ( ext.toLower() == "ogg" )
			_format = Format::OGG;
		else if ( ext.toLower() == "wav" )
			_format = Format::WAV;
		else if ( ext.toLower() == "flac" )
			_format = Format::FLAC;
		else if ( ext.toLower() == "m4a" )
			_format = Format::M4A;
		else if ( ext.toLower() == "wma" )
			_format = Format::WMA;
	}
	void setTitle( const QString& title )
	{
		_title = title;
	}
	QString genre() const
	{
		return _genre;
	}
	QString number() const
	{
		return _number;
	}
	QString description() const
	{
		return _description;
	}
	QString comment() const
	{
		return _comment;
	}
	QIcon thumb()
	{
		QIcon tmb/*( App::icons("fhsdjf") )*/;
//		if ( )
		return tmb;
	}
	void setGenre( const QString& genre )
	{
		_genre = genre;
	}
	void setNumber( const QString& number )
	{
		_number = number;
	}
	void setDescription( const QString& description )
	{
		_description = description;
	}
	void setComment( const QString& comment )
	{
		_comment = comment;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TRACK_H
