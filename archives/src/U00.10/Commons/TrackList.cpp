#include "TrackList.h"
/**********************************************/
/**********************************************/
#include "App.h"
/**********************************************/
/**********************************************/
void TrackList::addTrack( const QString& path )
{
	for ( Track* tck : App::librairies()->tracks() )
		if ( tck->path() == path )
		{
			_list.append( tck );
			break;
		}
	//		_list.append( new Track(path) );
	emit trackAdded();
}
/**********************************************/
/**********************************************/
