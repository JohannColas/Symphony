#ifndef PLAYERCONTROL_H
#define PLAYERCONTROL_H
/**********************************************/
#include <QPushButton>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerControl
        : public QPushButton
{
    Q_OBJECT

public:
    PlayerControl( QWidget* parent = 0 )
        : QPushButton( parent )
    {
        setFlat( true );
		setFixedSize( 50, 50 );
		setIconSize( {36,36} );
        setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERCONTROL_H
/**********************************************/
