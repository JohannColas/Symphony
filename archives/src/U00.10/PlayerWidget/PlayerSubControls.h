#ifndef PLAYERSUBCONTROLS_H
#define PLAYERSUBCONTROLS_H
/**********************************************/
#include <QWidget>
#include "BasicWidgets/SWidget.h"
#include "PlayerSubControl.h"
#include "BasicWidgets/Layouts.h"
#include "App.h"
#include "Commons/Icons.h"
/**********************************************/
/**********************************************/
/**********************************************/
class PlayerSubControls
        : public SWidget
{
    Q_OBJECT
private:
    PlayerSubControl* pb_repeat = new PlayerSubControl(this);
    PlayerSubControl* pb_shuffle = new PlayerSubControl(this);
    PlayerSubControl* pb_equalizer = new PlayerSubControl(this);
    PlayerSubControl* pb_volume = new PlayerSubControl(this);
//    Settings* _settings;

public:
    ~PlayerSubControls()
    {

    }
    PlayerSubControls( SWidget* parent = 0 )
        : SWidget( parent )
    {
        HorizontalLayout* lay_main = new HorizontalLayout;
//        lay_main->addSpacer();
        lay_main->addWidget( pb_repeat );
        lay_main->addWidget( pb_shuffle );
        lay_main->addWidget( pb_equalizer );
        lay_main->addWidget( pb_volume );
//        lay_main->addSpacer();
        setLayout( lay_main );
        setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
        lay_main->setSizeConstraint( QLayout::SetMinimumSize );

        connect( pb_repeat, &PlayerSubControl::released,
                 this, &PlayerSubControls::onRepeat );
        connect( pb_shuffle, &PlayerSubControl::released,
                 this, &PlayerSubControls::onShuffle );
        connect( pb_equalizer, &PlayerSubControl::released,
                 this, &PlayerSubControls::onEqualizer );
        connect( pb_volume, &PlayerSubControl::released,
                 this, &PlayerSubControls::onVolume );
        updateIcons();
    }
//    void setSettings( Settings* sets )
//    {
//        _settings = sets;
//        updateIcons();
//    }

public slots:
    void updateIcons() override
    {
//        if ( _settings )
        {
			pb_repeat->setIcon( Icons::get( IconsKeys::NoRepeat ) );
			pb_shuffle->setIcon( Icons::get( IconsKeys::NoShuffle ) );
			pb_equalizer->setIcon( Icons::get( IconsKeys::Equalizer ) );
			pb_volume->setIcon( Icons::get( IconsKeys::Volume ) );
        }
    }
    void noRepeatIcon()
    {
//        if ( _settings )
			pb_repeat->setIcon( Icons::get( IconsKeys::NoRepeat ) );
    }
    void repeatOneIcon()
    {
//        if ( _settings )
			pb_repeat->setIcon( Icons::get( IconsKeys::RepeatOne ) );
    }
    void repeatAllIcon()
    {
//        if ( _settings )
			pb_repeat->setIcon( Icons::get( IconsKeys::RepeatAll ) );
    }
    void noShuffleIcon()
    {
//        if ( _settings )
			pb_shuffle->setIcon( Icons::get( IconsKeys::NoShuffle ) );
    }
    void shuffleIcon()
    {
//        if ( _settings )
			pb_shuffle->setIcon( Icons::get( IconsKeys::Shuffle ) );
    }

signals:
    void onShuffle();
    void onRepeat();
    void onEqualizer();
    void onVolume();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERSUBCONTROLS_H
/**********************************************/
