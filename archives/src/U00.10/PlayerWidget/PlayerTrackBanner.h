#ifndef PLAYERTRACKBANNER_H
#define PLAYERTRACKBANNER_H
/**********************************************/
/**********************************************/
#include "BasicWidgets/SWidget.h"
#include "App.h"
/**********************************************/
#include "Commons/Track.h"
#include "Commons/TrackInfo.h"
/**********************************************/
#include "BasicWidgets/CoverImage.h"
#include "BasicWidgets/TitleLabel.h"
#include "BasicWidgets/ArtistLabel.h"
/**********************************************/

#include <QTimer>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerTrackBanner
        : public SWidget
{
    Q_OBJECT
protected:
    Track* _track = 0;
    TrackInfo _trackInfo;
    GridLayout* lay_main = new GridLayout;
    CoverImage* lb_cover = new CoverImage(this);
    TitleLabel* lb_title = new TitleLabel(this);
    ArtistLabel* lb_artist = new ArtistLabel(this);

public:
    ~PlayerTrackBanner()
    {
    }
    PlayerTrackBanner( SWidget* parent = 0 )
        : SWidget( parent )
    {
        setLayout( lay_main );
        lay_main->setSizeConstraint( QLayout::SetMinimumSize );
        show();
        lay_main->addWidget( lb_cover, 0, 0, 2 );
		lb_cover->setFixedSize( 48, 48 );
        lay_main->addWidget( lb_title, 0, 1 );
        lay_main->addWidget( lb_artist, 1, 1 );
        lb_title->setText("----");
        lb_artist->setText("----");

        lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
        setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
    }

public slots:
    void setTrack( Track* track )
    {
        _track = track;
        update();
    }
    void update()
    {
        if ( _track )
        {
            _trackInfo.setTrack(_track);
            Metadata meta(_track);
            QString title = meta.title();
            if ( title == "" )
                title = meta.path();
            lb_title->setText( title );
            QString artist = meta.artist();
            if ( artist == "" )
                artist = "[UNKNOWN]";
            lb_artist->setText( artist );
            updateCover();
//            //
//            resize( width(), sizeHint().height() );
//            repaint();
        }
    }
    void updateCover()
    {
//        if ( _settings )
        {
			lb_cover->setIcon( Icons::get("track") );
        }
        if ( _track )
        {
             QPixmap pix = _trackInfo.cover();
             if (  !pix.isNull() )
                 lb_cover->setIcon( pix );
             else
             {
                 Network* network = new Network;
                 connect( network, &Network::downloaded,
                          this, &PlayerTrackBanner::setCover );
                 network->download( _trackInfo.coverUrl() );
             }
        }
    }
    void setCover( const QByteArray& data )
    {
        QPixmap pixmap;
        if ( data.length() > 0 )
        {
            QFile file( _trackInfo.path()+"/cover.png" );
            file.open( QIODevice::WriteOnly );
            file.write( data );
            file.close();
            pixmap.loadFromData(data);
            lb_cover->setIcon( pixmap );
        }
        else
        {
            Metadata meta(_track);
            if (  pixmap.convertFromImage(meta.cover()) )
                lb_cover->setIcon( pixmap );
        }
    }
    void updateIcons() override
    {
//        _icons = icons;
//        if ( lb_cover->icon().isNull() )
//            lb_cover->setIcon( _icons->get("track") );
    }
    void resizeEvent( QResizeEvent* event ) override
    {
        repaint();
        SWidget::resizeEvent( event );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERTRACKBANNER_H
/**********************************************/
