#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "BasicWidgets/SWidget.h"
#include <QFrame>
#include <QPushButton>
#include <QElapsedTimer>
#include <QSlider>
#include <QProgressBar>
#include <QTimer>
#include <QMouseEvent>
#include "Commons/Track.h"
#include "Commons/TrackList.h"
#include "App.h"
/**********************************************/
#include "PlayerControls.h"
#include "PlayerSubControls.h"
#include "PlayerTrackBanner.h"
/**********************************************/
#include <QMediaPlayer>
#include <QMediaPlaylist>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlayerWidget
		: public SWidget
{
	Q_OBJECT
public:
	enum Repeat {
		NOREPEAT = 0,
		REPEATALL = 1,
		REPEATONE = 2
	};

private:
	bool _movable = false;
	TrackList* _queue = 0;
	QMediaPlayer* _player = new QMediaPlayer;
	QTimer* _timer = new QTimer(this);
	bool _isPlaying = false;
	BoxLayout* lay_main = new BoxLayout;
	// Player Widgets
	PlayerControls* _playerControls;// = new PlayerControls(this);
	PlayerSubControls* _playerSubControls;// = new PlayerSubControls(this);
	PlayerTrackBanner* _playerTrackBanner;// = new PlayerTrackBanner(this);
	QSpacerItem* _spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding );
	//
	Repeat _repeat = Repeat::NOREPEAT;
	bool _isShuffle = false;
	//
	QSlider* sl_time = new QSlider;
	QLabel* lb_time = new QLabel;

public:
	~PlayerWidget()
	{

	}
	PlayerWidget( SWidget* parent = 0 )
		: SWidget( parent )
	{

		setLayout( lay_main );
		lay_main->setSizeConstraint( QLayout::SetMinimumSize );
		show();

		_playerControls = new PlayerControls(this);
		_playerSubControls = new PlayerSubControls(this);
		_playerTrackBanner = new PlayerTrackBanner(this);

		connect( App::player(), &Player::positionChanged,
				 this, &PlayerWidget::changePosition );
		connect( App::player(), &Player::mediaStatusChanged,
				 this, &PlayerWidget::onMediaStatusChanged );
		connect( App::player(), &Player::trackChanged,
				 this, &PlayerWidget::updateTrackInfo );

		connect( _playerControls, &PlayerControls::playpause,
				 this, &PlayerWidget::playpause );
		connect( _playerControls, &PlayerControls::stop,
				 this, &PlayerWidget::stop );
		connect( _playerControls, &PlayerControls::backward,
				 this, &PlayerWidget::backward );
		connect( _playerControls, &PlayerControls::forward,
				 this, &PlayerWidget::forward );

		connect( _playerSubControls, &PlayerSubControls::onShuffle,
				 this, &PlayerWidget::shuffle );
		connect( _playerSubControls, &PlayerSubControls::onRepeat,
				 this, &PlayerWidget::repeat );
		//    connect( _playerSubControls, &PlayerSubControls::onEqualizer,
		//             this, &PlayerWidget::equalizer );
		//    connect( _playerSubControls, &PlayerSubControls::onVolume,
		//             this, &PlayerWidget::volume );

		connect( sl_time, &QSlider::valueChanged,
				 this, &PlayerWidget::setPlayerPos );
		_timer->setSingleShot( true );
		connect( _timer, &QTimer::timeout,
				 this, &PlayerWidget::updatePlayerPos );

		setMouseTracking( true );
		//
		initiate();
	}
	void initiate() override
	{
		_isShuffle = App::settings()->getBool( Keys::Player, Keys::Shuffle );//Settings::getBool( Keys::Player, Keys::Shuffle );
		updateShuffleIcon();
		if ( _isShuffle )
			App::player()->randomize();
		_repeat = (Repeat)App::settings()->getInt( Keys::Player, Keys::Repeat );//Settings::getInt( Keys::Player, Keys::Repeat );
		updateRepeatIcon();
		updateLayout();
	}

public slots:
	void playTrack( Track* /*track*/ )
	{
		//		_queue->clear();
		//		_queue->add( track );
		//		_index = 0;
		//		setTrack();
		//		play();
	}
	void addTrack( Track* track )
	{
		_queue->add( track );
	}
	void playTracklist( TrackList* /*tracklist*/ )
	{
		//		_queue->clear();
		//		_queue->add( tracklist );
		//		_index = 0;
		//		setTrack();
		//		play();
	}
	void addTracklist( TrackList* tracklist )
	{
		_queue->add( tracklist );
	}
	void insertTracklist( TrackList* /*tracklist*/ )
	{
//		if ( _isShuffle )
//			addTracklist( tracklist );
//		else
//			_queue->insert( _index, tracklist );
	}
	void receiveTrackList( TrackList* /*tracklist*/ )
	{
		//		_queue->clear();
		//		_queue->add( tracklist );
		//		_index = 0;
		//		setTrack();
		//		play();
	}
	void setPlaylistIndex( int /*index*/ )
	{
		//		_index = index;
		//		if ( _isShuffle )
		//			_index = _listOrder.indexOf( index );
		//		setTrack();
	}
	void setTrackList( TrackList* /*playlist*/ )
	{
		//		_queue = playlist;
		//		_index = 0;
		//		setTrack();
		//		connect( _queue, &TrackList::changed,
		//				 this, &PlayerWidget::randomize );
	}
	//
	void playpause()
	{
		App::player()->playpause();
		// Update Play/Pause Icon
		updatePlayPauseIcon();
	}
	void stop()
	{
		App::player()->stop();
	}
	void backward()
	{
		App::player()->backward();
	}
	void forward()
	{
		App::player()->forward();
	}
	void repeat()
	{
		App::player()->repeat();
		updateRepeatIcon();
	}
	void updateRepeatIcon()
	{
		Player::Repeat repeat = App::player()->getRepeat();
		if ( repeat== Player::NOREPEAT )
			_playerSubControls->noRepeatIcon();
		else if ( repeat == Player::REPEATALL )
			_playerSubControls->repeatAllIcon();
		else if ( repeat == Player::REPEATONE )
			_playerSubControls->repeatOneIcon();
	}
	void shuffle()
	{
		App::player()->shuffle();
		updateShuffleIcon();
	}
	void updateShuffleIcon()
	{
		if ( App::player()->getShuffle() )
			_playerSubControls->shuffleIcon();
		else
			_playerSubControls->noShuffleIcon();
	}
	void changePosition( qint64 /*pos*/ )
	{
//		QString curT = QTime( 0, 0, 0 ).addMSecs( pos ).toString();
//		if( curT.left(2) == "00" )
//			curT = curT.mid( 3 );
//		QString durT = QTime( 0, 0, 0).addSecs( _duration ).toString();
//		if( durT.left(2) == "00" )
//			durT = durT.mid( 3 );
//		lb_time->setText( curT + " / " + durT );
//		sl_time->blockSignals( true );
//		sl_time->setValue(0.001*pos);
//		sl_time->blockSignals( false );
	}
	void setPlayerPos( int /*pos*/ )
	{
//		_newPos = pos;
//		_timer->start(100);
	}
	void updatePlayerPos()
	{
//		if ( _newPos != -1 )
//		{
//			App::player()->setPosition( 1000*_newPos );
//		}
	}
	void updatePlayPauseIcon()
	{
		if ( App::player()->state() == QMediaPlayer::PlayingState )
			_playerControls->setPauseIcon();
		else
			_playerControls->setPlayIcon();
	}
	void mousePressEvent( QMouseEvent* event ) override
	{
		if ( event->button() == Qt::LeftButton )
		{
			_movable = true;
			emit moveWindow( event->globalPos() );
		}
		event->ignore();
	}
	void mouseReleaseEvent( QMouseEvent* event ) override
	{
		if ( event->button() == Qt::LeftButton )
		{
			_movable = false;
			emit moveWindow( {-1,0} );
		}
		event->ignore();
	}
	void mouseMoveEvent( QMouseEvent* event ) override
	{
		if ( _movable )
			emit moveWindow( event->globalPos() );
		event->ignore();
	}
	void mouseDoubleClickEvent( QMouseEvent* event ) override
	{
		if ( event->button() == Qt::LeftButton )
			onMaximize();
		event->ignore();
	}
	void connectTo( Frame* frame )
	{
		connect( frame, &Frame::trackToPlay,
				 this, &PlayerWidget::playTrack );
		connect( frame, &Frame::trackToAdd,
				 this, &PlayerWidget::addTrack );
		connect( frame, &Frame::tracklistToPlay,
				 this, &PlayerWidget::playTracklist );
		connect( frame, &Frame::tracklistToAdd,
				 this, &PlayerWidget::addTracklist );
		connect( frame, &Frame::tracklistToInsert,
				 this, &PlayerWidget::insertTracklist );
	}
	void connectTo( SWidget* swidget )
	{
		connect( swidget, &SWidget::trackToPlay,
				 this, &PlayerWidget::playTrack );
		connect( swidget, &SWidget::trackToAdd,
				 this, &PlayerWidget::addTrack );
		connect( swidget, &SWidget::tracklistToPlay,
				 this, &PlayerWidget::playTracklist );
		connect( swidget, &SWidget::tracklistToAdd,
				 this, &PlayerWidget::addTracklist );
		connect( swidget, &SWidget::tracklistToInsert,
				 this, &PlayerWidget::insertTracklist );
	}
	void onMediaStatusChanged()
	{
		updatePlayPauseIcon();
		if ( App::player()->mediaStatus() == QMediaPlayer::EndOfMedia )
		{
			if ( App::player()->getRepeat() == Player::REPEATONE )
			{
				App::player()->setPosition(0);
				App::player()->play();
			}
			else
				App::player()->forward();
		}
	}
	void updateTrackInfo( /*Track* track*/ )
	{
		_playerTrackBanner->setTrack( App::player()->currentTrack() );
	}
	void settingsChanged( const QString& key ) override
	{
		if ( key == SettingsFile::fullkey( Keys::Player, Keys::Position ) ||
			 key == SettingsFile::fullkey( Keys::Player, Keys::Size ) )
			updateLayout();
	}
	void updateLayout() override
	{
		QString pos = App::settings()->getString( Keys::Player, Keys::Position );
		lay_main->removeWidget( _playerControls );
		lay_main->removeWidget( _playerSubControls );
		lay_main->removeWidget( _playerTrackBanner );
		lay_main->removeItem( _spacer );
		if ( pos == 't' ||
			 pos == 'b' )
		{
			lay_main->setDirection( QBoxLayout::LeftToRight );
			lay_main->addWidget( _playerControls );
			lay_main->addWidget( _playerTrackBanner );
			lay_main->addWidget( _playerSubControls );
			lay_main->addItem( _spacer );
			setSizePolicy( QSizePolicy::Expanding,
						   QSizePolicy::Maximum );
		}
		else
		{
			lay_main->setDirection( QBoxLayout::TopToBottom );
			lay_main->addWidget( _playerTrackBanner );
			lay_main->addWidget( _playerControls );
			lay_main->addWidget( _playerSubControls );
			lay_main->addItem( _spacer );
			setSizePolicy( QSizePolicy::Maximum,
						   QSizePolicy::Expanding );
		}
		lay_main->setAlignment( _playerControls, Qt::AlignCenter );
		lay_main->setAlignment( _playerSubControls, Qt::AlignCenter );
	}
	void updateIcons() override
	{
		_playerControls->updateIcons();
		_playerSubControls->updateIcons();
	}

signals:
	void indexChanged();
	void playlistChanged( TrackList* list );
	void onMinimize();
	void onMaximize();
	void moveWindow( const QPoint& pos );
	void showSettings();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYERWIDGET_H
