#ifndef QUEUEWIDGET_H
#define QUEUEWIDGET_H
/**********************************************/
#include "BasicWidgets/SWidget.h"
/**********************************************/
#include "Commons/Theme.h"
#include "BasicWidgets/ListView.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class QueueWidget
        : public SWidget
{
	Q_OBJECT
private:
	TrackList* _nowPlaylist = 0;
//    TrackWidget* wid_trackInfo;
//    TrackSelector* wid_trackList;
    Settings* _sets = 0;
    GridLayout* lay_main = new GridLayout;
	ListView* ls_view = new ListView;

public:
    QueueWidget( SWidget* parent = nullptr )
        : SWidget( parent )
    {
        setLayout( lay_main );
//        wid_trackInfo = new TrackWidget(this);
//        wid_trackList = new TrackSelector(this);
		ls_view->setType( Type::Queue );
		lay_main->addWidget( ls_view, 0, 0 );
//        lay_main->addWidget( wid_trackList, 0, 1 );

//        Theme::init( Theme::availableThemes().first().absoluteFilePath() );
//        SLabel* label = new SLabel( this );
//        label->setFixedSize( 100, 76 );
//        lay_main->addWidget( label, 1, 0, 1, 2 );
//        wid_trackList->allowRemove();

//        wid_trackList->setMaximumWidth( 400 );
//        wid_trackList->avoidMultipleSelection();
//        wid_trackList->avoidRightClick();
//        connect( wid_trackList, &TrackSelector::indexToPlay,
//                 this, &QueueWidget::indexChanged );
    }
	void setPlaylists( /*Playlists* playlists*/ )
	{
//		wid_trackList->setPlaylists( playlists );
	}
//    void setSettings( Settings* sets = 0 )
//    {
//        _sets = sets;
//        wid_trackInfo->setSettings( _sets );
//        wid_trackList->setSettings( _sets );
//    }


public slots:
    void setPlaylist( TrackList* playlist )
    {
        _nowPlaylist = playlist;
//        wid_trackList->setTrackList( _nowPlaylist );
//        wid_trackInfo->setTrack( _nowPlaylist->track(0) );
//        connect( wid_trackList, &TrackSelector::trackChanged,
//                 wid_trackInfo, &TrackWidget::setTrack );
        connect( _nowPlaylist, &TrackList::changed,
                 this, &QueueWidget::update );
        connect( _nowPlaylist, &TrackList::trackAdded,
                 this, &QueueWidget::updateList );
    }
    void setTrackList( TrackList* playlist )
    {
        _nowPlaylist = playlist;
//        wid_trackList->setTrackList( _nowPlaylist );
//        wid_trackInfo->setTrack( _nowPlaylist->track(0) );
//        connect( wid_trackList, &TrackSelector::trackChanged,
//                 wid_trackInfo, &TrackWidget::setTrack );
        connect( _nowPlaylist, &TrackList::changed,
                 this, &QueueWidget::update );
        connect( _nowPlaylist, &TrackList::trackAdded,
                 this, &QueueWidget::updateList );
    }
    void updateList()
    {
//        wid_trackList->update();
//        wid_trackInfo->setTrack( _nowPlaylist->currentTrack() );
    }
    void update()
    {
//        wid_trackList->setBegin( _nowPlaylist->currentTrackIndex() );
    }
    void updateIcons()
    {
//        wid_trackInfo->updateIcons( _icons );
//        wid_trackList->updateIcons( _icons );
    }

signals:
	void indexChanged( int index );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // QUEUEWIDGET_H
