#ifndef SYMPHONY_H
#define SYMPHONY_H

#include "Commons/Settings.h"
#include "Commons/Theme.h"
//#include "Commons/Icons.h"
//#include "Commons/Lang.h"
#include "Commons/TrackList.h"
#include "Commons/Playlists.h"
#include "Commons/Libraries.h"

#include <QElapsedTimer>

class Symphony
{
    static inline Settings* _settings = 0;
    static inline Theme* _theme = 0;
	//static inline Icons* _icons = 0;
	//static inline Lang* _lang = 0;
	// Initialise TrackLists
	static inline TrackList* _queue = 0;
	static inline Playlists* _playlists = 0;
	static inline Libraries* _libraries = 0;

public:
	static inline void initialise()
	{
		_queue = new TrackList( Files::QueueName, TrackList::QUEUE );
		_playlists = new Playlists;
		_libraries = new Libraries;
	}
	static inline void save()
	{
		qDebug().noquote() << "";
		qDebug().noquote() << "--------------";
		qDebug().noquote() << "Saving...";
		QElapsedTimer timer;
		timer.start();
		_queue->save();
		_playlists->save();
		for ( TrackList* playlist : _playlists->list() )
			playlist->save();
		_libraries->saveLibraries();
		QString time = QString::number( timer.elapsed() );
		time.insert( time.length()-3, "s " );
		time += "ms";
		qDebug().noquote() << "Time to save Libraries & Playlists : " + time;
		qDebug().noquote() << "--------------";
	}
};

#endif // SYMPHONY_H
