QT       += core gui multimedia xml concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    BasicWidgets/Button.cpp \
    BasicWidgets/GroupItem.cpp \
    BasicWidgets/ListItem.cpp \
    BasicWidgets/ListView.cpp \
    BasicWidgets/Menu.cpp \
    BasicWidgets/Widget.cpp \
    Commons/Player.cpp \
    Commons/TrackList.cpp \
    SymphonyWindow.cpp \
    main.cpp

HEADERS += \
    App.h \
    ArtistsWidget/ArtistBanner.h \
    ArtistsWidget/ArtistInfoWidget.h \
    BasicWidgets/AppButton.h \
    BasicWidgets/ArtistLabel.h \
    ArtistsWidget/ArtistWidget.h \
    BasicWidgets/Button.h \
    BasicWidgets/CountLabel.h \
    BasicWidgets/CoverImage.h \
    BasicWidgets/GroupItem.h \
    BasicWidgets/LineEdit.h \
    BasicWidgets/ListView.h \
    BasicWidgets/LockedButton.h \
    BasicWidgets/Menu.h \
    BasicWidgets/PlayMenu.h \
    BasicWidgets/PlayerButton.h \
    BasicWidgets/PlayerOptionButton.h \
    BasicWidgets/SWidget.h \
    BasicWidgets/SectionButton.h \
    BasicWidgets/SystemButton.h \
    BasicWidgets/SystemControls.h \
    BasicWidgets/TabButton.h \
    BasicWidgets/TabWidget.h \
    BasicWidgets/Text.h \
    BasicWidgets/TextWidget.h \
    BasicWidgets/ThumbImage.h \
    BasicWidgets/TitleLabel.h \
    BasicWidgets/WebsiteLabel.h \
    BasicWidgets/Widget.h \
    Commons/Album.h \
    Commons/AlbumInfo.h \
    Commons/Artist.h \
    BasicWidgets/Frame.h \
    BasicWidgets/Label.h \
    BasicWidgets/Layouts.h \
    BasicWidgets/ListItem.h \
    Commons/ArtistInfo.h \
    Commons/Files.h \
    Commons/Icons.h \
    Commons/Libraries.h \
    Commons/Library.h \
    Commons/Network.h \
    Commons/OnLineMetadata.h \
    Commons/Player.h \
    Commons/Playlists.h \
    Commons/Settings.h \
    Commons/SettingsFile.h \
    Commons/Shorcuts.h \
    Commons/Sorting.h \
    Commons/Theme.h \
    Commons/TrackInfo.h \
    QueueWidget/QueueWidget.h \
    PlayerWidget/PlayerControl.h \
    PlayerWidget/PlayerControls.h \
    PlayerWidget/PlayerSubControl.h \
    PlayerWidget/PlayerSubControls.h \
    PlayerWidget/PlayerTrackBanner.h \
    ArtistsWidget/ArtistsWidget.h \
    Commons/Metadata.h \
    Commons/Track.h \
    Commons/TrackList.h \
    PlayerWidget/PlayerWidget.h \
    SectionsSelector/SectionsSelector.h \
    SettingsWidget/SettingsWidget.h \
    FrameLess.h \
    SymphonyWindow.h \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += \
    /usr/local/include/taglib
DEPENDPATH += /usr/local/include/taglib

LIBS += \
    -L/usr/local/lib \
    -ltag

INCLUDEPATH += \
    /usr/local/include
DEPENDPATH += /usr/local/include
LIBS += \
    -L/usr/local/lib \
    -lz

