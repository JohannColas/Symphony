#include "SymphonyWindow.h"
/**********************************************/
/**********************************************/
SymphonyWindow::~SymphonyWindow()
{
}

SymphonyWindow::SymphonyWindow( QWidget* parent )
	: SWidget( parent )
{
	// Initialisation
	App::init();

	wid_player = new PlayerWidget(this);
	_sectionsSelector = new SectionsSelector(this);
	wid_queue = new QueueWidget(this);
	wid_artists = new ArtistsWidget(this);
	//        wid_albums = new AlbumsWidget(this);
	//        wid_genres = new GenresWidget(this);
	//        wid_tracks = new TracksWidget(this);
	//        wid_playlists = new PlaylistsWidget(this);
	//        wid_libraries = new LibrariesWidget(this);
	//        wid_radios = new RadiosWidget(this);
	//        wid_podcasts = new PodcastsWidget(this);
	//        wid_settings = new SettingsWidget(this);
	_systemControls = new SystemControls(this);
	setLayout( lay_main );

	// Window Geometry
	setMinimumSize( 800, 500 );
	wid_player->setMinimumHeight( 100 );

	// Adding TrackLists to Widgets
	//        _queue = new TrackList( Files::QueueName, TrackList::QUEUE );
	//        _playlists = new Playlists;
	//        _libraries = new Libraries;

	// Initialize SectionsSelector
	lay_sections->addWidget( _sectionsSelector );
	addSection( wid_queue );
	addSection( wid_artists, Keys::HideArtists );
	addSection( new Widget, Keys::HideAlbums );
	addSection( new Widget, Keys::HideGenres );
	addSection( new Widget, Keys::HideTracks );
	connect( _sectionsSelector, &SectionsSelector::typeChanged,
			 wid_artists, &ArtistsWidget::onTypeChanged );
	//		addSection( wid_albums, Keys::HideAlbums );
	//        addSection( wid_genres, Keys::HideGenres );
	//        addSection( wid_tracks, Keys::HideTracks );
	//        addSection( wid_playlists, Keys::HidePlaylists );
	//        addSection( wid_libraries, Keys::HideLibraries );
	//        addSection( wid_radios, Keys::HideRadios );
	//        addSection( wid_podcasts, Keys::HidePodcasts );
	_sectionsSelector->extraSettings();
	// Show Queue Widget at the beginning
	_sectionsSelector->showWidget( 0 );
	// adding wid_settings to SectionsSelector
	//        _sectionsSelector->addWidget( wid_settings );
	//        lay_sections->addWidget( wid_settings );
	// Connections between widgets
	connect( wid_queue, &QueueWidget::indexChanged,
			 wid_player, &PlayerWidget::setPlaylistIndex );
	connect( wid_player, &PlayerWidget::playlistChanged,
			 wid_queue, &QueueWidget::setPlaylist );
	connect( _systemControls, &SystemControls::onMinimize ,
			 this, &SymphonyWindow::showMinimized );
	connect( _systemControls, &SystemControls::onMaximize,
			 this, &SymphonyWindow::maximise );
	connect( wid_player, &PlayerWidget::moveWindow,
			 this, &SymphonyWindow::move );
	connect( _systemControls, &SystemControls::onSettings,
			 this, &SymphonyWindow::showSettings );
	wid_player->connectTo( wid_artists );
	//        wid_player->connectTo( wid_albums );
	//        wid_player->connectTo( wid_tracks );
	//        wid_player->connectTo( wid_playlists );
	//        wid_player->connectTo( wid_libraries );


	// -----------------------
	// -----------------------
	setMouseTracking( true );

	//        connect( _settings, &Settings::valueChanged,
	//                 this, &App::settingsChanged );
	// Update Sidebar
	//        setWindowIcon( _settings->icon( "Symphony" ) );
	//        wid_queue->setSettings( _settings );
	//        _libraries->setSettings( _settings );
	//        _playlists->setSettings( _settings );
	//        _systemControls->setSettings( _settings );
	//        _sectionsSelector->setSettings( _settings );
	//        _settings->applyTheme();
	updateLayout();
	updateIcons();
	updateLang();
	//
	if ( App::settings()->getBool( Keys::Fullscreen ) )
		setWindowState(windowState() | Qt::WindowFullScreen);
	else if ( App::settings()->getBool( Keys::Maximized ) )
		setWindowState(windowState() | Qt::WindowMaximized);
	else
		setGeometry( App::settings()->getInt( Keys::Geometry, Keys::X),
					 App::settings()->getInt( Keys::Geometry, Keys::Y),
					 App::settings()->getInt( Keys::Geometry, Keys::Width),
					 App::settings()->getInt( Keys::Geometry, Keys::Height) );
}
/**********************************************/
/**********************************************/
void SymphonyWindow::addSection( QWidget* wid, const QString& key )
{
	_sectionsSelector->addSection( wid, key );
	lay_sections->addWidget( wid );
}
/**********************************************/
/**********************************************/
void SymphonyWindow::showSettings()
{
	//        if ( wid_settings->isHidden() )
	//        {
	//            wid_settings->show();
	//            _sectionsSelector->currentWidget()->hide();
	//        }
	//        else
	//        {
	//            wid_settings->hide();
	//            _sectionsSelector->currentWidget()->show();
	//        }
}
/**********************************************/
/**********************************************/
void SymphonyWindow::updateIcons()
{
	setWindowIcon( Icons::get( IconsKeys::Symphony ) );
	_sectionsSelector->updateIcons();
	wid_player->updateIcons();
	_systemControls->updateIcons();
	//        wid_queue->updateIcons( _icons );
	//        wid_artists->updateIcons( _icons );
	//        wid_albums->updateIcons( _icons );
	//        wid_tracks->updateIcons( _icons );
	//        wid_playlists->updateIcons( _icons );
	//        wid_libraries->updateIcons( _icons );
}
/**********************************************/
/**********************************************/
void SymphonyWindow::updateLang()
{
	//        _lang->update();

}
/**********************************************/
/**********************************************/
void SymphonyWindow::move( const QPoint& pos )
{
	if ( _lastPos.x() != -1 && pos.x() != -1 && !isMaximized() )
	{
		SWidget::move( this->pos() - _lastPos + pos );
	}
	_lastPos = pos;
}
/**********************************************/
/**********************************************/
void SymphonyWindow::resizeEvent( QResizeEvent* event )
{
	SWidget::resizeEvent( event );
}
/**********************************************/
/**********************************************/
void SymphonyWindow::closeEvent( QCloseEvent* event )
{
	SWidget::closeEvent( event );
	//        qDebug().noquote() << "";
	//        qDebug().noquote() << "--------------";
	//        qDebug().noquote() << "Saving...";
	//        QElapsedTimer timer;
	//        timer.start();
	App::settings()->save( Keys::Geometry, Keys::X, geometry().x() );
	App::settings()->save( Keys::Geometry, Keys::Y, geometry().y() );
	App::settings()->save( Keys::Geometry, Keys::Width, geometry().width() );
	App::settings()->save( Keys::Geometry, Keys::Height, geometry().height() );
	App::settings()->save( Keys::Maximized, isMaximized() );
	App::save();
	//        QString time = QString::number( timer.elapsed() );
	//        time.insert( time.length()-3, "s " );
	//        time += "ms";
	//        qDebug().noquote() << "Time to save Libraries & Playlists : " + time;
	//        qDebug().noquote() << "--------------";
}
/**********************************************/
/**********************************************/
void SymphonyWindow::maximise()
{
	isMaximized() ? showNormal() : showMaximized();
}
/**********************************************/
/**********************************************/
void SymphonyWindow::updateLayout()
{
	QString playerPos = App::settings()->getString( Keys::Player, Keys::Position );
	lay_main->removeWidget( wid_player );
	lay_main->removeWidget( _systemControls );
	lay_main->removeItem( lay_sections );
	lay_main->removeItem( _spacer );
	if ( playerPos == 't' )
	{
		lay_main->addWidget( wid_player, 0, 0, 1, 1 );
		lay_main->addLayout( lay_sections, 1, 0, 1, 2 );
		lay_main->addWidget( _systemControls, 0, 1, 1, 1 );
		//            lay_main->setAlignment( wid_player, Qt::AlignCenter );
	}
	else if ( playerPos == 'b' )
	{
		lay_main->addWidget( wid_player, 2, 0, 1, 2 );
		lay_main->addLayout( lay_sections, 1, 0, 1, 2  );
		lay_main->addWidget( _systemControls, 0, 1, 1, 1 );
		lay_main->addItem( _spacer, 0, 0);
		//            lay_main->setAlignment( wid_player, Qt::AlignVCenter );
	}
	else if ( playerPos == 'l' )
	{
		lay_main->addWidget( wid_player, 0, 0, 2, 1 );
		lay_main->addLayout( lay_sections, 1, 1, 1, 2 );
		lay_main->addWidget( _systemControls, 0, 2, 1, 1 );
		lay_main->addItem( _spacer, 0, 1);
		lay_main->setAlignment( wid_player, Qt::AlignTop );
	}
	else
	{
		lay_main->addWidget( wid_player, 1, 1, 1, 2 );
		lay_main->addLayout( lay_sections, 0, 0, 2, 1 );
		lay_main->addWidget( _systemControls, 0, 2, 1, 1 );
		lay_main->addItem( _spacer, 0, 1 );
		lay_main->setAlignment( wid_player, Qt::AlignTop );
	}
	lay_main->setAlignment( _systemControls, Qt::AlignVCenter );
	QString sectionsPos = App::settings()->getString( Keys::SectionsSelector, Keys::Position );
	if ( sectionsPos == 't' )
	{
		lay_sections->setDirection( QBoxLayout::TopToBottom );
		_sectionsSelector->setHorizontal();
	}
	else if ( sectionsPos == 'b' )
	{
		lay_sections->setDirection( QBoxLayout::BottomToTop );
		_sectionsSelector->setHorizontal();
	}
	else if ( sectionsPos == 'l' )
	{
		lay_sections->setDirection( QBoxLayout::LeftToRight );
		_sectionsSelector->setVertical();
	}
	else
	{
		lay_sections->setDirection( QBoxLayout::RightToLeft );
		_sectionsSelector->setVertical();
	}
}
/**********************************************/
/**********************************************/
void SymphonyWindow::settingsChanged( const QString& key )
{
	if ( key == SettingsFile::fullkey( Keys::Player, Keys::Position ) ||
		 key == SettingsFile::fullkey( Keys::Player, Keys::Size ) ||
		 key == SettingsFile::fullkey( Keys::SectionsSelector, Keys::Position ) )
	{
		updateLayout();
	}
	else if ( key == SettingsFile::fullkey( Keys::Appearance, Keys::Theme ) )
	{

	}
}
/**********************************************/
/**********************************************/
