#ifndef WIDGET_H
#define WIDGET_H
/**********************************************/
#include "BasicWidgets/Frame.h"
#include "App.h"
#include "Commons/TrackList.h"
#include "Commons/Playlists.h"
#include "Commons/Libraries.h"
#include <QListWidget>
/**********************************************/
#include "PlayerWidget/PlayerWidget.h"
#include "BasicWidgets/SystemControls.h"
#include "SectionsSelector/SectionsSelector.h"
#include "QueueWidget/QueueWidget.h"
#include "ArtistsWidget/ArtistsWidget.h"
#include "BasicWidgets/SWidget.h"
#include "BasicWidgets/Widget.h"
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SymphonyWindow
        : public SWidget
{
    Q_OBJECT
protected:
    // Window Parameters
    int invisibleBorderSize = 2;
    bool isPressedWidget;
    QPoint _lastPos = {-1,0};
    int _oldWidget = 0;
	QWidget* _oldOne = 0;
    // Initialise Widgets & Layouts
    GridLayout* lay_main = new GridLayout;
    BoxLayout* lay_sections = new BoxLayout;
    PlayerWidget* wid_player;
    SectionsSelector* _sectionsSelector;
    QueueWidget* wid_queue;
    ArtistsWidget* wid_artists;
//    AlbumsWidget* wid_albums;
//    GenresWidget* wid_genres;
//    TracksWidget* wid_tracks;
//    PlaylistsWidget* wid_playlists;
//    LibrariesWidget* wid_libraries;
//    RadiosWidget* wid_radios;
//    PodcastsWidget* wid_podcasts;
//    SettingsWidget* wid_settings;
    SystemControls* _systemControls;
    //
    QSpacerItem* _spacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Maximum );

public:
	~SymphonyWindow();
	SymphonyWindow( QWidget *parent = 0 );
	void addSection( QWidget* wid, const QString& key = QString() );

public slots:
	void showSettings();
//    void updateSettings()
//    {
        //	_settings->update();
        //	wid_player->updateIcons( _settings );
        //	wid_sidebar->updateIcons( _settings );
        //	wid_queue->updateIcons( _settings );
        //	wid_libraries->updateIcons( _settings );
//    }
	void updateIcons() override;
	void updateLang() override;
	void move( const QPoint& pos );
	void resizeEvent( QResizeEvent* event ) override;
	void closeEvent( QCloseEvent* event ) override;
	void maximise();
	void updateLayout() override;
	void settingsChanged( const QString& key ) override;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WIDGET_H
