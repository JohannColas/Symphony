#include "SymphonyWindow.h"
#include "FrameLess.h"
/**********************************************/
#include <QApplication>
/**********************************************/
/**********************************************/
/* */
int main( int argc, char *argv[] )
{
	QApplication a( argc, argv );
	QCoreApplication::addLibraryPath("./lib");
	SymphonyWindow w;
	FrameLess frameless(&w);
	w.show();
	return a.exec();
}
/**********************************************/
/**********************************************/
