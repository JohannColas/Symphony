#ifndef AUDIOTHREAD_H
#define AUDIOTHREAD_H

#include <QThread>
#include <QDebug>
#include <QTimer>
#include "bass.h"


class AudioThread
		: public QThread
{
	Q_OBJECT
private:
	static inline bool endOfMusic = false;
	double _currentPosition = 0;
public:
	explicit AudioThread(QObject *parent = 0);
	bool playing = false;
	void run();
	double currentPosition()
	{
		return _currentPosition;
	}
private:
	unsigned long chan;
	static void  syncFunc(HSYNC handle, DWORD channel, DWORD data, void *user);
	QTimer *t;
public slots:
	void play(QString filepath);
	void pause();
	void resume();
	void stop();
	void signalUpdate();
	void changePosition( int position );
signals:
	void endOfPlayback();
	void curPos(double Position, double Total);
};
#endif // AUDIOTHREAD_H
