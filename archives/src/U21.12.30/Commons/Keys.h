#ifndef KEYS_H
#define KEYS_H

#include <QString>

namespace Keys
{
	enum Type
	{
		None=-1,
		Queue=0,
		Artists=1,
		Albums=2,
		Tracks=3,
		Genres=4,
		Libraries=5,
		Playlists=6,
		Radios=7,
		Podcasts=8,
		Artist=10,
		Album=20,
		Track=30,
		Genre=40,
		Library=50,
		Playlist=60,
		Radio=70,
		Podcast=80
	};
	enum ListViewMode
	{
		NormalList,
		DetailedList,
		NormalTree,
		DetailedTree,
		Icon,
		Panel
	};
	enum GroupBy
	{
		NonGrouped,
		Grouped
	};
	enum SortBy
	{
		NotSort,
		SortByName,
		SortByArtist,
		SortByAlbum,
		//...
	};
	enum PlayerState
	{
		Playing,
		Paused,
		Stopped,
	};
	enum Repeat {
		NoRepeat = 0,
		RepeatAll = 1,
		RepeatOne = 2
	};
	enum Shuffle {
		NoShuffle,
		PureShuffle
	};


//	static inline QString Name = "name";
//	static inline QString Title = "title";
//	static inline QString Image = "name";
//	static inline QString Hide = "hide";
//	static inline QString Path = "path";

	enum Key
	{
		UnknownKey = -1,
		Name,
		Title,
		Image,
		Hide,
		Path,
		Year,
		Number,
		Duration,
		Bitrate
	};
	static inline QString toString( const Keys::Key& key )
	{
		switch ( key )
		{
			case Name:
			return "name";
			break;
			case Title:
			return "title";
			break;
			case Image:
			return "image";
			break;
			case Hide:
			return "hide";
			break;
			case Path:
			return "path";
			break;
			case Year:
			return "year";
			break;
			case Number:
			return "number";
			break;
			case Duration:
			return "duration";
			break;
			case Bitrate:
			return "bitrate";
			break;
			default:
			return "";
			break;
		}
	}
	static inline Keys::Key fromString( const QString& key )
	{
		if ( key == "name" )
			return Keys::Name;
		else if ( key == "title" )
			return Keys::Title;
		else if ( key == "image" )
			return Keys::Image;
		else if ( key == "hide" )
			return Keys::Hide;
		else if ( key == "path" )
			return Keys::Path;
		else if ( key == "year" )
			return Keys::Year;
		else if ( key == "number" )
			return Keys::Number;
		else if ( key == "duration" )
			return Keys::Duration;
		else if ( key == "bitrate" )
			return Keys::Bitrate;
		//else if ( key == "" )
		//	return Keys::;
		//else if ( key == "" )
		//	return Keys::;
		return Keys::UnknownKey;
	}
	static inline QString toString( const Keys::Type& key )
	{
		switch ( key )
		{
			case Artist:
			return "artist";
			break;
			case Album:
			return "album";
			break;
			case Track:
			return "track";
			break;
			case Genre:
			return "genre";
			break;
			case Library:
			return "library";
			break;
			case Playlist:
			return "playlist";
			break;
			case Radio:
			return "radio";
			break;
			case Podcast:
			return "podcast";
			break;
			default:
			return "";
			break;
		}
	}
	static inline Keys::Type toType( const QString& key )
	{
		if ( key == "artist" )
			return Keys::Artist;
		else if ( key == "album" )
			return Keys::Album;
		else if ( key == "track" )
			return Keys::Track;
		else if ( key == "genre" )
			return Keys::Genre;
		else if ( key == "library" )
			return Keys::Library;
		else if ( key == "playlist" )
			return Keys::Playlist;
		else if ( key == "radio" )
			return Keys::Radio;
		else if ( key == "podcast" )
			return Keys::Podcast;
		//else if ( key == "" )
		//	return Keys::;
		return Keys::None;
	}
}

#endif // KEYS_H
