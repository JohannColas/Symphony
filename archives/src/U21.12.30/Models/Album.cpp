#include "Album.h"

#include "Artist.h"
#include "Track.h"

#include "Commons/XML.h"

#include <QDomElement>

Album::Album( const QString& title )
	: Element(Keys::Album)
{
	_info.insert(Keys::Title,title);
}

Album::Album( QDomElement& dom_el )
	: Element(Keys::Album)
{
	QString title = dom_el.attributeNode(Keys::toString(Keys::Title)).value();
	if ( title.isEmpty() )
		title = "#Unknown Album";
	_info.insert(Keys::Title,title);
	//			album->setDate( el_album.attributeNode("date").value() );
	//			album->setGenre( el_album.attributeNode("genre").value() );
	//			album->setDescription( el_album.attributeNode("description").value() );
}

void Album::setArtist( Artist* artist )
{
	_parent = artist;
}

void Album::addTrack( Track* track )
{
	_children.append(track);
}

QString Album::title()
{
	QString title = _info.value(Keys::Title);
	if ( title.isEmpty() )
		return "#Unknown Album";
	return title;
}

QString Album::subTitle()
{
	if ( _parent != nullptr &&
		 _parent->type() == Keys::Artist )
		return parent()->title();
	return "#Unknown Artist";
	if ( _parent != nullptr )
		return "#Unknown Artist";
	QString artist = _parent->getInfo(Keys::Name);
	if ( artist.isEmpty() )
		return "#Unknown Artist";
	return artist;
}

bool Album::init( const QString& line )
{
	QString type;
	return XML::readLine( line, type, _info );
}

QString Album::save()
{
	return XML::writeLine( "album", _info );
}
