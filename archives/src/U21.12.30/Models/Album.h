#ifndef ALBUM_H
#define ALBUM_H

#include "Element.h"

class QDomElement;
class Artist;
class Track;

class Album : public Element
{
public:
	Album( const QString& title = "#Unknown Album" );
	Album( QDomElement& dom_el );

	void setArtist( Artist* artist );
	void addTrack( Track* track );

	QString title() override;
	QString subTitle() override;

	bool init( const QString &line ) override;
	QString save() override;
};

#endif // ALBUM_H
