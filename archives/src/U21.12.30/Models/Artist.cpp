#include "Artist.h"

#include "Album.h"

#include "Commons/XML.h"

#include <QDomElement>

Artist::Artist( const QString& str )
	: Element(Keys::Artist)
{
	_info.insert(Keys::Name,str);
}

Artist::Artist( QDomElement& dom_el )
	: Element(Keys::Artist)
{
	QString name = dom_el.attributeNode("name").value();
	if ( name.isEmpty() )
		name = "#Unknown Artist";
	_info.insert(Keys::Name,name);
	//		artist->setBiography( el_artist.attributeNode("biography").value() );
	//		artist->setDate( el_artist.attributeNode("date").value() );
	//		artist->setGender( el_artist.attributeNode("gender").value() );
	//		artist->setGenre( el_artist.attributeNode("genre").value() );
	//		artist->setNbMembers( el_artist.attributeNode("nbmembers").value() );
	//		artist->setCountry( el_artist.attributeNode("country").value() );
	//		artist->setWebsite( el_artist.attributeNode("website").value() );
}

void Artist::addAlbum( Album* album )
{
	_children.append(album);
}

QString Artist::title()
{
	QString title = _info.value(Keys::Name);
	if ( title.isEmpty() )
		return "#Unknown Artist";
	return _info.value(Keys::Name);
}

void Artist::set( QDomElement& /*dom_el*/ )
{

}

bool Artist::init( const QString& line )
{
	QString type;
	return XML::readLine( line, type, _info );
}

QString Artist::save()
{
	return XML::writeLine( "artist", _info );
}
