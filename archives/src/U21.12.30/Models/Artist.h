#ifndef ARTIST_H
#define ARTIST_H

#include "Element.h"

class Album;
class QDomElement;

class Artist : public Element
{
public:
	Artist( const QString& str = "#Unknown Artist" );
	Artist( QDomElement& dom_el );

	void addAlbum( Album* album );

	QString title() override;

	void set( QDomElement& dom_el );

	bool init( const QString &line ) override;
	QString save() override;
};

#endif // ARTIST_H
