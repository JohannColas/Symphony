#ifndef GENRE_H
#define GENRE_H

#include "Element.h"

class Genre : public Element
{
public:
	Genre();

	QString title() override;
};

#endif // GENRE_H
