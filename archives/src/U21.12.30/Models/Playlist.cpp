#include "Playlist.h"

#include "Commons/XML.h"

Playlist::Playlist() : Element(Keys::Playlist)
{
	_info.insert(Keys::Name, "Playlist");
//	_tracks.append( new class Track("dsfkjs") );
//	_tracks.append( new class Track("iaĝdvd") );
//	_tracks.append( new class Track("m;bhqc") );
//	_tracks.append( new class Track("wwqhdqs") );
//	_tracks.append( new class Track("amlfd") );
}

QString Playlist::title()
{
	return _info.value(Keys::Name);
}

QList<Element*> Playlist::tracks()
{
	return _children;
}

Element* Playlist::trackAt(int index)
{
	if ( indexIsValid(index) )
		return _children.at(index);
	return nullptr;
}

bool Playlist::indexIsValid(int index)
{
	if ( index >= 0 && index < _children.size() )
		return true;
	return false;
}

bool Playlist::init( const QString& line )
{
	QString type;
	if ( XML::readLine( line, type, _info ) )
	{
		XML::loadPlaylist( getInfo(Keys::Name), _children );
		return true;
	}
	return false;
}

QString Playlist::save()
{
	XML::savePlaylist( this );
	return XML::writeLine( "playlist", _info );
}
