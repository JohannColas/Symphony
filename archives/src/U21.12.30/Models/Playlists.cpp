#include "Playlists.h"

#include "Commons/XML.h"

Playlists::~Playlists()
{

}

Playlists::Playlists()
{
	init();
}

Playlist* Playlists::playlistAt( int index )
{
	if ( indexIsValid(index) )
		return _playlists.at(index);
	return nullptr;
}

bool Playlists::indexIsValid( int index )
{
	return ( index >= 0 && index < _playlists.size() );
}

void Playlists::addPlaylist( Playlist* playlist )
{
	_playlists.append(playlist);
}

void Playlists::init()
{
	XML::loadPlaylists(this);
}

void Playlists::save()
{
	XML::savePlaylists(this);
}
