#ifndef PLAYLISTS_H
#define PLAYLISTS_H

#include <QObject>
#include <QList>
#include "Playlist.h"

class Playlists : public QObject
{
	Q_OBJECT
private:
	QList<Playlist*> _playlists;
public:
	~Playlists();
	Playlists();

	QList<Playlist*> playlists()
	{
		return _playlists;
	}
	Playlist* playlistAt( int index );
	bool indexIsValid( int index );
	void addPlaylist( Playlist* playlist );

	void init();
	void save();
};

#endif // PLAYLISTS_H
