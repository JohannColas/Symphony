#include "Podcasts.h"

Podcasts::Podcasts()
{
	_podcasts.append( new Podcast() );
	_podcasts.append( new Podcast() );
	_podcasts.append( new Podcast() );
}

QList<Podcast*> Podcasts::podcasts()
{
	return _podcasts;
}

Podcast* Podcasts::podcastAt(int index)
{
	if ( indexIsValid(index) )
		return _podcasts.at(index);
	return nullptr;
}

bool Podcasts::indexIsValid(int index)
{
	return ( index >= 0 && index < _podcasts.size() );
}

void Podcasts::init()
{

}

void Podcasts::save()
{

}
