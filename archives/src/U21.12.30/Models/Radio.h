#ifndef RADIO_H
#define RADIO_H

#include "Element.h"

class Radio : public Element
{
public:
	Radio();

	QString title() override;
};

#endif // RADIO_H
