#include "Radios.h"

Radios::Radios()
{
//	_radios.append( new Radio() );
//	_radios.append( new Radio() );
//	_radios.append( new Radio() );
//	_radios.append( new Radio() );
//	_radios.append( new Radio() );
//	_radios.append( new Radio() );
//	_radios.append( new Radio() );
}

QList<Radio*> Radios::radios()
{
	return _radios;
}

Radio* Radios::radioAt(int index)
{	
	if ( indexIsValid(index) )
		return _radios.at(index);
	return nullptr;
}

bool Radios::indexIsValid(int index)
{
	return ( index >= 0 && index < _radios.size() );
}

void Radios::init()
{

}

void Radios::save()
{

}
