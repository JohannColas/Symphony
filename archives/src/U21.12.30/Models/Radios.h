#ifndef RADIOS_H
#define RADIOS_H

#include <QObject>
#include <QList>
#include "Radio.h"

class Radios : public QObject
{
	Q_OBJECT
private:
	QList<Radio*> _radios;
public:
	Radios();

	QList<Radio*> radios();
	Radio* radioAt(int index);
	bool indexIsValid(int index);

	void init();
	void save();
};

#endif // RADIOS_H
