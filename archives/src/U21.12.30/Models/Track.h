#ifndef TRACK_H
#define TRACK_H

#include "Element.h"

class QDomElement;
class Album;


class Track : public Element
{
public:
	Track( const QString& title = "track" );
	Track( const QString& title, const QString& path );
	Track( QDomElement& dom_el );

	void setAlbum( Album* album );

	QString title() override;
	QString subTitle() override;

	bool init( const QString &line ) override;
	QString save() override;
};

#endif // TRACK_H
