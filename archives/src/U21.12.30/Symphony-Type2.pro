QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets xml

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AudioManagement/AudioThread.cpp \
    Commons/App.cpp \
    Commons/Files.cpp \
    Commons/XML.cpp \
    Models/Album.cpp \
    Models/Artist.cpp \
    Models/Element.cpp \
    Models/Genre.cpp \
    Models/Libraries.cpp \
    Models/Library.cpp \
    Models/Player.cpp \
    Models/Playlist.cpp \
    Models/Playlists.cpp \
    Models/Podcast.cpp \
    Models/Podcasts.cpp \
    Models/Radio.cpp \
    Models/Radios.cpp \
    Models/Track.cpp \
    Views/BannerWidget.cpp \
    Views/GroupItem.cpp \
    Views/InfoWidget.cpp \
    Views/ListView.cpp \
    Views/ListViewItem.cpp \
    Views/MenuBar.cpp \
    Views/MenuBarButton.cpp \
    Views/PlayerButton.cpp \
    Views/PlayerTrackInfoWidget.cpp \
    Views/PlayerWidget.cpp \
    Views/SectionWidget.cpp \
    Views/SettingsWidget.cpp \
    Views/SubPlayerButton.cpp \
    main.cpp \
    SymphonyWindow.cpp

HEADERS += \
    AudioManagement/AudioThread.h \
    Commons/App.h \
    Commons/Files.h \
    Commons/Keys.h \
    Commons/XML.h \
    Models/Album.h \
    Models/Artist.h \
    Models/Element.h \
    Models/Genre.h \
    Models/Libraries.h \
    Models/Library.h \
    Models/Player.h \
    Models/Playlist.h \
    Models/Playlists.h \
    Models/Podcast.h \
    Models/Podcasts.h \
    Models/Radio.h \
    Models/Radios.h \
    Models/Track.h \
    SymphonyWindow.h \
    Views/BannerWidget.h \
    Views/GroupItem.h \
    Views/InfoWidget.h \
    Views/ListView.h \
    Views/ListViewItem.h \
    Views/MenuBar.h \
    Views/MenuBarButton.h \
    Views/PlayerButton.h \
    Views/PlayerTrackInfoWidget.h \
    Views/PlayerWidget.h \
    Views/SectionWidget.h \
    Views/SettingsWidget.h \
    Views/SubPlayerButton.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += \
    /usr/local/include/taglib \
    ./libs

DEPENDPATH += /usr/local/include/taglib

LIBS += \
    -L/usr/local/lib \
    -ltag \
    -L./libs \
    -lbass


RESOURCES += \
	resources.qrc
