#include "SymphonyWindow.h"

#include <QApplication>

#include <QGridLayout>
#include <QBoxLayout>
#include <QPushButton>
#include <QStackedLayout>

#include "Commons/Files.h"

#include "Models/Player.h"
#include "Models/Element.h"
#include "Models/Libraries.h"
#include "Models/Playlists.h"

#include "Views/PlayerWidget.h"
#include "Views/MenuBar.h"
#include "Views/ListView.h"
#include "Views/BannerWidget.h"
#include "Views/SettingsWidget.h"

#include <QDebug>


SymphonyWindow::~SymphonyWindow()
{
	delete pb_;
	delete pb_2;
	delete pb_3;
	delete pb_4;
	delete pb_4b;
	delete pb_4c;
	delete pb_6;
	delete pb_7;
	delete pb_8;
	delete wid_player;
	delete bar_menu;
	delete wid_list;
}

SymphonyWindow::SymphonyWindow(QWidget *parent)
    : QWidget(parent)
{
	setWindowIcon( QIcon(":/icons/darkTheme/Symphony.svg") );
	setWindowTitle("Symphony");
	// ---------------------------------
	// ---------------------------------

	// Application Style
	QString content;
	Files::read("/home/colas/Projects/Symphony/type2-src/themes/DarkTheme.qss", content);
	qApp->setStyleSheet(content);
	// ---------------------------------
	// ---------------------------------


	// Widgets Initialisation
	pb_ = new QPushButton("Player Pos : Top", this);
	pb_2 = new QPushButton("MenuBar Pos : Top", this);
	pb_3 = new QPushButton("MenuBar Mode : Bar", this);
	pb_4 = new QPushButton("MenuBar Align : Left", this);
	pb_4b = new QPushButton("MenuLay", this);
	pb_4c = new QPushButton("updateLibrary", this);

	wid_player = new PlayerWidget(this);

	bar_menu = new MenuBar(this);

	pb_6 = new QPushButton("NormalList", this);

	pb_7 = new QPushButton("Non Grouped", this);
	pb_8 = new QPushButton("Sort list", this);

	wid_list = new ListView(this);
	wid_list2 = new ListView(this);

	wid_ban = new BannerWidget(this);

	wid = new QWidget(this);

	wid_sets = new SettingsWidget(this);
	// ---------------------------------
	// ---------------------------------


	// Layouts Initialisation
	lay_main = new QBoxLayout(QBoxLayout::TopToBottom);
	QBoxLayout* lay = new QBoxLayout(QBoxLayout::LeftToRight);
	lay_player = new QBoxLayout(QBoxLayout::TopToBottom);
	stk_main = new QStackedLayout;
	lay_menu = new QBoxLayout(QBoxLayout::LeftToRight);
	QBoxLayout* laybox2 = new QBoxLayout(QBoxLayout::LeftToRight);
	lay_data = new QGridLayout();
	// ---------------------------------
	// ---------------------------------


	// Layouts Settings
	setLayout(lay_main);

	lay->addWidget(pb_);
	lay->addWidget(pb_2);
	lay->addWidget(pb_3);
	lay->addWidget(pb_4);
	lay->addWidget(pb_4b);
	lay->addWidget(pb_4c);
	lay->addStretch();
	lay_main->addItem(lay);

	lay_player->addWidget(wid_player);
	lay_player->addItem(stk_main);
	lay_main->addItem(lay_player);

	laybox2->addWidget(pb_6);
	laybox2->addWidget(pb_7);
	laybox2->addWidget(pb_8);
	laybox2->addStretch();

	lay_menu->addWidget(bar_menu, 0, Qt::AlignTop | Qt::AlignLeft);
	lay_data->addLayout(laybox2, 0, 0, 1, 2);
	lay_data->addWidget(wid_list, 1, 0, 2, 1);
	lay_data->addWidget(wid_ban, 1, 1);
	lay_data->addWidget(wid_list2, 2, 1);
	lay_data->setSpacing(6);
	lay_menu->addItem(lay_data);
	wid->setLayout(lay_menu);
	stk_main->addWidget(wid);
	stk_main->addWidget(wid_sets);
	// ---------------------------------
	// ---------------------------------


	// MAKE CONNECTIONS
	// ListView Parameters
	connect( pb_6, &QPushButton::released,
		 this, &SymphonyWindow::changeListViewMode );
	connect( pb_7, &QPushButton::released,
		 this, &SymphonyWindow::changeListViewGroupBy );
	connect( pb_8, &QPushButton::released,
		 this, &SymphonyWindow::sortListView );
	// ListView Changing
	connect( wid_list, &ListView::currentElementChanged,
			 wid_ban, &BannerWidget::setElement );
	connect( wid_list, &ListView::currentElementChanged,
			 wid_list2, &ListView::setElement );
	// MenuBar Parameters
	connect( pb_2, &QPushButton::released,
		 this, &SymphonyWindow::changeMenuBarPos );
	connect( pb_3, &QPushButton::released,
		 this, &SymphonyWindow::changeMenuBarMode );
	connect( pb_4, &QPushButton::released,
		 this, &SymphonyWindow::changeMenuBarAlign );
	connect( bar_menu, &MenuBar::changeCategory,
		 this, &SymphonyWindow::onChangeCategory );
	// Player Parameters
	connect( pb_, &QPushButton::released,
		 this, &SymphonyWindow::changePlayerPos );
	// Settings Parameters
	connect( pb_4b, &QPushButton::released,
		 this, &SymphonyWindow::onSettings );
	// ---------------------------------
	// ---------------------------------
	connect( pb_4c, &QPushButton::released,
		 App::libraries(), &Libraries::updateLibrary );


	// Styles
	pb_->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	pb_2->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	pb_3->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	pb_4->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	pb_4b->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	pb_4c->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	pb_6->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	pb_7->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
//	frame2->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	// ---------------------------------
	// ---------------------------------


    updateLayout();
}

void SymphonyWindow::updateLayout()
{
    switch (wid_player->pos())
    {
	case PlayerWidget::Top:
	    lay_player->setDirection(QBoxLayout::TopToBottom);
	    pb_->setText("Player Pos : Top");
	break;
	case PlayerWidget::Left:
	    lay_player->setDirection(QBoxLayout::LeftToRight);
	    pb_->setText("Player Pos : Left");
	break;
	case PlayerWidget::Bottom:
	    lay_player->setDirection(QBoxLayout::BottomToTop);
	    pb_->setText("Player Pos : Bottom");
	break;
	case PlayerWidget::Right:
	    lay_player->setDirection(QBoxLayout::RightToLeft);
	    pb_->setText("Player Pos : Right");
	break;
    }
    switch (bar_menu->mode())
    {
	case MenuBar::Bar:
	    switch (bar_menu->pos())
	    {
		case MenuBar::Left:
		    lay_menu->setDirection(QBoxLayout::LeftToRight);
		    pb_2->setText("Menu Pos : Left");
		break;
		case MenuBar::Top:
		    lay_menu->setDirection(QBoxLayout::TopToBottom);
		    pb_2->setText("Menu Pos : Top");
		break;
		case MenuBar::Right:
		    lay_menu->setDirection(QBoxLayout::RightToLeft);
		    pb_2->setText("Menu Pos : Right");
		break;
		case MenuBar::Bottom:
		    lay_menu->setDirection(QBoxLayout::BottomToTop);
		    pb_2->setText("Menu Pos : Bottom");
		break;
	    }
	    pb_3->setText("Menu Mode : Bar");
	break;
	case MenuBar::Button:
	    lay_menu->setDirection(QBoxLayout::TopToBottom);
	    pb_2->setText("Menu Pos : Top");
	    pb_3->setText("Menu Mode : Button");
	break;
    }
}

void SymphonyWindow::changePlayerPos()
{
    switch (wid_player->pos())
    {
	case PlayerWidget::Top:
	    wid_player->setPos(PlayerWidget::Left);
	break;
	case PlayerWidget::Left:
	    wid_player->setPos(PlayerWidget::Bottom);
	break;
	case PlayerWidget::Bottom:
	    wid_player->setPos(PlayerWidget::Right);
	break;
	case PlayerWidget::Right:
	    wid_player->setPos(PlayerWidget::Top);
	break;
    }
    updateLayout();
}

void SymphonyWindow::changeMenuBarPos()
{
    switch (bar_menu->pos())
    {
	case MenuBar::Left:
	    bar_menu->setPos(MenuBar::Top);
	    bar_menu->setAlign(Qt::AlignLeft);
	    pb_4->setText("Menu Align : Left");
	break;
	case MenuBar::Top:
	    bar_menu->setPos(MenuBar::Right);
	    bar_menu->setAlign(Qt::AlignTop);
	    pb_4->setText("Menu Align : Left");
	break;
	case MenuBar::Right:
	    bar_menu->setPos(MenuBar::Bottom);
	    bar_menu->setAlign(Qt::AlignLeft);
	    pb_4->setText("Menu Align : Left");
	break;
	case MenuBar::Bottom:
	    bar_menu->setPos(MenuBar::Left);
	    bar_menu->setAlign(Qt::AlignTop);
	    pb_4->setText("Menu Align : Left");
	break;
    }
    lay_menu->setAlignment(bar_menu, bar_menu->align());
    updateLayout();
}

void SymphonyWindow::changeMenuBarMode()
{
    switch (bar_menu->mode())
    {
	case MenuBar::Bar:
	    bar_menu->setMode(MenuBar::Button);
	break;
	case MenuBar::Button:
	    bar_menu->setMode(MenuBar::Bar);
	break;
    }
    updateLayout();
}

void SymphonyWindow::changeMenuBarAlign()
{
    if ( bar_menu->pos() == MenuBar::Top ||
	 bar_menu->pos() == MenuBar::Bottom ||
	 bar_menu->mode() == MenuBar::Button )
	switch (bar_menu->align())
	{
	    case Qt::AlignLeft:
		bar_menu->setAlign(Qt::AlignCenter);
		pb_4->setText("Menu Align : Center");
	    break;
	    case Qt::AlignCenter:
		bar_menu->setAlign(Qt::AlignRight);
		pb_4->setText("Menu Align : Right");
	    break;
	    default:
		bar_menu->setAlign(Qt::AlignLeft);
		pb_4->setText("Menu Align : Left");
	    break;
	}
    else
	switch (bar_menu->align())
	{
	    case Qt::AlignTop:
		bar_menu->setAlign(Qt::AlignCenter);
		pb_4->setText("Menu Align : Center");
	    break;
	    case Qt::AlignCenter:
		bar_menu->setAlign(Qt::AlignBottom);
		pb_4->setText("Menu Align : Bottom");
	    break;
	    default:
		bar_menu->setAlign(Qt::AlignTop);
		pb_4->setText("Menu Align : Top");
	    break;
	}
    lay_menu->setAlignment(bar_menu, bar_menu->align());
    //	updateLayout();
}

void SymphonyWindow::changeListViewMode()
{
    switch (wid_list->mode())
    {
	case Keys::NormalList:
	    wid_list->setMode(Keys::DetailedList);
	    pb_6->setText("DetailedList");
		pb_6->setIcon(QIcon(":/icons/darkTheme/view-list.svg"));
	break;
	case Keys::DetailedList:
	    wid_list->setMode(Keys::NormalTree);
	    pb_6->setText("NormalTree");
		pb_6->setIcon(QIcon(":/icons/darkTheme/view-list.svg"));
	break;
	case Keys::NormalTree:
	    wid_list->setMode(Keys::DetailedTree);
	    pb_6->setText("DetailedTree");
		pb_6->setIcon(QIcon(":/icons/darkTheme/view-list.svg"));
	break;
	case Keys::DetailedTree:
	    wid_list->setMode(Keys::Icon);
	    pb_6->setText("Icons");
		pb_6->setIcon(QIcon(":/icons/darkTheme/view-thumb.svg"));
	break;
	case Keys::Icon:
	    wid_list->setMode(Keys::Panel);
	    pb_6->setText("Panel");
		pb_6->setIcon(QIcon(":/icons/darkTheme/view-thumb.svg"));
	break;
	default:
	    wid_list->setMode(Keys::NormalList);
	    pb_6->setText("NormalList");
		pb_6->setIcon(QIcon(":/icons/darkTheme/view-list.svg"));
	break;
    }
}

void SymphonyWindow::changeListViewGroupBy()
{
    switch (wid_list->groupBy())
    {
	case Keys::NonGrouped:
		wid_list->setGroupBy(Keys::Grouped);
	    pb_7->setText("Grouped");
	break;
	default:
		wid_list->setGroupBy(Keys::NonGrouped);
	    pb_7->setText("Non Grouped");
	break;
	}
}

void SymphonyWindow::sortListView()
{
	wid_list->sort( Keys::SortByName );
}

void SymphonyWindow::onSettings()
{
	if ( stk_main->currentIndex() == 0 )
		pb_4b->setText("Settings"),stk_main->setCurrentIndex(1);
	else
		pb_4b->setText("LayMenu"),stk_main->setCurrentIndex(0);
}

void SymphonyWindow::onChangeCategory(const Keys::Type& type)
{
	wid_list->setType( type );
}

void SymphonyWindow::closeEvent( QCloseEvent* event )
{
	App::save();
	QWidget::closeEvent(event);
}

