#ifndef BANNERWIDGET_H
#define BANNERWIDGET_H

#include <QWidget>

class QGridLayout;
class QLabel;

class Element;

class BannerWidget
		: public QWidget
{
	Q_OBJECT
	QGridLayout* lay_main = nullptr;
	QLabel* lb_cover = nullptr;
	QLabel* lb_title = nullptr;
	QLabel* lb_subtitle = nullptr;
public:
	BannerWidget( QWidget* parent = nullptr );

public slots:
	void setElement( Element* el );
};

#endif // BANNERWIDGET_H
