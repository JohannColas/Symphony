#include "ListView.h"

#include <QHBoxLayout>
#include <QScrollBar>
#include <QMenu>
#include "ListViewItem.h"
#include "GroupItem.h"
#include "SectionWidget.h"
#include "Models/Player.h"
#include "Models/Playlist.h"
#include "Models/Playlists.h"
#include "Models/Libraries.h"
#include "Models/Radios.h"
#include "Models/Podcasts.h"

#include <cmath>


#include <QDebug>
#include <QMouseEvent>

ListView::~ListView()
{
	while ( !_items.isEmpty() )
		delete _items.takeFirst();
	while ( !_groups.isEmpty() )
		delete _groups.takeFirst();

	delete wid_section;
	delete _scrllbr;
	delete wid_cont;
	if ( lay_main != nullptr )
		delete lay_main;
	if ( menu != nullptr )
		delete menu;
}

ListView::ListView( QWidget* parent ) : QWidget(parent)
{
	this->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );

	wid_cont = new QWidget(this);
	_scrllbr = new QScrollBar(this);
	connect( _scrllbr, &QScrollBar::valueChanged,
			 this, &ListView::scrollAt );
	wid_section = new SectionWidget(wid_cont);
	wid_section->hide();


	lay_main = new QHBoxLayout(this);
	lay_main->setSpacing(0);
	lay_main->setMargin(0);
	lay_main->addWidget(wid_cont, 0, Qt::AlignTop);
	lay_main->addWidget(_scrllbr);

	updateType();

	menu = new QMenu(this);
	QAction* ac_play = new QAction("Play Now", menu);
	menu->addAction(ac_play);
	connect( ac_play, &QAction::triggered,
			 this, &ListView::onPlay );
	QAction* ac_add = new QAction("Add to Queue", menu);
	menu->addAction(ac_add);
	connect( ac_add, &QAction::triggered,
			 this, &ListView::onAdd );
	QAction* ac_insert = new QAction("Insert in Queue", menu);
	menu->addAction(ac_insert);
	connect( ac_insert, &QAction::triggered,
			 this, &ListView::onInsert );
	QAction* ac_addandplay = new QAction("Add to Queue & Play", menu);
	menu->addAction(ac_addandplay);
	connect( ac_addandplay, &QAction::triggered,
			 this, &ListView::onAddAndPlay );

	connect( this, &ListView::playTracks,
			 App::Player(), &Player::playTracks );
	connect( this, &ListView::addTracks,
			 App::Player(), &Player::addTracks );
	connect( this, &ListView::insertTracks,
			 App::Player(), &Player::insertTracks );

	this->setAutoFillBackground(true);
	QPalette pal = this->palette();
	pal.setBrush(QPalette::Window, pal.base() );
	this->setPalette( pal );
}

Keys::Type ListView::type()
{
	return _type;
}

void ListView::setType(const Keys::Type& type)
{
	_type = type;
	updateType();
	updateLayout();
}

Keys::ListViewMode ListView::mode()
{
	return _viewMode;
}

void ListView::setMode(const Keys::ListViewMode& viewMode)
{
	_viewMode = viewMode;
	updateMode();
	updateLayout();
}

Keys::GroupBy ListView::groupBy()
{
	return _groupBy;
}

void ListView::setGroupBy(const Keys::GroupBy& groupby)
{
	_groupBy = groupby;
	updateMode();
	this->updateLayout();
}

void ListView::addItem()
{
	ListViewItem* item = new ListViewItem(wid_cont);
	item->setVisible(true);
	item->setFixedSize(_itemsize);
	connect( item, &ListViewItem::addElement,
			 this, &ListView::addElementToSelection );
	connect( item, &ListViewItem::removeElement,
			 this, &ListView::removeElementToSelection );
	_items.append(item);
}

ListViewItem* ListView::itemAt(int index)
{
	if ( index >= 0 && index < _items.size() )
		return _items.at(index);
	return nullptr;
}

GroupItem* ListView::groupAt(int index)
{
	if ( index >= 0 && index < _groups.size() )
		return _groups.at(index);
	return nullptr;
}

Element* ListView::getSourceElement(int index)
{
	if ( 0 <= index && index < _elements.count() )
		return _elements.at(index);
	return nullptr;
}

void ListView::wheelEvent( QWheelEvent* event )
{
	if ( event->delta() < 0 )
		_scrllbr->setValue(_scrllbr->value()+1);
	else
		_scrllbr->setValue(_scrllbr->value()-1);
}

void ListView::resizeEvent(QResizeEvent* ev)
{
	this->updateLayout();
	QWidget::resizeEvent(ev);
}

void ListView::mouseReleaseEvent( QMouseEvent* event )
{
	if ( event->button() == Qt::RightButton )
	{
//		if ( _selectionEl.isEmpty() )
		{
			for ( auto item : _items )
				if ( item->geometry().contains(event->pos()) )
					item->setChecked(true), item->onReleased();
		}
		menu->move(this->mapToGlobal(event->pos()));
		menu->exec();
	}
}

void ListView::keyPressEvent( QKeyEvent* event )
{
	if ( event->key() == Qt::Key_Shift )
		_multipleSelection = true;
}

void ListView::keyReleaseEvent( QKeyEvent* event )
{
	if ( event->key() == Qt::Key_Shift )
		_multipleSelection = false;
}

void ListView::updateScrollBar()
{
	_scrllbr->setValue(0);
	_scrllbr->setMinimum(0);
	_scrllbr->setMaximum(_rowFirstIndex.size()-1);
	_scrllbr->setVisible(true);
	if ( _elCount <= _items.size() )
		_scrllbr->setVisible(false);
}

void ListView::scrollAt( int value )
{
	if ( 0 <= value && value < _rowFirstIndex.size() )
	{
		_begin_index = _rowFirstIndex.at(value);
		updateLayout();
	}
}

void ListView::addElementToSelection(Element* el)
{
	if ( !_multipleSelection )
		_selectionEl.clear();
	_selectionEl.append(el);
	emit currentElementChanged(el);
	updateLayout();
}

void ListView::removeElementToSelection(Element* el)
{
	_selectionEl.removeOne(el);
}

void ListView::getElementPlacement()
{
	_rowFirstIndex.clear();
	QChar old_char = -1;
	int jt = 0; bool newgroup = false;
	for ( int it = 0; it < _elements.count(); ++it )
	{
		if ( _groupBy == Keys::Grouped )
		{
			QChar c = _elements.at(it)->firstChar();
			if ( c != old_char )
			{
				old_char = c;
				newgroup = true;
			}
		}
		++jt;
		if ( jt+1 > _nbCols || newgroup )
		{
			jt = 0;
			_rowFirstIndex.append(it);
			newgroup = false;
		}
	}
	if ( !_rowFirstIndex.isEmpty() && _rowFirstIndex.first() != 0 )
		_rowFirstIndex.push_front(0);
}

void ListView::setElement( Element* el )
{
	_elements.clear();

	if ( el->type() == Keys::Artist )
		_elements = el->children();
	else if ( el->type() == Keys::Album )
		_elements = el->children();
	else if ( el->type() == Keys::Track )
		_elements = el->children();
	else if ( el->type() == Keys::Playlist )
		_elements = el->children();


	std::sort( _elements.begin(), _elements.end(), Element::compare );


	_elCount = _elements.count();
	// -----------------------------------------
	// Sort QList<Element*>
	// -----------------------------------------
	getElementPlacement();
	updateLayout();

	_selectionEl.clear();
	_begin_index = 0;
	updateScrollBar();
}

void ListView::updateMode()
{
	for ( ListViewItem* item : _items )
		item->setViewMode(this->mode());
}

void ListView::updateType()
{
	// -----------------------------------------
	// Get QList<Element*>
	_elements.clear();
	switch ( type() )
	{
		case Keys::Artists:
			for ( Element* el : App::libraries()->artists() )
				_elements.append(el);
		break;
		case Keys::Albums:
			for ( Element* el : App::libraries()->albums() )
				_elements.append(el);
		break;
		case Keys::Tracks:
			for ( Element* el : App::libraries()->tracks() )
				_elements.append(el);
		break;
		case Keys::Genres:
			for ( Element* el : App::libraries()->genres() )
				_elements.append(el);
		break;
		case Keys::Playlists:
			for ( Element* el : App::playlists()->playlists() )
				_elements.append(el);
		break;
		case Keys::Libraries:
			for ( Element* el : App::libraries()->libraries() )
				_elements.append(el);
		break;
		case Keys::Radios:
			for ( Element* el : App::radios()->radios() )
				_elements.append(el);
		break;
		case Keys::Podcasts:
			for ( Element* el : App::podcasts()->podcasts() )
				_elements.append(el);
		break;
		default:
			for ( Element* el : App::Player()->queue() )
				_elements.append(el);
			_groupBy = Keys::NonGrouped;
		break;
	}
	_elCount = _elements.count();



	// -----------------------------------------
	// Sort QList<Element*>
	// -----------------------------------------
	getElementPlacement();

	_selectionEl.clear();
	_begin_index = 0;
	updateScrollBar();
}

void ListView::updateLayout()
{
	wid_section->move( wid_cont->width()-wid_section->width(), 0 );
	wid_cont->setFixedHeight(height()-2*_margin.y());
	int availableWidth = wid_cont->width()-2*(_margin.x()+_extramargin.x());
	_nbCols = 1;
	int truespacing = 0;
	if ( mode() == Keys::Icon || mode() == Keys::Panel )
	{
		_nbCols = floor( (availableWidth+_spacing.x())/(_itemsize.width()+_spacing.x()));
		if ( _nbCols == 1 )
			truespacing = 0.5*(availableWidth-_itemsize.width());
		else
			truespacing = floor( (availableWidth-_nbCols*_itemsize.width())/(_nbCols-1));
	}
	//	int nbRow = floor( (wid_cont->height()-2*(_margin.y()+_extramargin.y())+_spacing.y())/(_itemsize.height()+_spacing.y()));


	getElementPlacement();


	int itemWidth = _itemsize.width();
	if ( mode() != Keys::Icon && mode() != Keys::Panel )
		itemWidth = availableWidth;

	// New Code - More efficient
	int x = _margin.x()+_extramargin.x();
	int y = _margin.y()+_extramargin.y();
	int it = 0;
	_nbRows = 0;
	QChar old_char;
	int it_group = 0;
	ListViewItem* item = nullptr;
	do
	{
		Element* elem = getSourceElement(_begin_index+it);
		if ( elem == nullptr )
			break;
		// ----------------------
		// Group management
		if ( _groupBy == Keys::Grouped )
		{
			QChar c = elem->firstChar();
			if ( c != old_char )
			{
				GroupItem* groupitem = groupAt(it_group);
				if ( groupitem == nullptr )
				{
					groupitem = new GroupItem(c,wid_cont);
					_groups.append(groupitem);
				}
				else
					groupitem->setText(c);
				//
				groupitem->setVisible(true);
				// Get the exact value of x and y
				x = _margin.x()+_extramargin.x();
				if ( groupitem != _groups.first() )
					y = item->y() + _itemsize.height()+_spacing.y();
				groupitem->move(x, y);
				// Set the width of this GroupItem
				groupitem->setFixedWidth(availableWidth);
				//
				y += groupitem->height()+_spacing.y();
				++it_group;
				old_char = c;
			}
		}
		// ----------------------
		// Item management
		item = itemAt(it);
		if ( item == nullptr )
		{
			addItem();
			item = _items.last();
		}
		item->setElement( elem );
		if ( _selectionEl.indexOf(elem) == -1 )
			item->setChecked(false);
		else
			item->setChecked(true);
		item->setViewMode(this->mode());
		item->setFixedWidth(itemWidth);
		item->move( x, y );
		if ( _viewMode == Keys::Icon || _viewMode == Keys::Panel )
		{
			x += itemWidth+truespacing;
			if ( x > availableWidth )
			{
				x = _margin.x()+_extramargin.x();
				y += _itemsize.height()+_spacing.y();
				++_nbRows;
			}
		}
		else
		{
			y += _itemsize.height()+_spacing.y();
			++_nbRows;
		}
		++it;
	}
	while ( y < wid_cont->height() );

	for ( int dt = _items.size()-1; dt > it-1; --dt )
		delete _items.takeAt(dt);
	for ( int dt = _groups.size()-1; dt > it_group-1; --dt )
		delete _groups.takeAt(dt);

	//	updateScrollBar();
}

void ListView::sort( const Keys::SortBy& sort )
{
	if ( type() == Keys::Queue ) return;
	_sortBy = sort;
	std::sort( _elements.begin(), _elements.end(), Element::compare );
	getElementPlacement();
	updateLayout();
}

void ListView::onPlay()
{
	emit playTracks(_selectionEl);
}

void ListView::onInsert()
{
	emit insertTracks(_selectionEl);
}

void ListView::onAdd()
{
	emit addTracks(_selectionEl);
}

void ListView::onAddAndPlay()
{
	emit addAndPlayTracks(_selectionEl);
}

void ListView::onAddToPlaylist()
{
//	emit addTracksToPlaylists(_selectionEl, );
}
