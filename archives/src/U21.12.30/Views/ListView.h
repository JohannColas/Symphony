#ifndef LISTVIEW_H
#define LISTVIEW_H

#include "Commons/App.h"

#include <QWidget>
class QHBoxLayout;
class QScrollBar;
class ListViewItem;
class GroupItem;
class SectionWidget;
class Element;
class Library;
class QMenu;

class ListView : public QWidget
{
	Q_OBJECT
private:
	Keys::Type _type = Keys::Queue;
	Keys::ListViewMode _viewMode = Keys::NormalList;
	Keys::GroupBy _groupBy = Keys::Grouped;
	Keys::SortBy _sortBy = Keys::NotSort;

	QHBoxLayout* lay_main = nullptr;
	QScrollBar* _scrllbr = nullptr;
	QWidget* wid_cont = nullptr;
	QList<ListViewItem*> _items;
	QList<GroupItem*> _groups;
	SectionWidget* wid_section = nullptr;
	QList<Element*> _elements;
	int _nbCols = -1;
	int _nbRows = -1;
	QList<int> _rowFirstIndex;

	int _begin_index = 0;
	bool _multipleSelection = false;
	QList<int> _selection;
	QList<Element*> _selectionEl;
	int _elCount = -1;

	QMenu* menu = nullptr;

	// Style Parameters
	QPoint _margin = {5,5};
	QPoint _extramargin = {5,5};
	QPoint _spacing = {5,5};
	QSize _itemsize = {100,100};

	int elementRowsCount()
	{
		return 0;
	}

	void updateScrollBar();

public:
	~ListView();
	ListView( QWidget* parent = nullptr );

	Keys::Type type();
	void setType( const Keys::Type& type );
	Keys::ListViewMode mode();
	void setMode( const Keys::ListViewMode& viewMode );
	Keys::GroupBy groupBy();
	void setGroupBy( const Keys::GroupBy& groupby );

	void addItem();
	ListViewItem* itemAt( int index );
	GroupItem* groupAt( int index );

	Element* getSourceElement(int index);

private slots:
	void wheelEvent( QWheelEvent* event ) override;
	void resizeEvent( QResizeEvent* ev ) override;
	void mouseReleaseEvent( QMouseEvent* event ) override;
	void keyPressEvent( QKeyEvent* event ) override;
	void keyReleaseEvent( QKeyEvent* event ) override;

	void scrollAt( int value );
	void addElementToSelection( Element* el );
	void removeElementToSelection( Element* el );

	void getElementPlacement();



public slots:
	void updateMode();
	void updateType();
	void updateLayout();

	void changeViewMode( const Keys::ListViewMode& mode )
	{
		_viewMode = mode;
	}
	void changeGroupBy( const Keys::GroupBy& groupby )
	{
		_groupBy = groupby;
	}
	void sort( const Keys::SortBy& sort );

	void onPlay();
	void onInsert();
	void onAdd();
	void onAddAndPlay();
	void onAddToPlaylist();

	void setElement( Element* el );

signals:
	// Queue Interaction
	void playTracks( const QList<Element*>& tracks );
	void insertTracks( const QList<Element*>& tracks );
	void addTracks( const QList<Element*>& tracks );
	void addAndPlayTracks( const QList<Element*>& tracks );
	void addTracksToPlaylists( const QList<Element*>& tracks, Playlist* playlist );
	// Playlist Interaction
	void addPlaylist( const QString& name );
	void removePlaylist( Playlist* playlist );
	void renamePlaylist( const QString& name, Playlist* playlist );
	void exportPlaylist( Playlist* playlist );
	// Library Interaction
	void addLibrary( const QString& name );
	void removeLibrary( Library* library );
	void renameLibrary( const QString& name, Library* library );
	void exportLibrary( Library* library );
	// Radio Interaction
	void addRadio( const QString& name );
	void removeRadio( Library* library );
	// Podcast Interaction
	void addPodcast( const QString& name );
	void removePodcast( Library* library );

	//
	void currentElementChanged( Element* el );
};

#endif // LISTVIEW_H
