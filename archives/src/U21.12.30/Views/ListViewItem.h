#ifndef LISTVIEWITEM_H
#define LISTVIEWITEM_H

#include <QPushButton>
#include <QLabel>
#include <QGridLayout>

#include "Commons/Keys.h"

class Element;

class ListViewItem : public QPushButton
{
	Q_OBJECT
private:
	Element* _el = nullptr;
	QGridLayout* lay_main = nullptr;
	QLabel* lb_title = nullptr;
	QLabel* lb_subtitle = nullptr;
	QLabel* lb_image = nullptr;

	QTimer* t = nullptr;
	int it_title = 0;
	int it_subtitle = 0;
	bool _activeTimer = false;
public:
	~ListViewItem();
	ListViewItem(QWidget* parent = nullptr);
	ListViewItem(Element* el, QWidget* parent = nullptr);

	Element* element();
	void setElement( Element* el );
	void setViewMode( const Keys::ListViewMode& key );

public slots:
	void updateLayout();
	void onReleased();

protected slots:
	void resizeEvent( QResizeEvent* event ) override;
	void enterEvent( QEvent* event ) override;
	void leaveEvent( QEvent* event ) override;
	void onEnterTimer();

signals:
	void addElement(Element* el);
	void removeElement(Element* el);
};

#endif // LISTVIEWITEM_H
