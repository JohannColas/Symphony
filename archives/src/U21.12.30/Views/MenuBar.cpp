#include "MenuBar.h"

#include <QBoxLayout>
#include <QPushButton>
#include <QSpacerItem>

#include <QMenu>
#include <QAction>

#include <QDebug>

MenuBar::MenuBar(QWidget *parent)
	: QFrame(parent)
{
	setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);

	lay_main = new QBoxLayout(QBoxLayout::TopToBottom);
	setLayout(lay_main);
	updateLayout();
}

MenuBar::~MenuBar()
{
	_actions.clear();
	if ( _menu != nullptr )
		delete _menu, _menu = nullptr;
	while ( !_buttons.isEmpty() )
	{
		delete _buttons.takeFirst();
	}
	delete lay_main->takeAt(0);
}

MenuBar::Position MenuBar::pos()
{
	return _pos;
}

void MenuBar::setPos(const MenuBar::Position& pos)
{
	_pos = pos;
	updateLayout();
}

Qt::Alignment MenuBar::align()
{
	return _align;
}

void MenuBar::setAlign(const Qt::Alignment& align)
{
	_align = align;
}

MenuBar::Mode MenuBar::mode()
{
	return _mode;
}

void MenuBar::setMode(const MenuBar::Mode& mode)
{
	_mode = mode;
	updateLayout();
}

void MenuBar::addButton(const QString& text, const QIcon& icon, const Keys::Type& type)
{
	_buttons.append( new MenuButton(text,icon,type,this) );
	_buttons.last()->setIconSize({24,24});
	if ( type != Keys::None)
		connect( _buttons.last(), &MenuButton::activated,
				 this, &MenuBar::onCategory );
}

void MenuBar::addAction(const QString& text, const QIcon& icon, const Keys::Type& type)
{
	if ( _menu == nullptr ) return;
	MenuAction* ac = new MenuAction(text,icon,type);
	_menu->addAction(ac);
	_actions.append( ac );
	if ( type != Keys::None)
		connect( ac, &MenuAction::activated,
				 this, &MenuBar::onCategory );
}

void MenuBar::updateLayout()
{
	_actions.clear();
	if ( _menu != nullptr )
		delete _menu, _menu = nullptr;
	while ( !_buttons.isEmpty() )
	{
		delete _buttons.takeFirst();
	}
	if ( _mode == MenuBar::Bar )
	{

		addButton( "Queue", QIcon(":/icons/darkTheme/pulse.svg"), Keys::Queue );
		addButton( "Artists", QIcon(":/icons/darkTheme/user.svg"), Keys::Artists );
		addButton( "Albums", QIcon(), Keys::Albums );
		addButton( "Tracks", QIcon(":/icons/darkTheme/music.svg"), Keys::Tracks );
		addButton( "Genres", QIcon(), Keys::Genres );
		addButton( "Playlists", QIcon(), Keys::Playlists );
		addButton( "Libraries", QIcon(), Keys::Libraries );
		addButton( "Radios", QIcon(), Keys::Radios );
		addButton( "Podcasts", QIcon(), Keys::Podcasts );
		if ( _pos == MenuBar::Left || _pos == MenuBar::Right )
			lay_main->setDirection(QBoxLayout::TopToBottom);
		else
			lay_main->setDirection(QBoxLayout::LeftToRight);
	}
	else
	{
		addButton( "Queue", QIcon(":/icons/darkTheme/pulse.svg"), Keys::None );
		_menu = new QMenu(_buttons.first());
		_buttons.first()->setMenu(_menu);
		addAction( "Queue", QIcon(":/icons/darkTheme/pulse.svg"), Keys::Queue );
		addAction( "Artists", QIcon(":/icons/darkTheme/user.svg"), Keys::Artists );
		addAction( "Albums", QIcon(), Keys::Albums );
		addAction( "Tracks", QIcon(":/icons/darkTheme/music.svg"), Keys::Tracks );
		addAction( "Genres", QIcon(), Keys::Genres );
		addAction( "Playlists", QIcon(), Keys::Playlists );
		addAction( "Libraries", QIcon(), Keys::Libraries );
		addAction( "Radios", QIcon(), Keys::Radios );
		addAction( "Podcasts", QIcon(), Keys::Podcasts );
	}
	for ( QPushButton* pb : _buttons )
	{
		pb->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
		if ( mode() == MenuBar::Bar )
			pb->setCheckable(true);
		lay_main->addWidget(pb);
	}
	onCategory(_currentType);
}

void MenuBar::onCategory(const Keys::Type& type)
{
	_currentType = type;
	if ( mode() == MenuBar::Bar )
	{
		for ( MenuButton* pb : _buttons )
		{
			if ( pb->type() == type )
				pb->setChecked(true);
			else
				pb->setChecked(false);
		}
	}
	else if ( _menu != nullptr )
	{
		for ( MenuAction* ac : _actions )
		{
			if ( ac->type() == type )
			{
				_buttons.first()->setText( ac->text() );
				_buttons.first()->setIcon( ac->icon() );
			}
		}
	}
	emit changeCategory(type);
}
