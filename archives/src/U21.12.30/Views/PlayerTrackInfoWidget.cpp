#include "PlayerTrackInfoWidget.h"

#include <QBoxLayout>
#include <QIcon>
#include <QLabel>

#include "Models/Element.h"


#include <QDebug>

PlayerTrackInfoWidget::PlayerTrackInfoWidget( QWidget* parent)
	: QWidget(parent)
{
	lay_main = new QBoxLayout(QBoxLayout::LeftToRight);
	lay_info = new QVBoxLayout;
	lb_cover = new QLabel;
	lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	lay_main->addWidget(lb_cover, Qt::AlignHCenter | Qt::AlignVCenter );

	lb_title = new QLabel;
	lb_title->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	lb_title->setFixedWidth(150);
	lay_info->addWidget( lb_title, Qt::AlignHCenter | Qt::AlignTop );

	lb_subtitle = new QLabel;
	lb_subtitle->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	lb_subtitle->setFixedWidth(150);
	lay_info->addWidget( lb_subtitle, Qt::AlignHCenter | Qt::AlignTop );
	lay_main->addLayout(lay_info);

	setLayout( lay_main );
}

void PlayerTrackInfoWidget::setMode( bool horizontal )
{
	if ( horizontal )
	{
		lay_main->setDirection( QBoxLayout::LeftToRight );
		lay_main->setAlignment( lb_cover, Qt::AlignHCenter | Qt::AlignTop );
		lay_info->setAlignment( lb_title, Qt::AlignHCenter | Qt::AlignTop );
		lay_info->setAlignment( lb_subtitle, Qt::AlignHCenter | Qt::AlignTop );
	}
	else
	{
		lay_main->setDirection( QBoxLayout::TopToBottom );
		lay_main->setAlignment( lb_cover, Qt::AlignHCenter | Qt::AlignVCenter );
		lay_info->setAlignment( lb_title, Qt::AlignLeft | Qt::AlignVCenter );
		lay_info->setAlignment( lb_subtitle, Qt::AlignLeft | Qt::AlignVCenter );
	}
}

void PlayerTrackInfoWidget::setElement( Element* el )
{
	lb_cover->setPixmap(QIcon(":/icons/darkTheme/music.svg").pixmap(50,50));
	if ( el == nullptr ) return;
	lb_title->setText( el->title() );
	lb_subtitle->setText( el->subTitle() );
}
