#ifndef PLAYERTRACKINFOWIDGET_H
#define PLAYERTRACKINFOWIDGET_H

#include <QWidget>

class QBoxLayout;
class QVBoxLayout;
class QLabel;

class Element;

class PlayerTrackInfoWidget
		: public QWidget
{
	Q_OBJECT
	bool _mode = true;
	QBoxLayout* lay_main = nullptr;
	QVBoxLayout* lay_info = nullptr;
	QLabel* lb_cover = nullptr;
	QLabel* lb_title = nullptr;
	QLabel* lb_subtitle = nullptr;

public:
	PlayerTrackInfoWidget( QWidget* parent = nullptr );

	void setMode( bool horizontal = true );

public slots:
	void setElement( Element* el );
};

#endif // PLAYERTRACKINFOWIDGET_H
