#include "PlayerWidget.h"

#include "PlayerTrackInfoWidget.h"
#include "PlayerButton.h"
#include "SubPlayerButton.h"

#include "Commons/App.h"
#include "Models/Player.h"
#include "Models/Element.h"

#include <QBoxLayout>
#include <QDebug>

PlayerWidget::~PlayerWidget()
{
	delete pb_backward;
	delete pb_playpause;
	delete pb_stop;
	delete pb_forward;

	delete pb_repeat;
	delete pb_shuffle;
	delete pb_equalizer;
	delete pb_volume;
}

PlayerWidget::PlayerWidget(QWidget *parent) : QWidget(parent)
{
	setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);

	pb_backward = new PlayerButton(/*"back",*/this);
	pb_backward->setIcon( QIcon(":/icons/darkTheme/skip_previous.svg"));
	pb_backward->setIconSize({36,36});
	pb_backward->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
//	connect( pb_backward, &PlayerButton::released,
//			 this, &PlayerWidget::backward );
	pb_playpause = new PlayerButton(/*"play",*/this);
	pb_playpause->setIcon( QIcon(":/icons/darkTheme/play.svg"));
	pb_playpause->setIconSize({36,36});
	pb_playpause->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
//	connect( pb_playpause, &PlayerButton::released,
//			 this, &PlayerWidget::playpause );
	pb_stop = new PlayerButton(/*"stop",*/this);
	pb_stop->setIcon( QIcon(":/icons/darkTheme/stop.svg"));
	pb_stop->setIconSize({36,36});
	pb_stop->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
//	connect( pb_stop, &PlayerButton::released,
//			 this, &PlayerWidget::onStop );
	pb_forward = new PlayerButton(/*"for",*/this);
	pb_forward->setIcon( QIcon(":/icons/darkTheme/skip_next.svg"));
	pb_forward->setIconSize({36,36});
	pb_forward->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
//	connect( pb_forward, &PlayerButton::released,
//			 this, &PlayerWidget::forward );

	pb_repeat = new SubPlayerButton(this);
	pb_repeat->setIcon( QIcon(":/icons/darkTheme/repeat.svg"));
	pb_repeat->setIconSize({24,24});
	pb_repeat->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	pb_shuffle = new SubPlayerButton(this);
	pb_shuffle->setIcon( QIcon(":/icons/darkTheme/shuffle.svg"));
	pb_shuffle->setIconSize({24,24});
	pb_shuffle->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	pb_equalizer = new SubPlayerButton(this);
	pb_equalizer->setIcon( QIcon(":/icons/darkTheme/equalizer.svg"));
	pb_equalizer->setIconSize({24,24});
	pb_equalizer->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	pb_volume = new SubPlayerButton(this);
	pb_volume->setIcon( QIcon(":/icons/darkTheme/volume.svg"));
	pb_volume->setIconSize({24,24});
	pb_volume->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);

	wid_info = new PlayerTrackInfoWidget(this);

	lay_main = new QBoxLayout(QBoxLayout::LeftToRight);
	lay_maincontrols = new QBoxLayout(QBoxLayout::LeftToRight);
	lay_subcontrols = new QBoxLayout(QBoxLayout::LeftToRight);

	this->setLayout(lay_main);
	lay_main->addItem(lay_maincontrols);
	lay_main->addWidget(wid_info, Qt::AlignCenter);
	lay_main->addItem(lay_subcontrols);

	lay_maincontrols->addWidget(pb_backward);
	lay_maincontrols->addWidget(pb_playpause);
	lay_maincontrols->addWidget(pb_stop);
	lay_maincontrols->addWidget(pb_forward);

	lay_subcontrols->addWidget(pb_repeat);
	lay_subcontrols->addWidget(pb_shuffle);
	lay_subcontrols->addWidget(pb_equalizer);
	lay_subcontrols->addWidget(pb_volume);

	makeConnections();
}
/**********************************************/
/**********************************************/
void PlayerWidget::makeConnections()
{
	connect( pb_playpause, &PlayerButton::released,
			 App::Player(), &Player::playpause );
	connect( pb_stop, &PlayerButton::released,
			 App::Player(), &Player::stop );
	connect( pb_forward, &PlayerButton::released,
			 App::Player(), &Player::forward );
	connect( pb_backward, &PlayerButton::released,
			 App::Player(), &Player::backward );
	connect( pb_repeat, &PlayerButton::released,
			 App::Player(), &Player::repeat );
	connect( pb_shuffle, &PlayerButton::released,
			 App::Player(), &Player::shuffle );

	connect( App::Player(), &Player::stateChanged,
			 this, &PlayerWidget::onPlayerStateChanged );
	connect( App::Player(), &Player::repeatChanged,
			 this, &PlayerWidget::onPlayerRepeatChanged );
	connect( App::Player(), &Player::shuffleChanged,
			 this, &PlayerWidget::onPlayerShuffleChanged );
	connect( App::Player(), &Player::sendCurrentTrack,
			 wid_info, &PlayerTrackInfoWidget::setElement );
}
/**********************************************/
/**********************************************/
void PlayerWidget::updateLayout()
{
	if ( lay_main->direction() == QBoxLayout::LeftToRight )
	{
		lay_main->setDirection(QBoxLayout::TopToBottom); // 2
//	else if ( lay_main->direction() == QBoxLayout::TopToBottom )
//		lay_main->setDirection(QBoxLayout::RightToLeft); // 1
//	else if ( lay_main->direction() == QBoxLayout::RightToLeft )
//		lay_main->setDirection(QBoxLayout::BottomToTop); // 3
		wid_info->setMode( true );
	}
	else
	{
		lay_main->setDirection(QBoxLayout::LeftToRight); // 0
		wid_info->setMode( false );
	}
}
/**********************************************/
/**********************************************/
//void PlayerWidget::playpause()
//{
////	if ( App::Player()->isPlaying() )
////	{
//////		pb_playpause->setIcon( QIcon(":/icons/darkTheme/play.svg"));
////		emit pause();
////	}
////	else
////	{
//////		pb_playpause->setIcon( QIcon(":/icons/darkTheme/pause.svg"));
//////		pb_playpause->setIcon( QIcon(":/icons/darkTheme/pause.svg"));
////		emit play();
////	}
//}

//void PlayerWidget::onStop()
//{
////	pb_playpause->setIcon( QIcon(":/icons/darkTheme/play.svg"));
////	emit stop();
//}
/**********************************************/
/**********************************************/
void PlayerWidget::onPlayerStateChanged( const Keys::PlayerState& state )
{
	switch ( state )
	{
		case Keys::Playing:
			pb_playpause->setIcon( QIcon(":/icons/darkTheme/pause.svg"));
		break;
		default:
			pb_playpause->setIcon( QIcon(":/icons/darkTheme/play.svg"));
		break;
	}
}
/**********************************************/
/**********************************************/
void PlayerWidget::onPlayerRepeatChanged( const Keys::Repeat& repeat )
{
	switch ( repeat )
	{
		case Keys::RepeatAll:
//			pb_repeat->setIcon( QIcon(":/icons/darkTheme/pause.svg"));
		break;
		case Keys::RepeatOne:
//			pb_repeat->setIcon( QIcon(":/icons/darkTheme/pause.svg"));
		break;
		default:
//			pb_repeat->setIcon( QIcon(":/icons/darkTheme/play.svg"));
		break;
	}
}
/**********************************************/
/**********************************************/
void PlayerWidget::onPlayerShuffleChanged( const Keys::Shuffle& shuffle )
{
	switch ( shuffle )
	{
		case Keys::PureShuffle:
//			pb_shuffle->setIcon( QIcon(":/icons/darkTheme/pause.svg"));
		break;
		default:
//			pb_shuffle->setIcon( QIcon(":/icons/darkTheme/play.svg"));
		break;
	}
}
/**********************************************/
/**********************************************/

