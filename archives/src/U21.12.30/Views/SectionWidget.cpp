#include "SectionWidget.h"

#include <QPushButton>
#include <QVBoxLayout>


SectionWidget::~SectionWidget()
{
	while ( !_sections.isEmpty() )
		delete _sections.takeLast();
}

SectionWidget::SectionWidget(QWidget *parent) : QWidget(parent)
{
	setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	layout = new QVBoxLayout;
	setLayout(layout);
	addSection('#');
	addSection('A');
	addSection('B');
	addSection('C');
	addSection('D');
	addSection('E');
	addSection('F');
	addSection('G');
	addSection('H');
	addSection('I');
	addSection('J');
	addSection('K');
	addSection('L');
	addSection('M');
}

void SectionWidget::addSection(const QChar& s)
{
	QPushButton* newsection = new QPushButton(s,this);
	newsection->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
	newsection->setFlat(true);
	_sections.append(newsection);
	updateSections();
}

void SectionWidget::updateSections()
{
	for ( auto section : _sections )
		layout->removeWidget(section);
	for ( auto section : _sections )
		layout->addWidget(section);
}
