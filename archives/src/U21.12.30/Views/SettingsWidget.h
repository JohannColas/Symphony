#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>

class QHBoxLayout;
class QListWidget;
class QStackedWidget;
class QListWidgetItem;

class SettingsWidget
		: public QWidget
{
	Q_OBJECT
	QHBoxLayout* lay_main = nullptr;
	QListWidget* wid_sections = nullptr;
	QStackedWidget* wid_settings = nullptr;

public:
	~SettingsWidget();
	SettingsWidget( QWidget* parent = nullptr );

	int addSections( const QString& name, QWidget* widget );

protected slots:
	void onSectionChanged( int index );
};

#endif // SETTINGSWIDGET_H
