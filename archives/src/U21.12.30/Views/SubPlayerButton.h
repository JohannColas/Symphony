#ifndef SUBPLAYERBUTTON_H
#define SUBPLAYERBUTTON_H

#include <QPushButton>

class SubPlayerButton : public QPushButton
{
public:
	explicit SubPlayerButton(QWidget *parent = nullptr);
	explicit SubPlayerButton(const QString& text, QWidget *parent = nullptr);
};

#endif // SUBPLAYERBUTTON_H
