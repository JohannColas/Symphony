#include "AudioThread.h"

#include <iostream>

//bool endOfMusic;

void AudioThread::syncFunc(HSYNC handle, DWORD channel, DWORD /*data*/, void */*user*/)
{
	BASS_ChannelRemoveSync(channel, handle);
	BASS_ChannelStop(channel);
//	qDebug() << "End of playback!";
	endOfMusic = true;
}

AudioThread::AudioThread(QObject *parent) :
	QThread(parent)
{
	if (!BASS_Init(-1, 44100, 0, NULL, NULL))
		qDebug() << "Cannot initialize device";
//	BASS_Init(-1, 44100, 0, NULL, NULL);
	t = new QTimer(this);
	connect(t, SIGNAL(timeout()), this, SLOT(signalUpdate()));
	endOfMusic = true;
}

void AudioThread::play(QString filename)
{
	_currentPosition = 0;
	BASS_ChannelStop(chan);
	if (!(chan = BASS_StreamCreateFile(false, filename.toUtf8(), 0, 0, BASS_SAMPLE_LOOP))
		&& !(chan = BASS_MusicLoad(false, filename.toUtf8(), 0, 0, BASS_MUSIC_RAMP | BASS_SAMPLE_LOOP, 1)))
	{
//		qDebug() << "Can't play file";
	}
	else
	{
//		qDebug() << "File is play :" << filename;
		endOfMusic = false;
		BASS_ChannelPlay(chan, true);
		t->start(100);
		BASS_ChannelSetSync(chan, BASS_SYNC_END, 0, &syncFunc, 0);
		playing = true;
		BASS_DEVICEINFO dffl;
		BASS_GetDeviceInfo( -1, &dffl );
	}
}

void AudioThread::pause()
{
	BASS_ChannelPause(chan);
	t->stop();
//	playing = false;
}

void AudioThread::resume()
{
//	qDebug() << "resume";
	if (!BASS_ChannelPlay(chan, false))
	{
//		qDebug() << "Error resuming";
	}
	else
	{
		t->start(98);
		playing = true;
	}
}

void AudioThread::stop()
{
	BASS_ChannelStop(chan);
	t->stop();
	playing = false;
}

void AudioThread::signalUpdate()
{
	if (endOfMusic == false)
	{
		_currentPosition = BASS_ChannelBytes2Seconds(chan, BASS_ChannelGetPosition(chan, BASS_POS_BYTE));
		emit curPos( _currentPosition,
					 BASS_ChannelBytes2Seconds(chan, BASS_ChannelGetLength(chan, BASS_POS_BYTE)));
	}
	else
	{
		emit endOfPlayback();
		playing = false;
	}
}

void AudioThread::changePosition( int position )
{
	BASS_ChannelSetPosition(chan, BASS_ChannelSeconds2Bytes(chan, position), BASS_POS_BYTE);
}

void AudioThread::run()
{
	while (1);
}
