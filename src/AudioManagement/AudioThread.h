#ifndef AUDIOTHREAD_H
#define AUDIOTHREAD_H

#include <QThread>
#include <QDebug>
#include <QTimer>
#include "bass.h"


class AudioThread
		: public QThread
{
	Q_OBJECT
private:
	static inline bool endOfMusic = false;
	double _currentPosition = 0;
public:
	explicit AudioThread(QObject *parent = 0);
	bool playing = false;
	void run();
	double currentPosition()
	{
		return _currentPosition;
	}
private:
	unsigned long chan;
	static void  syncFunc(HSYNC handle, DWORD channel, DWORD data, void *user);
	QTimer *t;
public slots:
	void play(QString filepath);
	void pause();
	void resume();
	void stop();
	void signalUpdate();
	void changePosition( int position );
signals:
	void endOfPlayback();
	void curPos(double Position, double Total);
};
#endif // AUDIOTHREAD_H

// NORMALIZE SONG !
//int GetPeak(DWORD channel)
//{
//	int a, c, peak=0;
//	short buf[50000];
//	BASS_ChannelSetPosition(channel, 0); // make sure to start from the start
//	while (BASS_ChannelIsActive(channel)) { // not reached the end yet
//		c=BASS_ChannelGetData(channel, buf, sizeof(buf)); // decode some data
//		for (a=0; a<c/2; a++) // scan the data for the peak
//			if (peak<abs(buf)) peak=abs(buf); // new highest peak
//	}
//	if (peak>32767) peak=32767;
//	return peak;
//}
//void CALLBACK NormalizeDSP(HDSP handle, DWORD channel, void *buffer, DWORD length, DWORD peak)
//{
//	short *s=buffer;
//	int a;
//	for (a=0; a<length/2; a++)
//		s[a]=s[a]*32767/peak; // apply normalization
//}
//...
//HSTREAM decoder=BASS_StreamCreateFile(FALSE, filename, 0, 0, BASS_STREAM_DECODE); // create decoder to pre-scan
//int peak=GetPeak(decoder); // get the peak
//BASS_StreamFree(decoder); // free the decoder
//stream=BASS_StreamCreateFile(FALSE, filename, 0, 0, 0); // create the playback channel
//BASS_ChannelSetDSP(stream, NormalizeDSP, peak, 0); // set the normalization DSP on it
//BASS_ChannelPlay(stream, 0); // start playing
/***********************************/
//int GetPeak(DWORD channel)
//{
//	int peak=0;
//	BASS_ChannelSetPosition(channel, 0); // make sure to start from the start
//	while (BASS_ChannelIsActive(channel)) { // not reached the end yet
//		DWORD level=BASS_ChannelGetLevel(channel);
//		int left=LOWORD(level), right=HIWORD(level); // extract left & right peaks
//		if (peak<left) peak=left;
//		if (peak<right) peak=right;
//	}
//	if (peak>32767) peak=32767;
//	return peak;
//}
// ------------------------------

// EQUALIZER
//// set EQ effect on stream
//eqfx=BASS_ChannelSetFX(stream, BASS_FX_BFX_PEAKEQ, 0);
//// initialize EQ parameters
//BASS_BFX_PEAKEQ eqparam;
//eqparam.fBandwidth=1; // 1 octave bandwidth
//eqparam.lChannel=-1; // apply to all channels within the stream
//// setup 10 EQ bands
//for (int band=0, band<10; band++) {
//    eqparam.lBand=band;
//    eqparam.fCenter=freq[band]; // "freq" = array of band center frequencies
//    eqparam.fGain=gain[band]; // "gain" = array of band gains
//    BASS_FXSetParameters(eqfx, &eqparam);
//}
/***********************************/
//// parametric equalizer
//BASS_DX8_PARAMEQ p;
//BASS_DX8_PARAMEQ p2;
//float fCentre[10] = { 31.25, 62.5, 125.0, 250.0, 500.0, 1000.0, 2000.0, 4000.0, 8000.0, 16000.0 };
//p.fGain = 0.0;
//p.fBandwidth = 18.0;
//for (int i = 0; i < 10; i++){
//   fx = BASS_ChannelSetFX(chan, BASS_FX_DX8_PARAMEQ, 0);
//   p.fCenter = fCentre;
//   BASS_FXSetParameters(fx, &p);
//   BASS_FXGetParameters(fx, &p2);
//   sprintf(message, "Put %f saved %f", p.fCenter, p2.fCenter);
//   MESSBOX(message);
//   UpdateFX(i);
//}
//// end equalizer
// ------------------------------


