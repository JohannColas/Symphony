#include "App.h"

#include <Models/Player.h>
#include <Models/Playlist.h>
#include <Models/Playlists.h>
#include <Models/Libraries.h>
#include <Models/Radios.h>
#include <Models/Podcasts.h>

Player* App::Player()
{
	return _player;
}

Playlist* App::queue()
{
	return new Playlist();//_queue;
}

Playlists* App::playlists()
{
	return _playlists;
}

Libraries* App::libraries()
{
	return _libraries;
}

Radios* App::radios()
{
	return _radios;
}

Podcasts* App::podcasts()
{
	return _podcasts;
}

void App::init()
{
	// init libraries first !!!
	_libraries = new class Libraries();

	_player = new class Player();
//	_queue = new class Playlist();
	_playlists = new class Playlists();
	_radios = new class Radios();
	_podcasts = new class Podcasts();
}

void App::save()
{
	_libraries->save();
	_player->saveQueue();
	_playlists->save();
}
