#ifndef APP_H
#define APP_H

#include "Commons/Keys.h"

class Player;
class Playlist;
class Playlists;
class Libraries;
class Radios;
class Podcasts;

class App
{
//public:
//	enum Type { None=-1, Queue=0, Artists=1, Albums=2, Tracks=3, Genres=4, Libraries=5, Playlists=6, Radios=7, Podcasts=8, Artist=10, Album=20, Track=30, Genre=40, Library=50, Playlist=60, Radio=70, Podcast=80 };
private:
	static inline class Player* _player = nullptr;
//	static inline class Playlist* _queue = nullptr;
	static inline class Playlists* _playlists = nullptr;
	static inline class Libraries* _libraries = nullptr;
	static inline class Radios* _radios = nullptr;
	static inline class Podcasts* _podcasts = nullptr;
public:
	static class Player* Player();
	static class Playlist* queue();
	static class Playlists* playlists();
	static class Libraries* libraries();
	static class Radios* radios();
	static class Podcasts* podcasts();

	static void init();
	static void save();
};

#endif // APP_H
