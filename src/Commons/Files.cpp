#include "Files.h"
/**********************************************/
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QTextStream>
#include <QPixmap>
#include <QDomDocument>
#include <QDomElement>
/**********************************************/
/**********************************************/
/**********************************************/
QString Files::makePath(const QString& dirPath, const QString& filePath)
{
	if ( !filePath.isEmpty() )
		return dirPath + "/" + filePath;
	return dirPath;
}
/**********************************************/
/**********************************************/
QString Files::localPath(const QString& file)
{
	// Create user dir if not exists
	QString configPath = "/.local/share";
#ifdef Q_OS_WIN
	configPath = "/AppData/Roaming";
#endif
	QDir dir( QDir::homePath() + configPath );
	dir.mkdir( "Symphony" );
	if ( dir.cd( "Symphony" ) )
		return makePath( dir.path(), file );
	return QDir::homePath();
}
/**********************************************/
/**********************************************/
QString Files::librariesPath(const QString& path)
{
	QDir dir( localPath() );
	dir.mkdir( "Libraries" );
	if ( dir.cd( "Libraries" ) )
		return makePath( dir.path(), path );
	return QDir::homePath();
}
/**********************************************/
/**********************************************/
QString Files::playlistsPath(const QString& file)
{
	QDir dir( localPath() );
	dir.mkdir( "Playlists" );
	if ( dir.cd( "Playlists" ) )
		return makePath( dir.path(), file );
	return QDir::homePath();
}
/**********************************************/
/**********************************************/
QDir Files::localDir()
{
	// Create user dir if not exists
	QString configPath = "/.local/share";
#ifdef Q_OS_WIN
	configPath = "/AppData/Roaming";
#endif
	QDir dir( QDir::homePath() + configPath );
	if ( !dir.exists( "Symphony" ) )
		dir.mkdir( "Symphony" );
	dir.cd( "Symphony" );
	return dir;
}
/**********************************************/
/**********************************************/
QStringList Files::DirList(const QString& path)
{
	QDir dir( path );
	dir.setFilter( QDir::Dirs );
	dir.setSorting( QDir::SortFlag::Name |
					QDir::IgnoreCase |
					QDir::LocaleAware );
	return dir.entryList();
}
/**********************************************/
/**********************************************/
QStringList Files::AudioFileList(const QString& path)
{
	QDir dir( path );
	dir.setNameFilters( AudioFileFilter );
	dir.setFilter( QDir::Files );
	dir.setSorting( QDir::SortFlag::Name |
					QDir::IgnoreCase |
					QDir::LocaleAware );
	return dir.entryList();
}
/**********************************************/
/**********************************************/
bool Files::deleteFolder(const QString& path)
{
	QDir dir( path );
	return dir.removeRecursively();
}
/**********************************************/
/**********************************************/
bool Files::exists(const QString& path)
{
	QFileInfo file( path );
	return file.exists();
}
/**********************************************/
/**********************************************/
bool Files::read(const QString& path, QString& contents, const char* codec)
{
	QFile file( path );
	if ( !file.open( QIODevice::ReadOnly ) )
		return false;
	QTextStream in( &file );
	in.setCodec( codec );
	contents = in.readAll();
	file.close();
	return true;
}
/**********************************************/
/**********************************************/
bool Files::save(const QString& path, const QString& data, const char* codec)
{
	QFileInfo fileinfo( path );
	fileinfo.absoluteFilePath();
	QStringList pqthSeq = fileinfo.absoluteFilePath().split('/');
	QFile file( path );
	if ( !file.open( QIODevice::WriteOnly ) )
		return false;
	QTextStream out( &file );
	out.setCodec( codec );
	out << data;
	file.close();
	return true;
}
/**********************************************/
/**********************************************/
QPixmap Files::readImage(const QString& path)
{
	QPixmap pixmap;
	pixmap.load( path );
	return pixmap;
}
/**********************************************/
/**********************************************/
bool Files::remove(const QString& path)
{
	//        qDebug() << "File::remove" << path;
	if ( path == "" )
		return false;
	return QFile( path ).remove();
}
/**********************************************/
/**********************************************/
bool Files::saveXML(const QString& path, QDomDocument& xmlDoc)
{
	// Getting root element
	QDomElement el_root = xmlDoc.documentElement();
	// Writing the Settings of XML Files
	QDomProcessingInstruction settings =
			xmlDoc.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"utf-8\"" );
	xmlDoc.insertBefore( settings, el_root );
	// Create the XML if DONT exists
	QFile xmlFile( path );
	if( !xmlFile.open( QFile::WriteOnly ) )
		return false;
	// Create QTextStream
	QTextStream stream( &xmlFile );
	// Writing the
	xmlDoc.save( stream, 4, QDomDocument::EncodingFromTextStream );
	//Fermeture du fichier
	xmlFile.close();
	return true;
}
/**********************************************/
/**********************************************/
QDomDocument Files::readXML(const QString& path)
{
	// Getting root element
	QDomDocument xmlDoc;
	QFile file( path );
	// Opening and Checking if the file exists
	if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
		return xmlDoc;
	// Settings and Checking if the file containts XML data
	xmlDoc.setContent( &file );
	file.close();
	return xmlDoc;
}
/**********************************************/
/**********************************************/
bool Files::readXML(const QString& path, QDomDocument& contents)
{
	// Getting root element
	QFile file( path );
	// Opening and Checking if the file exists
	if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
		return false;
	// Settings and Checking if the file containts XML data
	contents.setContent( &file );
	file.close();
	if ( contents.isNull() )
		return false;
	return true;
}
/**********************************************/
/**********************************************/
bool Files::findElement(QDomElement& element, const QString& attribute, const QString& comp)
{
	while ( !element.isNull() )
	{
		if ( element.attribute( attribute ) == comp )
			return true;
		element = element.nextSiblingElement();
	}
	return false;
}
/**********************************************/
/**********************************************/
