#ifndef FILES_H
#define FILES_H
/**********************************************/
#include <QString>
#include <QStringList>
/**********************************************/
class QFile;
class QFileInfo;
class QDir;
class QPixmap;
class QDomDocument;
class QDomElement;
/**********************************************/
/**********************************************/
/**********************************************/
class Files
{
	static inline QStringList AudioFileFilter = {"*.mp3", "*.ogg", "*.wav", "*.flac", "*.m4a", "*.wma"};
	/******************************************/
 public:
	static inline QString PlaylistsName = "#playlists.spls";
	static inline QString QueueName = "#Queue.spl";
	static inline QString LibrariesName = "#libraries.slbs";
	static inline QString LibraryName = "#library.slb";
	static QString makePath( const QString& dirPath, const QString& filePath = QString() );
	static QString localPath( const QString& file = QString() );
	static QString librariesPath( const QString& path = QString() );
	static QString playlistsPath( const QString& file = QString() );
	static QDir localDir();
	static QStringList DirList( const QString& path );
	static QStringList AudioFileList( const QString& path );
	static bool deleteFolder( const QString& path );
	static bool exists( const QString& path );
	static bool read( const QString& path, QString& contents, const char* codec = "UTF-8" );
	static bool save( const QString& path, const QString& data, const char* codec = "UTF-8" );
	static QPixmap readImage( const QString& path );
	static bool remove( const QString& path );
	static bool saveXML( const QString& path, QDomDocument& xmlDoc );
	static QDomDocument readXML( const QString& path );
	static bool readXML( const QString& path, QDomDocument& contents );
	static bool findElement( QDomElement& element, const QString& attribute, const QString& comp );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FILES_H
