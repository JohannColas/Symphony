#include "XML.h"

#include <QDomDocument>

#include "App.h"
#include "Files.h"

#include "Models/Library.h"
#include "Models/Libraries.h"
#include "Models/Playlist.h"
#include "Models/Playlists.h"

#include <QElapsedTimer>
#include <QDebug>
#include <QDir>
#include <QString>


bool XML::readLine( const QString& line, QString& name, QMap<Keys::Key,QString>& info )
{
	if ( line.isEmpty() )
		return false;
	QString key, value;
	int begin = line.indexOf("<")+1;
	int end = line.lastIndexOf("/>");
	if ( end == -1 )
		end = line.lastIndexOf("\">");
	QString tmp = line.mid( begin, end-begin );
	int namelimit = tmp.indexOf(" ");
	name = tmp.left(namelimit), name = name.remove(" ");
	tmp = tmp.mid( namelimit );
	QStringList lst = tmp.split("\" ");
	for ( auto el : lst )
	{
		int keyend = el.indexOf("=\"");
		key = el.left( keyend ).remove(" ");
		value = el.mid( keyend+2 );
		if ( value.right(1) == "\"" )
			value = value.remove( value.size()-1, 1 );
		info.insert( Keys::fromString(key), value );
	}
	return true;
}

bool XML::getNodeName( const QString& line, QString& name )
{

	if ( line.isEmpty() )
		return false;
	QString key, value;
	int begin = line.indexOf("<")+1;
	int end = line.lastIndexOf("/>");
	QString tmp = line.mid( begin, end-begin );
	int namelimit = tmp.indexOf(" ");
	name = tmp.left(namelimit), name = name.remove(" ");
	return true;
}

QString XML::writeLine( const QString& name, const QMap<Keys::Key,QString>& info )
{
	QString line;
	line += "<" + name;
	for ( auto key : info.keys() )
		line += " " + Keys::toString(key) + "=\"" + info.value(key) + "\"";
	line += "/>";
	return line;
}

bool XML::loadQueue( QList<Element*>& queue )
{
	QFile file (Files::localPath(Files::QueueName));
	if ( !file.open(QIODevice::ReadOnly) )
		return false;
	QTextStream in (&file);
	in.setCodec( "UTF-8" );
	while ( !in.atEnd() )
	{
		QString line = in.readLine();
		Track* track = App::libraries()->getTrackByPath(line);
		if ( track == nullptr )
			track = new Track( "", line );
		queue.append(track);
	}
	file.close();
	return true;
}

bool XML::saveQueue( const QList<Element*>& queue )
{
	QFile file (Files::localPath(Files::QueueName));
	if ( !file.open(QIODevice::WriteOnly) )
		return false;
	QTextStream out (&file);
	out.setCodec( "UTF-8" );
//	out << playlist->save() << "\n";
	for ( auto track : queue )
//		out << "\t" << track->save() << "\n";
		out << track->getInfo(Keys::Path) << "\n";
	file.close();
	return true;
}

bool XML::loadPlaylists( Playlists* playlists )
{
	QFile file (Files::playlistsPath( Files::PlaylistsName ));
	if ( !file.open(QIODevice::ReadOnly) )
		return false;
	QTextStream in (&file);
	in.setCodec( "UTF-8" );
	while ( !in.atEnd() )
	{
		Playlist* playlist = new Playlist;
		if ( playlist->init(in.readLine()) )
			playlists->addPlaylist( playlist );
		else
			delete playlist;
	}
	file.close();
	return true;
}

bool XML::savePlaylists( Playlists* playlists )
{
	QFile file (Files::playlistsPath(Files::PlaylistsName));
	if ( !file.open(QIODevice::WriteOnly) )
		return false;
	QTextStream out (&file);
	out.setCodec( "UTF-8" );
	for ( auto playlist : playlists->playlists() )
		out << "\t" << playlist->save() << "\n";
	file.close();
	return true;
}

bool XML::loadPlaylist( const QString& path, QList<Element*>& playlists )
{
	QFile file (Files::playlistsPath(path +".spl"));
	if ( !file.open(QIODevice::ReadOnly) )
		return false;
	QTextStream in (&file);
	in.setCodec( "UTF-8" );
	while ( !in.atEnd() )
	{
		QString line = in.readLine();
		Track* track = App::libraries()->getTrackByPath(line);
		if ( track == nullptr )
			track = new Track( "", line );
		playlists.append(track);
	}
	file.close();
	return true;
}

bool XML::savePlaylist( Playlist* playlist )
{
	QFile file (Files::playlistsPath(playlist->getInfo(Keys::Name)+".spl"));
	if ( !file.open(QIODevice::WriteOnly) )
		return false;
	QTextStream out (&file);
	out.setCodec( "UTF-8" );
//	out << playlist->save() << "\n";
	for ( auto track : playlist->tracks() )
//		out << "\t" << track->save() << "\n";
		out << track->getInfo(Keys::Path) << "\n";
	file.close();
	return true;
}

bool XML::loadLibraries( Libraries* libraries )
{
	QFile file (Files::librariesPath( Files::LibrariesName ));
	if ( !file.open(QIODevice::ReadOnly) )
		return false;
	QTextStream in (&file);
	in.setCodec( "UTF-8" );
	while ( !in.atEnd() )
	{
		Library* library = new Library;
		if ( library->init(in.readLine()) )
			libraries->addLibrary( library );
		else
			delete library;
	}
	file.close();
	return true;
}

bool XML::saveLibraries( Libraries* libraries )
{
	QFile file (Files::librariesPath( Files::LibrariesName ));
	if ( !file.open(QIODevice::WriteOnly) )
		return false;
	QTextStream out (&file);
	out.setCodec( "UTF-8" );
//	out << libraries->save() << "\n";
	for ( auto library : libraries->libraries() )
		out << library->save() << "\n";

	return true;
}

bool XML::loadLibrary( Libraries* libraries )
{
	qDebug() << "";
	qDebug() << "Init all Libraries";
	qDebug() << "---------------------------------";

	QElapsedTimer timer;
	timer.start();

	QFile file (Files::librariesPath( Files::LibraryName ));
	if ( !file.open(QIODevice::ReadOnly) )
		return false;
	QTextStream in (&file);
	in.setCodec( "UTF-8" );
	Artist* artist = nullptr;
	Album* album = nullptr;
	Track* track = nullptr;
	Genre* genre = nullptr;
	while ( !in.atEnd() )
	{
		QString line = in.readLine();
		QString name;
		XML::getNodeName( line, name );
		Keys::Type type = Keys::toType(name);
		if ( type == Keys::Artist )
		{
			artist = new Artist;
			if ( artist->init(line) )
				libraries->addArtist(artist);
			else
				delete artist, artist = nullptr;
		}
		else if ( type == Keys::Album )
		{
			album = new Album;
			if ( album->init(line) )
				libraries->addAlbum(album);
			else
				delete album, album = nullptr;
			if ( artist != nullptr && album != nullptr )
			{
				artist->addAlbum(album);
				album->setArtist(artist);
			}
		}
		else if ( type == Keys::Track )
		{
			track = new Track;
			if ( track->init(line) )
				libraries->addTrack(track);
			else
				delete track, track = nullptr;
			if ( album != nullptr && track != nullptr )
			{
				album->addTrack(track);
				track->setAlbum(album);
			}
		}
	}
	file.close();
	qDebug() << "  Artists size:" << libraries->artists().size();
	qDebug() << "  Albums size: " << libraries->albums().size();
	qDebug() << "  Tracks size: " << libraries->tracks().size();
	qDebug() << "  Time for \"initLibrary()\":" << timer.elapsed() << "ms";
	qDebug() << "---------------------------------";
	qDebug() << "";
	return true;
}

bool XML::saveLibrary( const Libraries* libraries )
{
	qDebug() << "";
	qDebug() << "Saving Library";
	qDebug() << "---------------------------------";
	QElapsedTimer timer;
	timer.start();
	QFile file (Files::librariesPath( Files::LibraryName ));
	if ( !file.open(QIODevice::WriteOnly) )
		return false;
	QTextStream in (&file);
	in.setCodec( "UTF-8" );
	for ( Artist* artist : libraries->artists() )
	{
		in << artist->save() << "\n";
		for ( Element* album : artist->children() )
		{
			in << "\t" << album->save() << "\n";
			for ( Element* track : album->children() )
				in << "\t\t" << track->save() << "\n";
		}
	}
	file.close();
	return true;
	qDebug() << "  Time for \"saveLibrary()\":" << timer.elapsed() << "ms";
	qDebug() << "---------------------------------";
	qDebug() << "";
}
