#ifndef XML_H
#define XML_H

#include "Keys.h"

#include <QList>

class Element;
class Track;
class Playlist;
class Playlists;
class Library;
class Libraries;

class XML
{
public:

	static bool readLine( const QString& line, QString& name, QMap<Keys::Key,QString>& info );
	static bool getNodeName( const QString& line, QString& name );
	static QString writeLine( const QString& name, const QMap<Keys::Key,QString>& info );

	static bool loadQueue( QList<Element*>& queue );
	static bool saveQueue( const QList<Element*>& queue );

	static bool loadPlaylists( Playlists* playlists );
	static bool savePlaylists( Playlists* playlists );

	static bool loadPlaylist( const QString& path, QList<Element*>& playlist );
	static bool savePlaylist( Playlist* playlist );

	static bool loadLibraries( Libraries* libraries );
	static bool saveLibraries( Libraries* libraries );

	static bool loadLibrary( Libraries* libraries );
	static bool saveLibrary( const Libraries* libraries );
};

#endif // XML_H
