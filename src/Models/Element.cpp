#include "Element.h"
/**********************************************/
#include <QString>
/**********************************************/
/**********************************************/
/**********************************************/
Element::~Element()
{

}
/**********************************************/
/**********************************************/
Element::Element(const Keys::Type& type)
{
	_type = type;
}
/**********************************************/
/**********************************************/
QString Element::title()
{
	return _info.value(Keys::Name);
}
/**********************************************/
/**********************************************/
QString Element::subTitle()
{
	return QString();
}
/**********************************************/
/**********************************************/
QString Element::info1()
{
	return QString();
}
/**********************************************/
/**********************************************/
QString Element::info2()
{
	return QString();
}
/**********************************************/
/**********************************************/
QString Element::info3()
{
	return QString();
}
/**********************************************/
/**********************************************/
QString Element::info4()
{
	return QString();
}
/**********************************************/
/**********************************************/
QString Element::image()
{
	return QString();
}

QList<Element*> Element::children()
{
	return _children;
}

Element* Element::parent()
{
	return _parent;
}
/**********************************************/
/**********************************************/
QMap<Keys::Key, QString> Element::info()
{
	return _info;
}
/**********************************************/
/**********************************************/
QString Element::infoAt( const Keys::Key& key )
{
	return _info.value(key);
}
/**********************************************/
/**********************************************/
QChar Element::firstChar()
{
	QString tmp = title();

	QChar c;
	if ( tmp.isEmpty() )
		c = ' ';
	else if ( tmp.left(4) == "The " && tmp.size() > 5 )
		c = tmp.at(4);
	else
		c = tmp.front();
	if ( c.toLower() == "é" || c.toLower() == "è" || c.toLower() == "ê" )
		c = 'E';
	if ( c.toLower() == "à" )
		c = 'A';
	if ( !c.isLetter() )
		c = '#';
	else
		c = c.toUpper();
	return c;
}
/**********************************************/
/**********************************************/
QString Element::comp()
{
//	QString::fromUtf8("ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ");
	QString tmp = title().toLower();
	tmp.replace(QRegExp("[éèëê]"), "e");
	tmp.replace(QRegExp("[àáâä]"), "a");
	tmp.replace(QRegExp("[í]"), "i");
	tmp.replace(QRegExp("[ó]"), "o");
	tmp.replace(QRegExp("[úù]"), "u");
	tmp.replace(QRegExp("[ç]"), "c");
	tmp.replace(QRegExp("[ñ]"), "n");
	return tmp;
}
/**********************************************/
/**********************************************/
QString Element::getInfo( const Keys::Key& key )
{
	return _info.value( key );
}
/**********************************************/
/**********************************************/
void Element::addInfo( const Keys::Key& key, const QString& value )
{
	if ( !value.isEmpty() )
		_info.insert(key,value);
}
/**********************************************/
/**********************************************/
bool Element::init( const QString& line )
{

}
/**********************************************/
/**********************************************/
QString Element::save()
{
	QString line;
	return line;
}
/**********************************************/
/**********************************************/
