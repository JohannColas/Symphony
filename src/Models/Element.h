#ifndef ELEMENT_H
#define ELEMENT_H
/**********************************************/
#include "Commons/Keys.h"
/**********************************************/
#include <QMap>
/**********************************************/
class QString;
/**********************************************/
/**********************************************/
/**********************************************/
class Element
{
protected:
	Keys::Type _type = Keys::None;
	QMap<Keys::Key,QString> _info;
	/******************************************/
	Element* _parent = nullptr;
	QList<Element*> _children;
	/******************************************/
public:
	virtual ~Element();
	Element(const Keys::Type& type);
	Keys::Type type() { return _type; }
	virtual QString title();
	virtual QString subTitle();
	virtual QString info1();
	virtual QString info2();
	virtual QString info3();
	virtual QString info4();
	virtual QString image();
	QList<Element*> children();
	Element* parent();
	/******************************************/
	QMap<Keys::Key,QString> info();
	QString infoAt( const Keys::Key& key );
	/******************************************/
	QChar firstChar();
	QString comp();
	/******************************************/
	static inline bool compare( Element* art1, Element* art2 )
	{
		if ( clean(art1->title()) == clean(art2->title()) )
			return clean(art1->subTitle()) < clean(art2->subTitle());
		return ( clean(art1->title()) < clean(art2->title()) );
	}
	static inline QString clean( const QString& str )
	{
		QString temp = str;
		if ( temp.left(4) == "The " )
			temp = temp.mid( 4 );
		temp.replace("É", "E").replace("é", "e");
		temp.replace("À", "a").replace("à", "a");
		return temp.toLower();
	}
	QString getInfo( const Keys::Key& key );
	void addInfo( const Keys::Key& key, const QString& value );
	//
	virtual bool init( const QString& line );
	virtual QString save();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ELEMENT_H
