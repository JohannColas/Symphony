#include "Genre.h"

Genre::Genre() : Element(Keys::Genre)
{
	_info.insert(Keys::Name,"Genre1");
}

QString Genre::title()
{
	return _info.value(Keys::Name);
}
