#include "Libraries.h"

#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QElapsedTimer>
#include <QImage>

#include "Commons/Files.h"
#include "Commons/XML.h"

#include "Artist.h"
#include "Album.h"
#include "Track.h"
#include "Genre.h"
/**********************************************/
// Taglib
#include <attachedpictureframe.h>
#include <mpegfile.h>
#include <opusfile.h>
#include <id3v2tag.h>
#include <audioproperties.h>
#include <commentsframe.h>
using namespace TagLib;

#include <QDebug>

Libraries::~Libraries()
{

}

Libraries::Libraries()
{
	init();
	//	updateLibrary();
	//	saveLibrary();
	save();
	initLibrary();
}

QList<Library*> Libraries::libraries() const
{
	return _libraries;
}

Library* Libraries::libraryAt(int index)
{
	if ( indexIsValid(index) )
		return _libraries.at(index);
	return nullptr;
}

bool Libraries::indexIsValid(int index)
{
	return ( index >= 0 && index < _libraries.size() );
}

void Libraries::addLibrary( Library* library )
{
	_libraries.append( library );
}

QList<Artist*> Libraries::artists() const
{
	return _artists;
}

Artist* Libraries::artistAt(int index)
{
	if ( artistIndexIsValid(index) )
		return _artists.at(index);
	return nullptr;
}

bool Libraries::artistIndexIsValid(int index)
{
	return ( index >= 0 && index < _artists.size() );
}

void Libraries::addArtist( Artist* artist )
{
	_artists.append( artist );
}

Artist* Libraries::getArtistByName( const QString& name )
{
	for ( Artist* art : _artists )
		if ( art->getInfo(Keys::Name) == name )
			return art;
	return nullptr;
}

QList<Album*> Libraries::albums() const
{
	return _albums;
}

Album* Libraries::albumAt(int index)
{
	if ( albumIndexIsValid(index) )
		return _albums.at(index);
	return nullptr;
}

bool Libraries::albumIndexIsValid(int index)
{
	return ( index >= 0 && index < _albums.size() );
}

void Libraries::addAlbum( Album* album )
{
	_albums.append( album );
}

Album* Libraries::getAlbumByTitle( const QString& title, Artist* artist )
{
	for ( Album* alb : _albums )
		if ( alb->getInfo(Keys::Title) == title &&
			 (artist == nullptr || alb->parent() == artist) )
			return alb;
	return nullptr;
}

QList<Track*> Libraries::tracks() const
{
	return _tracks;
}

Track* Libraries::trackAt(int index)
{
	if ( trackIndexIsValid(index) )
		return _tracks.at(index);
	return nullptr;
}

bool Libraries::trackIndexIsValid(int index)
{
	return ( index >= 0 && index < _tracks.size() );
}

void Libraries::addTrack( Track* track )
{
	_tracks.append( track );
}

Track* Libraries::getTrackByTitle( const QString& title, Album* album )
{
	for ( Track* tck : _tracks )
		if ( tck->getInfo(Keys::Title) == title &&
			 (album == nullptr || tck->parent() == album) )
			return tck;
	return nullptr;
}

Track* Libraries::getTrackByPath( const QString& path )
{
	for ( Track* tck : _tracks )
		if ( tck->getInfo(Keys::Path) == path )
			return tck;
	return nullptr;
}

QList<Genre*> Libraries::genres() const
{
	return _genres;
}

Genre* Libraries::genreAt(int index)
{
	if ( genreIndexIsValid(index) )
		return _genres.at(index);
	return nullptr;
}

bool Libraries::genreIndexIsValid(int index)
{
	return ( index >= 0 && index < _genres.size() );
}

void Libraries::addGenre( Genre* genre )
{
	_genres.append( genre );
}

Genre* Libraries::getGenreByTitle( const QString& title )
{
	return nullptr;
}

void Libraries::init()
{
	XML::loadLibraries(this);
}

void Libraries::save()
{
	XML::saveLibraries(this);
}


void Libraries::initLibrary()
{
	XML::loadLibrary(this);
};

void Libraries::saveLibrary()
{
	XML::saveLibrary(this);
}

void Libraries::updateLibrary()
{
	qDebug() << "";
	qDebug() << "Update all Libraries";
	qDebug() << "---------------------------------";
	QElapsedTimer timer;
	timer.start();

	for ( Library* library : _libraries )
	{
		if ( !library->hide() )
		{
			QDir dir( Files::librariesPath() );
			dir.mkdir( library->title() );
			// Getting files in directory
			for ( QString trackpath : Files::AudioFileList( library->getInfo(Keys::Path) ) )
			{
				trackpath = library->getInfo(Keys::Path) + "/" + trackpath;
				QString artist, album, title;
				if ( trackpath != "" )
				{
					if ( trackpath.right(4) == ".mp3" )
					{
						MPEG::File file( trackpath.toUtf8() );
						if ( file.isValid() )
						{
							ID3v2::Tag *tag = file.ID3v2Tag(true);
							if ( tag )
							{
								album = tag->album().toCString(true);
								title = tag->title().toCString(true);
								artist = tag->artist().toCString(true);
								Artist* el_artist = getArtistByName(artist);
								if ( el_artist == nullptr )
								{
									el_artist = new Artist( artist );
									_artists.append(el_artist);
								}
								Album* el_album = getAlbumByTitle(album, el_artist);
								if ( el_album == nullptr )
								{
									el_album = new Album( album );
									_albums.append(el_album);
									el_artist->addAlbum(el_album);
									el_album->setArtist(el_artist);
								}
								Track* el_track = getTrackByTitle(title,el_album);
								if ( el_track == nullptr )
								{
									el_track = new Track( title, trackpath );
									_tracks.append(el_track);
									el_album->addTrack(el_track);
									el_track->setAlbum(el_album);
								}
								el_track->addInfo( Keys::Year, QString::number(tag->year()) );
								el_track->addInfo( Keys::Number, QString::number(tag->track()) );
								el_track->addInfo( Keys::Duration, QString::number(file.audioProperties()->lengthInMilliseconds()) );
								el_track->addInfo( Keys::Bitrate, QString::number(file.audioProperties()->bitrate()) );
								//								setGenre( tag->genre().toCString(true) );
								QImage image;
								ID3v2::AttachedPictureFrame *frame = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(tag->frameList("APIC").front());
								if ( frame )
								{
									image.loadFromData(
												(const uchar *)frame->picture().data(),
												frame->picture().size());
									//									setCover( image );
								}
							}
							//				else
							//				  qDebug() << "file does not have a valid id3v2 tag" << endl;
						}
						else
							qDebug() << "File" << trackpath << "is invalid !";
					}
					else if ( trackpath.right(4) == ".ogg" )
					{
						Ogg::Opus::File file( trackpath.toUtf8() );
						if ( file.isValid() )
						{
							Ogg::XiphComment *tag = file.tag();
							if ( tag )
							{
								album = tag->album().toCString(true);
								title = tag->title().toCString(true);
								artist = tag->artist().toCString(true);
								Artist* el_artist = getArtistByName(artist);
								if ( el_artist == nullptr )
								{
									el_artist = new Artist( artist );
									_artists.append(el_artist);
								}
								Album* el_album = getAlbumByTitle(album, el_artist);
								if ( el_album == nullptr )
								{
									el_album = new Album( album );
									_albums.append(el_album);
									el_artist->addAlbum(el_album);
									el_album->setArtist(el_artist);
								}
								Track* el_track = getTrackByTitle(title,el_album);
								if ( el_track == nullptr )
								{
									el_track = new Track( title, trackpath );
									_tracks.append(el_track);
									el_album->addTrack(el_track);
									el_track->setAlbum(el_album);
								}
								el_track->addInfo( Keys::Year, QString::number(tag->year()) );
								el_track->addInfo( Keys::Number, QString::number(tag->track()) );
								el_track->addInfo( Keys::Duration, QString::number(file.audioProperties()->lengthInMilliseconds()) );
								el_track->addInfo( Keys::Bitrate, QString::number(file.audioProperties()->bitrate()) );
								//								setGenre( tag->genre().toCString(true) );
								QImage image;
								ByteVector frame = tag->pictureList().front()->data();
								if ( !frame.isEmpty() )
								{
									//							image.loadFromData(
									//										(const uchar *)frame,
									//										frame.size());
									//							setCover( image );
								}
							}
							//				else
							//				  qDebug() << "file does not have a valid id3v2 tag" << endl;
						}
					}
					else
						qDebug() << "No file extension found !";
				}
				else
					qDebug() << "Track path is empty !";
			}
		}

	}

	std::sort( _artists.begin(), _artists.end(), Element::compare );
	std::sort( _albums.begin(), _albums.end(), Element::compare );
	std::sort( _tracks.begin(), _tracks.end(), Element::compare );

	saveLibrary();
	qDebug() << "  Time for \"updateLibrary()\":" << timer.elapsed() << "ms";
	qDebug() << "---------------------------------";
	qDebug() << "";
}
