#ifndef LIBRARIES_H
#define LIBRARIES_H

#include <QObject>
#include <QList>
#include "Artist.h"
#include "Album.h"
#include "Track.h"
#include "Genre.h"
#include "Library.h"

class Libraries : public QObject
{
	Q_OBJECT
private:
	QList<Library*> _libraries;

	QList<Artist*> _artists;
	QList<Album*> _albums;
	QList<Track*> _tracks;
	QList<Genre*> _genres;
public:
	~Libraries();
	Libraries();

	QList<Library*> libraries() const;
	Library* libraryAt( int index );
	bool indexIsValid(int index);
	void addLibrary( Library* library );

	// Artists
	QList<Artist*> artists() const;
	Artist* artistAt( int index );
	bool artistIndexIsValid(int index);
	void addArtist( Artist* artist );
	Artist* getArtistByName( const QString& name );
	// Albums
	QList<Album*> albums() const;
	Album* albumAt( int index );
	bool albumIndexIsValid(int index);
	void addAlbum( Album* album );
	Album* getAlbumByTitle( const QString& title, Artist* artist = nullptr );
	// Tracks
	QList<Track*> tracks() const;
	Track* trackAt( int index );
	bool trackIndexIsValid(int index);
	void addTrack( Track* track );
	Track* getTrackByTitle( const QString& title, Album* album = nullptr );
	Track* getTrackByPath( const QString& path );
	// Genres
	QList<Genre*> genres() const;
	Genre* genreAt( int index );
	bool genreIndexIsValid(int index);
	void addGenre( Genre* genre );
	Genre* getGenreByTitle( const QString& title );

	void init();
	void save();

	void initLibrary();
	void saveLibrary();

public slots:
	void updateLibrary();
};

#endif // LIBRARIES_H
