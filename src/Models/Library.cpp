#include "Library.h"

#include "Commons/XML.h"

#include <QDebug>

Library::~Library()
{

}

Library::Library( const QString& name )
	: Element(Keys::Library)
{
	QString nametmp = name;
	if ( nametmp.isEmpty() )
		nametmp = "a Library";
	_info.insert(Keys::Name,nametmp);

}

Library::Library( const QString& name, const QString& hide, const QString& path )
	: Element(Keys::Library)
{
	QString nametmp = name;
	if ( nametmp.isEmpty() )
		nametmp = "a Library";
	_info.insert(Keys::Name,nametmp);
	_info.insert(Keys::Hide,hide);
	_info.insert(Keys::Path,path);
}

QString Library::title()
{
	return _info.value(Keys::Name);
}

bool Library::hide()
{
	QString hd = _info.value(Keys::Hide);
	if ( hd.isEmpty() )
		return false;
	return hd.toInt();
}

bool Library::init( const QString& line )
{
	QString str;
	return XML::readLine( line, str, _info );
}

QString Library::save()
{
	return XML::writeLine( "library", _info );
}
