#ifndef LIBRARY_H
#define LIBRARY_H

#include "Element.h"

class Library : public Element
{
public:
	virtual ~Library();
	Library( const QString& name = QString() );
	Library( const QString& name, const QString& hide, const QString& path = "" );

	QString title() override;
	bool hide();

	bool init( const QString& line );
	QString save() override;
};

#endif // LIBRARY_H
