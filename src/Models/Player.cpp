#include "Player.h"
#include "Commons/App.h"
#include "Commons/XML.h"
#include "Models/Element.h"
#include "Models/Track.h"
/**********************************************/
/**********************************************/
Player::Player()
{
	connect( &_athread, &AudioThread::endOfPlayback,
			 this, &Player::onTrackEnded );
	initQueue();
}
/**********************************************/
/**********************************************/
void Player::randomize()
{
	_listOrder.clear();
	for ( int it = 0; it < _queue.count(); ++it )
	{
		_listOrder.append( it );
	}
	std::srand(std::time(nullptr));
	std::random_shuffle( _listOrder.begin(), _listOrder.end() );
	if ( _index != -1 && _listOrder.contains(_index) )
	{
		while ( _listOrder.first() != _index )
		{
			_listOrder.append( _listOrder.takeFirst() );
		}
	}
}
/**********************************************/
/**********************************************/
void Player::reset()
{
	_index = 0;
	if ( _isShuffle )
		randomize();
	else
		_listOrder.clear();
	setTrack();
}
/**********************************************/
/**********************************************/
Track* Player::currentTrack()
{
	if ( _index >= 0 && _index < _queue.count() )
		return static_cast<Track*>(_queue.at(_index));
	return nullptr;
}

void Player::initQueue()
{
	XML::loadQueue( _queue );
}

void Player::saveQueue()
{
	XML::saveQueue( _queue );
}
/**********************************************/
/**********************************************/
void Player::play()
{
	if ( _index >= 0 && _index < _queue.size() )
	{
		Element* element = _queue.at(_index);
		_athread.play(element->getInfo(Keys::Path));
		_state = Keys::Playing;
		emit stateChanged(_state);
	}
	else if ( _index == -1 && !_queue.isEmpty() )
	{
		_index = 0;
		setTrack();
		play();
	}
}
/**********************************************/
/**********************************************/
void Player::pause()
{
	if ( _state == Keys::Playing )
	{
		_athread.pause();
		_state = Keys::Paused;
		emit stateChanged(_state);
	}
}
/**********************************************/
/**********************************************/
void Player::resume()
{
	_athread.resume();
	if ( _athread.playing )
	{
		_state = Keys::Playing;
		emit stateChanged(_state);
	}
}
/**********************************************/
/**********************************************/
void Player::playpause()
{
	switch ( state() )
	{
		case Keys::Playing:
			pause();
		break;
		default:
			if ( _athread.playing )
				resume();
			else
				play();
		break;
	}
}
/**********************************************/
/**********************************************/
void Player::stop()
{
	_athread.stop();
	_state = Keys::Stopped;
	emit stateChanged(_state);
}
/**********************************************/
/**********************************************/
void Player::forward()
{
	++_index;
	if ( _index == _queue.count() )
	{
		_index = 0;
		if ( _repeat == Keys::NoRepeat )
		{
			stop();
		}
	}
	setTrack();
}
/**********************************************/
/**********************************************/
void Player::backward()
{
	if ( _athread.currentPosition() < 1000 )
	{
		--_index;
		if ( _index == -1 )
			_index = _queue.count() - 1;
		setTrack();
	}
	else
		_athread.changePosition(0);
}
/**********************************************/
/**********************************************/
void Player::repeat()
{
	if ( _repeat == Keys::NoRepeat )
		_repeat = Keys::RepeatAll;
	else if ( _repeat == Keys::RepeatAll )
		_repeat = Keys::RepeatOne;
	else if ( _repeat == Keys::RepeatOne )
		_repeat = Keys::NoRepeat;
	//  saveSettings( _repeat, Keys::Player, Keys::Repeat );
	//	App::settings()->save( Keys::Player, Keys::Repeat, _repeat );
}
/**********************************************/
/**********************************************/
void Player::shuffle()
{
	_isShuffle = !_isShuffle;
	if ( _isShuffle )
	{
		randomize();
	}
	else
	{
		if ( _index >= 0 && _index < _listOrder.size() )
			_index = _listOrder.at(_index);
		_listOrder.clear();
	}
	//	App::settings()->save( Keys::Player, Keys::Shuffle, _isShuffle );
}

void Player::equalize()
{

}
/**********************************************/
/**********************************************/
void Player::setTrack()
{
	int index = _index;
	if ( index < 0 || index >= _queue.size() )
		return;
	if ( _isShuffle && _listOrder.size() > 0)
		index = _listOrder.at( index );
	Element* element = _queue.at(index);
	_athread.play(element->getInfo(Keys::Path));
	if ( state() == Keys::Playing )
		play();
	emit trackChanged();
	emit sendCurrentTrack( element );
}
/**********************************************/
/**********************************************/
void Player::addToQueue( const QList<Element*>& elements , bool insertMode )
{
	QList<Element*> tracks;
	for ( Element* el : elements )
	{
		if ( el->type() == Keys::Artist )
		{
			for ( Element* cel : el->children() )
				if ( cel->type() == Keys::Album )
					for ( Element* ccel : cel->children() )
						tracks.append( ccel );
		}
		else if ( el->type() == Keys::Album ||
				  el->type() == Keys::Genres ||
				  el->type() == Keys::Playlist )
		{
			for ( Element* cel : el->children() )
				tracks.append( cel );
		}
		else if ( el->type() == Keys::Track )
		{
			tracks.append( el );
		}
	}
	for ( int it = 0; it < tracks.size(); ++it )
	{
		if ( insertMode && _index+it+1 < _queue.size() )
			_queue.insert( _index+it+1, tracks.at(it) );
		else
			_queue.append( tracks.at(it) );
	}
}
/**********************************************/
/**********************************************/
void Player::playTracks( const QList<Element*>& elements )
{
	_queue.clear();
	addToQueue( elements );
	if ( !_queue.isEmpty() )
	{
		_index = 0;
		setTrack();
		play();
	}
}
/**********************************************/
/**********************************************/
void Player::addTracks( const QList<Element*>& elements )
{
	addToQueue( elements );
}
/**********************************************/
/**********************************************/
void Player::insertTracks( const QList<Element*>& elements )
{
	addToQueue( elements, true );
}
/**********************************************/
/**********************************************/
void Player::addAndPlayTracks( const QList<Element*>& elements )
{
	if ( elements.size() > 0 )
	{
		_index = _queue.count();
		addToQueue( elements );
		setTrack();
		play();
	}
}
/**********************************************/
/**********************************************/
void Player::onTrackEnded()
{
	if ( state() == Keys::Playing )
		++_index,setTrack();
}
/**********************************************/
/**********************************************/
