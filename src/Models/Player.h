#ifndef PLAYER_H
#define PLAYER_H
/**********************************************/
#include <QObject>
/**********************************************/
#include "AudioManagement/AudioThread.h"
#include "Commons/Keys.h"
/**********************************************/
class Track;
class Element;
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Player
		: public QObject
{
	Q_OBJECT
private:
	Keys::PlayerState _state = Keys::Stopped;

	AudioThread _athread;
	//
	Keys::Repeat _repeat = Keys::NoRepeat;
	bool _isShuffle = false;
	//
	//
	QList<Element*> _queue;
	int _index = -1;
	QList<int> _listOrder;

	void setTrack();
	void addToQueue( const QList<Element*>& elements, bool insertMode = false );

public:
	Player();
	void randomize();
	void reset();
	Keys::Repeat getRepeat() const {return _repeat;}
	bool getShuffle() const {return _isShuffle;}
	Track* currentTrack();
	Keys::PlayerState state()
	{
		return _state;
	}
	QList<Element*> queue()
	{
		return _queue;
	}
	void initQueue();
	void saveQueue();

public slots:
	void play();
	void pause();
	void resume();
	void playpause();
	void stop();
	void forward();
	void backward();
	void repeat();
	void shuffle();
	void equalize();

public slots:
	void playTracks( const QList<Element*>& elements );
	void addTracks( const QList<Element*>& elements );
	void insertTracks( const QList<Element*>& elements );
	void addAndPlayTracks( const QList<Element*>& elements );

private slots:
	void onTrackEnded();

signals:
	void trackChanged();
	void currentTrackChanged( Track* track );
	void sendCurrentTrack( Element* el );

	void currentPositionChanged( int pos );
	void stateChanged( const Keys::PlayerState& state );
	void repeatChanged( const Keys::Repeat& repeat );
	void shuffleChanged( const Keys::Shuffle& shuffle );
	void trackEnded();
	void queueEnded();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLAYER_H
