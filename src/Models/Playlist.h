#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QList>

#include "Track.h"

class Playlist : public Element
{
//	QList<class Track*> _tracks;
public:
	Playlist();

	QString title() override;

	QList<class Element*> tracks();
	class Element* trackAt(int index);
	bool indexIsValid(int index);

	bool init( const QString& line ) override;
	QString save() override;


	QString path() const
	{
//        if ( type() == Type::QUEUE )
//            return Files::localPath( name() );
//        else if ( type() == Type::PLAYLIST )
//            return Files::playlistsPath( name() + ".spl" );
		return QString();
	}
};

#endif // PLAYLIST_H
