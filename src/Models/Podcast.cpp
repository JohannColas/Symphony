#include "Podcast.h"

Podcast::Podcast() : Element(Keys::Podcast)
{
	_info.insert(Keys::Title,"podcast");
}

QString Podcast::title()
{
	return _info.value(Keys::Title);
}
