#ifndef PODCAST_H
#define PODCAST_H

#include "Element.h"

class Podcast : public Element
{
public:
	Podcast();

	QString title() override;
};

#endif // PODCAST_H
