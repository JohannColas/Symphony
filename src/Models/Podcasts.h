#ifndef PODCASTS_H
#define PODCASTS_H

#include <QObject>
#include <QList>
#include "Podcast.h"

class Podcasts : public QObject
{
	Q_OBJECT
private:
	QList<Podcast*> _podcasts;
public:
	Podcasts();

	QList<Podcast*> podcasts();
	Podcast* podcastAt(int index);
	bool indexIsValid(int index);

	void init();
	void save();
};

#endif // PODCASTS_H
