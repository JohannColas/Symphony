#include "Track.h"

#include "Album.h"

#include "Commons/XML.h"


Track::Track( const QString& title )
	: Element(Keys::Track)
{
	_info.insert(Keys::Title,title);
}

Track::Track( const QString& title, const QString& path )
	: Element(Keys::Track)
{
	_info.insert(Keys::Title,title);
	_info.insert(Keys::Path,path);
}

Track::Track( QDomElement& dom_el )
	: Element(Keys::Track)
{
//	QString path = dom_el.attributeNode(Keys::toString(Keys::Path)).value();
//	QString title = dom_el.attributeNode(Keys::toString(Keys::Title)).value();
//	if ( title.isEmpty() )
//		title = path.split('/').last().split('.').first();
//	_info.insert(Keys::Title,title);
//	_info.insert(Keys::Path,path);
//	track->setTitle( el_track.attributeNode("title").value() );
//	track->setGenre( el_track.attributeNode("genre").value() );
//	track->setNumber( el_track.attributeNode("number").value() );
//	track->setDescription( el_track.attributeNode("description").value() );
//	track->setComment( el_track.attributeNode("comment").value() );
}

void Track::setAlbum( Album* album )
{
	_parent = album;
}

QString Track::title()
{
	return _info.value(Keys::Title);
}

QString Track::subTitle()
{
	if ( _parent != nullptr &&
		 _parent->type() == Keys::Album &&
		 _parent->parent() != nullptr &&
		 _parent->parent()->type() == Keys::Artist )
		return _parent->parent()->title();
	return "#Unknown Artist";
	if ( _parent != nullptr )
		return "#Unknown Artist";
	QString artist = _parent->getInfo(Keys::Name);
	if ( artist.isEmpty() )
		return "#Unknown Artist";
	return artist;
}

bool Track::init( const QString& line )
{
	QString type;
	return XML::readLine( line, type, _info );
}

QString Track::save()
{
	return XML::writeLine( "track", _info );
}
