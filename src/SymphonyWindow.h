#ifndef SYMPHONYWINDOW_H
#define SYMPHONYWINDOW_H

#include "Commons/Keys.h"

#include <QWidget>

class QPushButton;
class QBoxLayout;
class QGridLayout;
class QStackedLayout;

class PlayerWidget;
class MenuBar;
class ListView;
class BannerWidget;
class SettingsWidget;


class SymphonyWindow : public QWidget
{
	Q_OBJECT
private:
	PlayerWidget* wid_player = nullptr;
	MenuBar* bar_menu = nullptr;
	ListView* wid_list = nullptr;
	ListView* wid_list2 = nullptr;
	BannerWidget* wid_ban = nullptr;
	SettingsWidget* wid_sets = nullptr;
	QWidget* wid = nullptr;
	QPushButton* pb_ = nullptr;
	QPushButton* pb_2 = nullptr;
	QPushButton* pb_3 = nullptr;
	QPushButton* pb_4 = nullptr;
	QPushButton* pb_4b = nullptr;
	QPushButton* pb_4c = nullptr;
	QPushButton* pb_6 = nullptr;
	QPushButton* pb_7 = nullptr;
	QPushButton* pb_8 = nullptr;

	QBoxLayout* lay_main = nullptr;
	QBoxLayout* lay_player = nullptr;
	QBoxLayout* lay_menu = nullptr;
	QGridLayout* lay_data = nullptr;
	QStackedLayout* stk_main = nullptr;

public:
	SymphonyWindow(QWidget *parent = nullptr);
	~SymphonyWindow();

public slots:
	void updateLayout();
	void changePlayerPos();
	void changeMenuBarPos();
	void changeMenuBarMode();
	void changeMenuBarAlign();

	void changeListViewMode();
	void changeListViewGroupBy();
	void sortListView();

	void onSettings();

	void onChangeCategory( const Keys::Type& type );

protected slots:
	void closeEvent( QCloseEvent* event ) override;

};
#endif // SYMPHONYWINDOW_H
