#include "BannerWidget.h"


#include <QGridLayout>
#include <QIcon>
#include <QLabel>

#include "Models/Element.h"

#include <QDebug>

BannerWidget::BannerWidget( QWidget* parent )
	: QWidget(parent)
{
	lay_main = new QGridLayout;
	lb_cover = new QLabel;
		lb_cover->setFixedSize(80, 80);
	lb_cover->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	lay_main->addWidget(lb_cover, 0, 0, 2, 1 );
	lb_title = new QLabel;
	lb_title->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
	lay_main->addWidget( lb_title, 0, 1 );
	lb_subtitle = new QLabel;
	lb_subtitle->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
	lay_main->addWidget( lb_subtitle, 1, 1 );

	setLayout( lay_main );
}

void BannerWidget::setElement( Element* el )
{
	lb_cover->setPixmap(QIcon(":/icons/darkTheme/music.svg").pixmap(80,80));
	if ( el == nullptr ) return;
	lb_title->setText( el->title() );
	lb_subtitle->setText( el->subTitle() );
}
