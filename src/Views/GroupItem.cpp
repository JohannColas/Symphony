#include "GroupItem.h"

GroupItem::GroupItem(const QChar& c, QWidget* parent) : QPushButton(c,parent)
{
	setFlat(true);
}

GroupItem::GroupItem(QWidget *parent) : QPushButton(parent)
{
	setFlat(true);
}
