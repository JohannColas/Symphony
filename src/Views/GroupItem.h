#ifndef GROUPITEM_H
#define GROUPITEM_H

#include <QPushButton>

class GroupItem : public QPushButton
{
	Q_OBJECT
public:
	GroupItem(const QChar& c, QWidget *parent = nullptr);
	explicit GroupItem(QWidget *parent = nullptr);

signals:

};

#endif // GROUPITEM_H
