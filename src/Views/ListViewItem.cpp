#include "ListViewItem.h"

#include "Models/Element.h"

#include <QTimer>
#include <QtDebug>

ListViewItem::~ListViewItem()
{
	delete lay_main;
	delete lb_title;
	delete lb_subtitle;
	delete lb_image;
}

ListViewItem::ListViewItem(QWidget* parent) : QPushButton(parent)
{
	setCheckable(true);
	lay_main = new QGridLayout(this);
	lb_image = new QLabel(this);
	lb_image->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	lay_main->addWidget(lb_image,0,0,2,1);
	lb_title = new QLabel(this);
	lb_title->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
	lay_main->addWidget(lb_title,0,1/*, Qt::AlignLeft*/);
	lb_subtitle = new QLabel(this);
	lb_subtitle->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Maximum );
	lay_main->addWidget(lb_subtitle,1,1/*, Qt::AlignLeft*/);
	this->setLayout(lay_main);
}

ListViewItem::ListViewItem(Element* el, QWidget* parent) : QPushButton(parent)
{
	setCheckable(true);
	lay_main = new QGridLayout(this);
	lb_image = new QLabel(this);
	lay_main->addWidget(lb_image,0,0,2,1);
	lb_title = new QLabel(this);
	lay_main->addWidget(lb_title,0,1/*, Qt::AlignLeft*/);
	lb_subtitle = new QLabel(this);
	lay_main->addWidget(lb_subtitle,1,1/*, Qt::AlignLeft*/);
	this->setLayout(lay_main);
	if ( el != nullptr )
	{
		setElement(el);
	}
}

Element* ListViewItem::element()
{
	return _el;
}

void ListViewItem::setElement( Element* el )
{
	_el = el;
	disconnect( this, &ListViewItem::released,
			 this, &ListViewItem::onReleased );
	if ( _el == nullptr )
	{
		lb_image->clear();
		lb_title->clear();
		lb_subtitle->clear();
		return;
	}
	connect( this, &ListViewItem::released,
			 this, &ListViewItem::onReleased );
	QIcon image;
	switch ( _el->type() )
	{
		case Keys::Artist:
			image = QIcon(":/icons/darkTheme/user.svg");
		break;
		case Keys::Album:
			image = QIcon(":/icons/darkTheme/media-record.svg");
		break;
		case Keys::Track:
			image = QIcon(":/icons/darkTheme/music.svg");
		break;
		case Keys::Genre:
			image = QIcon(":/icons/darkTheme/bookmark.svg");
		break;
		case Keys::Library:
			image = QIcon(":/icons/darkTheme/folder.svg");
		break;
		case Keys::Playlist:
			image = QIcon(":/icons/darkTheme/list.svg");
		break;
		case Keys::Radio:
			image = QIcon(":/icons/darkTheme/pulse.svg");
		break;
		case Keys::Podcast:
			image = QIcon(":/icons/darkTheme/web.svg");
		break;
		default:
		break;
	}
	lb_image->setPixmap(image.pixmap(50,50));
	it_title = 0;
	onEnterTimer();
//	lb_title->setText(_el->title());
//	lb_subtitle->setText(_el->subTitle());
}

void ListViewItem::setViewMode(const Keys::ListViewMode& key)
{

	lay_main->removeWidget(lb_image);
	lay_main->removeWidget(lb_title);
	lay_main->removeWidget(lb_subtitle);
	switch (key)
	{
		case Keys::NormalList:
			lay_main->addWidget(lb_image,0,0,2,1);
			lay_main->addWidget(lb_title,0,1/*, Qt::AlignLeft*/);
			lay_main->addWidget(lb_subtitle,1,1/*, Qt::AlignLeft*/);
			lb_subtitle->show();
			lb_title->setAlignment( Qt::AlignLeft );
		break;
		case Keys::DetailedList:
			lay_main->addWidget(lb_image,0,0,2,1);
			lay_main->addWidget(lb_title,0,1/*, Qt::AlignLeft*/);
			lay_main->addWidget(lb_subtitle,1,1/*, Qt::AlignLeft*/);
			lb_subtitle->show();
			lb_title->setAlignment( Qt::AlignLeft );
		break;
		case Keys::NormalTree:
			lay_main->addWidget(lb_image,0,0,2,1);
			lay_main->addWidget(lb_title,0,1/*, Qt::AlignHCenter*/);
			lay_main->addWidget(lb_subtitle,1,1/*, Qt::AlignHCenter*/);
			lb_subtitle->show();
			lb_title->setAlignment( Qt::AlignLeft );
		break;
		case Keys::DetailedTree:
			lay_main->addWidget(lb_image,0,0,2,1);
			lay_main->addWidget(lb_title,0,1/*, Qt::AlignHCenter*/);
			lay_main->addWidget(lb_subtitle,1,1/*, Qt::AlignHCenter*/);
			lb_subtitle->show();
			lb_title->setAlignment( Qt::AlignLeft );
		break;
		case Keys::Icon:
			lay_main->addWidget(lb_image,0,0, Qt::AlignHCenter/*, Qt::AlignHCenter*/);
			lay_main->addWidget(lb_title,1,0/*, Qt::AlignHCenter*/);
			lb_subtitle->hide();
			lb_title->setAlignment( Qt::AlignHCenter );
		break;
		case Keys::Panel:
			lay_main->addWidget(lb_image,0,0, Qt::AlignHCenter/*, Qt::AlignHCenter*/);
			lay_main->addWidget(lb_title,1,0/*, Qt::AlignHCenter*/);
			lb_subtitle->hide();
			lb_title->setAlignment( Qt::AlignHCenter );
		break;
	}
	it_title = 0;
	onEnterTimer();
}

void ListViewItem::updateLayout()
{
}

void ListViewItem::onReleased()
{
	if ( this->isChecked() )
		emit addElement(_el);
	else
		emit removeElement(_el);
}

void ListViewItem::resizeEvent( QResizeEvent* event )
{
	QPushButton::resizeEvent( event );
	it_title = 0;
	onEnterTimer();
}

void ListViewItem::enterEvent( QEvent* event )
{
	it_title = 0;
	if ( _activeTimer )
	{
		t = new QTimer(this);
		connect( t, &QTimer::timeout,
				 this, &ListViewItem::onEnterTimer );
		t->start(350);
	}
	QPushButton::enterEvent(event);
}

void ListViewItem::leaveEvent( QEvent* event )
{
	if ( t != nullptr )
	{
		t->stop();
		delete t, t = nullptr;
	}
	it_title = 0;
	onEnterTimer();
	QPushButton::leaveEvent(event);
}

void ListViewItem::onEnterTimer()
{
	if ( _el == nullptr ) return;
	QFontMetrics titleMetrics = lb_title->fontMetrics();
	QFontMetrics subtitleMetrics = lb_subtitle->fontMetrics();
	QString title = _el->title();
	if ( lb_title->contentsRect().width() < titleMetrics.horizontalAdvance(title) )
	{
		if ( it_title + 1 > title.size() )
			it_title = 0;
		title = title.remove(0, it_title);
		lb_title->setText( titleMetrics.elidedText( title, Qt::ElideRight, lb_title->contentsRect().width() ) );
		++it_title;
		_activeTimer = true;
	}
	else
		lb_title->setText( title );
	QString subtitle = _el->subTitle();
	if ( lb_subtitle->contentsRect().width() < subtitleMetrics.horizontalAdvance(subtitle) )
	{
		if ( it_subtitle + 1 > subtitle.size() )
			it_subtitle = 0;
		subtitle = subtitle.remove(0, it_subtitle);
		lb_subtitle->setText( subtitleMetrics.elidedText( subtitle, Qt::ElideRight, lb_subtitle->contentsRect().width() ) );
		++it_title;
		_activeTimer = true;
	}
	else
		lb_subtitle->setText( subtitle );
}

