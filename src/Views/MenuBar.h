#ifndef MENUBAR_H
#define MENUBAR_H

#include <QFrame>
#include <QList>
class QBoxLayout;
#include <QPushButton>
#include <QAction>

#include "Commons/App.h"

class MenuButton : public QPushButton
{
	Q_OBJECT
	Keys::Type _type = Keys::None;
public:
	MenuButton(QWidget* parent = nullptr) : QPushButton(parent)
	{
		connect( this, &MenuButton::released,
				 this, &MenuButton::onReleased );
	}
	MenuButton(const QString& text, const QIcon& icon, const Keys::Type& type = Keys::None, QWidget* parent = nullptr) : QPushButton(icon,text,parent)
	{
		_type = type;
		if ( _type != Keys::None )
			connect( this, &MenuButton::released,
					 this, &MenuButton::onReleased );
	}
	int type()
	{
		return _type;
	}
public slots:
	void onReleased()
	{
		emit activated(_type);
	}
signals:
	void activated(const Keys::Type&);
};

class MenuAction : public QAction
{
	Q_OBJECT
	Keys::Type _type = Keys::None;
public:
	MenuAction(QObject* obj = nullptr) : QAction(obj)
	{
		connect( this, &MenuAction::triggered,
				 this, &MenuAction::onTriggered );
	}
	MenuAction(const QString& text, const QIcon& icon, const Keys::Type& type = Keys::None, QObject* obj = nullptr) : QAction(icon,text,obj)
	{
		_type = type;
		if ( _type != Keys::None )
			connect( this, &MenuAction::triggered,
					 this, &MenuAction::onTriggered );
	}
	int type()
	{
		return _type;
	}
public slots:
	void onTriggered()
	{
		emit activated(_type);
	}
signals:
	void activated(const Keys::Type&);
};

class MenuBar : public QFrame
{
	Q_OBJECT
public:
	enum Mode {Bar=0, Button=1};
	enum Position {Left=0, Top=1, Right=2, Bottom=3};
//	enum Action {Queue,Artists,Albums,Tracks,Genres,Playlists,Libraries,Radios,Podcasts};

private:
	MenuBar::Position _pos =  MenuBar::Left;
	MenuBar::Mode _mode = MenuBar::Bar;
	Qt::Alignment _align = Qt::AlignTop;
	QBoxLayout* lay_main = nullptr;
	QList<MenuButton*> _buttons;
	QMenu* _menu = nullptr;
	QList<MenuAction*> _actions;
	Keys::Type _currentType = Keys::Queue;

public:
	MenuBar(QWidget *parent = nullptr);
	~MenuBar();

	MenuBar::Position pos();
	void setPos(const MenuBar::Position& pos);
	Qt::Alignment align();
	void setAlign(const Qt::Alignment& align);
	MenuBar::Mode mode();
	void setMode(const MenuBar::Mode& mode);

	void addButton( const QString& text, const QIcon& icon, const Keys::Type& type);
	void addAction( const QString& text, const QIcon& icon, const Keys::Type& type);

	void updateLayout();

private slots:
	void onCategory(const Keys::Type& type);

signals:
	void changeCategory( const Keys::Type& type );
};

#endif // MENUBAR_H
