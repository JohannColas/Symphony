#ifndef PLAYERBUTTON_H
#define PLAYERBUTTON_H

#include <QPushButton>

class PlayerButton : public QPushButton
{
public:
	explicit PlayerButton(QWidget *parent = nullptr);
	explicit PlayerButton(const QString& text, QWidget *parent = nullptr);
};

#endif // PLAYERBUTTON_H
