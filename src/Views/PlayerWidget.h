#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H

#include <QWidget>

#include "Commons/Keys.h"

class PlayerTrackInfoWidget;
class PlayerButton;
class SubPlayerButton;
class QBoxLayout;

class PlayerWidget : public QWidget
{
	Q_OBJECT
public:
	enum Position {Top=0, Left=1, Bottom=2, Right=3};
private:
	PlayerWidget::Position _pos = PlayerWidget::Top;

	PlayerButton* pb_backward = nullptr;
	PlayerButton* pb_playpause = nullptr;
	PlayerButton* pb_stop = nullptr;
	PlayerButton* pb_forward = nullptr;
	SubPlayerButton* pb_repeat = nullptr;
	SubPlayerButton* pb_shuffle = nullptr;
	SubPlayerButton* pb_equalizer = nullptr;
	SubPlayerButton* pb_volume = nullptr;

	PlayerTrackInfoWidget* wid_info = nullptr;

	QBoxLayout* lay_main = nullptr;
	QBoxLayout* lay_maincontrols = nullptr;
	QBoxLayout* lay_subcontrols = nullptr;

public:
	virtual ~PlayerWidget();
	explicit PlayerWidget(QWidget *parent = nullptr);

	PlayerWidget::Position pos()
	{
		return _pos;
	}
	void setPos(const PlayerWidget::Position& pos)
	{
		_pos = pos;
		updateLayout();
	}
	void makeConnections();

private slots:
	void updateLayout();
//	void playpause();
//	void onStop();

	void onPlayerStateChanged( const Keys::PlayerState& state );
	void onPlayerRepeatChanged( const Keys::Repeat& repeat );
	void onPlayerShuffleChanged( const Keys::Shuffle& shuffle );

signals:
//	void play();
//	void pause();
//	void stop();
//	void forward();
//	void backward();
//	void repeat();
//	void shuffle();

};

#endif // PLAYERWIDGET_H
