#ifndef SECTIONWIDGET_H
#define SECTIONWIDGET_H

#include <QWidget>
class QVBoxLayout;
class QPushButton;

class SectionWidget : public QWidget
{
	Q_OBJECT
	QVBoxLayout* layout;
	QList<QPushButton*> _sections;
public:
	~SectionWidget();
	explicit SectionWidget(QWidget *parent = nullptr);

	void addSection(const QChar& s);

	void updateSections();
signals:

};

#endif // SECTIONWIDGET_H
