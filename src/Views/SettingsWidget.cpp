#include "SettingsWidget.h"

#include <QHBoxLayout>
#include <QListWidget>
#include <QStackedWidget>
#include <QListWidgetItem>


SettingsWidget::~SettingsWidget()
{
	delete wid_sections;
}

SettingsWidget::SettingsWidget( QWidget* parent )
	: QWidget(parent)
{
	lay_main = new QHBoxLayout;
	this->setLayout( lay_main );

	wid_sections = new QListWidget(this);
	lay_main->addWidget( wid_sections );

	wid_settings = new QStackedWidget(this);
	lay_main->addWidget( wid_settings );

	QListWidget* ld = new QListWidget;
	QWidget* wid = new QWidget;

	addSections("FirstSection", ld);
	addSections("Section 2", wid);

	connect( wid_sections, &QListWidget::currentRowChanged,
			 this, &SettingsWidget::onSectionChanged );
//	connect( ld, &QListWidget::currentRowChanged,
//			 wid_settings, &QStackedWidget::setCurrentIndex );
}

int SettingsWidget::addSections( const QString& name, QWidget* widget )
{
	if ( widget == nullptr ) return -1;

	wid_sections->addItem( name );
	wid_settings->addWidget( widget );
	return wid_sections->count()-1;
}

void SettingsWidget::onSectionChanged( int index )
{
	if ( index >= 0 && index < wid_settings->count() )
		wid_settings->setCurrentIndex( index );
}

