#include "SymphonyWindow.h"
#include "Commons/App.h"

#include <QApplication>

int main(int argc, char *argv[])
{
	App::init();
	QApplication a(argc, argv);
	SymphonyWindow w;
	w.show();
	return a.exec();
}
